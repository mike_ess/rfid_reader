
// tinymce.init({
// 	mode : "exact",
//     selector: '#tinymce'
// });


function initMCEexact(e){
	tinyMCE.init({
	  mode : "exact",
	  elements : e,
	  height: 400,
	  plugins: [
		"image imagetools",
		" lists link media noneditable preview",
		" table template textcolor visualblocks wordcount"
	  ],
	});
  }
  initMCEexact("tinymce");
  initMCEexact("tinymce1");
  initMCEexact("tinymce2");
  initMCEexact("tinymce3");
  initMCEexact("tinymce4");
  initMCEexact("tinymce5");
  initMCEexact("tinymce6");
  initMCEexact("tinymce7");
  initMCEexact("tinymce8");
  initMCEexact("tinymce9");
  initMCEexact("tinymce10");
  initMCEexact("tinymce11");
  initMCEexact("tinymce12");
  initMCEexact("tinymce13");
  initMCEexact("tinymce14");
  initMCEexact("tinymce15");
  initMCEexact("tinymce16");


	//background movement
	// $('body').mousemove( function(e){
	// 	var moveX = (e.pageX * -1 / 15);
	// 	var moveY = (e.pageY * -1 / 15);
	
		// $('#overlay-bg').css("background-position", moveX + 'px' + moveY + 'px' + '!important');
	// });




/** Start Ajax URL call */
 
/* $(function() {

		if(Modernizr.history){

		var newHash      = "",
			$mainContent = $("#main-content"),
			$pageWrap    = $(".wrapper"),
			baseHeight   = 0,
			$el;
			
		$pageWrap.height($pageWrap.height());
		baseHeight = $pageWrap.height() - $mainContent.height();
		
		$(".nav").delegate("a.smooth", "click", function() {
			_link = $(this).attr("href");
			history.pushState(null, null, _link);
			loadContent(_link);
			return false;
		});

		function loadContent(href){
			$mainContent
					.find("#guts")
					.fadeOut(200, function() {
						$mainContent.hide().load(href + " #guts", function() {
							$mainContent.fadeIn(200, function() {
								$pageWrap.animate({
									height: baseHeight + $mainContent.height() + "px"
								});
							});
							$("nav a").removeClass("current");
							console.log(href);
							$("nav a[href$="+href+"]").addClass("current");
						});
					});
		}
		
		$(window).bind('popstate', function(){
		_link = location.pathname.replace(/^.*[\\\/]/, '');  
		loadContent(_link);
		});

	}  

		
	});
*/
/** End Ajax URL call */


/** Initialize FOundation */
$(document).foundation()


$(document).ready( function () {
	
	/**
	 * HOMEPAGE ANIMATIONS
	 */

	 //ripple effect
	// 	try {
	// 		$('body').ripples({
	// 			resolution: 512,
	// 			dropRadius: 20,  
	// 			perturbance: 0.04,
	// 		});
	// 		$('.wrapper').ripples({
	// 			resolution: 128,
	// 			dropRadius: 10,  
	// 			perturbance: 0.1,
	// 			interactive: false
	// 		});
	// 	}
	// 	catch (e) {
	// 		$('.error').show().text(e);
	// 	}

	// 	// Automatic drops
	// setInterval(function() {
	// 	var $el = $('body');
	// 	var x = Math.random() * $el.outerWidth();
	// 	var y = Math.random() * $el.outerHeight();
	// 	var dropRadius = 20;
	// 	var strength = 0.1 + Math.random() * 0.1;

	// 	$el.ripples('drop', x, y, dropRadius, strength);
	// }, 500);


	
	

		//quotes slider
		$('.lw-quote-slider').slick({
			dots: false,
			infinite: true,
			speed: 500,
			fade: true,
			cssEase: 'linear',
			// autoplay: true,
			// autoplaySpeed: 10000,
			nextArrow: '<div class="arrow next"></div>',
  			prevArrow: '<div class="arrow prev"></div>',
		});

		//preload images
		// var images = new Array()
		// 	function preload() {
		// 		for (i = 0; i < preload.arguments.length; i++) {
		// 			images[i] = new Image()
		// 			images[i].src = preload.arguments[i]
		// 		}
		// 	}
		// 	preload(
		// 		"http://localhost:8080/rfid_reader/uploads/bg0.jpg",
		// 		"http://localhost:8080/rfid_reader/uploads/bg1.jpg",
		// 		"http://localhost:8080/rfid_reader/uploads/bg2.jpg",
		// 		"http://localhost:8080/rfid_reader/uploads/bg3.jpg",
		// 		"http://localhost:8080/rfid_reader/uploads/bg4.jpg",
	
		// 	)


		//background change on slider change
		var filename = "uploads/big-one.jpg";
			// document.getElementById("overlay-bg").style.backgroundImage="url(" + filename + ")";
			

		$('.lw-quote-slider').on('afterChange', function(event, slick, currentSlide){
			
			console.log(currentSlide);

			
			var fname = "uploads/bg" + currentSlide + ".jpg";
			// document.getElementById("overlay-bg").style.backgroundImage="url(" + fname + ")";
				$('#overlay-bg').fadeOut(200);
				setTimeout( function(){
					$('#overlay-bg').css("backgroundImage", "url(' "+ fname +" ')").fadeIn(1000);

				}, 200);
			
		});


	
	
		/**
		 * DATATABLES
		 */

    //singleMemberList & resultList
    $('#singleMemberList').DataTable({});
    $('#bibNumberList').DataTable({});
    $('#mediaList').DataTable({});

    //change race event on select change - PARTICIPANT'S PAGE
    $('select[name="choose-race-event-select"]').on('change', function () {
        $('button.choose-race-event-button').removeAttr('disabled');
        return false;
    });

    $('button.choose-race-event-button').on('click', function(){

        var url = $('select[name="choose-race-event-select"]').val(); // get selected value
        
        if (url) { // require a URL
            window.location = url; // redirect
        }
        return false;
    });
    
    //toggle views in tables members
    var newHeight = $(window).height() - 340 + "px";
		var table = $('#memberList').DataTable( {
			"scrollY": newHeight,//"500px",
			//"paging": false
			"columnDefs": [
				{
					"targets": [ 0 ], //id
					"visible": false,
				},
				{
					"targets": [ 1 ], //roles
					"visible": false,
				},
				{
					"targets": [ 2 ], //last name
					"visible": true,
				},
				{
					"targets": [ 3 ], //first name
					"visible": true,
				},
				{
					"targets": [ 4 ], //middle name
					"visible": true,
				},
				{
					"targets": [ 5 ], //email
					"visible": true,
				},
				{
					"targets": [ 6 ], //phone
					"visible": true,
				},
				{
					"targets": [ 7 ], //address
					"visible": false,
				},
				{
					"targets": [ 8 ], //clubs/groups
					"visible": false,
				},
				{
					"targets": [ 9 ], // e. contact
					"visible": false,
				},
				{
					"targets": [ 10 ], // actions
					"visible": true,
				}
			]
			
        } );

     
    
        
    $('a.toggle-vis').on( 'click', function (e) {
        e.preventDefault();
 
        // Get the column API object
        var column = table.column( $(this).attr('data-column') );
 
        // Toggle the visibility
        column.visible( ! column.visible() );

        $(this).toggleClass("success alert");
    } );
});