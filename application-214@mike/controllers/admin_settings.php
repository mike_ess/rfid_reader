<?php
class Admin_settings extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->helper('url_helper');
        $this->load->library('form_validation');
        
        $this->load->model('admin_settings_model');
     }

   
  
    public function index()
    {
        if( isset($_SESSION['isAdmin']) ) {
            
            $data['title'] = 'Admin Settings';
            
            $data['sg'] = $this->admin_settings_model->select_specific('settings_general'); 
            $data['et'] = $this->admin_settings_model->select_specific('email_templates');  

            $data['banks'] = $this->admin_settings_model->select('bank_payment_option'); 
            $data['mtos'] = $this->admin_settings_model->select('money_transfer_option'); 
            $data['pls'] = $this->admin_settings_model->select('pickup_location'); 
            $data['fees'] = $this->admin_settings_model->select('fees'); 
            
            $data['media_items'] = $this->admin_settings_model->get_media();

            $this->load->view('templates/header', $data);
            $this->load->view('admin_settings/index', $data);
            $this->load->view('templates/footer');
            
        }else{
            redirect('');
        }
    }
    
    
   

}