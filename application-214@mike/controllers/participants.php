<?php
class Participants extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('participants_model');
        $this->load->helper('url_helper');
    }

    

    /**
     * View All Event List
     */
    public function index()
    {
        if( isset($_SESSION['isAdmin']) ) {
            $data['events'] = $this->participants_model->get_events(); //get model from results_model
            $data['title'] = 'Race Participants';

            $this->load->view('templates/header', $data);
            $this->load->view('participants/index', $data);
            //$this->load->view('templates/footer-content');
            $this->load->view('templates/footer');
        }
        else{
            redirect('');
        }
    }

    /**
     * View Single Race Results Page. Get all contents in the events table
     */
    public function view($re_slug = NULL, $rc_id = NULL)
    {
        if( isset($_SESSION['isAdmin']) ) {
            //get currently selected race event
            $data['current_events_item'] = $this->participants_model->get_events($re_slug); 
            $re_id = $data['current_events_item']['re_id'];
            
            //list all events in dropdown
            $data['events'] = $this->participants_model->get_events();
            
            if (empty($re_id))
            {
                show_404();
            }
            if (!is_numeric($rc_id) && !empty($rc_id))
            {
                show_404(); 
                
            }
        
            //display data by re_slug
            $data['re_slug'] = $data['current_events_item']['re_slug'];
            
            //display categories from get_race_categories function via re_id
            $data['categories'] = $this->participants_model->get_race_categories($re_id);
            
            //get race cat name via specific rc_id
            $data['rc_id'] = $rc_id;
            
            //display race participants from get_race_participants function via re_slug
            $data['participants_only'] = $this->participants_model->get_race_participants_only($re_id, $rc_id);
        
            $this->load->view('templates/header', $data);
            $this->load->view('participants/view', $data);
            $this->load->view('templates/footer');
        }
        else{
            redirect('');
        }
    }

    //ajax  
    //get payment status
    function get_payment_status($a){
        if($a == 'paid'){
        return '1';
        }
        else if($a == 'refunded'){
        return '2';
        }	
        
        else {
        return '0';
        }
    }
    
    //--changeParticipantStatus
    public function changePaymentStatus($p_id, $action) { 
        
        //get the m_id based on p_id first
        $query = $this->db->get_where('participants', array('p_id' => $p_id));
        $row = $query->row_array();
        $showUserID = $row['m_id'];

        //secure the connection. Check if admin is doing it
        if( isset($_SESSION['isAdmin']) ) {

            $updatePaymentStat = array(
                'p_payment_status' => $this->get_payment_status($action),
                
            );
            $this->db->where('m_id',  $showUserID );
            $this->db->update('participants', $updatePaymentStat);

            $arr = array(
                'status' => 'Success!',
                'msg' => 'User ID: '. $showUserID .' changed to: '. $action
            );
        
        
        $result = $arr;
        echo json_encode($result);
        }
    }

    //--deleteParticipant
    public function deleteParticipant($p_id) { 
        

        //secure the connection. Check if admin is doing it
        if( isset($_SESSION['isAdmin']) ) {

            //validate data
            if (is_numeric($p_id)){

                //get the bib_id based on p_id first
                $query = $this->db->get_where('participants', array('p_id' => $p_id));
                    $row = $query->row_array();
                    $showBib = $row['bib_id'];

                //then, remove from bib_number table
                $this->db->delete('bib_number', array('bib_id' => $showBib) );
                
                //remove to family members
                $this->db->delete('family_friend_member', array('bib_id' => $showBib) );
                
                //then, remove from participants table
                $this->db->delete('participants', array('p_id' => $p_id) );
                


                $arr = array(
                    'status' => 'Success!',
                    'msg' => 'Participant removed from event! ID: '. $p_id
                );
                
            }else {

                $arr = array(
                    'status' => 'Error!',
                    'msg' => 'Invalid Participant ID!'
                );
            }
            
            
            $result = $arr;
            echo json_encode($result);

        }
        else{
            $arr = array(
                'status' => 'Error!',
                'msg' => 'You are not an admin!'
            );
            $result = $arr;
            echo json_encode($result);
        }
    }

   
}