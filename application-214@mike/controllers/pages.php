<?php
class Pages extends CI_Controller {

        public function __construct()
        {
                parent::__construct();
                 $this->load->model('pages_model');
                 $this->load->model('validate_model');
                $this->load->helper('url_helper');
                $this->load->helper('url');
		$this->load->helper('form');
        }
        
        /**
         * Controls all pages. Home is the default page.
         */
        public function view($page = 'home', $the_link = NULL)
        {
                if ( ! file_exists(APPPATH.'views/pages/'.$page.'.php'))
                {
                        // Whoops, we don't have a page for that!
                        show_404();
                }

                //get quotes
                $data['the_quotes'] = $this->pages_model->get_quotes();

                //-----for link validation
                if($page == 'validate'){
                        $data['the_link'] = $this->validate_model->get_validation_link($the_link);
                
                        // echo 'validation page';
                        $data['activate_user_status'] = $this->validate_model->activate_user_status($the_link);
                }

                //-----for the page titles
                $data['title'] = ucwords(str_replace('-',' ', $page)); // Capitalize the first letter

                $this->load->view('templates/header');
                $this->load->view('pages/'.$page, $data);
                $this->load->view('templates/footer-content');
                $this->load->view('templates/footer');
        }
        

        
        

}