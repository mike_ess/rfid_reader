<?php
class Results extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('results_model');
        $this->load->helper('url_helper');
    }
    /**
     * View All Event List
     */
    public function index()
    {
        $data['events'] = $this->results_model->get_events(); //get model from results_model
        $data['title'] = 'Race Results';

        $this->load->view('templates/header', $data);
        $this->load->view('results/index', $data);
        $this->load->view('templates/footer-content');
        $this->load->view('templates/footer');
    }

    
    /**
     * View Single Race Results Page. Get all contents in the events table via re_slug
     */
    public function view($re_slug = NULL, $rc_id = NULL)
    {
        //get currently selected race event
        $data['current_events_item'] = $this->results_model->get_events($re_slug); 
        $re_id = $data['current_events_item']['re_id'];
        
        //list all events in dropdown
        $data['events'] = $this->results_model->get_events();
        
        if (empty($re_id))
        {
            show_404();
        }
        if (!is_numeric($rc_id) && !empty($rc_id))
        {
            show_404();
            
        }
       
        //display data by re_slug
        $data['re_slug'] = $data['current_events_item']['re_slug'];
        
        //display categories from get_race_categories function via re_id
        $data['categories'] = $this->results_model->get_race_categories($re_id);

        //get race cat name via specific rc_id
        $data['rc_id'] = $rc_id;

        //display race participants from get_race_participants function via re_slug
        $data['participants_only'] = $this->results_model->get_race_participants_only($re_id, $rc_id);

        //display race participants with time results from get_race_participants function via re_slug
        $data['participants_with_time_records'] = $this->results_model->get_race_participants_with_time_records($re_id, $rc_id);

        $this->load->view('templates/header', $data);
        $this->load->view('results/view', $data);
        $this->load->view('templates/footer-content');
        $this->load->view('templates/footer');

    }
    
}