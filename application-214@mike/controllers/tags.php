<?php
class Tags extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('tags_model');
        $this->load->helper(array('form', 'url', 'url_helper'));
    }

    

    /**
     * Hide the index page and redirect to events page
     */
    public function index($tags = NULL)
    {
        if($tags == ''){
            redirect('events');
        }
    }
  
    /**
     * View All Event List based on tags
     */
    public function view($tags = NULL)
    {
         
        $data['tags'] = $tags;

        $data['events'] = $this->tags_model->get_events($tags);
        $data['lwc_events'] = $this->tags_model->get_lwc_events($tags);

        
        $tagClean = str_replace('_', ' ', $tags);
        $data['title'] = 'Events under "'.$tagClean.'" tag.';

        $this->load->view('templates/header', $data);
        $this->load->view('tags/index', $data);
        $this->load->view('templates/footer-content');
        $this->load->view('templates/footer');
    }

    
}