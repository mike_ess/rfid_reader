<?php
class Members extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('members_model');
        $this->load->model('events_model');
        $this->load->helper('url_helper');
        $this->load->library('form_validation');
        $this->load->helper('inflector');
        $this->load->helper('string');
    }
 

    //** generate random string for validation **//
    public function randomString() {
        $alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
        $theLinkCode = array(); //remember to declare $theLinkCode as an array
        $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
        for ($i = 0; $i < 20; $i++) {
            $n = rand(0, $alphaLength);
            $theLinkCode[] = $alphabet[$n];
        }
        return implode($theLinkCode); //turn the array into a string
    }
    //  $randomLinkCode = $this->md5(randomString());

    //** generate random password **//
    public function randomPassword() {
        $alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890@#&*_+=%';
        $pass = array(); //remember to declare $pass as an array
        $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
        for ($i = 0; $i < 8; $i++) {
            $n = rand(0, $alphaLength);
            $pass[] = $alphabet[$n];
        }
        return implode($pass); //turn the array into a string
    }
    // $randomPassword = $this->randomPassword();

    //** generate aditional random suffix for username **//
    public function randomUsernmaneSuffix() {
       $alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
       $pass = array(); //remember to declare $pass as an array
       $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
       for ($i = 0; $i < 3; $i++) {
           $n = rand(0, $alphaLength);
           $pass[] = $alphabet[$n];
       }
       return implode($pass); //turn the array into a string
   }
//    $randomUsernmaneSuffix = $this->randomUsernmaneSuffix();
    
    public function insert_file($filename){
        $data = array(
            'mf_file_name'      => $filename,
            // 'mf_file_name_thumb'      => $filename_thumb,
            'mf_added_by'      => $_SESSION['sess_user_id'],
        );
        $this->db->insert('media_files', $data);
        return $this->db->insert_id();
    }

    public function get_file($data, $table, $column)
    {
        return $this->db->select()
                ->from($table)
                ->where($column, $data)
                ->get()
                ->row();
    }

    /**
     * View All Member List
     */
    
    
    public function index()
    {
        // if( isset($_SESSION['isAdmin']) ) {
        if( isset($_SESSION['isAdmin']) || isset($_SESSION['isAuthor'])   ) {
            $data['members'] = $this->members_model->get_members();
            $data['family_members'] = $this->members_model->get_family_members();
            $data['title'] = 'Member List';
            
            $this->load->view('templates/header', $data);
            $this->load->view('members/index', $data);
            $this->load->view('templates/footer');
            
        }else{
            redirect('');
        }
    }

    /**
     * View Single Member Page
     */
        
    public function view($m_id = NULL)
    {
        
        //check if the logged in user is the owner of the member page currently viewed
        if( isset($_SESSION['isUserLoggedIn']) &&  $m_id ==  $_SESSION['sess_user_id'] ) {
            $data['members_item'] = $this->members_model->get_members($m_id);
            
            if (empty($data['members_item']))
            {
                show_404();
            }
            
            // $data['m_id'] = $data['members_item']['m_id'];

            //display all emergency contacts in the table
            $data['ecd_item'] = $this->members_model->get_emergency_contact($m_id);

            //get participated event for Single Runner
            $data['participated_event_single'] = $this->members_model->get_participated_events_single($m_id);

            //get participated event with FFM runners
            $data['participated_event'] = $this->members_model->get_participated_events($m_id);
            $data['participated_event_ffm'] = $this->members_model->get_participated_events_ffm($m_id);
            
            $this->load->view('templates/header', $data);
            $this->load->view('members/view', $data);
            $this->load->view('templates/footer-content');
            $this->load->view('templates/footer');
        }else{
            if( isset($_SESSION['isUserLoggedIn']) ){
                redirect(base_url('members/').$_SESSION['sess_user_id']);
            }
            else{
                redirect('');
            }

        }

    }

    public function edit($m_id = NULL)
    {
        if( isset($_SESSION['isAdmin']) ) {
            //display all events in a dropdown
            $data['events'] = $this->members_model->get_events();

            //display fees get from events_model
            $data['web_fee'] = $this->events_model->get_fees('web_fee');
            $data['delivery_fee_fixed'] = $this->events_model->get_fees('delivery_fee_fixed');
            $data['delivery_fee_manila'] = $this->events_model->get_fees('delivery_fee_manila');
            $data['delivery_fee_outside_manila'] = $this->events_model->get_fees('delivery_fee_outside_manila');

            //get participated event for Single Runner
            $data['participated_event_single'] = $this->members_model->get_participated_events_single($m_id);

            //get participated event with FFM runners
            $data['participated_event'] = $this->members_model->get_participated_events($m_id);
            $data['participated_event_ffm'] = $this->members_model->get_participated_events_ffm($m_id);

            //display all members in the table
            $data['members_item'] = $this->members_model->get_members($m_id);
            
            //check if user has family members
            $data['count_ffm'] = $this->members_model->check_ffm($m_id);

            if (empty($data['members_item']))
            {
                    show_404();
            }
            // $data['m_id'] = $data['members_item']['m_id'];

            //display all emergency contacts in the table
            $data['ecd_item'] = $this->members_model->get_emergency_contact($m_id);

            //display all events in a dropdown
            $data['events'] = $this->members_model->get_events();

            //display all race categories in a dropdown
            //$data['categories'] = $this->members_model->get_race_categories();

            //include page templates
            $this->load->view('templates/header', $data);
            $this->load->view('members/edit', $data);
            $this->load->view('templates/footer');  
        }
        else{
            if( isset($_SESSION['isUserLoggedIn']) ){
                redirect(base_url('members/').$_SESSION['sess_user_id']);
            }
            else{
                redirect('');
            }
        }
    }

    /**
     * Add new Members Page
     */
     public function add(){
        // if( isset($_SESSION['isAdmin']) ) {
        if( isset($_SESSION['isAdmin']) || isset($_SESSION['isAuthor'])   ) {
            //display all events in a dropdown
            $data['events'] = $this->members_model->get_events();

            //display fees get from events_model
            $data['web_fee'] = $this->events_model->get_fees('web_fee');
            $data['delivery_fee_fixed'] = $this->events_model->get_fees('delivery_fee_fixed');
            $data['delivery_fee_manila'] = $this->events_model->get_fees('delivery_fee_manila');
            $data['delivery_fee_outside_manila'] = $this->events_model->get_fees('delivery_fee_outside_manila');

            //display pickup locations
            $data['pickup_location'] = $this->events_model->select('pickup_location'); 
            $data['bank_payment_option'] = $this->events_model->select('bank_payment_option'); 
            $data['money_transfer_option'] = $this->events_model->select('money_transfer_option'); 
            
            //include page templates
            // $data['title'] = 'Add New Member';
            $this->load->view('templates/header');
            $this->load->view('members/add', $data);
            $this->load->view('templates/footer');
        }else{
            redirect('');
        }
     }

    

    public function myformAjax($re_id, $do_action) { 

        //settings_email
        if($do_action == 'settings_email'){
            if( isset($_SESSION['isAdmin'])  ) {

                //validate the form
                $this->form_validation->set_rules('admin_email', 'Admin Email', 'required|valid_email');
                
                // $this->form_validation->set_rules('admin_pass_reset', 'Admin Password Reset Email', 'required');
                // $this->form_validation->set_rules('admin_reg_rec', 'Admin Reg. Received Email', 'required');
                // $this->form_validation->set_rules('admin_payment_pending', 'Admin Payment Pending', 'required');
                // $this->form_validation->set_rules('admin_payment_completed', 'Admin Payment Completed', 'required');
                // $this->form_validation->set_rules('admin_payment_refunded', 'Admin Payment Refunded', 'required');
                // $this->form_validation->set_rules('admin_payment_cancelled', 'Admin Payment Cancelled', 'required');
                // $this->form_validation->set_rules('admin_del_completed', 'Admin Devliery Completed', 'required');
                
                // $this->form_validation->set_rules('cust_pass_reset', 'Cust. Password Reset Email', 'required');
                // $this->form_validation->set_rules('cust_reg_rec', 'Cust. Reg. Received Email', 'required');
                // $this->form_validation->set_rules('cust_payment_pending', 'Cust. Payment Pending', 'required');
                // $this->form_validation->set_rules('cust_payment_completed', 'Cust. Payment Completed', 'required');
                // $this->form_validation->set_rules('cust_payment_refunded', 'Cust. Payment Refunded', 'required');
                // $this->form_validation->set_rules('cust_payment_cancelled', 'Cust. Payment Cancelled', 'required');
                // $this->form_validation->set_rules('cust_del_completed', 'Cust. Devliery Completed', 'required');

                //if there's an error
                if ($this->form_validation->run() == FALSE){

                    $arr = array(
                        'status' => 'Invalid!',
                        'msg' => 'You have invalid details.',
                        'admin_email' => form_error('admin_email'),
                        // 'admin_pass_reset' => form_error('admin_pass_reset'),
                        // 'admin_reg_rec' => form_error('admin_reg_rec'),
                        // 'admin_payment_pending' => form_error('admin_payment_pending'),
                        // 'admin_payment_completed' => form_error('admin_payment_completed'),
                        // 'admin_payment_refunded' => form_error('admin_payment_refunded'),
                        // 'admin_payment_cancelled' => form_error('admin_payment_cancelled'),
                        // 'admin_del_completed' => form_error('admin_del_completed'),
                        
                        // 'cust_pass_reset' => form_error('cust_pass_reset'),
                        // 'cust_reg_rec' => form_error('cust_reg_rec'),
                        // 'cust_payment_pending' => form_error('cust_payment_pending'),
                        // 'cust_payment_completed' => form_error('cust_payment_completed'),
                        // 'cust_payment_refunded' => form_error('cust_payment_refunded'),
                        // 'cust_payment_cancelled' => form_error('cust_payment_cancelled'),
                        // 'cust_del_completed' => form_error('cust_del_completed'),
                        
                    );
                }
                else{

                    $admin_email = $this->input->post('admin_email');
                   
                    $val_link = $this->input->post('val_link');
                    $admin_pass_reset = $this->input->post('admin_pass_reset');
                    $admin_reg_rec = $this->input->post('admin_reg_rec');
                    $admin_payment_pending = $this->input->post('admin_payment_pending');
                    $admin_payment_completed = $this->input->post('admin_payment_completed');
                    $admin_payment_refunded = $this->input->post('admin_payment_refunded');
                    $admin_payment_cancelled = $this->input->post('admin_payment_cancelled');
                    $admin_del_completed = $this->input->post('admin_del_completed');

                    $cust_pass_reset = $this->input->post('cust_pass_reset');
                    $cust_reg_rec = $this->input->post('cust_reg_rec');
                    $cust_payment_pending = $this->input->post('cust_payment_pending');
                    $cust_payment_completed = $this->input->post('cust_payment_completed');
                    $cust_payment_refunded = $this->input->post('cust_payment_refunded');
                    $cust_payment_cancelled = $this->input->post('cust_payment_cancelled');
                    $cust_del_completed = $this->input->post('cust_del_completed');

                    //update email templates
                    $dataEmailTemplates = array(
                        'ges_val_link' => $val_link,  
                        'ae_pass_reset' => $admin_pass_reset,  
                        'ae_reg_received' => $admin_reg_rec,  
                        'ae_payment_pending' => $admin_payment_pending,  
                        'ae_payment_complete' => $admin_payment_completed,  
                        'ae_payment_refunded' => $admin_payment_refunded,  
                        'ae_payment_cancelled' => $admin_payment_cancelled,  
                        'ae_delivery_completed' => $admin_del_completed,  
                        
                        'cust_pass_reset' => $cust_pass_reset,  
                        'cust_reg_received' => $cust_reg_rec,  
                        'cust_payment_pending' => $cust_payment_pending,  
                        'cust_payment_complete' => $cust_payment_completed,  
                        'cust_payment_refunded' => $cust_payment_refunded,  
                        'cust_payment_cancelled' => $cust_payment_cancelled,  
                        'cust_delivery_completed' => $cust_del_completed,  
                    );
                    $this->db->update('email_templates', $dataEmailTemplates);   

                
                    //update settings general
                    $dataSettings = array(
                        'sg_admin_email' => $admin_email,  
                    );
                    $this->db->update('settings_general', $dataSettings);   


                    $arr = array(
                        'status' => 'Success!',
                        'msg' => 'Email Templates Updated!' 
                    );
                }

            

            }else{
                $arr = array(
                    'status' => 'Error!',
                    'msg' => 'You don\'t have the power to do this! ' 
                );
            }
            echo json_encode($arr);
        }

        //settings_images
        if($do_action == 'settings_images'){
            if( isset($_SESSION['isAdmin'])  ) {

                //validate the form
                $this->form_validation->set_rules('upload_folder', 'Upload Folder', 'required');
                $this->form_validation->set_rules('ti_height', 'Thumb Image Max. Height', 'required');
                $this->form_validation->set_rules('ti_width', 'Thumb Image Max. Width', 'required');
                $this->form_validation->set_rules('ar_max_height', 'Aspect Ratio Max. Height', 'required');
                $this->form_validation->set_rules('ar_max_width', 'Aspect Ratio Max. Width', 'required');
                $this->form_validation->set_rules('ar_prefix', 'Aspect Ratio Prefix', 'required');

                //if there's an error
                if ($this->form_validation->run() == FALSE){

                    $arr = array(
                        'status' => 'Invalid!',
                        'msg' => 'You have invalid details.',
                        'ti_height' => form_error('ti_height'),
                        'ti_width' => form_error('ti_width'),
                        'ar_max_height' => form_error('ar_max_height'),
                        'ar_max_width' => form_error('ar_max_width'),
                        'ar_prefix' => form_error('ar_prefix'),
                        
                    );
                }
                else{

                    $upload_folder = url_title($this->input->post('upload_folder'), 'underscore', TRUE);
                    //trim_slashes(underscore($this->input->post('upload_folder')));
                    
                    $ti_height = $this->input->post('ti_height');
                    $ti_width = $this->input->post('ti_width');
                    $ar_max_height = $this->input->post('ar_max_height');
                    $ar_max_width = $this->input->post('ar_max_width');
                    $ar_prefix = $this->input->post('ar_prefix');

                    //update settings
                    $dataSettings = array(
                        'sg_upload_path' => $upload_folder,  
                        'sg_thumb_max_height' => $ti_height,  
                        'sg_thumb_max_width' => $ti_width,  
                        'sg_ari_max_height' => $ar_max_height,  
                        'sg_ari_max_width' => $ar_max_width,  
                        'sg_ari_prefix' => $ar_prefix,  
                    );
                    $this->db->update('settings_general', $dataSettings);   


                    $arr = array(
                        'status' => 'Success!',
                        'msg' => 'Image Settings Updated!' 
                    );
                }

            

            }else{
                $arr = array(
                    'status' => 'Error!',
                    'msg' => 'You don\'t have the power to do this! ' 
                );
            }
            echo json_encode($arr);
        }

        //settings_delivery
        if($do_action == 'settings_delivery'){
            if( isset($_SESSION['isAdmin'])  ) {

                $f_name = $this->input->post('f_name');
                // $f_id = $this->input->post('f_id');
                // $f_slug = $this->input->post('f_slug');
                // $f_cost = $this->input->post('f_cost');

                $Pl_est_Name = $this->input->post('pl_est_name');
                // $Pl_Id = $this->input->post('pl_id');
                // $Pl_est_address = $this->input->post('pl_est_address');
                // $Pl_phone = $this->input->post('pl_phone');
                

                // save here
                if(!empty($f_name ) ){

                    //save fees
                    foreach ($f_name as $key => $the_fees) {
                        
                        $the_fees_id = $this->input->post('f_id')[$key];
                        $the_fees_name = $this->input->post('f_name')[$key];
                        $the_fees_slug = underscore($the_fees_name);//$this->input->post('f_slug')[$key];
                        $the_fees_cost = $this->input->post('f_cost')[$key];
                    
                        //check if fee ID already exist.
                        $query = $this->db->get_where('fees', array(
                            'f_id' => $the_fees_id,
                        ));
                        $check_num_rows = $query->num_rows();

                        //save to fees table
                        $dataFees = array(
                            'f_name' => $the_fees_name,
                            'f_slug' => $the_fees_slug,
                            'f_cost' => $the_fees_cost,
                        );
                        if($check_num_rows >= 1){
                            $this->db->where('f_id', $the_fees_id);
                            $this->db->update('fees', $dataFees);
                        
                        }else{
                            $this->db->insert('fees', $dataFees);   
                        }                         
                    }
                    
                    $arr = array(
                        'status' => 'Success!',
                        'msg' => 'Fees and Pickup Loc. Settings Updated!' 
                    );
                }
                if(!empty($Pl_est_Name) ){

                    //save pickup location
                    foreach ($Pl_est_Name as $key => $the_pls) {

                        $the_pls_Id = $this->input->post('pl_id')[$key];
                        $the_pls_est_name = $this->input->post('pl_est_name')[$key];
                        $the_pls_est_address = $this->input->post('pl_est_address')[$key];
                        $the_pls_phone = $this->input->post('pl_phone')[$key];
                        $the_pls_est_photo = $this->input->post('pl_est_photo')[$key];
                        
                    
                        //check if the_pls_Id already exist.
                        $query = $this->db->get_where('pickup_location', array(
                            'pl_id' => $the_pls_Id,
                        ));
                        $check_num_rows = $query->num_rows();

                        //save to pickup locaton table
                        $dataPls = array(
                            'pl_est_name' => $the_pls_est_name,
                            'pl_est_address' => $the_pls_est_address,
                            'pl_phone' => $the_pls_phone,
                            'mf_id' => $the_pls_est_photo
                        );
                        if($check_num_rows >= 1){
                            $this->db->where('pl_id', $the_pls_Id);
                            $this->db->update('pickup_location', $dataPls);
                        
                        }else{
                            $this->db->insert('pickup_location', $dataPls);   
                        }                         
                    }
                    
                    $arr = array(
                        'status' => 'Success!',
                        'msg' => 'Fees and Pickup Loc. Settings Updated!' 
                    );
                }
                else{
                    $arr = array(
                        'status' => 'Error!',
                        'msg' => 'Please add details for each sections.' 
                    );
                }

            }else{
                $arr = array(
                    'status' => 'Error!',
                    'msg' => 'You don\'t have the power to do this! ' 
                );
            }
            echo json_encode($arr);
        }

        //settings_payments
        if($do_action == 'settings_payments'){
            if( isset($_SESSION['isAdmin'])  ) {

                //validate the form
                $this->form_validation->set_rules('paypal_email', 'Paypal Email', 'required|valid_email');
               
                //if there's an error
                if ($this->form_validation->run() == FALSE){

                    $arr = array(
                        'status' => 'Invalid!',
                        'msg' => 'You have invalid details.',
                        'paypal_email' => form_error('paypal_email'),
                        
                    );
                }
                else{

                    $paypalEmail = $this->input->post('paypal_email');

                    $bankName = $this->input->post('bank_name');
                    // $bankId = $this->input->post('bank_id');
                    // $accName = $this->input->post('acc_name');
                    // $accNumber = $this->input->post('acc_number');
                    // $accType = $this->input->post('acc_type');
                    
                    $mtoId = $this->input->post('mto_id');
                    $mtoEstName = $this->input->post('mto_est_name');
                    $mtoRecName = $this->input->post('mto_rec_name');
                    $mtoMobileNum = $this->input->post('mto_mobile_number');
                    // $mtoAddress = $this->input->post('mto_add');

                    // save here banks and moneytransfers
                    if(!empty($bankName ) ){

                        //save banks
                        foreach ($bankName as $key => $the_bankNames) {
    
                            $the_bpo_id = $this->input->post('bank_id')[$key];
                            $the_acc_name = $this->input->post('acc_name')[$key];
                            $the_acc_number = $this->input->post('acc_number')[$key];
                            $the_acc_type = $this->input->post('acc_type')[$key];
                            $the_bpo_photo = $this->input->post('bpo_photo')[$key];
                        
                            //check if bank ID already exist.
                            $query = $this->db->get_where('bank_payment_option', array(
                                'bpo_id' => $the_bpo_id,
                            ));
                            $check_num_rows = $query->num_rows();

                            //save to bank_payment_option table
                            $dataBank = array(
                                'bpo_name' => $the_bankNames,
                                'bpo_acct_name' => $the_acc_name,
                                'bpo_acct_number' => $the_acc_number,
                                'bpo_type' => $the_acc_type,
                                'mf_id' => $the_bpo_photo,
                            );
                            if($check_num_rows >= 1){
                                $this->db->where('bpo_id', $the_bpo_id);
                                $this->db->update('bank_payment_option', $dataBank);
                            
                            }else{
                                $this->db->insert('bank_payment_option', $dataBank);   
                            }                         
                        }
                        //update paypal email
                        $dataPaypal = array(
                            'sg_paypal_email' => $paypalEmail,  
                        );
                        $this->db->update('settings_general', $dataPaypal);   


                        $arr = array(
                            'status' => 'Success!',
                            'msg' => 'Payment Settings Updated!' 
                        );
                    }
                    if(!empty($mtoEstName) ){
                        //save money transfers
                        foreach ($mtoEstName as $key => $the_mtoEstNames) {
    
                            $the_mtoId = $this->input->post('mto_id')[$key];
                            $the_mtoEstNames = $this->input->post('mto_est_name')[$key];
                            $the_mtoRecName = $this->input->post('mto_rec_name')[$key];
                            $the_mtoMobileNum = $this->input->post('mto_mobile_number')[$key];
                            $the_mtoAdd = $this->input->post('mto_address')[$key];
                            $the_mto_photo = $this->input->post('mto_photo')[$key];
                        
                            //check if mtoId already exist.
                            $query = $this->db->get_where('money_transfer_option', array(
                                'mto_id' => $the_mtoId,
                            ));
                            $check_num_rows = $query->num_rows();

                            //save to money_transfer_option table
                            $dataMto = array(
                                'mto_est_name' => $the_mtoEstNames,
                                'mto_receivers_name' => $the_mtoRecName,
                                'mto_mobile' => $the_mtoMobileNum,
                                'mto_address' => $the_mtoAdd,
                                'mf_id' => $the_mto_photo,
                            );
                            if($check_num_rows >= 1){
                                $this->db->where('mto_id', $the_mtoId);
                                $this->db->update('money_transfer_option', $dataMto);
                            
                            }else{
                                $this->db->insert('money_transfer_option', $dataMto);   
                            }                         
                        }
                        //update paypal email
                        $dataPaypal = array(
                            'sg_paypal_email' => $paypalEmail,  
                        );
                        $this->db->update('settings_general', $dataPaypal);   


                        $arr = array(
                            'status' => 'Success!',
                            'msg' => 'Payment Settings Updated!' 
                        );
                    }
                    else{
                        $arr = array(
                            'status' => 'Error!',
                            'msg' => 'Please add details under Bank and Money Transfers.' 
                        );
                    }
                        
                    
                }

            }else{
                $arr = array(
                    'status' => 'Error!',
                    'msg' => 'You don\'t have the power to do this! ' 
                );
            }
            echo json_encode($arr);
        }

        //upload csv Time Records
        if($do_action == 'upload_csv'){
            if( isset($_SESSION['isAdmin']) || isset($_SESSION['isAuthor'])   ) 
            {
                
                set_time_limit(0); //remove the max_execution_time default is 30sec.

                $filename = explode(".", $_FILES['csv_file']['name']);
                if($filename[1] == 'csv')
                {
                    $allowedColNum = 8;
                    $handle = fopen($_FILES['csv_file']['tmp_name'], "r");
                    while($data = fgetcsv($handle))
                    {
                        // count($data) is the number of columns
                        $numcols = count($data);

                        //check number of columns
                        if ($numcols == $allowedColNum) {
                                            
                            //check if csv already uploaded.
                            $query_tr = $this->db->get_where('time_records', array(
                                'bib_assigned_number' => $data[1],
                                're_id' => $data[6],
                                'rc_id' => $data[7]
                            ));
                            $check_tr_num_rows = $query_tr->num_rows();

                            $dataTimeRecords = array(
                                'tr_gun_start' => $data[0],
                                'bib_assigned_number' => $data[1],
                                'tr_start_time' => $data[2],
                                'tr_finish_time' => $data[3],
                                'tr_loops_laps' => $data[4],
                                'lr_id' => $data[5],
                                're_id' => $data[6],
                                'rc_id' => $data[7],
                            );

                            //just update if already exist
                            if($check_tr_num_rows >= 1){
                                $this->db->where('bib_assigned_number', $data[1]);
                                $this->db->update('time_records', $dataTimeRecords);
                            
                            }

                            //add new if not
                            else{

                                $this->db->insert('time_records', $dataTimeRecords);
                                // $lastinsertedId = $this->db->insert_id();
                            }
                            
                            
                            
                        }

                        if ($numcols != $allowedColNum) {
                            $arr = array(
                                'status' => 'Error!',
                                'msg' => 'Time Record\'s columns does not match! It must be '.$allowedColNum.'. You have '.$numcols  
                            );
                        }else{

                            $fp = file($_FILES['csv_file']['tmp_name']);
                            $countSavedItems = number_format(count($fp));

                            $arr = array(
                                'status' => 'Success!',
                                'msg' => $countSavedItems.' 708 Time Records uploaded successfully!'
                            );
                        }
                    }
                    fclose($handle);
                    
                    
                }else{
                    $arr = array(
                        'status' => 'Error!',
                        'msg' => 'Wrong file!. Choose only .csv'
                    );
                }

            }else{
                $arr = array(
                    'status' => 'Error!',
                    'msg' => 'You don\'t have the power to do this! ' 
                );
            }
            echo json_encode($arr);
        }

        //upload csv Loop Records
        if($do_action == 'upload_csv_loop_records'){
            if( isset($_SESSION['isAdmin']) || isset($_SESSION['isAuthor'])   ) {

                set_time_limit(0); //remove the max_execution_time default is 30sec.

                $filename = explode(".", $_FILES['csv_file_loop_records']['name']);
                if($filename[1] == 'csv')
                {
                    $allowedColNum = 3;
                    $handle = fopen($_FILES['csv_file_loop_records']['tmp_name'], "r");
                    while($data = fgetcsv($handle))
                    {
                        // count($data) is the number of columns
                        $numcols = count($data);

                        //check number of columns
                        if ($numcols == $allowedColNum) {

                            //check if csv already uploaded.
                            $query_tr = $this->db->get_where('time_records', array(
                                'bib_assigned_number' => $data[1],
                                're_id' => $data[6],
                                'rc_id' => $data[7]
                            ));
                            $check_tr_num_rows = $query_tr->num_rows();

                            $dataLoopRecords = array(
                                'lr_loop_number' => $data[0],
                                'lr_loop_elapsed_time' => $data[1],
                                'bib_assigned_number' => $data[2],
                            );

                            //just update if already exist
                            if($check_tr_num_rows >= 1){

                                $updateWhere = array(
                                    'lr_loop_number' => $data[0],
                                    'bib_assigned_number' => $data[2],
                                );

                                $this->db->where($updateWhere);
                                $this->db->update('loops_records', $dataLoopRecords);
                            
                            }

                            //add new if not
                            else{

                                $this->db->insert('loops_records', $dataLoopRecords);
                                // $lastinserted_lrId = $this->db->insert_id();
                            }

                        }
                        if ($numcols != $allowedColNum) {
                            $arr = array(
                                'status' => 'Error!',
                                'msg' => 'Loop Record\'s columns does not match! It must be '.$allowedColNum.'. You have '.$numcols
                            );
                        }else{

                            $fp = file($_FILES['csv_file']['tmp_name']);
                            $countSavedItems = number_format(count($fp));

                            $arr = array(
                                'status' => 'Success!',
                                'msg' => $countSavedItems.' 796 Loop Records uploaded successfully!'
                            );
                        }
                    }
                    fclose($handle);
                    
                   

                }else{
                    $arr = array(
                        'status' => 'Error!',
                        'msg' => 'Wrong file!. Choose only .csv'
                    );
                }

            }else{
                $arr = array(
                    'status' => 'Error!',
                    'msg' => 'You don\'t have the power to do this! ' 
                );
            }
            echo json_encode($arr);
        }

        //remove item
        if($do_action == 'remove_item'){
            if( isset($_SESSION['isAdmin']) || isset($_SESSION['isAuthor']) || isset($_SESSION['isContributor'])   ) {
                
                $dataTable = $this->input->post('dataTable');
                $dataColumn = $this->input->post('dataColumn');
                $id = $this->input->post('id');
                
                //filter columns to be deleted
                //accept only if table is from: members, time_records, participants 
                
                if($dataTable != 'members' || $dataTable != 'time_records' || $dataTable != 'participants'){

                    $this->db->where($dataColumn, $id)->delete($dataTable);

                    $arr = array(
                        'status' => 'Success!',
                        'msg' => '1 "'.ucwords(str_replace("_"," ", $dataTable)).'" removed.'
                    );
                }else{
                    $arr = array(
                        'status' => 'Error!',
                        'msg' => 'You\'re not allowed to do this! ' 
                    );
                }
                echo json_encode($arr);
            
            }else{
                $arr = array(
                    'status' => 'Error!',
                    'msg' => 'You don\'t have the power to do this! ' 
                );
            }
        }

        //remove featured image
        if($do_action == 'remove_featured_image'){
            if( isset($_SESSION['isAdmin']) || isset($_SESSION['isAuthor']) || isset($_SESSION['isContributor'])   ) {

                $imgId = $this->input->post('imgId');
                $file = $this->get_file($imgId, 'media_files', 'mf_id');

                //put file name in session for removing thumbnails
                // $this->session->set_userdata('sess_file_name',$file->mf_file_name); 
                $fileName = $file->mf_file_name; // $_SESSION['sess_file_name'];
                // $fileNameThumb = $file->mf_file_name_thumb; // $_SESSION['sess_file_name'];

                if($fileName != ''){                  
                    

                    unlink('./uploads/ar_' . $fileName);   
                    unlink('./uploads/' . $fileName);  

                    
                    
                    
                    $this->db->where('mf_id', $imgId)->delete('media_files');

                    //destroy session after delete
                    // $this->session->unset_userdata('sess_file_name');

                    // return TRUE;
                    $arr = array(
                        'status' => 'Success!',
                        'msg' => 'Featured Image Removed.'
                    );
                }else{
                    $arr = array(
                        'status' => 'Error!',
                        'msg' => 'No image found.'
                    );
                }
                                
                echo json_encode($arr);
            }else{
                $arr = array(
                    'status' => 'Error!',
                    'msg' => 'You don\'t have the power to do this! ' 
                );
            }
        }
        
        //upload featured image
        if($do_action == 'add_featured_image'){
            //check user role
            if( isset($_SESSION['isAdmin']) || isset($_SESSION['isAuthor']) || isset($_SESSION['isContributor'])   ) {
                 
                $file_element_name = 'event_featured_image';
     
                $config['upload_path']          = './uploads/'; //root folder
                $config['allowed_types']        = 'gif|jpg|png|jpeg';
                $config['max_size']             = 1024 * 5; //5mb
                $config['max_width']            = 1024 * 7; //7K px
                $config['max_height']           = 1024 * 7; //7K px

                $this->load->library('upload', $config);

                //upload images if any
                if ( ! $this->upload->do_upload($file_element_name))
                {
                    $arr = array(
                        'status' => 'Error!',
                        'msg' => $this->upload->display_errors()
                    );
                }
                else 
                {
                    /*** upload the main image ***/
                    $data = $this->upload->data();  

                    /*** create thumbnail ***/

                    //aspect ratio min 300px
                    $config['image_library'] = 'gd2';
                    $config['source_image'] = './uploads/'.$data["file_name"];
                    $config['new_image'] = './uploads/ar_'.$data["file_name"];
                    $config['create_thumb'] = FALSE;
                    $config['maintain_ratio'] = TRUE;
                    $config['width'] = 300;
                    $config['height'] = 300;

                    $this->load->library('image_lib', $config);
                    $this->image_lib->resize();
                    
                    /*** save to database ***/
                    $fileInsert = $this->insert_file($data['file_name']);
                    $lastImage_ID = $this->db->insert_id();

                    if($fileInsert)
                    {
                        


 
                        $arr = array(
                            'status' => 'Success!',
                            'msg' => 'Uploaded Successfully!',
                            'img' => '<button type="button" class="floating-label button small removeFeaturedImage" id="'.$lastImage_ID.'">x</button> <button type="button" data-src="'.base_url().'uploads/ar_'.$data["file_name"].'" id="'.$lastImage_ID.'" class="button small btn-insert-to-post">Insert to post</button> <img src="'.base_url().'uploads/'.$data["file_name"].'" width="300" height="225" class="img-thumbnail" /><br>Image ID: <input type="text" value="'.$lastImage_ID.'"> '
                        );
                        
                    }
                    else
                    {
                        unlink($data['full_path']);
                        $arr = array(
                            'status' => 'Success!',
                            'msg' => 'Something went wrong when saving the file, please try again.',
                        );
                        
                    }
                
                    
                    
                    
                }

                @unlink($_FILES[$file_element_name]);
                            
                 
                echo json_encode($arr);
            }else{
                $arr = array(
                    'status' => 'Error!',
                    'msg' => 'You don\'t have the power to do this! ' 
                );
            }
        }

        //edit_event
        if($do_action == 'edit_event'){
           
            //check user role
            if( isset($_SESSION['isAdmin']) || isset($_SESSION['isAuthor']) || isset($_SESSION['isContributor'])   ) {

                //validate the form
                $this->form_validation->set_rules('event_title', 'Event title ', 'required');
                // $this->form_validation->set_message('is_unique', 'The %s "'.$this->input->post('event_title').'" already exist.');

                $this->form_validation->set_rules('event_slug', 'Event slug ', 'required');
                // $this->form_validation->set_message('is_unique', 'The %s "'.$this->input->post('event_slug').'" already exist.');

                // $this->form_validation->set_rules('event_race_categories_[]', 'Race categories are ', 'required');
                // $this->form_validation->set_rules('catItemPrice_[]', 'Check the category item prices. It must match!', 'required');

                // $this->form_validation->set_rules('event_singlets_[]', 'Singlets ', 'required');
                $this->form_validation->set_rules('event_status', 'Event status ', 'required');
                $this->form_validation->set_rules('event_start_date', 'Event start date ', 'required');
                $this->form_validation->set_rules('event_end_date', 'Event end date ', 'required');
                $this->form_validation->set_rules('event_tags', 'Event tags ', 'required');
                $this->form_validation->set_rules('event_publish_status', 'Publish status ', 'required');
                $this->form_validation->set_rules('event_featured_image', 'Featured image ', 'required');
                
                //if there's an error
                if ($this->form_validation->run() == FALSE){

                    $arr = array(
                        'status' => 'Invalid!',
                        'msg' => 'You have invalid details.',
                        'event_title' => form_error('event_title'),
                        'event_slug' => form_error('event_slug'),
                        // 'event_race_categories' => form_error('event_race_categories_[]'),
                        // 'catItemPrice' => form_error('catItemPrice_[]'),
                        // 'event_singlets' => form_error('event_singlets_[]'),
                        'event_status' => form_error('event_status'),
                        'event_start_date' => form_error('event_start_date'),
                        'event_end_date' => form_error('event_end_date'),
                        'event_tags' => form_error('event_tags'),
                        'event_publish_status' => form_error('event_publish_status'),
                        'event_featured_image' => form_error('event_featured_image'),
                    );
                    
                }

                //if NO errors
                else{
                    $eventID = $this->input->post('re_id');
                    $eventTitle = $this->input->post('event_title');
                    $eventSlug =  url_title($this->input->post('event_slug'), 'underscore', TRUE);//trim($this->input->post('event_slug'));
                    $eventDescription = $this->input->post('event_description');
                    $eventStatus = $this->input->post('event_status');
                    $eventStartDate = $this->input->post('event_start_date');
                    $eventEndDate = $this->input->post('event_end_date');
                    $eventTags = $this->input->post('event_tags');
                    $eventFeaturedImage = $this->input->post('event_featured_image');

                    //check if the event exist
                    $query_event = $this->db->get_where('race_events', array('re_id' => $eventID));
                    $row_event_num_rows = $query_event->num_rows();
                    
                    if($row_event_num_rows >= 1){ 
                        
                        //-----Auto rename slug if having the same slug name
                        $query_slug = $this->db->get_where('race_events', array('re_slug' => $eventSlug));
                        $check_num_slug = $query_slug->num_rows();
                        
                        //if exist
                        if($check_num_slug >= 1){
                            
                            $re_slug_query = $this->db->select('*')
                            ->where('re_slug', $eventSlug)
                            ->get('race_events'); 
                            $row_event = $re_slug_query->row();
                            $rowEventId = $row_event->re_id;
                            $rowEventSlug = $row_event->re_slug;
                            
                            //check if same ID
                            if($rowEventId == $eventID){
                                $eventSlug = $eventSlug; //url_title($this->input->post('event_slug'), 'underscore', TRUE)  ; //just update the existing slug
                            }
                            //if not same ID
                            else{
                                //rename the slug increment the name by 1
                                $i = 2;
                                $eventSlug = $eventSlug .'_'. $i;
                                
                            }

                        }
                        //not exist
                        else{
                            $eventSlug = $eventSlug ;
                        }
                    

                        
                        
                        //force contributors to post as draft for approval
                        if( isset($_SESSION['isContributor'])){
                            $eventPublishStatus = '0';
                        }else{
                            $eventPublishStatus = $this->input->post('event_publish_status');
                        }
    
                        
                        $dataEvent = array(
                            're_name' => $eventTitle,
                            're_slug' => $eventSlug,
                            're_description' => $eventDescription,
                            're_status' => $eventStatus,
                            're_start_date' => $eventStartDate,
                            're_end_date' => $eventEndDate,
                            're_added_by' => $_SESSION['sess_user_id'],
                            're_tags' => $eventTags,
                            're_publish_status' => $eventPublishStatus,
                            'mf_id' => $eventFeaturedImage,
                        );
    
                        $this->db->where('re_id', $eventID);
                        $this->db->update('race_events', $dataEvent);

                        $lastEventSaved_ID = $eventID;//$this->db->insert_id();
                    
                        //save each categories to race_categories table
                        $race_categories = $this->input->post('event_race_categories_');
                        if($race_categories != ''){

                            foreach ($race_categories as $key => $captured_race_categories) {
        
                                $captured_catItemPrice = $this->input->post('catItemPrice_')[$key];
                            
                                //save to race_categories table
                                $dataCategories = array(
                                    're_id' => $lastEventSaved_ID,
                                    'rc_name' => $captured_race_categories,
                                    'rc_fee' => $captured_catItemPrice,
                                );
                                $this->db->insert('race_categories', $dataCategories);
                                
                            
                                
                            }
                        }

                        //save each singlet_sizes table
                        $race_singlets = $this->input->post('event_singlets_') ;

                        if( $race_singlets !=''){
                            foreach ($race_singlets as $key => $captured_singlets) {
        
                            //save to singlet_sizes table
                            $dataSinglets = array(
                                're_id' => $lastEventSaved_ID,
                                'ss_name' => $captured_singlets,
                            );
                            $this->db->insert('singlet_sizes', $dataSinglets);
                            
                        }
        
                        }
                        $arr = array(
                            'status' => 'Success!',
                            'msg' =>  'Event updated successfully!'//.$eventDescription 
                        );
                    }else{
                        $arr = array(
                            'status' => 'Error!',
                            'msg' => 'This event does not exist! Do not cheat!' 
                        );
                    }

                }

                
                
            }else{
                $arr = array(
                    'status' => 'Error!',
                    'msg' => 'You don\'t have the power to do this! ' 
                );
            }

            
            echo json_encode($arr);
        }

        //add_new_event
        if($do_action == 'add_new_event'){
            // $data['members'] = $this->members_model->get_members($_SESSION['sess_user_id']);
            // $user_role = $data['members']['m_role'];
            // if($user_role != '2'){
                
            //check user role
            if( isset($_SESSION['isAdmin']) || isset($_SESSION['isAuthor']) || isset($_SESSION['isContributor'])   ) {

                //validate the form
                $this->form_validation->set_rules('event_title', 'Event title ', 'required|is_unique[race_events.re_name]');
                $this->form_validation->set_message('is_unique', 'The %s "'.$this->input->post('event_title').'" already exist.');
                $this->form_validation->set_rules('event_slug', 'Event slug ', 'required|is_unique[race_events.re_slug]');
                $this->form_validation->set_message('is_unique', 'The %s "'.$this->input->post('event_slug').'" already exist.');

                $this->form_validation->set_rules('event_race_categories_[]', 'Race categories are ', 'required');
                $this->form_validation->set_rules('catItemPrice_[]', 'Check the category item prices. It must match!', 'required');

                $this->form_validation->set_rules('event_singlets_[]', 'Singlets ', 'required');
                $this->form_validation->set_rules('event_status', 'Event status ', 'required');
                $this->form_validation->set_rules('event_start_date', 'Event start date ', 'required');
                $this->form_validation->set_rules('event_end_date', 'Event end date ', 'required');
                $this->form_validation->set_rules('event_tags', 'Event tags ', 'required');
                $this->form_validation->set_rules('event_publish_status', 'Publish status ', 'required');
                $this->form_validation->set_rules('event_featured_image', 'Featured image ', 'required');
                
                //if there's an error
                if ($this->form_validation->run() == FALSE){

                    $arr = array(
                        'status' => 'Invalid!',
                        'msg' => 'You have invalid details.',
                        'event_title' => form_error('event_title'),
                        'event_slug' => form_error('event_slug'),
                        'event_race_categories' => form_error('event_race_categories_[]'),
                        'catItemPrice' => form_error('catItemPrice_[]'),
                        'event_singlets' => form_error('event_singlets_[]'),
                        'event_status' => form_error('event_status'),
                        'event_start_date' => form_error('event_start_date'),
                        'event_end_date' => form_error('event_end_date'),
                        'event_tags' => form_error('event_tags'),
                        'event_publish_status' => form_error('event_publish_status'),
                        'event_featured_image' => form_error('event_featured_image'),
                    );
                    
                }

                //if NO errors
                else{

                    $eventTitle = $this->input->post('event_title');
                    $eventSlug = url_title($this->input->post('event_slug'), 'underscore', TRUE); //$this->input->post('event_slug');

                    $eventDescription = $this->input->post('event_description');
                    $eventStatus = $this->input->post('event_status');
                    $eventStartDate = $this->input->post('event_start_date');
                    $eventEndDate = $this->input->post('event_end_date');
                    $eventTags = $this->input->post('event_tags');
                    $eventFeaturedImage = $this->input->post('event_featured_image');
                    
                    
                        //force contributors to post as draft for approval
                        if( isset($_SESSION['isContributor'])){
                            $eventPublishStatus = '0';
                        }else{
                            $eventPublishStatus = $this->input->post('event_publish_status');
                        }
    
                        
                        $dataEvent = array(
                            're_name' => $eventTitle,
                            're_slug' => $eventSlug,
                            're_description' => $eventDescription,
                            're_status' => $eventStatus,
                            're_start_date' => $eventStartDate,
                            're_end_date' => $eventEndDate,
                            're_added_by' => $_SESSION['sess_user_id'],
                            're_tags' => $eventTags,
                            're_publish_status' => $eventPublishStatus,
                            'mf_id' => $eventFeaturedImage,
                        );
    
                        $this->db->insert('race_events', $dataEvent);
                        $lastEventSaved_ID = $this->db->insert_id();
                       
                         //save each categories to race_categories table
                         foreach ($this->input->post('event_race_categories_') as $key => $captured_race_categories) {
    
                            //  $captured_singlets = $this->input->post('event_singlets_')[$key];
                             $captured_catItemPrice = $this->input->post('catItemPrice_')[$key];
                           
                            //save to race_categories table
                            $dataCategories = array(
                                're_id' => $lastEventSaved_ID,
                                'rc_name' => $captured_race_categories,
                                'rc_fee' => $captured_catItemPrice,
                            );
                            $this->db->insert('race_categories', $dataCategories);
                            
                            //save to singlet_sizes table
                            // $dataSinglets = array(
                            //     're_id' => $lastEventSaved_ID,
                            //     'ss_name' => $captured_singlets,
                            // );
                            // $this->db->insert('singlet_sizes', $dataSinglets);
                            
                        }

                        //save each singlet_sizes table
                        foreach ($this->input->post('event_singlets_') as $key => $captured_singlets) {
    
                           //save to singlet_sizes table
                           $dataSinglets = array(
                               're_id' => $lastEventSaved_ID,
                               'ss_name' => $captured_singlets,
                           );
                           $this->db->insert('singlet_sizes', $dataSinglets);
                           
                       }
    
                        $arr = array(
                            'status' => 'Success!',
                            'msg' =>  'Event added successfully!'.$lastEventSaved_ID 
                        );
                    // }

                }

                
                
            }else{
                $arr = array(
                    'status' => 'Error!',
                    'msg' => 'You don\'t have the power to do this! '. $user_role
                );
            }

            
            echo json_encode($arr);
        }

        //add_new_lakwatcharity_event
        if($do_action == 'add_new_lakwatcharity_event'){
             
            //check user role
            if( isset($_SESSION['isAdmin']) || isset($_SESSION['isAuthor']) || isset($_SESSION['isContributor'])   ) {

                //validate the form
                $this->form_validation->set_rules('event_title', 'Event Title ', 'required|is_unique[lakwatcharity_events.lwc_name]');
                $this->form_validation->set_message('is_unique', 'The %s "'.$this->input->post('event_title').'" already exist.');
                $this->form_validation->set_rules('event_slug', 'Event Slug ', 'required|is_unique[lakwatcharity_events.lwc_slug]');
                $this->form_validation->set_message('is_unique', 'The %s "'.$this->input->post('event_slug').'" already exist.');
                $this->form_validation->set_rules('event_status', 'Event Status ', 'required');
                $this->form_validation->set_rules('event_start_date', 'Event start date ', 'required');
                $this->form_validation->set_rules('event_end_date', 'Event end date ', 'required');
                $this->form_validation->set_rules('event_tags', 'Event tags ', 'required');
                $this->form_validation->set_rules('event_publish_status', 'Publish Status ', 'required');
                $this->form_validation->set_rules('event_featured_image', 'Featured Image ', 'required');
                $this->form_validation->set_rules('event_description', 'Event Description ', 'required');
                
                //if there's an error
                if ($this->form_validation->run() == FALSE){

                    $arr = array(
                        'status' => 'Invalid!',
                        'msg' => 'You have invalid details.',
                        'event_title' => form_error('event_title'),
                        'event_slug' => form_error('event_slug'),
                        'event_status' => form_error('event_status'),
                        'event_start_date' => form_error('event_start_date'),
                        'event_end_date' => form_error('event_end_date'),
                        'event_tags' => form_error('event_tags'),
                        'event_publish_status' => form_error('event_publish_status'),
                        'event_featured_image' => form_error('event_featured_image'),
                        'event_description' => form_error('event_description'),
                    );
                    
                }

                //if NO errors
                else{

                    $eventTitle = $this->input->post('event_title');
                    $eventSlug = url_title($this->input->post('event_slug'), 'underscore', TRUE); //$this->input->post('event_slug');

                    $eventDescription = $this->input->post('event_description');
                    $eventStatus = $this->input->post('event_status');
                    $eventStartDate = $this->input->post('event_start_date');
                    $eventEndDate = $this->input->post('event_end_date');
                    $eventTags = $this->input->post('event_tags');
                    $eventFeaturedImage = $this->input->post('event_featured_image');
                    
                    
                        //force contributors to post as draft for approval
                        if( isset($_SESSION['isContributor'])){
                            $eventPublishStatus = '0';
                        }else{
                            $eventPublishStatus = $this->input->post('event_publish_status');
                        }
    
                        if( $eventDescription != ''){
                            $dataEvent = array(
                                'lwc_name' => $eventTitle,
                                'lwc_slug' => $eventSlug,
                                'lwc_description' => $eventDescription,
                                'lwc_status' => $eventStatus,
                                'lwc_start_date' => $eventStartDate,
                                'lwc_end_date' => $eventEndDate,
                                'lwc_added_by' => $_SESSION['sess_user_id'],
                                'lwc_tags' => $eventTags,
                                'lwc_publish_status' => $eventPublishStatus,
                                'mf_id' => $eventFeaturedImage,
                            );
        
                            $this->db->insert('lakwatcharity_events', $dataEvent);
                            $lastEventSaved_ID = $this->db->insert_id();
                            
                            $arr = array(
                                'status' => 'Success!',
                                'msg' =>  'Event added successfully!'  
                            );
                        }else{
                            $arr = array(
                                'status' => 'Error!',
                                'msg' =>  'Description is blank!' 
                            );
                        }
                        
                }

                
                
            }else{
                $arr = array(
                    'status' => 'Error!',
                    'msg' => 'You don\'t have the power to do this! '. $user_role
                );
            }

            
            echo json_encode($arr);
        }

         //edit_lakwatcharity_event
         if($do_action == 'edit_lakwatcharity_event'){
           
            //check user role
            if( isset($_SESSION['isAdmin']) || isset($_SESSION['isAuthor']) || isset($_SESSION['isContributor'])   ) {

                //validate the form
                $this->form_validation->set_rules('event_title', 'Event title ', 'required');
                $this->form_validation->set_rules('event_slug', 'Event slug ', 'required');
                $this->form_validation->set_rules('event_status', 'Event status ', 'required');
                $this->form_validation->set_rules('event_start_date', 'Event start date ', 'required');
                $this->form_validation->set_rules('event_end_date', 'Event end date ', 'required');
                $this->form_validation->set_rules('event_tags', 'Event tags ', 'required');
                $this->form_validation->set_rules('event_publish_status', 'Publish status ', 'required');
                $this->form_validation->set_rules('event_featured_image', 'Featured image ', 'required');
                $this->form_validation->set_rules('event_description', 'Event Description ', 'required');
                
                //if there's an error
                if ($this->form_validation->run() == FALSE){

                    $arr = array(
                        'status' => 'Invalid!',
                        'msg' => 'You have invalid details.',
                        'event_title' => form_error('event_title'),
                        'event_slug' => form_error('event_slug'),                    
                        'event_description' => form_error('event_description'),
                        'event_status' => form_error('event_status'),
                        'event_start_date' => form_error('event_start_date'),
                        'event_end_date' => form_error('event_end_date'),
                        'event_tags' => form_error('event_tags'),
                        'event_publish_status' => form_error('event_publish_status'),
                        'event_featured_image' => form_error('event_featured_image'),
                    );
                    
                }

                //if NO errors
                else{
                    $eventID = $this->input->post('lwc_id');
                    $eventTitle = $this->input->post('event_title');
                    $eventSlug =  url_title($this->input->post('event_slug'), 'underscore', TRUE);//trim($this->input->post('event_slug'));
                    $eventDescription = $this->input->post('event_description');
                    $eventStatus = $this->input->post('event_status');
                    $eventStartDate = $this->input->post('event_start_date');
                    $eventEndDate = $this->input->post('event_end_date');
                    $eventTags = $this->input->post('event_tags');
                    $eventFeaturedImage = $this->input->post('event_featured_image');

                    //check if the event exist
                    $query_event = $this->db->get_where('lakwatcharity_events', array('lwc_id' => $eventID));
                    $row_event_num_rows = $query_event->num_rows();
                    
                    if($row_event_num_rows >= 1){ 
                        
                        //-----Auto rename slug if having the same slug name
                        $query_slug = $this->db->get_where('lakwatcharity_events', array('lwc_slug' => $eventSlug));
                        $check_num_slug = $query_slug->num_rows();
                        
                        //if exist
                        if($check_num_slug >= 1){
                            
                            $re_slug_query = $this->db->select('*')
                            ->where('lwc_slug', $eventSlug)
                            ->get('lakwatcharity_events'); 
                            $row_event = $re_slug_query->row();
                            $rowEventId = $row_event->lwc_id;
                            $rowEventSlug = $row_event->lwc_slug;
                            
                            //check if same ID
                            if($rowEventId == $eventID){
                                $eventSlug = $eventSlug; //url_title($this->input->post('event_slug'), 'underscore', TRUE)  ; //just update the existing slug
                            }
                            //if not same ID
                            else{
                                //rename the slug increment the name by 1
                                $i = 2;
                                $eventSlug = $eventSlug .'_'. $i;
                                
                            }

                        }
                        //not exist
                        else{
                            $eventSlug = $eventSlug ;
                        }
                    

                        
                        
                        //force contributors to post as draft for approval
                        if( isset($_SESSION['isContributor'])){
                            $eventPublishStatus = '0';
                        }else{
                            $eventPublishStatus = $this->input->post('event_publish_status');
                        }
    
                        
                        $dataEvent = array(
                            'lwc_name' => $eventTitle,
                            'lwc_slug' => $eventSlug,
                            'lwc_description' => $eventDescription,
                            'lwc_status' => $eventStatus,
                            'lwc_start_date' => $eventStartDate,
                            'lwc_end_date' => $eventEndDate,
                            'lwc_added_by' => $_SESSION['sess_user_id'],
                            'lwc_tags' => $eventTags,
                            'lwc_publish_status' => $eventPublishStatus,
                            'mf_id' => $eventFeaturedImage,
                        );
    
                        $this->db->where('lwc_id', $eventID);
                        $this->db->update('lakwatcharity_events', $dataEvent);

                        $lastEventSaved_ID = $eventID;//$this->db->insert_id();
                     
                        $arr = array(
                            'status' => 'Success!',
                            'msg' =>  'Event updated successfully!'//.$eventDescription 
                        );
                    }else{
                        $arr = array(
                            'status' => 'Error!',
                            'msg' => 'This charity event does not exist! Do not cheat!' 
                        );
                    }

                }

                
                
            }else{
                $arr = array(
                    'status' => 'Error!',
                    'msg' => 'You don\'t have the power to do this! ' 
                );
            }

            
            echo json_encode($arr);
        }

        //re
        if($do_action == "re"){
            $result = $this->db->where("re_id",$re_id)->get("race_categories")->result();
            echo json_encode($result);
        }//re

        //rc
        if($do_action == "rc"){
            $result = $this->db->where("re_id",$re_id)->get("singlet_sizes")->result();
            echo json_encode($result);
        } //rc

        //add new member via members page
        if($do_action == "reg"){
            if( isset($_SESSION['isAdmin']) ) {
                //** DO VALIDATIONS *//
                
                /** BIB NUMBER */
                // if($this->input->post('assigned_bib_number') != ''){
                //     $this->form_validation->set_rules('assigned_bib_number', 'Bib Number under "Race Details & Kits Tab"', 'required|integer|greater_than[0]');
                //     $this->form_validation->set_message('greater_than', 'The %s is invalid.');
                // }

                /** Payment Method */
                $this->form_validation->set_rules('payment_method', 'Payment Method under "Review Info Tab"', 'required');
                
                /** Race Category, Race Event ID,  and Singlet Size */
                //$this->form_validation->set_rules('race-event', 'Race Event under "Race Details & Kits Tab"', '');
                //$this->form_validation->set_rules('choose_race_category', 'Race Category under "Race Details & Kits Tab"', '');
                //$this->form_validation->set_rules('choose_singlet_size', 'Singlet Size under "Race Details & Kits Tab"', '');
                // $this->form_validation->set_rules('race-event', 'Race Event ID not found!', 'required|integer');
                
                /** Claiming Details */
                $this->form_validation->set_rules('raceKitDeliverPickup', 'Preferred Pickup under "Claiming Details Tab"', 'required');
                $this->form_validation->set_rules('terms_condition_checkbox', 'Terms and Condition under "Claiming Details Tab"', 'required');
                
                /** Check if Claiming Details is Delivery or Pickup */
                $raceKitDeliverPickup = $this->input->post('raceKitDeliverPickup');
                if(  $raceKitDeliverPickup == "delivered" ){
                    $this->form_validation->set_rules('delivery_address', 'Delivery address under "Claiming Details Tab"', 'required');
                }

                /** Personal Details */
                $this->form_validation->set_rules('lastName', 'Last Name under "Member Info Tab"', 'required');
                $this->form_validation->set_rules('firstName', 'First Name under "Member Info Tab"', 'required');
                $this->form_validation->set_rules('middleName', 'Middle Name', '');
                $this->form_validation->set_rules('gender', 'Gender', '');
                $this->form_validation->set_rules('club_group', 'Clubs / Groups', '');
                $this->form_validation->set_rules('address', 'Address under "Member Info Tab"', 'required');
                $this->form_validation->set_rules('mobile_phone', 'Mobile under "Member Info Tab"', 'required');

                $this->form_validation->set_rules('email', 'Email', 'required|valid_email|is_unique[members.m_email]');
                $this->form_validation->set_message('is_unique', 'The %s "'.$this->input->post('email').'" is already registered.');

                
                $this->form_validation->set_rules('age_group', 'Age Group under "Member Info Tab"', 'required');

                /** Emergency Contact Details */
                $this->form_validation->set_rules('ecd_last_name', 'Last Name for Emerg. Contact under "Member Info Tab"', 'required');
                
                $this->form_validation->set_rules('ecd_first_name', 'First Name for Emerg. Contact under "Member Info Tab"', 'required');
                // $this->form_validation->set_message('required', 'First name for Emergency Contact Info is needed.');
                
                $this->form_validation->set_rules('ecd_middle_name', 'Middle Name for Emerg. Contact under "Member Info Tab"', '');
                $this->form_validation->set_rules('ecd_mobile', 'Mobile / Phone for Emerg. Contact under "Member Info Tab"', 'required');
                // $this->form_validation->set_message('required', 'Mobile / Phone for Emergency Contact Info is needed.');
                
                $this->form_validation->set_rules('ecd_email', 'Email for Emerg. Contact under "Member Info Tab"', 'valid_email');
                $this->form_validation->set_rules('ecd_address', 'Address for Emerg. Contact under "Member Info Tab"', '');
                $this->form_validation->set_rules('ecd_relationship', 'Relationship for Emerg. Contact under "Member Info Tab"', '');


                if ($this->form_validation->run() == FALSE)
                {
                    $raceKitDeliverPickup = $this->input->post('raceKitDeliverPickup');
                    if(  $raceKitDeliverPickup == "delivered" ){
                        $formError_deliverAddress = form_error('delivery_address');
                    }
                    else{
                        $formError_deliverAddress = '';
                    }

                    // if($this->input->post('assigned_bib_number') != ''){
                    //     $formError_assigned_bib_number = form_error('assigned_bib_number');
                    // }else{
                    //     $formError_assigned_bib_number = '';
                    // }

                    $arr = array(
                        'status' => 'Error!',
                        //'assigned_bib_number' => $formError_assigned_bib_number,
                        'delivery_address' => $formError_deliverAddress,
                        'payment_method' => form_error('payment_method'),
                    
                        'raceKitDeliverPickup' => form_error('raceKitDeliverPickup'),
                        'terms_condition_checkbox' => form_error('terms_condition_checkbox'),

                        'firstName' => form_error('firstName'),
                        'lastName' => form_error('lastName'),
                        'address' => form_error('address'),
                        'mobile_phone' => form_error('mobile_phone'),
                        'email' => form_error('email'),
                        'age_group' => form_error('age_group'),

                        'ecd_last_name' => form_error('ecd_last_name'),
                        'ecd_first_name' => form_error('ecd_first_name'),
                        'ecd_mobile' => form_error('ecd_mobile'),
                        'ecd_email' => form_error('ecd_email'),
                    );

                    $result = $arr;
                    echo json_encode($result);
                    
                }
                else
                {
                    //** Check if Race Event has content *//
                    if(  $this->input->post('race-event') != ''){
                        $race_event_content =  $this->input->post('race-event');
                        $race_category_content =  $this->input->post('choose_race_category');
                        $singlet_size_content =  $this->input->post('choose_singlet_size');
                    }else{ 
                        $race_event_content = 0;
                        $race_category_content = 0;
                        $singlet_size_content = 0;
                    }

                    //** check if the event is done or not_done **//
                    $query_re = $this->db->get_where('race_events', array('re_id' => $race_event_content));
                    $row_re = $query_re->row_array();
                    $re_status = $row_re['re_status'];

                    if($re_status == 'not_done'){
                    
                        $randomUsernmaneSuffix = $this->randomUsernmaneSuffix();
                        $randomPassword = $this->randomPassword();
                        $getUsername = strtolower($this->input->post('firstName'). '.' .$this->input->post('lastName'));
                        $generatedUsername = str_replace(" ", "", $getUsername);
                        
                        //** gather data **//
                        $data = array(
                            'm_role' => 2,//member role
                            'm_status' => 1,//member status is activated if added by an admin
                            'm_username' => $generatedUsername.".".$randomUsernmaneSuffix,
                            'm_password' => md5($randomPassword),
                            'm_first_name' => $this->input->post('firstName'),
                            'm_last_name' => $this->input->post('lastName'),
                            'm_middle_name' => $this->input->post('middleName'),
                            'm_gender' => $this->input->post('gender'),
                            'm_address' => $this->input->post('address'),
                            'm_mobile' => $this->input->post('mobile_phone'),
                            'm_email' => $this->input->post('email'),
                            'm_age_group' => $this->input->post('age_group'),
                            'm_clubs_groups' => $this->input->post('club_group')
                        );

                        //** insert to members data **//
                        $this->db->insert('members', $data);
                        
                        //** get the latest m_id of saved members **//
                        $lastMemberSaved_ID = $this->db->insert_id();

                        //** insert to emergency_contact_details together with m_id **//
                        $dataEcd = array(
                            'ecd_relationship' => $this->input->post('ecd_relationship'),
                            'ecd_first_name' => $this->input->post('ecd_first_name'),
                            'ecd_middle_name' => $this->input->post('ecd_middle_name'),
                            'ecd_last_name' => $this->input->post('ecd_last_name'),
                            'ecd_address' => $this->input->post('ecd_address'),
                            'ecd_mobile' => $this->input->post('ecd_mobile'),
                            'ecd_email' => $this->input->post('ecd_email'),
                            'm_id' => $lastMemberSaved_ID
                        );

                        $this->db->insert('emergency_contact_details', $dataEcd);

                        //** get the actual prize of selected race category **//
                        if( $this->input->post('choose_race_category') != ''){
                            $rc_id =  $this->input->post('choose_race_category');
                            $rc_id_query = $this->db->select('rc_fee')
                                ->where('rc_id', $rc_id)
                                ->get('race_categories'); 
                                $get_rc_fee = $rc_id_query->row();
                                $rc_fee = $get_rc_fee->rc_fee;
                        }else{
                            $rc_id = 0;
                            $rc_fee = 0;
                        }

                                
                        //** check selected pickup or delivery and **//
                        //** Validate if delivery has content *//
                        if(  $this->input->post('raceKitDeliverPickup') == "delivered" ){
                            $inputFor_deliverAddress = $this->input->post('delivery_address');
                            
                            $dff_query = $this->db->select('f_cost')
                                ->where('f_slug', 'delivery_fee_fixed')
                                ->get('fees'); 
            
                            $cost_dff = $dff_query->row();
                            $delivery_fee_fixed = $cost_dff->f_cost;
                        }
                        else{
                            $inputFor_deliverAddress = 'Pickup at the event area.';
                            $delivery_fee_fixed = 0;
                        }
                            

                        //** Get WebFee cost *//
                        $wf_query = $this->db->select('f_cost')
                            ->where('f_slug', 'web_fee')
                            ->get('fees'); 

                        $cost_wf = $wf_query->row();
                        $web_fee = $cost_wf->f_cost;
                            
                        
                        
                        
                        //SAVE IF re_id IS NOT BLANK, save to participants and bib_number
                        if( $race_event_content != 0){

                            //** Save to bib numbers with bib_assigned_number = 0 if empty **//
                            // if($this->input->post('assigned_bib_number') != ''){
                            //     $assignedBibNumber = $this->input->post('assigned_bib_number');
                            // }else{ 
                                $assignedBibNumber = 0;
                            // }
                            // $assignedBibNumber = $this->input->post('assigned_bib_number');

                            $dataBibNumbers = array(
                                'bib_assigned_number' => $assignedBibNumber,
                                'm_id' => $lastMemberSaved_ID,
                                're_id' => $race_event_content,
                                'rc_id' => $race_category_content,
                            );
                            $this->db->insert('bib_number', $dataBibNumbers);
                            
                            //** get the latest saved bib_id  **//
                            $lastSaved_BibId = $this->db->insert_id();



                            //** save to participants with p_payment_status = 0 as "pending" **//
                            $dataParticipants = array(
                                'm_id' => $lastMemberSaved_ID,
                                'bib_id' => $lastSaved_BibId,
                                'rc_id' => $race_category_content,
                                're_id' => $race_event_content,
                                'ss_id' => $singlet_size_content,
                                'p_payment_status' => 0, //pending payment status
                                'p_payment_method' => $this->input->post('payment_method'),
                                'p_payment_amount' => $rc_fee + $delivery_fee_fixed + $web_fee,
                                'p_payment_total' => $rc_fee + $delivery_fee_fixed + $web_fee,
                                'p_delivery_address' => $inputFor_deliverAddress
                                
                            );
                            $this->db->insert('participants', $dataParticipants);
                        }


                        //** send email to members and admin **//
                        //**  include: payment breakdown (Deliver Fee, Web Fee, Registration Fee) **//

                        $arr = array(
                            "status" => 'Success!',
                            "msg" => '1582 Success! Email sent to the new member.'
                        );
                        $result = $arr;
                        echo json_encode($result);
                    }
                    else{

                        $arr = array(
                            "status" => 'EventDone!',
                            "msg" => 'This event is finished. You can\'t add or modify any participants.'
                        );
                        $result = $arr;
                        echo json_encode($result);
                    }
                    
                    
                }
            }
            else{
                $arr = array(
                    "status" => 'Error!',
                    "msg" => 'You have no power to do this!'
                );
                $result = $arr;
                echo json_encode($result);

            }
        }//add new member via members page

        //assign_bib_number via members page
        if($do_action == "assign_bib_number"){

            // if( isset($_SESSION['isAdmin']) ) {
            if( isset($_SESSION['isAdmin']) || isset($_SESSION['isAuthor'])    ) {

                // $this->form_validation->set_rules('race-event', 'Race Event under "Assign Bib Number Tab"', 'required|integer');
                // $this->form_validation->set_message('integer', 'The %s is invalid.');
                
                // $this->form_validation->set_rules('choose_race_category', 'Race Category under "Assign Bib Number Tab"', 'required|integer');
                // $this->form_validation->set_message('integer', 'The %s is invalid.');
               
                // $this->form_validation->set_rules('choose_singlet_size', 'Siglet Size under "Assign Bib Number Tab"', 'required|integer');
                // $this->form_validation->set_message('integer', 'The %s is invalid.');
                
                $this->form_validation->set_rules('current_bib_id', 'Bib ID under "Assign Bib Number Tab"', 'required|integer');
                $this->form_validation->set_message('integer', 'The %s is invalid.');
                
                $this->form_validation->set_rules('assigned_bib_number', 'Bib Number under "Assign Bib Number Tab"', 'required|integer');
                $this->form_validation->set_message('integer', 'The %s is invalid.');

                if ($this->form_validation->run() == FALSE)
                {
                    $arr = array(
                        'status' => 'Invalid!',
                        'msg' => '',
                        // 'race_event' => form_error('race-event'),
                        // 'choose_race_category' => form_error('choose_race_category'),
                        // 'choose_singlet_size' => form_error('choose_singlet_size'),
                        'current_bib_id' => form_error('current_bib_id'),
                        'assigned_bib_number' => form_error('assigned_bib_number')
                    );

                    $result = $arr;
                    echo json_encode($result);

                }else{ //if no errors

                    //**gather data **//
                    // $reId =$this->input->post('race-event');
                    // $rcId = $this->input->post('choose_race_category');
                    // $ssId = $this->input->post('choose_singlet_size');
                    $bibID = $this->input->post('current_bib_id');
                    $bibNumber = $this->input->post('assigned_bib_number');
                    $userId_from_post = $this->input->post('userId');

                    // get participant details based on bib_id number
                    $query_participants = $this->db->get_where('participants', array('bib_id' => $bibID));
                    $row_participants = $query_participants->row_array();
                    $p_reId = $row_participants['re_id'];
                    $p_rcId = $row_participants['rc_id'];
                    $p_mId = $row_participants['m_id'];



                    //** check if the event is done or not_done **//
                    $query = $this->db->get_where('race_events', array('re_id' => $p_reId));
                    $row = $query->row_array();
                    $re_status = $row['re_status'];

                    if($re_status == 'not_done'){

                        //check who owns the bib ID
                        if($p_mId == $userId_from_post){
                            //if he owns the bib ID, check if the bib number is not assigned to others yet
                            $query_bib_number = $this->db->get_where('bib_number', array( 're_id' => $p_reId, 'rc_id' => $p_rcId, 'bib_assigned_number' => $bibNumber));
                            $row_bib_number = $query_bib_number->num_rows();

                            //already assigned to others
                            if($row_bib_number >= 1){
                                $arr = array(
                                    "status" => 'Error!',
                                    "msg" => 'Error! The bib number is already used by others!'// owner: '.$p_mId.', bibID: '.$bibID. ', p_reId: '.$p_reId.', p_rcId: '.$p_rcId .', count:'.$row_bib_number
                                );
                            }
                            
                            else{
                                //** update bib number **//
                                $dataBib = array(
                                    'bib_assigned_number' => $bibNumber
                                );
                                
                                $this->db->where('bib_id', $bibID);
                                $this->db->update('bib_number', $dataBib);

                                $arr = array(
                                    "status" => 'Success!',
                                    "msg" => 'Bib number updated!'// owner: '.$p_mId.', bibID: '.$bibID. ', p_reId: '.$p_reId.', p_rcId: '.$p_rcId .', count:'.$row_bib_number
                                );
                            }

                        }
                        else if ($p_mId == ''){ //if no one owns it

                            $arr = array(
                                "status" => 'Error!',
                                "msg" => 'Error! No one is using this BIB ID.'
                            );
                        }
                        else  { //if he's not owners, check who owns it

                            $arr = array(
                                "status" => 'Error!',
                                "msg" => 'Error! You do not own this bib ID. Owner: '.$p_mId
                            );
                        }
                    }
                    else  { //if he's not owners, check who owns it

                        $arr = array(
                            "status" => 'EventDone!',
                            "msg" => 'This event is done. You can\'t add or modify any participants.'
                        );
                    }
                    
                    
                    $result = $arr;
                    echo json_encode($result);
                

                }
            }else{
                $arr = array(
                    "status" => 'Error!',
                    "msg" => 'You have no power to do this! Go run!'
                );
                $result = $arr;
                echo json_encode($result);
                
            }
        }
        //assign_bib_number via members page

        //update
        if($do_action == "update"){
            if( isset($_SESSION['isUserLoggedIn']) ) {
                
                $get_user_from_field = $this->input->post('userId');

                // if( isset($_SESSION['isAdmin'])) {
                if( isset($_SESSION['isAdmin']) || isset($_SESSION['isAuthor']) ) {
                    if( $_SESSION['sess_user_id'] == $get_user_from_field) {
                        /** Account Details */
                        $this->form_validation->set_rules('user_role', 'User Role under "Member Info Tab"', 'required|integer');
                        $this->form_validation->set_message('integer', 'The %s is invalid.');
                        
                        $this->form_validation->set_rules('user_status', 'User Role under "Member Info Tab"', 'required|integer');
                        $this->form_validation->set_message('integer', 'The %s is invalid.');
                    }
                }

                /** Personal Details */
                $this->form_validation->set_rules('lastName', 'Last Name under "Member Info Tab"', 'required');
                $this->form_validation->set_rules('firstName', 'First Name under "Member Info Tab"', 'required');
                $this->form_validation->set_rules('middleName', 'Middle Name', '');
                $this->form_validation->set_rules('gender', 'Gender', '');
                $this->form_validation->set_rules('club_group', 'Clubs / Groups', '');
                $this->form_validation->set_rules('address', 'Address under "Member Info Tab"', 'required');
                $this->form_validation->set_rules('mobile_phone', 'Mobile under "Member Info Tab"', 'required');
                $this->form_validation->set_rules('age_group', 'Age Group under "Member Info Tab"', 'required');

                /** Emergency Contact Details */
                $this->form_validation->set_rules('ecd_last_name', 'Last Name for Emerg. Contact under "Member Info Tab"', 'required');
                
                $this->form_validation->set_rules('ecd_first_name', 'First Name for Emerg. Contact under "Member Info Tab"', 'required');
                // $this->form_validation->set_message('required', 'First name for Emergency Contact Info is needed.');
                
                $this->form_validation->set_rules('ecd_middle_name', 'Middle Name for Emerg. Contact under "Member Info Tab"', '');
                $this->form_validation->set_rules('ecd_mobile', 'Mobile / Phone for Emerg. Contact under "Member Info Tab"', 'required');
                // $this->form_validation->set_message('required', 'Mobile / Phone for Emergency Contact Info is needed.');
                
                $this->form_validation->set_rules('ecd_email', 'Email for Emerg. Contact under "Member Info Tab"', 'valid_email');
                $this->form_validation->set_rules('ecd_address', 'Address for Emerg. Contact under "Member Info Tab"', '');
                $this->form_validation->set_rules('ecd_relationship', 'Relationship for Emerg. Contact under "Member Info Tab"', '');

                if ($this->form_validation->run() == FALSE)
                {
                    // if( isset($_SESSION['isAdmin']) ) {
                    if( isset($_SESSION['isAdmin']) || isset($_SESSION['isAuthor']) ) {
                        if( $_SESSION['sess_user_id'] == $get_user_from_field) {
                            $formUserRole = form_error('user_role');
                            $formUserStatus = form_error('user_status');
                        }
                    }else{
                        $formUserRole = '';
                        $formUserStatus = '';
                    }
                    $arr = array(
                        'status' => 'Error!',
                        'user_role' => $formUserRole,
                        'user_status' => $formUserStatus,
                        
                        'firstName' => form_error('firstName'),
                        'lastName' => form_error('lastName'),
                        'address' => form_error('address'),
                        'mobile_phone' => form_error('mobile_phone'),
                        'email' => form_error('email'),
                        'age_group' => form_error('age_group'),

                        'ecd_last_name' => form_error('ecd_last_name'),
                        'ecd_first_name' => form_error('ecd_first_name'),
                        'ecd_mobile' => form_error('ecd_mobile'),
                        'ecd_email' => form_error('ecd_email'),
                    );

                    $result = $arr;
                    echo json_encode($result);
                }
                
                else{

                    //----- check first if the username is not blank
                    $query = $this->db->query('SELECT m_username FROM members WHERE m_id = "'.$get_user_from_field.'" ');
                    $row = $query->row();//get m_username
                    $checkUserName = $row->m_username;

                    //----- create random username
                    $getUsername = strtolower($this->input->post('firstName'). '.' .$this->input->post('lastName'));
                    $generatedUsername = str_replace(" ", "", $getUsername);
                    $randomUsernmaneSuffix = $this->randomUsernmaneSuffix();

                    
                    //TO know who's user will be edited, check if the username is same with the session. If not get the user's ID in the field
                    if ( $_SESSION['sess_user_id'] == $get_user_from_field){
                        $editingUserID = $_SESSION['sess_user_id'];
                    }                                              
                    else{
                        $editingUserID = $get_user_from_field;
                    }                                    

                    //** update members data **//
                    // if( isset($_SESSION['isAdmin']) ) { 
                    if( isset($_SESSION['isAdmin']) || isset($_SESSION['isAuthor']) ) {
                        //----- then if username is blank, create a random one
                        if($checkUserName == ''){
                            $data = array(
                                'm_username' => $generatedUsername.".".$randomUsernmaneSuffix,
                                'm_role' => $this->input->post('user_role'),
                                'm_status' => $this->input->post('user_status'),
                                'm_first_name' => $this->input->post('firstName'),
                                'm_last_name' => $this->input->post('lastName'),
                                'm_middle_name' => $this->input->post('middleName'),
                                'm_gender' => $this->input->post('gender'),
                                'm_address' => $this->input->post('address'),
                                'm_mobile' => $this->input->post('mobile_phone'),
                                'm_age_group' => $this->input->post('age_group'),
                                'm_clubs_groups' => $this->input->post('club_group')
                            );

                        //if username is not blank, just update the table
                        }else{
                            $data = array(
                                'm_role' => $this->input->post('user_role'),
                                'm_status' => $this->input->post('user_status'),
                                'm_first_name' => $this->input->post('firstName'),
                                'm_last_name' => $this->input->post('lastName'),
                                'm_middle_name' => $this->input->post('middleName'),
                                'm_gender' => $this->input->post('gender'),
                                'm_address' => $this->input->post('address'),
                                'm_mobile' => $this->input->post('mobile_phone'),
                                'm_age_group' => $this->input->post('age_group'),
                                'm_clubs_groups' => $this->input->post('club_group')
                            );
                        }
                    }
                    //normal user update
                    else{ 
                        
                        //----- then if username is blank, create a random one
                        if($checkUserName == ''){
                            //remove user_role and user_status
                            $data = array(
                                'm_username' => $generatedUsername.".".$randomUsernmaneSuffix,
                                'm_first_name' => $this->input->post('firstName'),
                                'm_last_name' => $this->input->post('lastName'),
                                'm_middle_name' => $this->input->post('middleName'),
                                'm_gender' => $this->input->post('gender'),
                                'm_address' => $this->input->post('address'),
                                'm_mobile' => $this->input->post('mobile_phone'),
                                'm_age_group' => $this->input->post('age_group'),
                                'm_clubs_groups' => $this->input->post('club_group')
                            );
                        }
                        //if username is not blank, just update the table
                        else{
                            $data = array(
                                'm_first_name' => $this->input->post('firstName'),
                                'm_last_name' => $this->input->post('lastName'),
                                'm_middle_name' => $this->input->post('middleName'),
                                'm_gender' => $this->input->post('gender'),
                                'm_address' => $this->input->post('address'),
                                'm_mobile' => $this->input->post('mobile_phone'),
                                'm_age_group' => $this->input->post('age_group'),
                                'm_clubs_groups' => $this->input->post('club_group')
                            );
                        }
                    }
                    
                    //** update ecd data **//
                    $dataEcd = array(
                        'ecd_relationship' => $this->input->post('ecd_relationship'),
                        'ecd_first_name' => $this->input->post('ecd_first_name'),
                        'ecd_middle_name' => $this->input->post('ecd_middle_name'),
                        'ecd_last_name' => $this->input->post('ecd_last_name'),
                        'ecd_address' => $this->input->post('ecd_address'),
                        'ecd_mobile' => $this->input->post('ecd_mobile'),
                        'ecd_email' => $this->input->post('ecd_email'),
                        
                    );
                    //----ecd
                    $this->db->where('m_id', $editingUserID);
                    $this->db->update('emergency_contact_details', $dataEcd);

                    //----memebers
                    $this->db->where('m_id', $editingUserID);
                    $this->db->update('members', $data);


                    //send email notification to member if $this->input->post('user_status') is activated (=1)
                     if($this->input->post('user_status') == 1){
                        //send email
                     }

                    $arr = array(
                        "status" => 'Success!',
                        "msg" => 'Hooray!'//.$this->input->post('ecd_email')
                    );
                    $result = $arr;
                    echo json_encode($result);

                }
            }
            //if not logged in
            else{
                $arr = array(
                    "status" => 'Error!',
                    "err_message" => 'You have no power to do this! Go run!'
                );
                $result = $arr;
                echo json_encode($result);
                
            }
        }//update

        //resend_validation_link
        if($do_action == "resend_validation_link"){

           
            $getRandomLinkCode = $this->randomString();
            $randomLinkCode = md5($getRandomLinkCode);


            $userEmail = $this->input->post('email') ;

            $this->form_validation->set_rules('email', 'Email', 'required|valid_email');
            $this->form_validation->set_message('valid_email', 'The %s "'.$userEmail .'" is invalid.');                

            if ($this->form_validation->run() == FALSE)
            {
                $arr = array(
                    "status" => 'Invalid!',
                    'msg' => form_error('email'),
                );

                $result = $arr;
                echo json_encode($result);
            }
            else{
                
                //check if the email exits
                $query = $this->db->query('SELECT * FROM members WHERE m_email = "'. $userEmail.'"  ' );
                $checkLoginDetails = $query->num_rows(); //check if exist
                $row = $query->row();//get m_id
                $userId = $row->m_id;

                //success
                if($checkLoginDetails >= 1){

                    //update the existing validation link
                    $dataLink = array(
                        'v_link' => $randomLinkCode,
                    );
                    $this->db->where('m_id', $userId);
                    $this->db->update('validation_link', $dataLink);
        
        
                    //send validation link and password
                    $send_validation_link = base_url('validate/').$randomLinkCode;
        
                    //return status
                    $arr = array(
                        'status' => 'Success!',
                        'msg' => 'Successful! Please check your email for details.',
                        'validation_link' => '<a href="'.$send_validation_link.'">'.$send_validation_link.'</a>'
                    );
                    $result = $arr;
                    echo json_encode($result);
                }
                else{
                    $arr = array(
                        "status" => 'Not registered!',
                        "msg" => 'You\'re not registered! Try creating an account first.',
                    ); 
                    $result = $arr;
                    
                    echo json_encode($result);
                }
            }
            



        }
        //resend_validation_link

        //update password
        if($do_action == "update_password"){

            if( isset($_SESSION['isUserLoggedIn']) ) {
                //get data
                $getOldPassword = md5($this->input->post('oldPassword'));  
                $getNewPassword = $this->input->post('password');
                $getConfPassword = $this->input->post('passconf');

                //validate
                $this->form_validation->set_rules('oldPassword', 'Old Password', 'required');
                
                $this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[8]');
                $this->form_validation->set_message('min_length', '%s must be 8 characters.');
                
                $this->form_validation->set_rules('passconf', 'Password Confirmation', 'trim|required|matches[password]');
                $this->form_validation->set_message('matches', '%s does not match.');

                //check if $getOldPassword is correct
                $query1 = $this->db->query('SELECT m_password FROM members WHERE m_id = "'. $_SESSION['sess_user_id'] .'" ' );
                $row = $query1->row(); 
                $currentPassword = $row->m_password; 

                if($currentPassword == $getOldPassword){

                    //then validate data
                    if ($this->form_validation->run() == FALSE)
                    {
                        $arr = array(
                            "status" => 'Error!',
                            "msg" => 'There\'s a problem!',
                            'oldPassword' => form_error('oldPassword'),
                            'password' => form_error('password'),
                            'passconf' => form_error('passconf'),
                        );

                        $result = $arr;
                        echo json_encode($result);
                    }
                    else {

                        //udpate new password to database
                        $data = array(
                            'm_password' => md5($getNewPassword)
                        );

                        //** insert to members data **//
                        $this->db->where('m_id', $_SESSION['sess_user_id']);
                        $this->db->update('members', $data);

                        //send to email
                        if($this->input->post('send_to_email') != ''){
                            $sendToEmail = $this->input->post('send_to_email');
                        }else{
                            $sendToEmail = $this->input->post('existing_email');
                        }


                        $arr = array(
                            "status" => 'Success!',
                            'msg' => '',
                            // 'msg' => $getOldPassword.' '.$getNewPassword.' '.$getConfPassword,
                        );

                        $result = $arr;
                        echo json_encode($result);
                    }
                }
                //Current password is wrong.
                else {
                    $arr = array(
                        "status" => 'Wrong!',
                        'msg' => 'Current password is wrong.'
                        // 'msg' => 'Current password is wrong. '. $currentPassword.'<br>'.$getOldPassword,
                    );

                    $result = $arr;
                    echo json_encode($result);
                }
            }

            else{
                redirect('');
            }
        }
        //update password

        //reset password 
        if($do_action == "reset_password"){

             
            $randomPassword = $this->randomPassword();

            //-----if the admin / author is requesting the reset from members page
            // if( isset($_SESSION['isAdmin']) ) {
            if( isset($_SESSION['isAdmin']) || isset($_SESSION['isAuthor']) ) {

                $userId = $this->input->post('userId');

                if($this->input->post('send_to_email') != ''){
                    $this->form_validation->set_rules('send_to_email', '"Send To Email"', 'valid_email');
                    $this->form_validation->set_message('valid_email', 'The %s "'.$this->input->post('send_to_email').'" is invalid.');

                }
                    $this->form_validation->set_rules('existing_email', 'Existing Email', 'valid_email');
                    $this->form_validation->set_message('valid_email', 'The %s "'.$this->input->post('send_to_email').'" is invalid.');


                if ($this->form_validation->run() == FALSE)
                {
                    $arr = array(
                        "status" => 'Error!',
                        'send_to_email' => form_error('send_to_email'),
                    );

                    $result = $arr;
                    echo json_encode($result);
                }else{


                    //udpate new password to database
                    $data = array(
                        'm_password' => md5($randomPassword)
                    );

                    //** insert to members data **//
                    $this->db->where('m_id', $userId);
                    $this->db->update('members', $data);

                    //send to email
                    if($this->input->post('send_to_email') != ''){
                        $sendToEmail = $this->input->post('send_to_email');
                    }else{
                        $sendToEmail = $this->input->post('existing_email');
                    }

                    $arr = array(
                        "status" => 'Success!',
                        "msg" => $randomPassword,
                        "id" => $userId,
                    ); 
                    $result = $arr;
                    
                    echo json_encode($result);
                }

            }
            //-----if the normal user is requesting a forgot password from the modal
            else{

                $userEmail = $this->input->post('email') ;

                $this->form_validation->set_rules('email', 'Email', 'required|valid_email');
                $this->form_validation->set_message('valid_email', 'The %s "'.$userEmail .'" is invalid.');                

                if ($this->form_validation->run() == FALSE)
                {
                    $arr = array(
                        "status" => 'Invalid!',
                        'msg' => form_error('email'),
                    );

                    $result = $arr;
                    echo json_encode($result);
                }
                else{
                    
                    //check if the email exists
                    $query = $this->db->query('SELECT * FROM members WHERE m_email = "'. $userEmail.'"  ' );
                    $checkLoginDetails = $query->num_rows(); //check if exist

                    //success
                    if($checkLoginDetails >= 1){

                        //udpate new password to database
                        $data = array(
                            'm_password' => md5($randomPassword)
                        );

                        //** insert to members data **//
                        $this->db->where('m_email', $userEmail);
                        $this->db->update('members', $data);

                        //send to email
                        // $userEmail;

                        $arr = array(
                            "status" => 'Success!',
                            "msg" => 'Please check your email for details. ',$randomPassword,
                        ); 
                        $result = $arr;
                        
                        echo json_encode($result);
                    }
                     else{
                        $arr = array(
                            "status" => 'Not registered!',
                            "msg" => 'You\'re not registered! Try creating an account first.',
                        ); 
                        $result = $arr;
                        
                        echo json_encode($result);
                    }
                    
                    
                }
            }

        }//reset password
        
        //login
        if($do_action == "login"){
            
           
                $this->form_validation->set_rules('email', 'Email', 'required|valid_email');
                $this->form_validation->set_message('valid_email', '%s is invalid.');
                $this->form_validation->set_rules('password', 'password', 'required');



                if ($this->form_validation->run() == TRUE) {

                   

                    $query = $this->db->query('SELECT * FROM members WHERE m_email = "'. $this->input->post('email') .'" AND m_password = "'.md5($this->input->post('password')).'" ' );

                    $checkLoginDetails = $query->num_rows(); //check if exist
                    $row = $query->row();//get data

                    
                    //success
                    if($checkLoginDetails >= 1){
                        
                        //----- Get user DATA of logged in user
                        $userId = $row->m_id;
                        $userEmail = $row->m_email;
                        $userRole = $row->m_role;
                        $userStatus = $row->m_status;

                        //----- if User is not activated, do not login
                        if($userStatus == 1){

                            //------ Put in sessions
                            $this->session->set_userdata('isUserLoggedIn',TRUE); //USAGE:  isset($_SESSION['isUserLoggedIn'])

                            if($userRole == 9){
                                $this->session->set_userdata('isAdmin',TRUE); //USAGE:  isset($_SESSION['isAdmin'])
                            }
                            else if($userRole == 2){
                                $this->session->set_userdata('isMember',TRUE); //USAGE:  isset($_SESSION['isMember'])
                            }
                            else if($userRole == 3){
                                $this->session->set_userdata('isAuthor',TRUE); //USAGE:  isset($_SESSION['isAuthor'])
                            }
                            else{// $userRole == 4 
                                $this->session->set_userdata('isContributor',TRUE); //USAGE:  isset($_SESSION['isContributor'])
                            }
                            

                            $this->session->set_userdata('sess_user_id', $userId);
                            $this->session->set_userdata('sess_user_email', $userEmail);
                            

                            $arr = array(
                                "status" => 'Success!',
                                "msg" => $userRole,
                            ); 
                            $result = $arr;
                            echo json_encode($result);

                        }else{
                            $arr = array(
                                "status" => 'Not Activated!',
                                "msg" => 'Your account is not activated. Please check your email for the validation link. Or request for a new one.',
                                "validation_link_request" => '<a data-open="vlinkModal" class="button small">Request Validation Link</a>',
                            ); 
                            $result = $arr;
                            echo json_encode($result);
                        }

                    //error
                    }
                    
                    else{
                        $arr = array(
                            "status" => 'Error!',
                            "msg" => 'Wrong email or password, please try again.',
                            // "pass" => md5($this->input->post('password'))
                        ); 
                        $result = $arr;
                        echo json_encode($result);
                    }
                }
                
                else{
                    $arr = array(
                        'status' => 'Invalid!',
                        'email_error' => form_error('email'),
                        'password_error' => form_error('password')
                    );
                    $result = $arr;
                    echo json_encode($result);
                }
            
        }
        //login

        //register simple
        if($do_action == "register"){
            $this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[8]');
            $this->form_validation->set_message('min_length', '%s must be 8 characters.');
            
            $this->form_validation->set_rules('passconf', 'Password Confirmation', 'trim|required|matches[password]');
            $this->form_validation->set_message('matches', '%s does not match.');

            $this->form_validation->set_rules('email', 'Email', 'required|valid_email|is_unique[members.m_email]');
            $this->form_validation->set_message('is_unique', '%s already registered.');

            if ($this->form_validation->run() == TRUE) {
                
                $theEmail = $this->input->post('email');
                $thePassword = $this->input->post('password');
                
                $randomPassword = $this->randomPassword();

                $getRandomLinkCode = $this->randomString();
                $randomLinkCode =  md5($getRandomLinkCode);

                //save email to members
                $dataEmail = array(
                    'm_email' => $theEmail,
                    'm_status' => 0,
                    'm_password' => md5($thePassword),
                    'm_role' => 2
                );
                $this->db->insert('members', $dataEmail);
               
                //** get the latest saved m_id  **//
                $lastSaved_m_id = $this->db->insert_id();

                //add emergency contact details
                $dataEcd = array(
                    'm_id' => $lastSaved_m_id
                );
                $this->db->insert('emergency_contact_details', $dataEcd);

                //save validation links
                $dataLink = array(
                    'v_link' => $randomLinkCode,
                    'm_id' => $lastSaved_m_id
                );
                $this->db->insert('validation_link', $dataLink);


                //send validation link and password
                $send_pass = '******** hidden';
                $send_validation_link = base_url('validate/').$randomLinkCode;

                //return status
                $arr = array(
                    'status' => 'Success!',
                    'msg' => 'Successful! Please check your email for your login details!',
                    'validation_link' => '<a href="'.$send_validation_link.'">'.$send_validation_link.'</a>'
                );
                $result = $arr;
                echo json_encode($result);
            }
            else{
                $arr = array(
                    'status' => 'Invalid!',
                    'email_error' => form_error('email'),
                    'password_error' => form_error('password'),
                    'passconf_error' => form_error('passconf'),
                );
                $result = $arr;
                echo json_encode($result);
            }
        }
        //register simple

        //logout
        if($do_action == 'logout'){
            $this->session->unset_userdata('isUserLoggedIn');
            
            $this->session->unset_userdata('isAdmin');
            $this->session->unset_userdata('isMember');
            $this->session->unset_userdata('isAuthor');
            $this->session->unset_userdata('isContributor');

            $this->session->unset_userdata('user_id');
            $this->session->unset_userdata('user_email');
            $this->session->sess_destroy();

            $arr = array(
                "status" => 'Success!',
                "msg" => ''
            ); 
            $result = $arr;
            
            echo json_encode($result);
        }
        //logout

        //addQuotes
        if($do_action == "addQuotes"){
            
            $this->form_validation->set_rules('nickPenName', '"Nick / Pen Name"', 'required');
            $this->form_validation->set_rules('authorUrl', '"Author\'s URL"', 'valid_url');

            $this->form_validation->set_rules('theQuote', '"Quotes"', 'required|min_length[5]|max_length[200]|is_unique[quotables.q_content]');
            $this->form_validation->set_message('min_length', '%s must be minimum of 5 characters.');
            $this->form_validation->set_message('max_length', '%s must be maximum of 200 characters.');
            $this->form_validation->set_message('is_unique', 'This quote is already added by someone. Try another one.');

            // $this->form_validation->set_rules('backgroundUrl', 'Background URL', '');
            
            if ($this->form_validation->run() == TRUE) {
                
                $nickPen = $this->input->post('nickPenName');
                $authorUrl = $this->input->post('authorUrl');
                $theQuote = $this->input->post('theQuote');
                // $backgroundUrl = $this->input->post('backgroundUrl');
                 

                //save quotes to Quotables
                $dataQUotes = array(
                    'q_owner_name' => $nickPen,
                    'q_owner_link' => prep_url($authorUrl),
                    'q_content' => $theQuote,
                    'q_image_src' => '',//$backgroundUrl,
                    'q_status' => 0,
                );
                $this->db->insert('quotables', $dataQUotes);
               
                

                //return status
                $arr = array(
                    'status' => 'Success!',
                    'msg' => 'Thanks for your contribution! Your quotes will be posted in a while.',
                );
                $result = $arr;
                echo json_encode($result);
            }
            else{
                $arr = array(
                    'status' => 'Invalid!',
                    'nick_error' => form_error('nickPenName'),
                    'authorUrl_error' => form_error('authorUrl'),
                    'theQuote_error' => form_error('theQuote'),
                );
                $result = $arr;
                echo json_encode($result);
            }
        }
        //addQuotes
    } //myformAjax

    

   

}