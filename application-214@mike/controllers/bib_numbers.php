<?php
class Bib_numbers extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('bib_numbers_model');
        $this->load->helper('url_helper');
        $this->load->library('form_validation');
    }

   
    /**
     * View All Member List
     */
    
    
    public function index()
    {
        if( isset($_SESSION['isAdmin']) ) {
            $data['bib_numbers'] = $this->bib_numbers_model->get_bib_numbers();
            // $data['ff_details'] = $this->bib_numbers_model->get_ff_details();
            $data['title'] = 'Bib Number List';
            
            $this->load->view('templates/header', $data);
            $this->load->view('bib_numbers/index', $data);
            $this->load->view('templates/footer');
            
        }else{
            redirect('');
        }
    }

    /**
     * View Single Member Page
     */
        
    public function view($m_id = NULL)
    {
        if( isset($_SESSION['isUserLoggedIn']) ) {
            $data['members_item'] = $this->members_model->get_members($m_id);
            
            if (empty($data['members_item']))
            {
                show_404();
            }
            
            // $data['m_id'] = $data['members_item']['m_id'];

            //display all emergency contacts in the table
            $data['ecd_item'] = $this->members_model->get_emergency_contact($m_id);

            //get participated event for Single Runner
            $data['participated_event_single'] = $this->members_model->get_participated_events_single($m_id);

            //get participated event with FFM runners
            $data['participated_event'] = $this->members_model->get_participated_events($m_id);
            $data['participated_event_ffm'] = $this->members_model->get_participated_events_ffm($m_id);
            
            $this->load->view('templates/header', $data);
            $this->load->view('members/view', $data);
            $this->load->view('templates/footer');
        }else{
            redirect('');
        }

    }

    
    

   

}