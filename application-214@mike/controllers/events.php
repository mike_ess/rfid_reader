<?php
class Events extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('events_model');
        $this->load->model('members_model');
        // $this->load->helper('url_helper');
        $this->load->helper(array('form', 'url', 'url_helper'));
        $this->load->library('form_validation');
    }

    //** generate random string for validation **//
    public function randomString() {
        $alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
        $theLinkCode = array(); //remember to declare $theLinkCode as an array
        $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
        for ($i = 0; $i < 20; $i++) {
            $n = rand(0, $alphaLength);
            $theLinkCode[] = $alphabet[$n];
        }
        return implode($theLinkCode); //turn the array into a string
    }
    //  $randomLinkCode = $this->md5(randomString());

    //** generate random password **//
    public function randomPassword() {
        $alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890@#&*_+=%';
        $pass = array(); //remember to declare $pass as an array
        $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
        for ($i = 0; $i < 8; $i++) {
            $n = rand(0, $alphaLength);
            $pass[] = $alphabet[$n];
        }
        return implode($pass); //turn the array into a string
    }
    // $randomPassword = $this->randomPassword();

    //** generate aditional random suffix for username **//
    public function randomUsernmaneSuffix() {
       $alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
       $pass = array(); //remember to declare $pass as an array
       $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
       for ($i = 0; $i < 3; $i++) {
           $n = rand(0, $alphaLength);
           $pass[] = $alphabet[$n];
       }
       return implode($pass); //turn the array into a string
   }
//    $randomUsernmaneSuffix = $this->randomUsernmaneSuffix();
    

    /**
     * View All Event List
     */
    public function index()
    {
        $data['events'] = $this->events_model->get_events();
        $data['title'] = 'Race Events';

        $this->load->view('templates/header', $data);
        $this->load->view('events/index', $data);
        $this->load->view('templates/footer-content');
        $this->load->view('templates/footer');
    }

    /**
     * Edit New Event
     */
    public function edit($re_id = NULL){

        if( isset($_SESSION['isAdmin']) || isset($_SESSION['isAuthor']) || isset($_SESSION['isContributor'])   ) {
            
            $data['singlet_items'] = $this->events_model->get_singlets($re_id);
            $data['category_items'] = $this->events_model->get_categories($re_id);
            $data['events'] = $this->events_model->get_events_to_edit($re_id);

            // if ($re_id != $data['events']['re_id'] )
            if (empty($re_id) )
            {
                show_404();
            }
            
            $data['media_items'] = $this->events_model->get_media();
            $data['src_image'] = $this->events_model->get_media($re_id);
            
            $data['title'] = 'Edit Event';
            $this->load->view('templates/header', $data);
            $this->load->view('events/edit', $data);
            $this->load->view('templates/footer-content');
            $this->load->view('templates/footer');
        }else{
            redirect('');
        }
    }

    /**
     * Add New Event
     */
    public function add($re_slug = NULL){

        if( isset($_SESSION['isAdmin']) || isset($_SESSION['isAuthor']) || isset($_SESSION['isContributor'])   ) {
            
            $data['media_items'] = $this->events_model->get_media();
            
            $data['title'] = 'Add New Event';
            $this->load->view('templates/header', $data);
            $this->load->view('events/add', $data);
            $this->load->view('templates/footer-content');
            $this->load->view('templates/footer');
        }else{
            redirect('');
        }
    }
    
    /**
     * Send Email
     */
     
     public function send_email(){
         $emailMsg =  'email sent';
         return $emailMsg;
     }

    /**
     * View Single Events Page
     */
    public function view($re_slug = NULL)
    {
        $data['events_item'] = $this->events_model->get_events($re_slug);
        $data['event_categories'] = $this->events_model->get_race_categories($re_slug);

        if (empty($data['events_item']))
        {
                show_404();
        }

        //$data['re_slug'] = $data['events_item']['re_slug'];

        $this->load->view('templates/header', $data);
        $this->load->view('events/view', $data);
        $this->load->view('templates/footer-content');
        $this->load->view('templates/footer');

    }

    public function register($re_slug = NULL){
        //display categories from get_race_categories function via re_slug
        $data['categories'] = $this->events_model->get_race_categories($re_slug);
        
        //display singelt sizes from get_singlet_sizes funciton via re_slug
        $data['sizes'] = $this->events_model->get_singlet_sizes($re_slug);
        
        //display pickup locations
        $data['pickup_location'] = $this->events_model->select('pickup_location'); 
        $data['bank_payment_option'] = $this->events_model->select('bank_payment_option'); 
        $data['money_transfer_option'] = $this->events_model->select('money_transfer_option'); 

        //display fees
        $data['web_fee'] = $this->events_model->get_fees('web_fee');
        $data['delivery_fee_fixed'] = $this->events_model->get_fees('delivery_fee_fixed');
        $data['delivery_fee_manila'] = $this->events_model->get_fees('delivery_fee_manila');
        $data['delivery_fee_outside_manila'] = $this->events_model->get_fees('delivery_fee_outside_manila');

        $data['events_item'] = $this->events_model->get_events($re_slug); 

        $this->load->view('templates/header', $data);

        if( $data['events_item']['re_status'] == 'not_done' ){
            $this->load->view('events/register', $data);
        }else{
            redirect('events/'.$re_slug);
        }

        
        $this->load->view('templates/footer-content');
        $this->load->view('templates/footer');
    }

    
    
    /**
     * Ajax Registration from events page
     */
     public function registerFromEventsPage($do = NULL)
     {
        if($do == 'reg'){

            //validations
            /** Personal Details */
            $this->form_validation->set_rules('chosen_runners_email', 'main account must be selected under "Participant info tab".', 'required|valid_email');
            // $this->form_validation->set_message('is_unique', 'The %s "'.$this->input->post('chosen_runners_email').'" is already registered.');

            $this->form_validation->set_rules('mma_[]', 'main account must be selected under "Participant info tab"', 'required');
            $this->form_validation->set_rules('choose_singlet_size_[]', 'Singlet Sizes of each runner under "Participant info tab"', 'required');
            $this->form_validation->set_rules('choose_race_category_[]', 'Race Category of each runner under "Participant info tab"', 'required');
            $this->form_validation->set_rules('firstName_[]', 'First Names of each runner under "Participant info tab"', 'required');
            $this->form_validation->set_rules('lastName_[]', 'Last Names of each runner under "Participant info tab"', 'required');
            $this->form_validation->set_rules('gender_[]', 'Genders of each runner under "Participant info tab"', 'required');
            $this->form_validation->set_rules('address_[]', 'Addresses of each runner under "Participant info tab"', 'required');
            $this->form_validation->set_rules('email_[]', 'Emails of each runner under "Participant info tab"', 'required', 'required|valid_email');
            $this->form_validation->set_rules('age_group_[]', 'Age group of each runner under "Participant info tab"', 'required', 'required|valid_email');
            $this->form_validation->set_rules('ecd_cr_first_name', 'Name of Emergency Contact Details under "Participant info tab"', 'required', 'required');
            $this->form_validation->set_rules('ecd_cr_mobile', 'Phone of Emergency Contact Details under "Participant info tab"', 'required', 'required');

             /** Payment Method */
             $this->form_validation->set_rules('payment_method', 'Payment Method under "Review Info Tab"', 'required');
            
             /** Claiming Details */
             $this->form_validation->set_rules('raceKitDeliverPickup', 'Preferred Pickup under "Claiming Details Tab"', 'required');
             $this->form_validation->set_rules('terms_condition_checkbox', 'Terms and Condition under "Claiming Details Tab"', 'required');
             
             /** Check if Claiming Details is Delivery or Pickup */
             $raceKitDeliverPickup = $this->input->post('raceKitDeliverPickup');
             if(  $raceKitDeliverPickup == "delivered" ){
                $this->form_validation->set_rules('delivery_address', 'Delivery address under "Claiming Details Tab"', 'required');
            }

            //** Validate if delivery has content for fees *//
            if(  $raceKitDeliverPickup == "delivered" ){
                $inputFor_deliverAddress = $this->input->post('delivery_address');
                
                $dff_query = $this->db->select('f_cost')
                    ->where('f_slug', 'delivery_fee_fixed')
                    ->get('fees'); 

                $cost_dff = $dff_query->row();
                $delivery_fee_fixed = $cost_dff->f_cost;
            }
            else{
                $inputFor_deliverAddress = 'Pickup at the event area.';
                $delivery_fee_fixed = 0;
            }
                
            /** Check event details */
            $this->form_validation->set_rules('choose_singlet_size', 'Singlets under "Participant Info. Tab"', 'required');
            $this->form_validation->set_rules('choose_race_category', 'Race Category under "Participant Info. Tab"', 'required');

            //get event details
            $race_event_id =  $this->input->post('race_event_id');
            $race_category_id =  $this->input->post('choose_race_category');
            $singlet_size_content =  $this->input->post('choose_singlet_size');

            //get all details specifically for the $make_main_account
            $make_main_account = $this->input->post('chosen_runners_email');
            $chosen_runners_email = $make_main_account;
            $chosen_runners_first_name = $this->input->post('chosen_runners_first_name');
            $chosen_runners_last_name = $this->input->post('chosen_runners_last_name');
            $chosen_runners_middle_name = $this->input->post('chosen_runners_middle_name');
            $chosen_runners_gender = $this->input->post('chosen_runners_gender');
            $chosen_runners_address = $this->input->post('chosen_runners_address');
            $chosen_runners_mobile = $this->input->post('chosen_runners_mobile');
            $chosen_runners_ageGroup = $this->input->post('chosen_runners_ageGroup');
            $chosen_runners_clubsGroups = $this->input->post('chosen_runners_clubsGroups');

            $ecd_cr_first_name = $this->input->post('ecd_cr_first_name');
            $ecd_cr_last_name = $this->input->post('ecd_cr_last_name');
            $ecd_cr_middle_name = $this->input->post('ecd_cr_middle_name');
            $ecd_cr_relationship = $this->input->post('ecd_cr_relationship');
            $ecd_cr_address = $this->input->post('ecd_cr_address');
            $ecd_cr_mobile = $this->input->post('ecd_cr_mobile');
            $ecd_cr_email = $this->input->post('ecd_cr_email');

            $runnersCount = $this->input->post('theRunnersCount');
            $additionalRunners = $this->input->post('additionalRunners');

            $rc_fee_total = $this->input->post('raceCategoryTotal');
            $web_fee_total = $this->input->post('webFeeTotal');

            if ($this->form_validation->run() == FALSE)
            {
                
                if(  $raceKitDeliverPickup == "delivered" ){
                    $formError_deliverAddress = form_error('delivery_address');
                }
                else{
                    $formError_deliverAddress = '';
                }
                
                $arr = array(
                    'status'=> 'Error!',

                    //chosen main account
                    'choose_race_category' => form_error('choose_race_category'),
                    'choose_singlet_size' => form_error('choose_singlet_size'),
                    'chosen_runners_email' => form_error('chosen_runners_email'),
                    'chosen_runners_first_name' => form_error('chosen_runners_first_name'),
                    'chosen_runners_last_name' => form_error('chosen_runners_last_name'),
                    'chosen_runners_middle_name' => form_error('chosen_runners_middle_name'),
                    'chosen_runners_gender' => form_error('chosen_runners_gender'),
                    'chosen_runners_address' => form_error('chosen_runners_address'),
                    'chosen_runners_mobile' => form_error('chosen_runners_mobile'),
                    'chosen_runners_ageGroup' => form_error('chosen_runners_ageGroup'),
                    'chosen_runners_clubsGroups' => form_error('chosen_runners_clubsGroups'),
                    
                    'ecd_cr_first_name' => form_error('ecd_cr_first_name'),
                    // 'ecd_cr_last_name' => form_error('ecd_cr_last_name'),
                    // 'ecd_cr_middle_name' => form_error('ecd_cr_middle_name'),
                    // 'ecd_cr_relationship' => form_error('ecd_cr_relationship'),
                    // 'ecd_cr_address' => form_error('ecd_cr_address'),
                    'ecd_cr_mobile' => form_error('ecd_cr_mobile'),
                    // 'ecd_cr_email' => form_error('ecd_cr_email'),


                    'delivery_address' => $formError_deliverAddress,
                    'payment_method' => form_error('payment_method'),
                    
                    'raceKitDeliverPickup' => form_error('raceKitDeliverPickup'),
                    'terms_condition_checkbox' => form_error('terms_condition_checkbox'),

                    //runner's info
                    // 'mma' => form_error('mma_[]'),
                    'choose_singlet_size_' => form_error('choose_singlet_size_[]'),
                    'choose_race_category_' => form_error('choose_race_category_[]'),
                    'firstNames' => form_error('firstName_[]'),
                    'lastNames' => form_error('lastName_[]'),
                    'emails' => form_error('email_[]'),
                    'genders' => form_error('gender_[]'),
                    'addresses' => form_error('address_[]'),
                    'ageGroups' => form_error('age_group_[]'),     
                    
                    // 'ecd_firstName' => form_error('ecd_first_name_[]'),
                    // 'ecd_lastName' => form_error('ecd_last_name_[]'),
                    // 'ecd_middleName' => form_error('ecd_middle_name_[]'),
                    // 'ecd_relationship' => form_error('ecd_relationship_[]'),
                    // 'ecd_address' => form_error('ecd_address_[]'),
                    // 'ecd_mobile' => form_error('ecd_mobile_[]'),
                    // 'ecd_email' => form_error('ecd_email_[]'),
                );
                $result = $arr;
                echo json_encode($result);
                
            }else{

                //** check if the event is done or not_done **//
                $query_re = $this->db->get_where('race_events', array('re_id' => $race_event_id));
                $row_re = $query_re->row_array();
                $re_status = $row_re['re_status'];

                if($re_status == 'not_done'){
                        
                //check the email if already registered
                    $query = $this->db->query('SELECT * FROM members WHERE m_email = "'. $make_main_account .'" ' );
                    $checkLoginDetails = $query->num_rows(); //check if exist
                    $rowUser = $query->row();//get data
                                
                    if($checkLoginDetails == 1){//already registered
 
                        //check the email if same with the logged in user
                        if( isset($_SESSION['sess_user_email']) == $make_main_account ) {

                            
                            //** add to participant **//
                            //** Get the fees **//
                            //** Get Race Category fee *//
                            $query = $this->db->select('rc_fee')
                            ->where('rc_id', $race_category_id)
                            ->get('race_categories'); 
                            $get_rc_fee = $query->row();
                            $rc_fee = $get_rc_fee->rc_fee;

                            //** Get WebFee cost *//
                            $wf_query = $this->db->select('f_cost')
                            ->where('f_slug', 'web_fee')
                            ->get('fees'); 

                            $cost_wf = $wf_query->row();
                            $web_fee = $cost_wf->f_cost;

                            if($runnersCount == 1 && $additionalRunners < 1){

                                //-----check if already a partcipant
                                $queryParticipants = $this->db->query('SELECT * FROM participants WHERE m_id = "'. $_SESSION['sess_user_id'] .'"  AND re_id = "'.$race_event_id.'" ' );
                                $checkParticipants = $queryParticipants->num_rows(); //check if exist

                                if ($checkParticipants < 1 ){
                                
                                        //save to bib numbers table
                                    $dataBibNumbers = array(
                                        'bib_assigned_number' => 0,
                                        'm_id' => $_SESSION['sess_user_id'],
                                        're_id' => $race_event_id,
                                        'rc_id' => $race_category_id,
                                    );
                                    $this->db->insert('bib_number', $dataBibNumbers);
                                    $lastSaved_BibId = $this->db->insert_id();


                                    $dataParticipants = array(
                                        'm_id' => $_SESSION['sess_user_id'],
                                        'bib_id' => $lastSaved_BibId,
                                        'rc_id' => $race_category_id,
                                        're_id' => $race_event_id,
                                        'ss_id' => $singlet_size_content,//$this->input->post('choose_singlet_size'),
                                        'p_payment_status' => 0,
                                        'p_payment_method' => $this->input->post('payment_method'),
                                        'p_payment_amount' => $rc_fee + $delivery_fee_fixed + $web_fee, 
                                        'p_payment_total' => $rc_fee + $delivery_fee_fixed + $web_fee, 
                                        'p_delivery_address' => $inputFor_deliverAddress
                                        
                                    );
                                    $this->db->insert('participants', $dataParticipants);     
                                    
                                    $arr = array(
                                        'status' => 'Updated!',
                                        'msg' => 'Your profile was added to participants.<br>We will send an email with all the details for this event.'
                                    );
                                }
                                else
                                {
                                    $arr = array(
                                        'status' => 'AlreadyParticipant!',
                                        'msg' => 'You\'re already a participant for this event. 404'
                                    );
                                }
                            }

                            //if more than 1 main runnner and more than 1 added runners, save each runner
                            if($runnersCount > 1 || $additionalRunners >= 1){

                                //-----get ecd data
                                $query = $this->db->query('SELECT ecd_id FROM emergency_contact_details WHERE m_id = "'. $_SESSION['sess_user_id'] .'" ' );
                                $rowEcd = $query->row();//get data

                                //-----check if already a partcipant
                                $queryParticipants = $this->db->query('SELECT * FROM participants WHERE m_id = "'. $_SESSION['sess_user_id'] .'"  AND re_id = "'.$race_event_id.'" ' );
                                $checkParticipants = $queryParticipants->num_rows(); //check if exist

                                //-----check if a family member exist using re_id
                                $query_ffm = $this->db->query('SELECT * FROM family_friend_member WHERE m_id = "'. $_SESSION['sess_user_id'] .'"  AND re_id = "'.$race_event_id.'" ' );
                                $check_ffm = $query_ffm->num_rows(); //check if exist

                                if ($checkParticipants < 1 && $check_ffm < 1){

                                    //save each runners to family_friend_member table
                                    foreach ($this->input->post('firstName_') as $key => $firstName) {

                                        $getUsername = strtolower($this->input->post('firstName_')[$key]. '.' .$this->input->post('lastName_')[$key]);
                                        $generatedUsername = str_replace(" ", "", $getUsername);

                                        //get each data

                                        $mma = $this->input->post('mma_')[$key];
                                        

                                        $captured_race_categories = $this->input->post('choose_race_category_')[$key];
                                        $captured_singlets = $this->input->post('choose_singlet_size_')[$key];
                                        $captured_emails = $this->input->post('email_')[$key];

                                        $captured_lastNames = $this->input->post('firstName_')[$key];
                                        $captured_lastNames = $this->input->post('lastName_')[$key];
                                        $captured_middleNames = $this->input->post('middleName_')[$key];
                                        $captured_genders = $this->input->post('gender_')[$key];
                                        $captured_addresses = $this->input->post('address_')[$key];
                                        $captured_mobile_phones = $this->input->post('mobile_phone_')[$key];
                                        $captured_age_groups = $this->input->post('age_group_')[$key];
                                        $captured_club_groups = $this->input->post('club_group_')[$key];
                                        
                                        
                                        
                                        //save to bib numbers table
                                        $dataBibNumbers = array(
                                            'bib_assigned_number' => 0,
                                            'm_id' => $_SESSION['sess_user_id'],
                                            're_id' => $race_event_id,
                                            'rc_id' => $captured_race_categories,
                                        );
                                        $this->db->insert('bib_number', $dataBibNumbers);
                                        $lastSaved_BibId = $this->db->insert_id();
                                        
                                        //save to family_friend_member table
                                        $data_ffm = array(
                                            
                                            'm_id' => $_SESSION['sess_user_id'],
                                            'is_main' => $mma,
                                            'ecd_id' => $rowEcd->ecd_id,
                                            'bib_id' => $lastSaved_BibId,
                                            're_id' => $race_event_id,
                                            'ffm_first_name' => $firstName,
                                            'ffm_last_name' => $captured_lastNames,
                                            'ffm_middle_name' => $captured_middleNames,//$this->input->post('middleName'),
                                            'ffm_gender' => $captured_genders,//$this->input->post('gender'),
                                            'ffm_address' => $captured_addresses,//$this->input->post('address'),
                                            'ffm_mobile' => $captured_mobile_phones,//$this->input->post('mobile_phone'),
                                            'ffm_email' => $captured_emails,//$this->input->post('email'),
                                            'ffm_age_group' => $captured_age_groups,//$this->input->post('age_group'),
                                            'ffm_clubs_groups' => $captured_club_groups,//$this->input->post('club_group')
                                        );

                                        $this->db->insert('family_friend_member', $data_ffm); //save to ffm_ 
                                        $lastSaved_ffm_Id = $this->db->insert_id();

                                        //** add to participant **//
                                        //** Get the fees
                                        //** Get Race Category fee *//
                                        $query = $this->db->select('rc_fee')
                                        ->where('rc_id', $captured_race_categories)
                                        ->get('race_categories'); 
                                        $get_rc_fee = $query->row();
                                        $rc_fee_each = $get_rc_fee->rc_fee;

                                        //** Get WebFee cost *//
                                        $wf_query = $this->db->select('f_cost')
                                        ->where('f_slug', 'web_fee')
                                        ->get('fees'); 

                                        $cost_wf = $wf_query->row();
                                        $web_fee_each = $cost_wf->f_cost;
                                        
                                        $dataParticipants = array(
                                            'm_id' => $_SESSION['sess_user_id'],
                                            'ffm_id' => $lastSaved_ffm_Id, //use ffm_id 
                                            'bib_id' => $lastSaved_BibId,
                                            'rc_id' => $captured_race_categories,
                                            're_id' => $race_event_id,
                                            'ss_id' => $captured_singlets,//$this->input->post('choose_singlet_size'),
                                            'p_payment_status' => 0,
                                            'p_payment_method' => $this->input->post('payment_method'),
                                            'p_payment_amount' => $rc_fee_each + $web_fee_each, 
                                            //'p_delivery_address' => $inputFor_deliverAddress
                                            
                                        );
                                        $this->db->insert('participants', $dataParticipants);
                                        
                                        
                                    }//end foreach
                        
                            
                                    //get the bib id of the MAIN ACCOUNT in family_friend_member table
                                    $query = $this->db->query('SELECT bib_id FROM family_friend_member WHERE m_id = "'. $_SESSION['sess_user_id'].'" AND re_id ="'.$race_event_id.'" AND is_main = "1" ' );
                                    $row = $query->row();//get data
                                    $ffm_bib_id = $row->bib_id;
                                    // $ffm_ecd_id = $row->ecd_id;
                                    
                                

                                    $dataParticipant_mainMember = array(
                                        'p_payment_total' => $rc_fee_total  + $delivery_fee_fixed + $web_fee_total,
                                        'p_payment_amount' => $rc_fee  + $delivery_fee_fixed + $web_fee,
                                        'p_delivery_address' => $inputFor_deliverAddress
                                        
                                    );
                                    $this->db->where('bib_id',  $ffm_bib_id );
                                    $this->db->update('participants', $dataParticipant_mainMember);

                                    //send email
                                    $this->send_email();

                                    $arr = array(
                                        'status' => 'Updated!',
                                        'msg' => '549 Your profile was added to participants together with your added runner(s).<br>We will send an email with all the details for this event.'
                                    );
                                }
                                else
                                {
                                    $arr = array(
                                        'status' => 'AlreadyParticipant!',
                                        'msg' => '546 You\'re already a participant for this event.'
                                    );
                                }
                                
                            }
                            
                             
                            
                        }
                        //if he's not the owner
                        else{
                            
                            //$this->send_email();

                            //throw error tell the user to login
                            $arr = array(
                                'status' => 'Confused!',
                                'msg' => $emailMsg. '568 We already saved your information. Please check your email for more details.'
                            );
                        }
                        
                    }
                    //if not registered to someone else, create new account
                    else{
                            
                        $randomUsernmaneSuffix = $this->randomUsernmaneSuffix();
                        $randomPassword = $this->randomPassword();
                
                        //----- add the main account to members
                        $getUsername = strtolower( $chosen_runners_first_name. '.' .$chosen_runners_last_name );
                        $generatedUsername = str_replace(" ", "", $getUsername);

                        //save to MEMBERS table
                        $data = array(
                            'm_role' => 2,//member role
                            'm_status' => 0,//member status needs activation. Send email link for activation
                            'm_username' => $generatedUsername.".".$randomUsernmaneSuffix,
                            'm_password' => md5($randomPassword),
                            'm_first_name' => $chosen_runners_first_name,
                            'm_last_name' => $chosen_runners_last_name,
                            'm_middle_name' => $chosen_runners_middle_name,
                            'm_gender' => $chosen_runners_gender,
                            'm_address' => $chosen_runners_address,
                            'm_mobile' => $chosen_runners_mobile,
                            'm_email' => $make_main_account,
                            'm_age_group' => $chosen_runners_ageGroup,
                            'm_clubs_groups' => $chosen_runners_clubsGroups,
                        );
                        $this->db->insert('members', $data);
                        $lastMemberSaved_ID = $this->db->insert_id();

                        
                        //save validation links
                        $getRandomLinkCode = $this->randomString();
                        $randomLinkCode =  md5($getRandomLinkCode);
                        $dataLink = array(
                            'v_link' => $randomLinkCode,
                            'm_id' => $lastMemberSaved_ID
                        );
                        $this->db->insert('validation_link', $dataLink);                    
                        
                        //** Get Race Category fee *//
                        $query = $this->db->select('rc_fee')
                        ->where('rc_id', $race_category_id)
                        ->get('race_categories'); 
                        $get_rc_fee = $query->row();
                        $rc_fee = $get_rc_fee->rc_fee;

                        //** Get WebFee cost *//
                        $wf_query = $this->db->select('f_cost')
                        ->where('f_slug', 'web_fee')
                        ->get('fees'); 

                        $cost_wf = $wf_query->row();
                        $web_fee = $cost_wf->f_cost;
                        
                        //save to ECD table
                        $dataEcd_cr = array(
                            'ecd_first_name' => $ecd_cr_first_name,
                            'ecd_last_name' => $ecd_cr_last_name,
                            'ecd_middle_name' => $ecd_cr_middle_name,
                            'ecd_relationship' => $ecd_cr_relationship,
                            'ecd_address' => $ecd_cr_address,
                            'ecd_mobile' => $ecd_cr_mobile,
                            'ecd_email' => $ecd_cr_email,
                            'm_id' => $lastMemberSaved_ID,
                        );
                        $this->db->insert('emergency_contact_details', $dataEcd_cr);
                        $lastSaved_ecd_Id = $this->db->insert_id();

                        //send email validation link and password
                        $send_pass = '******** hidden';
                        $send_validation_link = base_url('validate/').$randomLinkCode;

                        //IF ONLY 1 RUNNER
                        if($runnersCount == 1){


                            //save to bib numbers table
                            $dataBibNumbers = array(
                                'bib_assigned_number' => 0,
                                'm_id' => $lastMemberSaved_ID,
                                're_id' => $race_event_id,
                                'rc_id' => $race_category_id,
                            );
                            $this->db->insert('bib_number', $dataBibNumbers);
                            $lastSaved_BibId = $this->db->insert_id();
                            
                            //** save to participants table
                            $dataParticipants = array(
                                'm_id' => $lastMemberSaved_ID,
                                'ffm_id' => 0,
                                'bib_id' => $lastSaved_BibId,
                                'rc_id' => $race_category_id,
                                're_id' => $race_event_id,
                                'ss_id' => $singlet_size_content,
                                'p_payment_status' => 0,
                                'p_payment_method' => $this->input->post('payment_method'),
                                'p_payment_amount' => $rc_fee + $delivery_fee_fixed + $web_fee,
                                'p_delivery_address' => $inputFor_deliverAddress
                                
                            );
                            $this->db->insert('participants', $dataParticipants);
                            
                            //get the bib id of the MAIN ACCOUNT
                            // $query = $this->db->query('SELECT bib_id FROM family_friend_member WHERE m_id = "'. $lastMemberSaved_ID.'" AND is_main = "1" ' );
                            // $row = $query->row();//get data
                            // $ffm_bib_id = $row->bib_id;
                            // $ffm_ecd_id = $row->ecd_id;
                            
                            //udpate the fee for the main account. Add the delivery fee
                            $dataParticipant_mainMember = array(
                                'p_payment_total' => $rc_fee_total  + $delivery_fee_fixed + $web_fee_total,
                                'p_payment_amount' => $rc_fee  + $delivery_fee_fixed + $web_fee,
                                'p_delivery_address' => $inputFor_deliverAddress
                                
                            );
                            $this->db->where('bib_id',  $lastSaved_BibId );
                            $this->db->update('participants', $dataParticipant_mainMember);

                            //show success message
                            $arr = array(
                                'status'=> 'Success!',
                                'msg' => '687 1 user not registered added.',
                                
                            );
                        }
                        

                        //if 2 more runnners, save each runner
                        if($runnersCount > 1){

                            //save each runners to family_friend_member table
                            foreach ($this->input->post('firstName_') as $key => $firstName) {

                                $getUsername = strtolower($this->input->post('firstName_')[$key]. '.' .$this->input->post('lastName_')[$key]);
                                $generatedUsername = str_replace(" ", "", $getUsername);

                                //get each data
                                $mma = $this->input->post('mma_')[$key];
                                $captured_race_categories = $this->input->post('choose_race_category_')[$key];
                                $captured_singlets = $this->input->post('choose_singlet_size_')[$key];
                                $captured_emails = $this->input->post('email_')[$key];

                                $captured_lastNames = $this->input->post('firstName_')[$key];
                                $captured_lastNames = $this->input->post('lastName_')[$key];
                                $captured_middleNames = $this->input->post('middleName_')[$key];
                                $captured_genders = $this->input->post('gender_')[$key];
                                $captured_addresses = $this->input->post('address_')[$key];
                                $captured_mobile_phones = $this->input->post('mobile_phone_')[$key];
                                $captured_age_groups = $this->input->post('age_group_')[$key];
                                $captured_club_groups = $this->input->post('club_group_')[$key];
                                
                                //save to bib numbers table
                                $dataBibNumbers = array(
                                    'bib_assigned_number' => 0,
                                    'm_id' => $lastMemberSaved_ID,
                                    're_id' => $race_event_id,
                                    'rc_id' => $captured_race_categories,
                                );
                                $this->db->insert('bib_number', $dataBibNumbers);
                                $lastSaved_BibId = $this->db->insert_id();

                                //save to family_friend_member table
                                $data_ffm = array(
                                    
                                    'm_id' => $lastMemberSaved_ID,
                                    'is_main' => $mma,
                                    'ecd_id' => $lastSaved_ecd_Id,
                                    'bib_id' => $lastSaved_BibId,
                                    're_id' => $race_event_id,
                                    'ffm_first_name' => $firstName,
                                    'ffm_last_name' => $captured_lastNames,
                                    'ffm_middle_name' => $captured_middleNames,//$this->input->post('middleName'),
                                    'ffm_gender' => $captured_genders,//$this->input->post('gender'),
                                    'ffm_address' => $captured_addresses,//$this->input->post('address'),
                                    'ffm_mobile' => $captured_mobile_phones,//$this->input->post('mobile_phone'),
                                    'ffm_email' => $captured_emails,//$this->input->post('email'),
                                    'ffm_age_group' => $captured_age_groups,//$this->input->post('age_group'),
                                    'ffm_clubs_groups' => $captured_club_groups,//$this->input->post('club_group')
                                );

                                $this->db->insert('family_friend_member', $data_ffm); //save to ffm_ 
                                $lastSaved_ffm_Id = $this->db->insert_id();
                                
                                

                                //update the bib_id of ffm
                                // $data_update_ffm = array(
                                //     'bib_id' => $lastSaved_BibId,
                                // );

                                // $this->db->where('m_id',  $lastMemberSaved_ID );
                                // $this->db->update('family_friend_member', $data_update_ffm);

                                //** add to participant **//
                                //** Get the fees
                                //** Get Race Category fee *//
                                $query = $this->db->select('rc_fee')
                                ->where('rc_id', $captured_race_categories)
                                ->get('race_categories'); 
                                $get_rc_fee = $query->row();
                                $rc_fee_each = $get_rc_fee->rc_fee;

                                //** Get WebFee cost *//
                                $wf_query = $this->db->select('f_cost')
                                ->where('f_slug', 'web_fee')
                                ->get('fees'); 

                                $cost_wf = $wf_query->row();
                                $web_fee_each = $cost_wf->f_cost;
                                
                                $dataParticipants = array(
                                    'm_id' => $lastMemberSaved_ID,
                                    'ffm_id' => $lastSaved_ffm_Id, //use ffm_id 
                                    'bib_id' => $lastSaved_BibId,
                                    'rc_id' => $captured_race_categories,
                                    're_id' => $race_event_id,
                                    'ss_id' => $captured_singlets,//$this->input->post('choose_singlet_size'),
                                    'p_payment_status' => 0,
                                    'p_payment_method' => $this->input->post('payment_method'),
                                    'p_payment_amount' => $rc_fee_each + $web_fee_each, 
                                    //'p_delivery_address' => $inputFor_deliverAddress
                                    
                                );
                                $this->db->insert('participants', $dataParticipants);
                                
                            }//end foreach 
                            
                            //get the bib id of the MAIN ACCOUNT
                            $query = $this->db->query('SELECT bib_id FROM family_friend_member WHERE m_id = "'. $lastMemberSaved_ID.'" AND is_main = "1" ' );
                            $row = $query->row();//get data
                            $ffm_bib_id = $row->bib_id;
                            // $ffm_ecd_id = $row->ecd_id;
                            
                            //udpate the fee for the main account. Add the delivery fee
                            $dataParticipant_mainMember = array(
                                'p_payment_total' => $rc_fee_total  + $delivery_fee_fixed + $web_fee_total,
                                'p_payment_amount' => $rc_fee  + $delivery_fee_fixed + $web_fee,
                                'p_delivery_address' => $inputFor_deliverAddress
                                
                            );
                            $this->db->where('bib_id',  $ffm_bib_id );
                            $this->db->update('participants', $dataParticipant_mainMember);
        
                            //send email
                            $this->send_email();

                            //show success message
                            $arr = array(
                                'status'=> 'Success!',
                                'msg' => '812 more than 1 not registered',
                                
                            );
                        }

                    }
                    
                    $result = $arr;
                    echo json_encode($result);

                }else{
                    $arr = array(
                        'status' => 'EventDone',
                        'msg' => 'This event is done. Please check other events.'
                    );
                    $result = $arr;
                    echo json_encode($result);
                }
            } 

        }
        

        if($do == 'check_email'){
            $this->form_validation->set_rules('existing_mail', 'Email', 'required|valid_email');
            $this->form_validation->set_rules('existing_raceCat', 'Race Category', 'required');
            $this->form_validation->set_rules('existing_singletSize', 'Singlet Size', 'required');
            
            $query = $this->db->query('SELECT * FROM members WHERE m_email = "'. $this->input->post('existing_mail') .'" ' );
            $checkLoginDetails = $query->num_rows(); //check if exist
            $row = $query->row();//get data

            if ($this->form_validation->run() == FALSE)
            {
                //check if logged-in even if blank email
                if( isset($_SESSION['isUserLoggedIn']) ) {

                    //race cat and singlets must have content
                    $existing_raceCat = $this->input->post('existing_raceCat');
                    $existing_singletSize = $this->input->post('existing_singletSize');

                    if($existing_raceCat != '' && $existing_singletSize != ''){

                        //get user data
                        $query = $this->db->query('SELECT * FROM members WHERE m_id = "'.$_SESSION['sess_user_id'].'" ');
                        $row = $query->row();
    
                        $userName = $row->m_username;
                        $firstName = $row->m_first_name;
                        $lastName = $row->m_last_name;
                        $middleName = $row->m_middle_name;
                        $gender = $row->m_gender;
                        $address = $row->m_address;
                        $mobile = $row->m_mobile;
                        $email = $row->m_email;
                        $ageGroup = $row->m_age_group;
                        $clubsGroups = $row->m_clubs_groups;
                        
                        //ecd data
                        $queryEcd = $this->db->query('SELECT * FROM emergency_contact_details WHERE m_id = "'.$row->m_id.'" ');
                        $rowEcd = $queryEcd->row();
    
                        $ecd_relationship = $rowEcd->ecd_relationship;
                        $ecd_firstName = $rowEcd->ecd_first_name;
                        $ecd_lastName = $rowEcd->ecd_last_name;
                        $ecd_middleName = $rowEcd->ecd_middle_name;
                        $ecd_address = $rowEcd->ecd_address;
                        $ecd_mobile = $rowEcd->ecd_mobile;
                        $ecd_email = $rowEcd->ecd_email;
    
                        $arr = array(
                            'status'=> 'IsLoggedIn',
                            'msg' => 'We filled up the fields for you. <br>If you want to change any details, please visit your account and go back here.',
                            'firstName' => $firstName,
                            'lastName' => $lastName,
                            'middleName' => $middleName,
                            'gender' => $gender,
                            'address' => $address,
                            'mobile' => $mobile,
                            'email' => $email,
                            'ageGroup' => $ageGroup,
                            'clubsGroups' => $clubsGroups,
    
                            'ecd_relationship' => $ecd_relationship,
                            'ecd_firstName' => $ecd_firstName,
                            'ecd_lastName' => $ecd_lastName,
                            'ecd_middleName' => $ecd_middleName,
                            'ecd_address' => $ecd_address,
                            'ecd_mobile' => $ecd_mobile,
                            'ecd_email' => $ecd_email
                        );
                    }else{
                        $arr = array(
                            'status'=> 'RaceCatSingletError!',
                            'existing_raceCat_error' => form_error('existing_raceCat'),
                            'existing_singletSize_error' => form_error('existing_singletSize'),
                        );
                    }
                }

                //throw error to force user to input data
                else{

                    $arr = array(
                        'status'=> 'Error!',
                        'email_error' => form_error('existing_mail'),
                        'existing_raceCat_error' => form_error('existing_raceCat'),
                        'existing_singletSize_error' => form_error('existing_singletSize'),
                    );
                }

            }else{
            
                //if already exist
                if($checkLoginDetails == 1){

                    //check if logged in 
                    if( isset($_SESSION['isUserLoggedIn']) ) {

                        //get user data
                        $query = $this->db->query('SELECT * FROM members WHERE m_id = "'.$_SESSION['sess_user_id'].'" ');
                        $row = $query->row();

                        $userName = $row->m_username;
                        $firstName = $row->m_first_name;
                        $lastName = $row->m_last_name;
                        $middleName = $row->m_middle_name;
                        $gender = $row->m_gender;
                        $address = $row->m_address;
                        $mobile = $row->m_mobile;
                        $email = $row->m_email;
                        $ageGroup = $row->m_age_group;
                        $clubsGroups = $row->m_clubs_groups;
                        
                        //ecd data
                        $queryEcd = $this->db->query('SELECT * FROM emergency_contact_details WHERE m_id = "'.$_SESSION['sess_user_id'].'" ');
                        $rowEcd = $queryEcd->row();

                        $ecd_relationship = $rowEcd->ecd_relationship;
                        $ecd_firstName = $rowEcd->ecd_first_name;
                        $ecd_lastName = $rowEcd->ecd_last_name;
                        $ecd_middleName = $rowEcd->ecd_middle_name;
                        $ecd_address = $rowEcd->ecd_address;
                        $ecd_mobile = $rowEcd->ecd_mobile;
                        $ecd_email = $rowEcd->ecd_email;

                        $arr = array(
                            'status'=> 'IsLoggedIn',
                            'msg' => 'We filled up the fields for you. <br>If you want to change any details, please visit your account and go back here.',
                            'firstName' => $firstName,
                            'lastName' => $lastName,
                            'middleName' => $middleName,
                            'gender' => $gender,
                            'address' => $address,
                            'mobile' => $mobile,
                            'email' => $email,
                            'ageGroup' => $ageGroup,
                            'clubsGroups' => $clubsGroups,

                            'ecd_relationship' => $ecd_relationship,
                            'ecd_firstName' => $ecd_firstName,
                            'ecd_lastName' => $ecd_lastName,
                            'ecd_middleName' => $ecd_middleName,
                            'ecd_address' => $ecd_address,
                            'ecd_mobile' => $ecd_mobile,
                            'ecd_email' => $ecd_email
                        );
                    }
                    //if not, tell the user to login
                    else{
                        $arr = array(
                            'status'=> 'AlreadyRegistered',
                            'msg' => '"'.$this->input->post('existing_mail').'" is already registered. Please <a data-open="loginModal" class="font-bold-underline">login</a> so we we can fill up the fields for you.',
                        );
                    }
    
                }
                //not exist
                else{
                    //check if logged in 
                    if( isset($_SESSION['isUserLoggedIn']) ) {
                        $arr = array(
                            'status' => 'AlreadyLoggedIn',
                            'msg' => 'You can only make your self as the main account. <br>If you are adding other runner(s) who are NOT registered, please click the "Add More Runner" button below.'
                        );
                    }
                    else{

                        $arr = array(
                            'status' => 'RegNewAccount',
                            'msg' => 'New account will be created using this email: "'.$this->input->post('existing_mail').'"<br>If you already have an account, you may <a data-open="loginModal" class="font-bold-underline">login</a> so we can fill it up automatically.'
                        );
                    }
                }
            }
            
            $result = $arr;
            echo json_encode($result);
            
        }

        if($do == 'old-reg'){

            //** DO VALIDATIONS *//
            
            /** Race Category, Race Event ID,  and Singlet Size */
            $this->form_validation->set_rules('choose_race_category', 'Race Category under "Race Details & Kits Tab"', 'required');
            $this->form_validation->set_rules('choose_singlet_size', 'Singlet Size under "Race Details & Kits Tab"', 'required');
            // $this->form_validation->set_rules('race_event_id', 'Race Event ID not found!', 'required|integer');
            
            /** Payment Method */
            $this->form_validation->set_rules('payment_method', 'Payment Method under "Review Info Tab"', 'required');
            
            /** Claiming Details */
            $this->form_validation->set_rules('raceKitDeliverPickup', 'Preferred Pickup under "Claiming Details Tab"', 'required');
            $this->form_validation->set_rules('terms_condition_checkbox', 'Terms and Condition under "Claiming Details Tab"', 'required');
            
            /** Check if Claiming Details is Delivery or Pickup */
            $raceKitDeliverPickup = $this->input->post('raceKitDeliverPickup');
            if(  $raceKitDeliverPickup == "delivered" ){
                $this->form_validation->set_rules('delivery_address', 'Delivery address under "Claiming Details Tab"', 'required');
            }

            /** Personal Details */
            $this->form_validation->set_rules('lastName', 'Last Name under "Participant Info Tab"', 'required');
            $this->form_validation->set_rules('firstName', 'First Name under "Participant Info Tab"', 'required');
            $this->form_validation->set_rules('middleName', 'Middle Name', '');
            $this->form_validation->set_rules('gender', 'Gender', '');
            $this->form_validation->set_rules('club_group', 'Clubs / Groups', '');
            $this->form_validation->set_rules('address', 'Address under "Participant Info Tab"', 'required');
            $this->form_validation->set_rules('mobile_phone', 'Mobile under "Participant Info Tab"', 'required');

            $this->form_validation->set_rules('email', 'Email', 'required|valid_email|is_unique[members.m_email]');
            $this->form_validation->set_message('is_unique', 'The %s "'.$this->input->post('email').'" is already registered.');

            
            $this->form_validation->set_rules('age_group', 'Age Group under "Participant Info Tab"', 'required');

            /** Emergency Contact Details */
            $this->form_validation->set_rules('ecd_last_name', 'Last Name for Emerg. Contact under "Participant Info Tab"', 'required');
            
            $this->form_validation->set_rules('ecd_first_name', 'First Name for Emerg. Contact under "Participant Info Tab"', 'required');
            // $this->form_validation->set_message('required', 'First name for Emergency Contact Info is needed.');
            
            $this->form_validation->set_rules('ecd_middle_name', 'Middle Name for Emerg. Contact under "Participant Info Tab"', '');
            $this->form_validation->set_rules('ecd_mobile', 'Mobile / Phone for Emerg. Contact under "Participant Info Tab"', 'required');
            // $this->form_validation->set_message('required', 'Mobile / Phone for Emergency Contact Info is needed.');
            
            $this->form_validation->set_rules('ecd_email', 'Email for Emerg. Contact under "Participant Info Tab"', 'valid_email');
            $this->form_validation->set_rules('ecd_address', 'Address for Emerg. Contact under "Participant Info Tab"', '');
            $this->form_validation->set_rules('ecd_relationship', 'Relationship for Emerg. Contact under "Participant Info Tab"', '');


            if ($this->form_validation->run() == FALSE)
            {
                $raceKitDeliverPickup = $this->input->post('raceKitDeliverPickup');
                if(  $raceKitDeliverPickup == "delivered" ){
                    $formError_deliverAddress = form_error('delivery_address');
                }
                else{
                    $formError_deliverAddress = '';
                }
                $arr = array(
                    'delivery_address' => $formError_deliverAddress,
                    'payment_method' => form_error('payment_method'),
                    'choose_race_category' => form_error('choose_race_category'),
                    'choose_singlet_size' => form_error('choose_singlet_size'),
                    'raceKitDeliverPickup' => form_error('raceKitDeliverPickup'),
                    'terms_condition_checkbox' => form_error('terms_condition_checkbox'),

                    'firstName' => form_error('firstName'),
                    'lastName' => form_error('lastName'),
                    'address' => form_error('address'),
                    'mobile_phone' => form_error('mobile_phone'),
                    'email' => form_error('email'),
                    'age_group' => form_error('age_group'),

                    'ecd_last_name' => form_error('ecd_last_name'),
                    'ecd_first_name' => form_error('ecd_first_name'),
                    'ecd_mobile' => form_error('ecd_mobile'),
                    'ecd_email' => form_error('ecd_email'),
                );

                $result = $arr;
               
                
            }
            else
            {
                    
                    $randomUsernmaneSuffix = $this->randomUsernmaneSuffix();
                    $randomPassword = $this->randomPassword();

                    $getUsername = strtolower($this->input->post('firstName'). '.' .$this->input->post('lastName'));
                    $generatedUsername = str_replace(" ", "", $getUsername);
                    //** gather data **//
                    $data = array(
                        'm_role' => 2,//member role
                        'm_status' => 0,//member status needs activation. Send email link for activation
                        'm_username' => $generatedUsername.".".$randomUsernmaneSuffix,
                        'm_password' => md5($randomPassword),
                        'm_first_name' => $this->input->post('firstName'),
                        'm_last_name' => $this->input->post('lastName'),
                        'm_middle_name' => $this->input->post('middleName'),
                        'm_gender' => $this->input->post('gender'),
                        'm_address' => $this->input->post('address'),
                        'm_mobile' => $this->input->post('mobile_phone'),
                        'm_email' => $this->input->post('email'),
                        'm_age_group' => $this->input->post('age_group'),
                        'm_clubs_groups' => $this->input->post('club_group')
                    );

                    //** insert to members data **//
                    $this->db->insert('members', $data);
                    
                    //** get the latest m_id of saved members **//
                    $lastMemberSaved_ID = $this->db->insert_id();

                    //** insert to emergency_contact_details together with m_id **//
                    $dataEcd = array(
                        'ecd_relationship' => $this->input->post('ecd_relationship'),
                        'ecd_first_name' => $this->input->post('ecd_first_name'),
                        'ecd_middle_name' => $this->input->post('ecd_middle_name'),
                        'ecd_last_name' => $this->input->post('ecd_last_name'),
                        'ecd_address' => $this->input->post('ecd_address'),
                        'ecd_mobile' => $this->input->post('ecd_mobile'),
                        'ecd_email' => $this->input->post('ecd_email'),
                        'm_id' => $lastMemberSaved_ID
                        
                    );

                    $this->db->insert('emergency_contact_details', $dataEcd);

                    //** get the actual prize of selected race category **//
                    $rc_id = $this->input->post('choose_race_category');

                    $rc_id_query = $this->db->select('rc_fee')
                        ->where('rc_id', $rc_id)
                        ->get('race_categories'); 
                     $get_rc_fee = $rc_id_query->row();
                            
                    //** check selected pickup or delivery **//
                     $raceKitDeliverPickup = $this->input->post('raceKitDeliverPickup');

                    if(  $raceKitDeliverPickup == "delivered" ){
                        $inputFor_deliverAddress = $this->input->post('delivery_address');
                    }
                    else{
                        $inputFor_deliverAddress = 'Pickup at the event area.';
                    }
                    
                   
                        
                    if(  $raceKitDeliverPickup == "delivered" ){
                        $dff_query = $this->db->select('f_cost')
                            ->where('f_slug', 'delivery_fee_fixed')
                            ->get('fees'); 

                        $cost_dff = $dff_query->row();
                        $delivery_fee_fixed = $cost_dff->f_cost;
                    }
                    else{
                        $delivery_fee_fixed = 0;
                    }

                    $wf_query = $this->db->select('f_cost')
                        ->where('f_slug', 'web_fee')
                        ->get('fees'); 

                    $cost_wf = $wf_query->row();
                    $web_fee = $cost_wf->f_cost;
                        
                    

                    //** Save to bib numbers with bib_assigned_number = 0 **//
                    $dataBibNumbers = array(
                        'bib_id' => 0,
                        'm_id' => $lastMemberSaved_ID,
                        're_id' => $this->input->post('race_event_id'),
                        'rc_id' => $this->input->post('choose_race_category'),
                    );
                    $this->db->insert('bib_number', $dataBibNumbers);
                    $lastSaved_BibId = $this->db->insert_id();

                    //** save to participants with p_payment_status = 0 & bib_id = 0 **//
                    $dataParticipants = array(
                        'm_id' => $lastMemberSaved_ID,
                        'bib_id' => $lastSaved_BibId,
                        'rc_id' => $this->input->post('choose_race_category'),
                        're_id' => $this->input->post('race_event_id'),
                        'ss_id' => $this->input->post('choose_singlet_size'),
                        'p_payment_status' => 0,
                        'p_payment_method' => $this->input->post('payment_method'),
                        'p_payment_amount' => $get_rc_fee->rc_fee + $delivery_fee_fixed + $web_fee,
                        'p_delivery_address' => $inputFor_deliverAddress
                        
                    );
                    $this->db->insert('participants', $dataParticipants);
                    $lastParticipantSaved_ID = $this->db->insert_id();


                    //** send email to members and admin **//
                    //**  include: payment breakdown (Deliver Fee, Web Fee, Registration Fee)
                    //** and activation link */

                    $arr = array(   
                        'status' => 'Success!',
                        'msg'   => $lastParticipantSaved_ID,
                    );
                    $result = $arr;
                    echo json_encode($result);

            }

        }
        
    }

 

}