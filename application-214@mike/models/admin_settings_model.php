<?php
class Admin_settings_model extends CI_Model {

    public function __construct()
    {
        $this->load->database();

    }

    public function select_where($table, $column, $columnData)
    {
        $query = $this->db->select('*')
            ->where($column, $columnData)
            ->get($table); 
        return $query->result_array();
    }
    public function select($table)
    {
        $query = $this->db->select('*')
            ->get($table); 
        return $query->result_array();
    }

    public function select_specific($table)
    {
        $query = $this->db->select('*')
            ->get($table); 
        return $query->row_array();
    }
   
    
     /**
     * Get all "media" and display
     */
    public function get_media(){
            $query = $this->db->select('*')
            ->get('media_files');
            return $query->result_array(); 
        
           
    }
   
  
}