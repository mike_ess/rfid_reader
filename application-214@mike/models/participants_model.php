<?php
class Participants_model extends CI_Model {

    public function __construct()
    {
        $this->load->database();
    }

    
    /**
     * Get all events and display in dropdown
     */
    public function get_events($re_slug = FALSE)
    {
        if ($re_slug === FALSE)
        {
            $query = $this->db->get('race_events');
            return $query->result_array();
        }

        $query = $this->db->get_where('race_events', array('re_slug' => $re_slug));
        return $query->row_array();
    }

   
   
    /**
      * Get all race categories and display in a dropdown selection based on Race Event selected
      */
      public function get_race_categories($re_id)
      {
        //get race categories ID based on race event ID
        $query2 = $this->db->select('*')
            ->where('re_id', $re_id)
            ->get('race_categories'); 
        return $query2->result_array();
     }

     
     /**
       * Get all participants ONLY from specific re_id and rc_id
       */
       public function get_race_participants_only($re_id = FALSE, $rc_id = FALSE){

        $this->db->select('
        participants.*,
        bib_number.*,
        members.*');

        $this->db->from('participants');
        $this->db->join('members', 'members.m_id = participants.m_id');
        $this->db->join('bib_number', 'bib_number.bib_id = participants.bib_id');
        $this->db->where('participants.re_id = '.$re_id.' '); 
        $this->db->where('participants.rc_id = '.$rc_id.' ');
        $query = $this->db->get();

        return $query->result_array(); 
        
   }

     /**
       * Get all participants with timerecords from specific event via on re_id
       */
       public function get_race_participants_with_time_records($re_id = FALSE, $rc_id = FALSE){

            $this->db->select(' 
            time_records.*,
            bib_number.*,
            members.*');

            $this->db->from('time_records');       
            $this->db->join('bib_number', 'bib_number.bib_assigned_number = time_records.bib_assigned_number');
            $this->db->join('members', 'members.m_id = bib_number.m_id');
            $this->db->where('time_records.re_id = '.$re_id.' '); //'.$theRaceEventID->re_id);
            $this->db->where('time_records.rc_id = '.$rc_id.' ');
            $query = $this->db->get();

            return $query->result_array();
            
       }


       /**
         * Get members details by JOINING tables "bib_numbers" and "members"
         *
         */
         public function get_member_details($m_id = FALSE){
            $this->db->select('time_records.*, bib_number.bib_assigned_number, members.*');
            $this->db->from('time_records');
            $this->db->join('bib_number', 'time_records.bib_assigned_number = bib_number.bib_assigned_number');
            $this->db->join('members', 'members.m_id = bib_number.m_id');
            $this->db->where('bib_number.m_id = "'.$m_id.'" ');
            $query = $this->db->get();

            return $query->row_array();
         }

      
}