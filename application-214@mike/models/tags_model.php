<?php
class Tags_model extends CI_Model {

    public function __construct()
    {
        $this->load->database();
    }
 
     /**
     * Get all events and display
     */
    
    public function get_events($tags = FALSE)
    {
        $tags = str_replace('_', ' ', $tags);
        
        if( isset($_SESSION['isAdmin']) || isset($_SESSION['isAuthor']) || isset($_SESSION['isContributor'])   ) {
            $array = array(
                're_tags' => $tags,
            );
        }else{
            $array = array(
                're_tags' => $tags,
                're_publish_status' => '1',
            );
        }
    
        $result = $this->db->like($array)->get('race_events');
        $checkEvents = $result->num_rows();


        if($checkEvents >= 1){
            return $result->result_array();
        }
        else{
            return $checkEvents;
        }
 
    }

    public function get_lwc_events($tags = FALSE)
    {
        $tags = str_replace('_', ' ', $tags);
        
        if( isset($_SESSION['isAdmin']) || isset($_SESSION['isAuthor']) || isset($_SESSION['isContributor'])   ) {
            $array = array(
                'lwc_tags' => $tags,
            );
        }else{
            $array = array(
                'lwc_tags' => $tags,
                'lwc_publish_status' => '1',
            );
        }
    
        $result = $this->db->like($array)->get('lakwatcharity_events');
        $checkEvents = $result->num_rows();


        if($checkEvents >= 1){
            return $result->result_array();
        }
        else{
            return $checkEvents;
        }
 
    }
    
}