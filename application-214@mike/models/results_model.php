<?php
class Results_model extends CI_Model {

    public function __construct()
    {
        $this->load->database();
    }

    
    /**
     * Get all events and display
     */
    public function get_events($re_slug = FALSE)
    {
        
        if ($re_slug === FALSE)
        {
            if( isset($_SESSION['isAdmin']) || isset($_SESSION['isAuthor']) || isset($_SESSION['isContributor'])   ) {
                $query = $this->db->select('*')
                ->where('re_slug', $re_slug)
                ->get('race_events');
                
                return $query->result_array();
            }else{
                $query = $this->db->select('*')
                    ->where('re_publish_status', '1')
                    ->get('race_events');
                return $query->result_array();
            }
        }else{

            if( isset($_SESSION['isAdmin']) || isset($_SESSION['isAuthor']) || isset($_SESSION['isContributor'])   ) {
                $query = $this->db->select('*')
                ->where('re_slug', $re_slug)
                ->get('race_events');
                
                // $query = $this->db->get_where('race_events', array('re_slug' => $re_slug));

                return $query->row_array();
            }else{
                $query = $this->db->select('*')
                ->where('re_publish_status', '1')
                ->get('race_events');
                return $query->row_array();
    
            }
        }
    }

    /**
      * Get all race categories and display in a dropdown selection based on Race Event selected
      */
      public function get_race_categories($re_id)
      {
        //get race categories ID based on race event ID
        $query2 = $this->db->select('*')
            ->where('re_id', $re_id)
            ->get('race_categories'); 
        return $query2->result_array();
     }
    
      
     
    

    /**
       * Get all participants ONLY from specific re_id and rc_id
       */
       public function get_race_participants_only($re_id = FALSE, $rc_id = FALSE){

        $this->db->select('participants.m_id,
        bib_number.bib_assigned_number,
        members.*');
        $this->db->from('participants');
        $this->db->join('members', 'members.m_id = participants.m_id');
        $this->db->join('bib_number', 'bib_number.m_id = participants.m_id');
        $this->db->where('participants.re_id = '.$re_id.' '); 
        $this->db->where('participants.rc_id = '.$rc_id.' ');
        $query = $this->db->get();

        return $query->result_array();
        
   }

    /**
    * Get all participants with timerecords from specific event and category
    */
    public function get_race_participants_with_time_records($re_id = FALSE, $rc_id = FALSE){

        $this->db->select(' 
        time_records.*,
        bib_number.*,
        members.*');

        $this->db->from('time_records');       
        $this->db->join('bib_number', 'bib_number.bib_assigned_number = time_records.bib_assigned_number');
        $this->db->join('members', 'members.m_id = bib_number.m_id');
        $this->db->where('time_records.re_id = '.$re_id.' '); //'.$theRaceEventID->re_id);
        $this->db->where('time_records.rc_id = '.$rc_id.' ');
        $query = $this->db->get();

        return $query->result_array();
        
    }
}