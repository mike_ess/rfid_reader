<?php
class Members_model extends CI_Model {

    public function __construct()
    {
        $this->load->database();

        //$this->userTbl = 'members';
    }

    /*
     * get rows from the users table
     */
    /*
    public function getRows($params = array()){
        $this->db->select(' *');
        $this->db->from($this->userTbl);
        
        //fetch data by conditions
        if(array_key_exists("conditions",$params)){
            foreach ($params['conditions'] as $key => $value) {
                $this->db->where($key,$value);
            }
        }
        
        if(array_key_exists("m_id",$params)){
            $this->db->where('m_id',$params['m_id']);
            $query = $this->db->get();
            $result = $query->row_array();
        }else{
            //set start and limit
            if(array_key_exists("start",$params) && array_key_exists("limit",$params)){
                $this->db->limit($params['limit'],$params['start']);
            }elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){
                $this->db->limit($params['limit']);
            }
            $query = $this->db->get();
            if(array_key_exists("returnType",$params) && $params['returnType'] == 'count'){
                $result = $query->num_rows();
            }elseif(array_key_exists("returnType",$params) && $params['returnType'] == 'single'){
                $result = ($query->num_rows() > 0)?$query->row_array():FALSE;
            }else{
                $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
            }
        }

        //return fetched data
        return $result;
    }
    */
    
    /**
     * Get singlet name
     */
    public function get_singlet_size($a){

        $querySinglets = $this->db->query('SELECT * FROM singlet_sizes WHERE ss_id = "'. $a .'" ' );
        $resultSinglet = $querySinglets->row_array(); 
      
        return $resultSinglet['ss_name'];
      
      }
   

    /**
     * Get all members and display
     */
    public function get_members($m_id = FALSE)
    {
        if ($m_id === FALSE)
        {
            $query = $this->db->get('members');
            return $query->result_array();
        }

        $query = $this->db->get_where('members', array('m_id' => $m_id));
        return $query->row_array();
    }

     /**
     * Get all Family members and display
     */
    public function get_family_members()
    {
        // if ($m_id === FALSE)
        // {
        //     $query = $this->db->get('members');
        //     return $query->result_array();
        // }

        $query = $this->db->get_where('family_friend_member', array('is_main' => 0));
        return $query->result_array();
    }

    /** Check if the user has family members */
    public function check_ffm($m_id = FALSE){
        $query = $this->db->query('SELECT * FROM family_friend_member WHERE m_id = '.$m_id.' ');
        return $query->num_rows();
    }

    /**
     * Get all emergency contact based on m_id
     */
     public function get_emergency_contact($m_id = FALSE)
     {
         if ($m_id === FALSE)
         {
             $query = $this->db->get('emergency_contact_details');
             return $query->result_array();
         }
 
         $query = $this->db->get_where('emergency_contact_details', array('m_id' => $m_id));
         return $query->row_array();
     }

    /**
     * Get all events and display in dropdown selection
     */
    
     public function get_events($re_slug = FALSE)
     {
         if ($re_slug === FALSE)
         {
             $query = $this->db->get('race_events');
             return $query->result_array();
         }
 
         $query = $this->db->get_where('race_events', array('re_slug' => $re_slug));
         return $query->row_array();
     }



    /**
     * Get Participated Events of the single runner
     */
     public function get_participated_events_single($m_id = FALSE){

        $this->db->select('
                participants.*,
                bib_number.*,
                race_categories.rc_name,
                race_events.re_name,
                singlet_sizes.ss_name
             '); 
 
        $this->db->from('participants');
        $this->db->join('bib_number', 'bib_number.bib_id = participants.bib_id');
        $this->db->join('race_events', 'race_events.re_id = participants.re_id');
        $this->db->join('singlet_sizes', 'singlet_sizes.ss_id = participants.ss_id');
        $this->db->join('race_categories', 'race_categories.rc_id = participants.rc_id');
        $this->db->where('participants.m_id = '.$m_id.'  ');
        $this->db->where('participants.p_payment_total != 0  '); //if NOT zero, he is the main account
         
 
         $query = $this->db->get();
         return $query->result_array();
 
      }

     /**
     * Get Participated Events of the Main account
     */
     public function get_participated_events($m_id = FALSE){

       $this->db->select('
            participants.*,
            bib_number.*,
            race_categories.rc_name,
            race_events.re_name,    
            family_friend_member.*
            
       '); 

       $this->db->from('participants');
       $this->db->join('bib_number', 'bib_number.bib_id = participants.bib_id');
       $this->db->join('race_events', 'race_events.re_id = participants.re_id');
       $this->db->join('family_friend_member', 'family_friend_member.m_id = '.$m_id.'');
       $this->db->join('race_categories', 'race_categories.rc_id = participants.rc_id');
       $this->db->where('participants.m_id = '.$m_id.'  ');
       $this->db->where('participants.p_payment_total != 0 '); //if NOT zero, he is the main account
        $this->db->where('family_friend_member.is_main = 1 ');
        
        //    $this->db->where

        $query = $this->db->get();
        return $query->result_array();

     }


    /**
     * Get Participated Events of family_frien_members
     */
     public function get_participated_events_ffm($m_id = FALSE){

        $this->db->select('
             participants.*,
             bib_number.*,
             race_categories.rc_name,
             race_events.re_name,    
             family_friend_member.*
             
        ');  
 
        $this->db->from('participants');
        $this->db->join('bib_number', 'bib_number.bib_id = participants.bib_id');
        $this->db->join('race_events', 'race_events.re_id = participants.re_id');
        $this->db->join('family_friend_member', 'family_friend_member.ffm_id = participants.ffm_id');
        $this->db->join('race_categories', 'race_categories.rc_id = participants.rc_id');
        $this->db->where('participants.m_id = '.$m_id.'  ');
        $this->db->where('participants.p_payment_total = 0 '); //if zero, he is not the main account
        //  $this->db->where('family_friend_member.is_main = 0 ');
         
         //    $this->db->where
 
         $query = $this->db->get();
         return $query->result_array();
 
      }
    
      /** Select Table **/
      public function select($table)
      {
          $query = $this->db->select('*')
              ->get($table); 
          return $query->result_array();
      }
}