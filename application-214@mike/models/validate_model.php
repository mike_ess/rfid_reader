<?php
class Validate_model extends CI_Model {

    public function __construct()
    {
        $this->load->database();
        //test
    }

    public function get_validation_link($theLink = FALSE)
    {
        $query = $this->db->query('SELECT * FROM validation_link WHERE v_link = "'.$theLink.'" ');
        $count = $query->num_rows();

       return $count;
    }

    public function activate_user_status($theLink = FALSE)
    {
        //check first if the lnk is valid
        $query = $this->db->query('SELECT * FROM validation_link WHERE v_link = "'.$theLink.'" ');
        $count = $query->num_rows();

        if($count ==1){
            //get the m_id from validation_link
            $query = $this->db->query('SELECT m_id FROM validation_link WHERE v_link = "'.$theLink.'" ');
            $row = $query -> row();
            $m_id = $row->m_id;
            
            //then, update the member
            $data = array(
                'm_status' => '1' //activated 
            );
            $this->db->where('m_id', $m_id);
            $this->db->update('members', $data);

            //then, delete the validation link
            $this->db->delete('validation_link', array('m_id' => $m_id) );

            //return $query;
        }
    }


}