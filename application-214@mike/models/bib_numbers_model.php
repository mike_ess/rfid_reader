<?php
class Bib_numbers_model extends CI_Model {

    public function __construct()
    {
        $this->load->database();

        //$this->userTbl = 'members';
    }

   
    /**
     * Get all bib numbers and other linked data
     */
    public function get_bib_numbers(){
        $this->db->select('
            bib_number.*,
            members.*,
            race_categories.*,
            race_events.*,
        '); 

        $this->db->from('bib_number');
        $this->db->join('members', 'members.m_id = bib_number.m_id');
        $this->db->join('race_categories', 'race_categories.rc_id = bib_number.rc_id');
        $this->db->join('race_events', 'race_events.re_id = bib_number.re_id');
        
        $query = $this->db->get();
        return $query->result_array();

    }
   
  
}