<?php

/*** Count all Blank Bib Numbers */
$this->db->like('bib_assigned_number','0');
$this->db->from('bib_number');
$countBlankBibNumbers = number_format($this->db->count_all_results());
     

/*** Count all Assigned Bib Numbers */
$this->db->not_like('bib_assigned_number','0');
$this->db->from('bib_number');
$countAssignedBibNumbers = number_format($this->db->count_all_results());
 
?>


<section id="main-content">
  <div id="guts">

<div class="grid-container">
    <div class="grid-x grid-margin-y">
        
        <div class="medium-12 cell">
            <div class="text-center">
				<h1 style="margin: 0;" class="text-center"><?php echo $title; ?></h1>
			</div>

			<hr>
        </div>

        <div class="medium-6 cell">
            <table class="  ">
                
                    <tr>
                        <td>Blank Bib / Not Assigned Bib: <?=$countBlankBibNumbers;?></td>
                        <td>Total Assigned Bib: <?=$countAssignedBibNumbers;?></td>
                    </tr>
                    

                </tbody>
            </table>
        </div>
         <!-- <div class="medium-9 cell">
            <div class="small button-group">
                <a class=" button hollow clear" >Filter View:</a> 
                <a class="success button toggle-vis" data-column="0">#</a>
                
            </div>
        </div>
       <div class="medium-3 cell">
            <a href="<?=base_url();?>members/add/new" class="button small float-right">Add New Member</a>
        </div> -->
       <div class="medium-12 cell">
        <table id="bibNumberList" class="hover" style="width: 100%;">
            <thead>
                <tr>
                    <th width="50">#</th>
                    <th width="100">Assigned Bib</th>
                    <th width="100">Bib Owner</th>
                    <th width="100">Race Event Name</th>
                    <th width="100">Race Category</th>
                    <th width="100">Action</th>
                </tr>
            </thead>
            <tbody>
            <?php foreach ($bib_numbers as $bib_numbers_item): ?>
                <tr>
                    <td><?php echo $bib_numbers_item['bib_id']; ?></td>
                    <td><?php echo fix_bib_number($bib_numbers_item['bib_assigned_number']); ?></td>
                    
                    <?php
 

                    $query = $this->db->query('SELECT * FROM family_friend_member WHERE bib_id = '.$bib_numbers_item['bib_id'].' ' );
                    $ifHasFfm = $query->num_rows();  //check if has FFM 
                    $row = $query->row();

                    if($ifHasFfm >= 1):
                    ?>

                    <td><?php echo $row->ffm_last_name; ?>, <?php echo $row->ffm_first_name; ?></td>
                    <?php else: ?>
                    <td><?php echo $bib_numbers_item['m_last_name']; ?>, <?php echo $bib_numbers_item['m_first_name']; ?></td>

                    <?php endif;?>
                    <td><?php echo $bib_numbers_item['re_name']; ?></td>
                    <td><?php echo $bib_numbers_item['rc_name']; ?></td>
                    <td><a class="button small" href="<?php echo base_url('members/edit/'.$bib_numbers_item['m_id']); ?>">Edit</a></td>
                </tr>
                <?php 
             /** End Show all bib numbers */
            endforeach; ?>
            </tbody>
        </table>
        </div>
        <!-- <div class="large-12">
            <button type="button" class="button float-right">Save</button>
        </div> -->
    </div>
</div>

</div>
</section>
   
