
<?php
// echo 'eventName = '.$current_events_item['re_name'].'<br>';
// echo 'rE_id = '.$current_events_item['re_id'] . '<br>';

$theRaceCatId = $rc_id; //$the_rc_name['rc_id'];
$theRaceEventId = $current_events_item['re_id'];

$get_query_rcId = $this->db->query('SELECT * FROM race_categories WHERE rc_id = "'.$theRaceCatId.'" ');
$get_row_rcId = $get_query_rcId->row();

$the_rcName = $get_row_rcId->rc_name;

// echo $the_rcName;
?>

<section id="main-content">
  <div id="guts">

<div class="grid-container">
    <div class="grid-x grid-margin-y grid-margin-x">
        <div class="text-center cell medium-12">
            <h1 style="margin: 0;" class="text-center">Race Results</h1>
        </div>

        <hr>
        <div class="cell medium-12">
            <div class="grid-container">
                <div class="grid-x grid-margin-y grid-margin-x">
                    <div class="cell medium-7">
                        <form>
                            <label>Choose event name to view result:
                                <div class="input-group">
                                    <!-- <span class="input-group-label">$</span> -->
                                    <!-- <input class="input-group-field" type="number"> -->
                                    <select name="choose-race-event-select" class="input-group-field">
                                        
                                        <?php 
                                            /** Start Show all events */ 
                                            foreach ($events as $events_item): 
                                                $reId = $events_item['re_id'];
                                                $query_rcId = $this->db->query('SELECT * FROM race_categories WHERE re_id = "'.$reId.'" ');
                                                $row_rcId = $query_rcId->row();
                                                $rc_rcId = $row_rcId->rc_id;

                                        ?>
                                        <option value="<?=base_url() . 'results/events/' . $events_item['re_slug'].'/'.$rc_rcId;?>" <?php if ($events_item['re_name'] == $current_events_item['re_name']) echo 'selected';?> ><?=$events_item['re_name'];?></option>

                                        <?php endforeach; ?>

                                    </select>
                                    <div class="input-group-button">
                                        <button type="button" class="button small choose-race-event-button"  disabled>Go</button>
                                    </div>
                                </div>
                            </label>
                        </form>
                        <hr>
                        <ul class="menu">
                            <li class="menu-text">Choose categories: </li>
                            <?php 
                            /** Start Show all race categories */
                            foreach ($categories as $race_categories) :
                                ?>
                            <?php //if($race_categories['rc_name'] == $current_category_name['rc_name']) { echo 'is-active'; } ?>
                            <li class="<?php if($race_categories['rc_id'] == $theRaceCatId) { echo 'is-active'; } ?>">
                                <a id="<?=$race_categories['rc_id'];?>" href="<?=base_url('results/events/'). $current_events_item['re_slug'] .'/'.$race_categories['rc_id']; ?>" aria-selected="true"><?=$race_categories['rc_name']; ?></a>
                            </li>
                            
                            <?php endforeach; ?>
                        </ul>
                        <hr>
                    </div>
                    <div class="medium-12 cell">
                         
                        

                        <div class="" >
                        <?php 
                            /** Start Show all race categories */
                             //if(!empty($theRaceCatId)) : ?>
                            <div class="" id="<?=$the_rcName; ?>">
                                <table id="resultList<?php echo str_replace(' ', '_', $the_rcName); ?>" class="hover" style="width: 100%;">
                                    <thead>
                                        <tr>
                                            <th width="">BIB No.</th>
                                            <th width="">Last Name</th>
                                            <th width="">First Name</th>
                                            <th width="">Start Time</th>
                                            <th width="">Laps</th>
                                            <th width="">Finished Time</th>
                                        </tr>
                                    </thead>
                                    
                                    <tbody>
                                        <?php                                      
                                            foreach ($participants_with_time_records as $participants) {
                                                echo " <tr id='".$participants['m_id']."'>";
                                                echo "<td>".fix_bib_number($participants['bib_assigned_number']) . "</td>";
                                                // echo "<td>";
                                             
                                                //check if has ffm details
                                                $query = $this->db->query('SELECT * FROM family_friend_member WHERE bib_id = '.$participants['bib_id'].' ' );
                                                $ifHasFfm = $query->num_rows();  //check if has FFM 
                                                $row = $query->row();

                                                if($ifHasFfm >= 1){
                                                    echo "<td>".$row->ffm_last_name."</td>";
                                                    echo "<td>".$row->ffm_first_name."</td>";
                                                }else{
                                                    echo "<td>".$participants['m_last_name']."</td>";
                                                    echo "<td>".$participants['m_first_name']."</td>";
                                                   
                                                }
                                                // echo "</td>";
                                                // echo "<td></td>";
                                                echo "<td>".$participants['tr_start_time']."</td>";
                                                echo "<td>".$participants['tr_loops_laps']."</td>";
                                                echo "<td>".$participants['tr_finish_time']."</td>";
                                                echo "</tr>";
                                            }
                                        ?>
         
                                    </tbody>
                                </table>
                            </div>
                        <?php /*else: ?>
                        <p class="callout" style="width: 100%;">Please choose race category.</p>
                        <?php endif; */?>
                       
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- <?php $this->load->view('templates/sidebar') ?> -->
        
    </div>
</div>

</div>
</section>
  
  <script type="text/javascript">
$(document).ready(function(){
    $('#resultList<?php echo str_replace(' ', '_', $the_rcName); ?>').DataTable( {
        "columns": [
            null,
            null,
            null,
            null,
            null,
            null
        ]
    } );
    



    
});
</script>