

<section id="main-content">
  <div id="guts">

<div class="grid-container">
	<div class="grid-x grid-margin-y">

		<div class="medium-12 cell">
            <div class="text-center">
				<h1 style="margin: 0;" class="text-center"><?php echo $title; ?></h1>
			</div>

			<hr>
        </div>

		<!-- <div class="cell medium-12">
		</div> -->
		<div class="medium-12 cell">
			<div class="status-messages"> </div>
			<ul class="tabs" data-responsive-accordion-tabs="tabs medium-accordion small-accordion large-tabs" id="upload-tabs">
				<li class="tabs-title is-active">
					<a href="#utr">Upload Time Records</a>
				</li>
				<li class="tabs-title">
					<a href="#ulr">Upload Loop Records</a>
				</li>
				<li class="tabs-title">
					<a href="#gs">General Settings</a>
				</li>
				<li class="tabs-title">
					<a href="#es">Email Settings</a>
				</li>
			</ul>

			<div class="tabs-content" data-tabs-content="upload-tabs">
				<div class="tabs-panel is-active" id="utr">
					<p>Time Records (CSV File)</p>
					<hr>
					<form method="post" id="import_csv" enctype="multipart/form-data">
						<div class="form-group">
							<input type="file" name="csv_file" id="csv_file" required accept=".csv" />
						</div>
						<br />
						<button type="submit" name="import_csv" class="button small" id="import_csv_btn">Import Time Records</button>
						<span>Make sure you are uploading the correct file name. You cannot undo this action.</span>
					</form>
					<br />
				</div>
				<div class="tabs-panel" id="ulr">
					<p>Individual Loop Records (CSV)</p>
					<hr>
					<form method="post" id="import_csv_loop_records" enctype="multipart/form-data">
						<div class="form-group">
							<input type="file" name="csv_file_loop_records" id="csv_file_loop_records" required accept=".csv" />
						</div>
						<br />
						<button type="submit" name="import_csv_loop_records" class="button small" id="import_csv_btn_loop_records">Import Loop Records</button>
						<span>Make sure you are uploading the correct file name. You cannot undo this action.</span>
					</form>
					<br />
				</div>
				<div class="tabs-panel" id="gs">
					<p>General Settings</p>
					<hr>
					<ul class="tabs" data-responsive-accordion-tabs="tabs medium-accordion large-tabs" id="gen-settings-tabs">
						<li class="tabs-title is-active">
							<a href="#genSettingsTab" aria-selected="true">Payments</a>
						</li>
						<li class="tabs-title">
							<a href="#delWebFeesTab">Delivery / Web Fees & Pickup Locations</a>
						</li>
						<li class="tabs-title">
							<a href="#imagesSettingsTab">Images</a>
						</li>

					</ul>

					<div class="tabs-content" data-tabs-content="gen-settings-tabs">
						<div class="tabs-panel is-active" id="genSettingsTab">
							<form id="payments" data-abide novalidate>
								<div class="grid-container">
									<div class="grid-x grid-margin-y">
										<div class="callout cell medium-12">
											
											<div class="input-group">
												<span class="input-group-label">Paypal Email Address:</span>

												<input class="input-group-field" type="email" name="paypal_email" value="<?=$sg['sg_paypal_email']; ?>">
												
											</div>
										</div>
										<div class="callout cell medium-12">
											<h3>
											Bank Payments 
											<!-- <button type="button" class="button alert small float-right delBank">-</button> -->
											<button type="button" class="button float-right addNewBank">+</button>
											</h3>
											<hr>

											<div class="grid-container">
												<div class="grid-x grid-margin-x grid-margin-y newly_added_bank_item">

													<?php if(empty($banks)): ?>
													<div class="medium-4 text-center cell callout noBankNotice">
														<h2>No added bank payments</h2>
													</div>
													<?php else: ?>


													<?php 
														foreach($banks as $bank):
															$bpo_id = $bank['bpo_id'];
															$bpo_name = $bank['bpo_name'];
															$bpo_acct_name = $bank['bpo_acct_name'];
															$bpo_acct_number = $bank['bpo_acct_number'];
															$bpo_type = $bank['bpo_type'];
															$bpo_mf_id = $bank['mf_id'];
														
													?>
													<div id="bpo_id-<?=$bpo_id;?>" class="medium-4 cell callout">
															
														<h3> 
															<input type="hidden" readonly name="bank_id[]" value="<?=$bpo_id;?>">
															<button id="<?=$bpo_id;?>" type="button" data-table="bank_payment_option" data-column="bpo_id" class="button small float-right delExistingItem">x</button>
														</h3> <hr>
														<ul>
															<li>Bank Name:
																<input type="text" name="bank_name[]" value="<?=$bpo_name;?>">
															</li>
															<li>Account Name:
																<input type="text" name="acc_name[]" value="<?=$bpo_acct_name;?>">
															</li>
															<li>Account Number:
																<input type="text" name="acc_number[]" value="<?=$bpo_acct_number;?>">
															</li>
															<li>Type:
																<input type="text" name="acc_type[]" value="<?=$bpo_type;?>">
															</li>
															<li>Bank Photo<br>
																<?php 
																	$query = $this->db->query('SELECT * FROM media_files WHERE mf_id = "'.$bpo_mf_id.'" ');
																	$row = $query->row();
																	$check_num_rows = $query->num_rows();
																	
																?>
																<?php if($bpo_mf_id != '0') : ?>
																	<input class="bpoPhoto" required type="number" name="bpo_photo[]" value="<?=$bpo_mf_id;?>">						
																	<button type="button" class="button small addFeaturedImageModal" data-open="openMediaFiles"><?php if($bpo_mf_id == '0') { echo 'Add Photo'; } else{ echo 'Change';} ?></button>
																	<?php if($check_num_rows >= 1): ?>
																	<div class="bpo_featuredImageDiv">
																		<img src="<?=base_url('uploads/ar_');?><?=$row->mf_file_name; ?>">
																	</div>
																	<?php else: ?>
																	<div class="bpo_featuredImageDiv">
																		<br><span class="label alert">Can't find the image. <br>Please check image location.</span>
																		<img src="https://placehold.it/200x200&amp;text=No Available Image!" alt="<?=$bpo_name;?>"> 
																	</div>
																	<?php endif; ?>
																<?php else: ?>
																	<input required type="number" name="bpo_photo[]" value="<?=$bpo_mf_id;?>">						
																		<button type="button" class="button small addFeaturedImageModal" data-open="openMediaFiles"><?php if($bpo_mf_id == '0') { echo 'Add Photo'; } else{ echo 'Change';} ?></button>
																<?php endif; ?>
															</li>
														</ul>
													</div>

													<?php endforeach; ?>
													<?php endif; ?>
												</div>
											</div>
										</div>
										<div class="callout cell medium-12">
											<h3>Money Transfers 
											<!-- <button type="button" class="button alert small float-right delMt">-</button> -->
											<button type="button" class="button float-right addMt">+</button>
											</h3>
											<hr>

											<div class="grid-container">
												<div class="grid-x grid-margin-x grid-margin-y newly_added_mt_item">
												<?php if(empty($mtos)): ?>
													<div class="medium-4 text-center cell callout noMtNotice">
														<h2>No added money transfer payments</h2>
													</div>
													<?php else: ?>


													<?php 
														foreach($mtos as $mto):
															$mto_id = $mto['mto_id'];
															$mto_est_name = $mto['mto_est_name'];
															$mto_receivers_name = $mto['mto_receivers_name'];
															$mto_mobile = $mto['mto_mobile'];
															$mto_address = $mto['mto_address'];
															$mto_mf_id = $mto['mf_id'];
														
													?>
													<div id="mto_id-<?=$mto_id;?>" class="medium-4 cell callout ">
														<h3> 
															<input type="hidden" readonly name="mto_id[]" value="<?=$mto_id;?>">
															<button id="<?=$mto_id;?>" type="button" data-table="money_transfer_option" data-column="mto_id" class="button small float-right delExistingItem">x</button>
														</h3> <hr>
														<ul>
															<li>Establishment Name:
																<input type="text" name="mto_est_name[]" value="<?=$mto_est_name;?>">
															</li>
															<li>Receiver's Name:
																<input type="text" name="mto_rec_name[]" value="<?=$mto_receivers_name;?>">
															</li>
															<li>Mobile Number:
																<input type="text" name="mto_mobile_number[]" value="<?=$mto_mobile;?>">
															</li>
															<li>Address:
																<input type="text" name="mto_address[]" value="<?=$mto_address;?>">
															</li>
															<li>Est.  Photo<br>
																<?php 
																	$query = $this->db->query('SELECT * FROM media_files WHERE mf_id = "'.$mto_mf_id.'" ');
																	$row = $query->row();
																	$check_num_rows = $query->num_rows();
																	
																?>
																<?php if($mto_mf_id != '0') : ?>
																	<input class="mtoPhoto" required type="number" name="mto_photo[]" value="<?=$mto_mf_id;?>">						
																	<button type="button" class="button small addFeaturedImageModal" data-open="openMediaFiles"><?php if($mto_mf_id == '0') { echo 'Add Photo'; } else{ echo 'Change';} ?></button>
																	<?php if($check_num_rows >= 1): ?>
																	<div class="mto_featuredImageDiv">
																		<img src="<?=base_url('uploads/ar_');?><?=$row->mf_file_name; ?>">
																	</div>
																	<?php else: ?>
																	<div class="mto_featuredImageDiv">
																		<br><span class="label alert">Can't find the image. <br>Please check image location.</span>
																		<img src="https://placehold.it/200x200&amp;text=No Available Image!" alt="<?=$bpo_name;?>"> 
																	</div>
																	<?php endif; ?>
																<?php else: ?>
																	<input required type="number" name="mto_photo[]" value="<?=$mto_mf_id;?>">						
																		<button type="button" class="button small addFeaturedImageModal" data-open="openMediaFiles"><?php if($mto_mf_id == '0') { echo 'Add Photo'; } else{ echo 'Change';} ?></button>
																<?php endif; ?>
															</li>
														</ul>
													</div>
													<?php endforeach; ?>
													<?php endif; ?>
												</div>
											</div>
										</div>
										<div class="cell medium-12">
											<button type="submit" class="button float-right">Save Payments</button>
										</div>
									</div>
								</div>
							</form>
						</div>
						<div class="tabs-panel" id="delWebFeesTab">
							<form id="delivery" data-abide novalidate>
								<div class="grid-container"> 
									<div class="grid-x grid-margin-x grid-margin-y">
										<div class="callout cell medium-12">
											<h3>Fees (₱) <button type="button" class="button float-right addNewFee">+</button></h3>
											<hr>
											<div class="grid-container">
												<div class="grid-x grid-margin-x newly_added_fee_item">
												<?php if(empty($fees)): ?>
													<div class="medium-4 text-center cell callout noFeeNotice">
														<h2>No added pickup fees yet.</h2>
													</div>
													<?php else: ?>
													<?php 
														foreach($fees as $fee):
															$f_id = $fee['f_id'];
															$f_name = $fee['f_name'];
															$f_slug = $fee['f_slug'];
															$f_cost = $fee['f_cost'];
														
													?>
													<div id="f_id-<?=$f_id;?>" class="cell medium-6">
														<input type="hidden" readonly name="f_id[]" value="<?=$f_id;?>">														
														<div class="input-group">
															<input type="text" name="f_name[]" class="input-group-field" value="<?=$f_name;?>">
															<input class="input-group-field" name="f_cost[]" type="number" value="<?=$f_cost;?>">
															<input id="<?=$f_id;?>" data-table="fees" data-column="f_id" type="button" class="input-group-button delExistingItem" name="" value="x">
														</div>
													</div>
													<?php endforeach; ?>
													<?php endif; ?>
												</div>
											</div>
										</div>
										<div class="callout cell medium-12">
											<h3>Pickp Locations <button type="button" class="button float-right addNewPL">+</button></h3>
											<hr>

											<div class="grid-container">
												<div class="grid-x grid-margin-x grid-margin-y newly_added_pl_item">

												<?php if(empty($pls)): ?>
													<div class="medium-4 text-center cell callout noPLNotice">
														<h2>No added pickup locations</h2>
													</div>
													<?php else: ?>
													<?php 
														foreach($pls as $pl):
															$pl_id = $pl['pl_id'];
															$mf_id = $pl['mf_id'];
															$pl_est_name = $pl['pl_est_name'];
															$pl_est_address = $pl['pl_est_address'];
															$pl_phone = $pl['pl_phone'];
														
													?>
													<div id="pl_id-<?=$pl_id;?>" class="medium-4 cell callout">
														<h3>
														<input type="hidden" readonly name="pl_id[]" value="<?=$pl_id;?>">														
														<button id="<?=$pl_id;?>" data-table="pickup_location" data-column="pl_id" type="button" class="button small float-right delExistingItem">x</button></h3><hr>
														<ul>
															<li>Est. Name:
																<input type="text" name="pl_est_name[]" value="<?=$pl_est_name;?>">
															</li>
															<li>Est. Address:
																<input type="text" name="pl_est_address[]" value="<?=$pl_est_address;?>">
															</li>
															<li>Est. Phone:
																<input type="text" name="pl_phone[]" value="<?=$pl_phone;?>">
															</li>
															 

															<li>Est. Photo<br>
																<?php 
																	$query = $this->db->query('SELECT * FROM media_files WHERE mf_id = "'.$mf_id.'" ');
																	$row = $query->row();
																	$check_num_rows = $query->num_rows();
																	
																?>
																<?php if($mf_id != '0') : ?>
																	<input class="plPhoto" required type="number" name="pl_est_photo[]" value="<?=$mf_id;?>">						
																	<button type="button" class="button small addFeaturedImageModal" data-open="openMediaFiles"><?php if($mf_id == '0') { echo 'Add Photo'; } else{ echo 'Change';} ?></button>
																	<?php if($check_num_rows >= 1): ?>
																	<div class="featuredImageDiv">
																		<img src="<?=base_url('uploads/ar_');?><?=$row->mf_file_name; ?>">
																	</div>
																	<?php else: ?>
																	<div class="featuredImageDiv">
																		<br><span class="label alert">Can't find the image. <br>Please check image location.</span>
																		<img src="https://placehold.it/200x200&amp;text=No Available Image!" alt="<?=$pl_est_name;?>"> 
																	</div>
																	<?php endif; ?>
																<?php else: ?>
																<input required type="number" name="pl_est_photo[]" value="<?=$mf_id;?>">						
																	<button type="button" class="button small addFeaturedImageModal" data-open="openMediaFiles"><?php if($mf_id == '0') { echo 'Add Photo'; } else{ echo 'Change';} ?></button>
															<?php endif; ?>
															</li>

															
														</ul>
													</div>
													<?php endforeach; ?>
													<?php endif; ?>
												</div>
											</div>
										</div>
										<div class="cell medium-12">
											<button type="submit" class="button float-right">Save Delivery / Web Fees & Pickup Locations</button>
										</div>
									</div>
								</div>
							</form>
						</div>
						<div class="tabs-panel" id="imagesSettingsTab">
							<form id="imageSettings" data-abide novalidate>
								<div class="grid-container"> 
									<div class="grid-x grid-margin-x grid-margin-y">
										<div class="cell medium-12">
											<div class="input-group">
												<span class="input-group-label">Upload Folder:</span>
												<input value="<?=$sg['sg_upload_path']; ?>" name="upload_folder" class="input-group-field" type="text" aria-describedby="helpText" placeholder="uploads">
												
											</div>
												<p class="help-text" id="helpText">No trailing slashes. Default is "uploads"</p>
										</div>
										<div class="cell medium-12">
											<h2>Thumb Image</h2>
										</div>
										<div class="cell medium-6">
											<div class="input-group">
												<span class="input-group-label" >Max. Height:</span>
												<input value="<?=$sg['sg_thumb_max_height']; ?>" name="ti_height" class="input-group-field" type="number" aria-describedby="helpText">
												<span class="input-group-label">px</span>
											</div>
											<p class="help-text" id="helpText">ex: (1024 x 5) 5,000px</p>
										</div>
										<div class="cell medium-6">
											<div class="input-group">
												<span class="input-group-label" >Max. Width:</span>
												<input value="<?=$sg['sg_thumb_max_width']; ?>" name="ti_width" class="input-group-field" type="number" aria-describedby="helpText">
												<span class="input-group-label">px</span>
											</div>
											<p class="help-text" id="helpText">ex: (1024 x 5) 5,000px</p>
										</div>
										<div class="cell medium-12">
											<h2>Aspect Ratio Image</h2>
										</div>
										<div class="cell medium-6 large-3">
											<div class="input-group">
												<span class="input-group-label">Max. Height:</span>
												<input value="<?=$sg['sg_ari_max_height']; ?>" name="ar_max_height" class="input-group-field" type="number" aria-describedby="helpText">
												<span class="input-group-label">px</span>
											</div>
											<p class="help-text" id="helpText">ex: (1024 x 5) 5,000px</p>
										</div>
										<div class="cell medium-6 large-3">
											<div class="input-group">
												<span class="input-group-label">Max. Width:</span>
												<input value="<?=$sg['sg_ari_max_width']; ?>" name="ar_max_width" class="input-group-field" type="number" aria-describedby="helpText">
												<span class="input-group-label">px</span>
												
											</div>
											<p class="help-text" id="helpText">ex: (1024 x 5) 5,000px</p>
										</div>
										<div class="cell medium-6 ">
											<div class="input-group">
												<span class="input-group-label">Aspect Ratio Prefix:</span>
												<input value="<?=$sg['sg_ari_prefix']; ?>" name="ar_prefix" class="input-group-field" type="text" aria-describedby="helpText">
												
											</div>
											<p class="help-text" id="helpText">ex: ar_</p>
										</div>
										<div class="cell medium-12">
											<button type="submit" class="button float-right">Save Image Settings</button>
										</div>
									</div>
								</div>
							</form>
						</div>

					</div>
				</div>
				<div class="tabs-panel" id="es">
					<p>Email Settings</p>
					<hr>
					<form id="emailSettings" data-abide novalidate>
						<div class="grid-container">
							<div class="grid-x grid-margin-y grid-margin-x">
								<div class="callout cell medium-12">
									<div class="input-group">
										<span class="input-group-label">Admin Email Address:</span>
										<input value="<?=$sg['sg_admin_email'];?>" name="admin_email" class="input-group-field" type="email">
										
									</div>
								</div>
								<div class="cell medium-12">
									<h2 class="text-center">Email Templates</h2>
									<button type="submit" class="button float-right">Save Email Settings</button>
								</div>

								<div class="cell medium-12">
									<ul class="tabs" data-responsive-accordion-tabs="tabs medium-accordion large-tabs" id="email_settings_tab">
										<li class="tabs-title is-active"><a href="#globalEmailTab" aria-selected="true">Global Email Settings</a></li>
										<li class="tabs-title"><a href="#adminEmailTab" aria-selected="true">Admin Emails</a></li>
										<li class="tabs-title"><a href="#custEmailTab">Customer Copy</a></li>
									</ul>
									<div class="tabs-content" data-tabs-content="email_settings_tab">
										<div class="tabs-panel is-active" id="globalEmailTab">
											<div class="grid-container">
												<div class="grid-x grid-margin-y grid-margin-x">
													<div class="cell medium-4">
														<h3>Validation Link</h3>
														<textarea name="val_link" id="tinymce13" rows="10" class="input-group-field"><?=$et['ges_val_link'];?></textarea>
													</div>				
													
												</div>	
											</div>	
										</div>
										<div class="tabs-panel" id="adminEmailTab">
											<div class="grid-container">
												<div class="grid-x grid-margin-y grid-margin-x">
													<div class="cell medium-4">
														<h3>Registration Received</h3>
														<textarea name="admin_reg_rec" id="tinymce1" rows="10" class="input-group-field"><?=$et['ae_reg_received'];?></textarea>
													</div>				
													<div class="cell medium-4">
														<h3>Payment Pending</h3>
														<textarea name="admin_payment_pending" id="tinymce2" rows="10" class="input-group-field"><?=$et['ae_payment_pending'];?></textarea>	
													</div>				
													<div class="cell medium-4">	
														<h3>Payment Complete</h3>
														<textarea name="admin_payment_completed" id="tinymce3" rows="10" class="input-group-field"><?=$et['ae_payment_complete'];?></textarea>
													</div>				
													<div class="cell medium-4">		
														<h3>Payment Refunded</h3>
														<textarea name="admin_payment_refunded" id="tinymce4" rows="10" class="input-group-field"><?=$et['ae_payment_refunded'];?></textarea>
													</div>				
													<div class="cell medium-4">		
														<h3>Payment Cancelled</h3>
														<textarea name="admin_payment_cancelled" id="tinymce5" rows="10" class="input-group-field"><?=$et['ae_payment_cancelled'];?></textarea>
													</div>		
													<div class="cell medium-4">		
														<h3>Delivery Completed</h3>
														<textarea name="admin_del_completed" id="tinymce6" rows="10" class="input-group-field"><?=$et['ae_delivery_completed'];?></textarea>
													</div>	
													<div class="cell medium-4">
														<h3>Password Reset</h3>
														<textarea name="admin_pass_reset" id="tinymce14" rows="10" class="input-group-field"><?=$et['ae_pass_reset'];?></textarea>
													</div>	
													<!-- <div class="cell medium-12">
														<button type="submit" class="button float-right">Save Admin Email Settings</button>
													</div> -->
												</div>	
											</div>	
										</div>
										<div class="tabs-panel" id="custEmailTab">
											<div class="grid-container">
												<div class="grid-x grid-margin-y grid-margin-x">
													<div class="cell medium-4">
														<h3>Registration Received - Cust. copy</h3>
														<textarea name="cust_reg_rec" id="tinymce7" rows="10" class="input-group-field"><?=$et['cust_reg_received'];?></textarea>
													</div>				
													<div class="cell medium-4">
														<h3>Payment Pending - Cust. copy</h3>
														<textarea name="cust_payment_pending" id="tinymce8" rows="10" class="input-group-field"><?=$et['cust_payment_pending'];?></textarea>	
													</div>				
													<div class="cell medium-4">	
														<h3>Payment Complete - Cust. copy</h3>
														<textarea name="cust_payment_completed" id="tinymce9" rows="10" class="input-group-field"><?=$et['cust_payment_complete'];?></textarea>
													</div>				
													<div class="cell medium-4">		
														<h3>Payment Refunded - Cust. copy</h3>
														<textarea name="cust_payment_refunded" id="tinymce10" rows="10" class="input-group-field"><?=$et['cust_payment_refunded'];?></textarea>
													</div>				
													<div class="cell medium-4">		
														<h3>Payment Cancelled - Cust. copy</h3>
														<textarea name="cust_payment_cancelled" id="tinymce11" rows="10" class="input-group-field"><?=$et['cust_payment_cancelled'];?></textarea>
													</div>		
													<div class="cell medium-4">		
														<h3>Delivery Completed - Cust. copy</h3>
														<textarea name="cust_del_completed" id="tinymce12" rows="10" class="input-group-field"><?=$et['cust_delivery_completed'];?></textarea>
													</div>
													<div class="cell medium-4">
														<h3>Password Reset - Cust. copy</h3>
														<textarea name="cust_pass_reset"  id="tinymce15" rows="10" class="input-group-field"><?=$et['cust_pass_reset'];?></textarea>
													</div>	
													<!-- <div class="cell medium-12">
														<button type="submit" class="button float-right">Save Cust. Copy Email Settings</button>
													</div>	 -->
												</div>	
											</div>	
										</div>
										
									</div>
								</div>
								<div class="cell medium-12">
									<button type="submit" class="button float-right">Save Email Settings</button>
								</div>
							</div>
						</div>
					</form>
				</div>

			</div>
		</div>

	</div>
</div>

<?php $this->load->view('templates/media-items') ?>
</div>
</section>

<script>
	$(document).ready(function () {

	

		//delExistingItem
		$('body').on('click', '.delExistingItem', function(){
			var dataTable = $(this).attr('data-table');
			var dataColumn = $(this).attr('data-column');
			var id = $(this).attr('id');
			var delItem = $('#'+dataColumn+'-'+id);
			console.log(id);

			$.ajax({
				url: '<?php echo base_url('members/ajax/do/remove_item')?>',
				type: "POST",
				dataType: "json",
				data: 'id='+id+'&dataColumn='+dataColumn+'&dataTable='+dataTable,
				success: function (data) {
				 
					if (data.status == 'Success!') {
						$('.status-messages').html(
						'<div class="success callout cell large-12" data-closable > <h3>'+data.msg+'</h3><button class="close-button" aria-label="Dismiss alert" type="button" data-close><span aria-hidden="true">&times;</span></button></div>'
						);
						
						delItem.fadeOut();
						setTimeout(() => {
							delItem.remove();
						}, 1000);

					console.log(data);
					}
					
					
					if (data.status == 'Error!') {
						$('.status-messages').html(
						'<div class="alert callout cell large-12" data-closable > <h3>'+data.msg+'</h3><button class="close-button" aria-label="Dismiss alert" type="button" data-close><span aria-hidden="true">&times;</span></button></div>'
						);
					console.log(data);
					}
					$('html,body').animate({ scrollTop: 0}, 'slow');

				},
				error: function (data) {
					console.log('Error:'+ data);
					if (data.status == 'Error!'){
					$('.stat-messages-modal').html(
						'<div class="alert callout cell large-12" data-closable >ERROR:<hr> <h3>There\'s an error response from the database.</h3><button class="close-button" aria-label="Dismiss alert" type="button" data-close><span aria-hidden="true">&times;</span></button></div>'
						);
					}
				}	
			});	
		});
		
		//del newly added item
		$('body').on('click', '.delNewlyAddedItem', function(){
			$(this).parent().parent().remove();
		});
		
		var counter = 2;

		//addNewPL
		var newlyAddedPLItem   = $(".newly_added_pl_item"); 
		var addNewPLButton      = $(".addNewPL");
		var noPLNotice   = $(".noPLNotice");
		
		addNewPLButton.click(function () {
			noPLNotice.remove();
			if (counter > 50) {
				alert("Only 50 pickup locations allowed.");
				return false;
			}

			var newPLItem_ = $(document.createElement('div'))
				.attr("id", 'newPLItem_' + counter)
				.attr("class",'medium-4 cell callout');

			newPLItem_.after().html('<h3><button type="button" class="button float-right delNewlyAddedItem">x</button></h3><hr><ul><li>Est. Name:<input type="text" name="pl_est_name[]" placeholder="Est. Name"></li><li>Est. Address:<input type="text" name="pl_est_address[]" placeholder="Est. Address"></li><li>Est. Phone:<input type="text" name="pl_phone[]" placeholder="Est. Phone"></li><li>Est. Photo<br><input required type="number" name="pl_est_photo[]" class="plPhoto" value="0"><button type="button" class="button small addFeaturedImageModal" data-open="openMediaFiles">Add Photo</button></li></ul>');
			newPLItem_.appendTo(newlyAddedPLItem);
			counter++;
		});

		//addNewFee
		var newlyAddedFeeItem   = $(".newly_added_fee_item"); 
		var addNewFeeButton      = $(".addNewFee");
		var noFeeNotice   = $(".noFeeNotice");
		
		addNewFeeButton.click(function () {
			noFeeNotice.remove();
			if (counter > 20) {
				alert("Only 20 fees allowed");
				return false;
			}

			var newFeeItem_ = $(document.createElement('div'))
				.attr("id", 'newFeeItem_' + counter)
				.attr("class",'medium-6 cell');

			newFeeItem_.after().html('<input type="hidden" readonly name="f_id[]" value="0"><div class="input-group"><input type="text" placeholder="fee name" name="f_name[]" class="input-group-field"><input class="input-group-field" placeholder="fee cost" name="f_cost[]" type="number"><input data-table="fees" data-column="f_id" type="button" class="input-group-button delNewlyAddedItem" name="" value="x"></div>');
			newFeeItem_.appendTo(newlyAddedFeeItem);
			counter++;
		});

		//addNewBank
		var newlyAddedBankItem   = $(".newly_added_bank_item"); 
		var addNewBankButton      = $(".addNewBank");
		var delBankButton   = $(".delBank");
		var noBankNotice   = $(".noBankNotice");
		
		addNewBankButton.click(function () {
			noBankNotice.remove();
			if (counter > 10) {
				alert("Only 10 banks allowed");
				return false;
			}

			var newBankItem_ = $(document.createElement('div'))
				.attr("id", 'newBankItem_' + counter)
				.attr("class",'medium-4 cell callout');

			newBankItem_.after().html('<h3><input type="hidden" readonly name="bank_id[]" value="0"><button type="button" class="button float-right delNewlyAddedItem">x</button></h3> <hr><ul><li>Bank Name:<input type="text" name="bank_name[]" value=""></li><li>Account Name:<input type="text" name="acc_name[]" value=""></li><li>Account Number:<input type="text" name="acc_number[]" value=""></li><li>Type:<input type="text" name="acc_type[]" value=""></li><li>Bank Photo<br><input class="bpoPhoto" required type="number" name="bpo_photo[]" value=""><button type="button" class="button small addFeaturedImageModal" data-open="openMediaFiles">Add Photo</button></li>	</ul>');
			newBankItem_.appendTo(newlyAddedBankItem);


			counter++;
		});
		
		//addNewMoneyTransfer
		var newlyAddedMtItem   = $(".newly_added_mt_item"); 
		var addNewMtButton      = $(".addMt");
		var delMtButton   = $(".delMt");
		var noMtNotice   = $(".noMtNotice");
		
		addNewMtButton.click(function () {
			noMtNotice.remove();
			if (counter > 10) {
				alert("Only 10 money transfers allowed");
				return false;
			}

			var newMtItem_ = $(document.createElement('div'))
				.attr("id", 'newMtItem_' + counter)
				.attr("class",'medium-4 cell callout');

			newMtItem_.after().html('<h3><input type="hidden" readonly name="mto_id[]" value="0"><button type="button" class="button float-right delNewlyAddedItem">x</button></h3> <hr><ul><li>Establishment Name:<input type="text" name="mto_est_name[]" value=""></li><li>Receiver\'s Name:<input type="text" name="mto_rec_name[]" value=""></li><li>Mobile Number:<input type="text" name="mto_mobile_number[]" value=""></li><li>Address:<input type="text" name="mto_address[]" value=""></li><li>Money Transfer Photo<br><input class="mtoPhoto" required type="number" name="mto_photo[]" value=""><button type="button" class="button small addFeaturedImageModal" data-open="openMediaFiles">Add Photo</button></li></ul>');
			newMtItem_.appendTo(newlyAddedMtItem);


			counter++;
		});

		
		// delMtButton.click(function () {
		// 	if (counter == 1) {
		// 		alert("No more money transfers to remove");
		// 		return false;
		// 	}
		// 	counter--;
		// 	$("#newMtItem_"	 + counter).remove();
		// });

		//emailSettings
		$("#emailSettings").on("submit", function (ev) {
			ev.preventDefault();
			tinyMCE.triggerSave();

			$.ajax({
				url: '<?php echo base_url('members/ajax/do/settings_email')?>',
				type: "POST",
				dataType: "json",
				data: $("#emailSettings").serialize(),
				success: function (data) {

					if (data.status == 'Success!') {
						$('.status-messages').html(
							'<div class="success callout cell large-12" data-closable > <h3>'+data.msg+'</h3><button class="close-button" aria-label="Dismiss alert" type="button" data-close><span aria-hidden="true">&times;</span></button></div>'
						);
					}
					else if (data.status == 'Error!') {
						$('.status-messages').html(
							'<div class="alert callout cell large-12" data-closable > <h3>'+data.msg+'</h3><button class="close-button" aria-label="Dismiss alert" type="button" data-close><span aria-hidden="true">&times;</span></button></div>'
						);
					}
					else if (data.status == 'Invalid!') {
						$('.status-messages').html(
						'<div class="alert callout cell large-12" data-closable > <h3>'+data.msg+'</h3><hr><ul class="error-wrapper"></ul><button class="close-button" aria-label="Dismiss alert" type="button" data-close><span aria-hidden="true">&times;</span></button></div>'
						);
						
						if(data.admin_email != ''){ $('.error-wrapper').append('<li>'+ data.admin_email +'</li>'  ); }
						// if(data.admin_reg_rec != ''){ $('.error-wrapper').append('<li>'+ data.admin_reg_rec +'</li>'  ); }
						// if(data.admin_payment_pending != ''){ $('.error-wrapper').append('<li>'+ data.admin_payment_pending +'</li>'  ); }
						// if(data.admin_payment_completed != ''){ $('.error-wrapper').append('<li>'+ data.admin_payment_completed +'</li>'  ); }
						// if(data.admin_payment_refunded != ''){ $('.error-wrapper').append('<li>'+ data.admin_payment_refunded +'</li>'  ); }
						// if(data.admin_payment_cancelled != ''){ $('.error-wrapper').append('<li>'+ data.admin_payment_cancelled +'</li>'  ); }
						// if(data.admin_del_completed != ''){ $('.error-wrapper').append('<li>'+ data.admin_del_completed +'</li>'  ); }
						
						// if(data.cust_pass_reset != ''){ $('.error-wrapper').append('<li>'+ data.cust_pass_reset +'</li>'  ); }
						// if(data.cust_reg_rec != ''){ $('.error-wrapper').append('<li>'+ data.cust_reg_received +'</li>'  ); }
						// if(data.cust_payment_pending != ''){ $('.error-wrapper').append('<li>'+ data.cust_payment_pending +'</li>'  ); }
						// if(data.cust_payment_completed != ''){ $('.error-wrapper').append('<li>'+ data.cust_payment_complete +'</li>'  ); }
						// if(data.cust_payment_refunded != ''){ $('.error-wrapper').append('<li>'+ data.cust_payment_refunded +'</li>'  ); }
						// if(data.cust_payment_cancelled != ''){ $('.error-wrapper').append('<li>'+ data.cust_payment_cancelled +'</li>'  ); }
						// if(data.cust_del_completed != ''){ $('.error-wrapper').append('<li>'+ data.cust_delivery_completed +'</li>'  ); }
						

					console.log(data);
					}
					else {
						$('.status-messages').html(
							'<div class="alert callout cell large-12" data-closable > <h3>'+data.msg+'</h3><button class="close-button" aria-label="Dismiss alert" type="button" data-close><span aria-hidden="true">&times;</span></button></div>'
						);
					}

					$('html,body').animate({
						scrollTop: 0
					}, 'slow');

					console.log(data);
				},
				error: function (data) {
					console.log(data);
					$('.status-messages').html(
						'<div class="alert callout cell large-12" data-closable >DATABASE ERROR:<hr> <h3>' + data.responseText +
						'</h3><button class="close-button" aria-label="Dismiss alert" type="button" data-close><span aria-hidden="true">&times;</span></button></div>'
					);
					$('html,body').animate({
						scrollTop: 0
					}, 'slow');
				}

			});
		});

		//imageSettings
		$("#imageSettings").on("submit", function (ev) {
			ev.preventDefault();
			// var eventId = $(this).attr('id');

			$.ajax({
				url: '<?php echo base_url('members/ajax/do/settings_images')?>',
				type: "POST",
				dataType: "json",
				data: $("#imageSettings").serialize(),
				success: function (data) {

					if (data.status == 'Success!') {
						$('.status-messages').html(
							'<div class="success callout cell large-12" data-closable > <h3>'+data.msg+'</h3><button class="close-button" aria-label="Dismiss alert" type="button" data-close><span aria-hidden="true">&times;</span></button></div>'
						);
					}
					else if (data.status == 'Error!') {
						$('.status-messages').html(
							'<div class="alert callout cell large-12" data-closable > <h3>'+data.msg+'</h3><button class="close-button" aria-label="Dismiss alert" type="button" data-close><span aria-hidden="true">&times;</span></button></div>'
						);
					}
					else if (data.status == 'Invalid!') {
						$('.status-messages').html(
						'<div class="alert callout cell large-12" data-closable > <h3>'+data.msg+'</h3><hr><ul class="error-wrapper"></ul><button class="close-button" aria-label="Dismiss alert" type="button" data-close><span aria-hidden="true">&times;</span></button></div>'
						);
						
						if(data.paypal_email != ''){ $('.error-wrapper').append('<li>'+ data.paypal_email +'</li>'  ); }
						

					console.log(data);
					}
					else {
						$('.status-messages').html(
							'<div class="alert callout cell large-12" data-closable > <h3>'+data.msg+'</h3><button class="close-button" aria-label="Dismiss alert" type="button" data-close><span aria-hidden="true">&times;</span></button></div>'
						);
					}

					$('html,body').animate({
						scrollTop: 0
					}, 'slow');

					console.log(data);
				},
				error: function (data) {
					console.log(data);
					$('.status-messages').html(
						'<div class="alert callout cell large-12" data-closable >DATABASE ERROR:<hr> <h3>' + data.responseText +
						'</h3><button class="close-button" aria-label="Dismiss alert" type="button" data-close><span aria-hidden="true">&times;</span></button></div>'
					);
					$('html,body').animate({
						scrollTop: 0
					}, 'slow');
				}

			});
		});

		//payments
		$("#payments").on("submit", function (ev) {
			ev.preventDefault();
			// var eventId = $(this).attr('id');

			$.ajax({
				url: '<?php echo base_url('members/ajax/do/settings_payments')?>',
				type: "POST",
				dataType: "json",
				data: $("#payments").serialize(),
				success: function (data) {

					if (data.status == 'Success!') {
						$('.status-messages').html(
							'<div class="success callout cell large-12" data-closable > <h3>'+data.msg+'</h3><button class="close-button" aria-label="Dismiss alert" type="button" data-close><span aria-hidden="true">&times;</span></button></div>'
						);
					}
					else if (data.status == 'Error!') {
						$('.status-messages').html(
							'<div class="alert callout cell large-12" data-closable > <h3>'+data.msg+'</h3><button class="close-button" aria-label="Dismiss alert" type="button" data-close><span aria-hidden="true">&times;</span></button></div>'
						);
					}
					else if (data.status == 'Invalid!') {
						$('.status-messages').html(
						'<div class="alert callout cell large-12" data-closable > <h3>'+data.msg+'</h3><hr><ul class="error-wrapper"></ul><button class="close-button" aria-label="Dismiss alert" type="button" data-close><span aria-hidden="true">&times;</span></button></div>'
						);
						
						if(data.paypal_email != ''){ $('.error-wrapper').append('<li>'+ data.paypal_email +'</li>'  ); }
						// if(data.bank_name != ''){ $('.error-wrapper').append('<li>'+ data.bank_name +'</li>'  ); }
						// if(data.acc_name != ''){ $('.error-wrapper').append('<li>'+ data.acc_name +'</li>'  ); }
						// if(data.acc_number != ''){ $('.error-wrapper').append('<li>'+ data.acc_number +'</li>'  ); }
						// if(data.acc_type != ''){ $('.error-wrapper').append('<li>'+ data.acc_type +'</li>'  ); }
						

					console.log(data);
					}
					else {
						$('.status-messages').html(
							'<div class="alert callout cell large-12" data-closable > <h3>'+data.msg+'</h3><button class="close-button" aria-label="Dismiss alert" type="button" data-close><span aria-hidden="true">&times;</span></button></div>'
						);
					}

					$('html,body').animate({
						scrollTop: 0
					}, 'slow');

					console.log(data);
				},
				error: function (data) {
					console.log(data);
					$('.status-messages').html(
						'<div class="alert callout cell large-12" data-closable >DATABASE ERROR:<hr> <h3>' + data.responseText +
						'</h3><button class="close-button" aria-label="Dismiss alert" type="button" data-close><span aria-hidden="true">&times;</span></button></div>'
					);
					$('html,body').animate({
						scrollTop: 0
					}, 'slow');
				}

			});
		});
		
		//delivery
		$("#delivery").on("submit", function (ev) {
			ev.preventDefault();
			// var eventId = $(this).attr('id');

			$.ajax({
				url: '<?php echo base_url('members/ajax/do/settings_delivery')?>',
				type: "POST",
				dataType: "json",
				data: $("#delivery").serialize(),
				success: function (data) {

					if (data.status == 'Success!') {
						$('.status-messages').html(
							'<div class="success callout cell large-12" data-closable > <h3>'+data.msg+'</h3><button class="close-button" aria-label="Dismiss alert" type="button" data-close><span aria-hidden="true">&times;</span></button></div>'
						);
					}
					else if (data.status == 'Error!') {
						$('.status-messages').html(
							'<div class="alert callout cell large-12" data-closable > <h3>'+data.msg+'</h3><button class="close-button" aria-label="Dismiss alert" type="button" data-close><span aria-hidden="true">&times;</span></button></div>'
						);
					}
					else if (data.status == 'Invalid!') {
						$('.status-messages').html(
						'<div class="alert callout cell large-12" data-closable > <h3>'+data.msg+'</h3><hr><ul class="error-wrapper"></ul><button class="close-button" aria-label="Dismiss alert" type="button" data-close><span aria-hidden="true">&times;</span></button></div>'
						);
						
						if(data.delivery_fee != ''){ $('.error-wrapper').append('<li>'+ data.delivery_fee +'</li>'  ); }
						

					console.log(data);
					}
					else {
						$('.status-messages').html(
							'<div class="alert callout cell large-12" data-closable > <h3>'+data.msg+'</h3><button class="close-button" aria-label="Dismiss alert" type="button" data-close><span aria-hidden="true">&times;</span></button></div>'
						);
					}

					$('html,body').animate({
						scrollTop: 0
					}, 'slow');

					console.log(data);
				},
				error: function (data) {
					console.log(data);
					$('.status-messages').html(
						'<div class="alert callout cell large-12" data-closable >DATABASE ERROR:<hr> <h3>' + data.responseText +
						'</h3><button class="close-button" aria-label="Dismiss alert" type="button" data-close><span aria-hidden="true">&times;</span></button></div>'
					);
					$('html,body').animate({
						scrollTop: 0
					}, 'slow');
				}

			});
		});

		//upload time records
		$("#import_csv").on("submit", function (ev) {

			ev.preventDefault();

			//confirm the file name via modal
			var get_csv_file = $('input[name="csv_file').val();
			var csv_file = get_csv_file.replace(/^.*[\\\/]/, '');
			var txt;

			var r = confirm("Sure to upload the file: " + csv_file + " ?");
			if (r == true) {

				var r = confirm("You can't undo this. \nPlease make sure you are upload the correct file name.\n\nFile Name: " +
					csv_file + "\n\nAgain, are you sure this is correct? ");
				if (r == true) {
					txt = "Please wait while we upload the results.";

					$('#import_csv_btn').attr('disabled', true);
					$('#import_csv_btn').addClass('alert');
					$('#import_csv_btn').removeClass('success');

					//upload the csv
					$.ajax({
						url: '<?php echo base_url('members/ajax/do/upload_csv')?>',
						contentType: false,
						cache: false,
						processData: false,
						type: "POST",
						dataType: "json",
						// fileElementId	:'userfile',
						data: new FormData(this),
						beforeSend: function () {
							$('#import_csv_btn').html('Importing. Please wait...');
							$('.status-messages').html(
								'<div class="success callout cell large-12" data-closable > <h3>Importing. Please wait... DO NOT CLOSE OR REFRESH YOUR BROWSER</h3></div>'
							);
						},
						success: function (data) {


							if (data.status == 'Success!') {
								$('.status-messages').html(
									'<div class="success callout cell large-12" data-closable > <h3>' + data.msg + '</h3></div>'
								);

								$('#import_csv')[0].reset();
								$('#import_csv_btn').attr('disabled', false);
								$('#import_csv_btn').removeClass('alert');
								$('#import_csv_btn').addClass('success');
								$('#import_csv_btn').html('Import Done!');
								// if(data.img != ''){ $('.error-wrapper').append('<li>'+ data.img +'</li>'  ); }

								console.log(data);
							}


							if (data.status == 'Error!') {
								$('.status-messages').html(
									'<div class="alert callout cell large-12" data-closable > <h3>' + data.msg +
									'</h3><button class="close-button" aria-label="Dismiss alert" type="button" data-close><span aria-hidden="true">&times;</span></button></div>'
								);

								console.log(data);

								$('#import_csv')[0].reset();
								$('#import_csv_btn').attr('disabled', false);
								$('#import_csv_btn').removeClass('alert');
								$('#import_csv_btn').removeClass('success');
								$('#import_csv_btn').html('Import Time Records');
							}


							$('html,body').animate({
								scrollTop: 0
							}, 'slow');

						},
						error: function (data) {
							console.log(data);
							$('.status-messages').html(
								'<div class="alert callout cell large-12" data-closable >ERROR:<hr> <h3>' + data.msg +
								'</h3><button class="close-button" aria-label="Dismiss alert" type="button" data-close><span aria-hidden="true">&times;</span></button></div>'
							);
						}
					});
				} else {
					txt = "Make sure you are uploading the correct file.";
					$('.status-messages').html(
						'<div class="alert callout cell large-12" data-closable > <h3>' + txt + '</h3></div>'
					);
				}
			} else {
				txt = "Make sure you are uploading the correct file.";

				$('.status-messages').html(
					'<div class="alert callout cell large-12" data-closable > <h3>' + txt + '</h3></div>'
				);
			}

		}); //upload time records

		//upload loop records
		$("#import_csv_loop_records").on("submit", function (ev) {

			ev.preventDefault();

			//confirm the file name via modal
			var get_csv_file = $('input[name="csv_file_loop_records').val();
			var csv_file = get_csv_file.replace(/^.*[\\\/]/, '');
			var txt;

			var r = confirm("Sure to upload the file: " + csv_file + " ?");
			if (r == true) {

				var r = confirm("You can't undo this. \nPlease make sure you are upload the correct file name.\n\nFile Name: " +
					csv_file + "\n\nAgain, are you sure this is correct? ");
				if (r == true) {
					txt = "Please wait while we upload the results.";

					$('#import_csv_btn_loop_records').attr('disabled', true);
					$('#import_csv_btn_loop_records').addClass('alert');
					$('#import_csv_btn_loop_records').removeClass('success');

					//upload the csv
					$.ajax({
						url: '<?php echo base_url('members/ajax/do/upload_csv_loop_records')?>',
						contentType: false,
						cache: false,
						processData: false,
						type: "POST",
						dataType: "json",
						// fileElementId	:'userfile',
						data: new FormData(this),
						beforeSend: function () {
							$('#import_csv_btn_loop_records').html('Importing. Please wait...');
						},
						success: function (data) {


							if (data.status == 'Success!') {
								$('.status-messages').html(
									'<div class="success callout cell large-12" data-closable > <h3>' + data.msg +
									'</h3><hr><ul class="error-wrapper"></ul></div>'
								);

								$('#import_csv_loop_records')[0].reset();
								$('#import_csv_btn_loop_records').attr('disabled', false);
								$('#import_csv_btn_loop_records').removeClass('alert');
								$('#import_csv_btn_loop_records').addClass('success');
								$('#import_csv_btn_loop_records').html('Import Done');


								console.log(data);
							}


							if (data.status == 'Error!') {
								$('.status-messages').html(
									'<div class="alert callout cell large-12" data-closable > <h3>' + data.msg +
									'</h3><button class="close-button" aria-label="Dismiss alert" type="button" data-close><span aria-hidden="true">&times;</span></button></div>'
								);

								$('#import_csv_loop_records')[0].reset();
								$('#import_csv_btn_loop_records').attr('disabled', false);
								$('#import_csv_btn_loop_records').removeClass('alert');
								$('#import_csv_btn_loop_records').removeClass('success');
								$('#import_csv_btn_loop_records').html('Import Loop Records');

								console.log(data);
							}


							$('html,body').animate({
								scrollTop: 0
							}, 'slow');

						},
						error: function (data) {
							console.log('Error:' + data);
							// if (data.status == 'Error!'){
							$('.status-messages').html(
								'<div class="alert callout cell large-12" data-closable >ERROR:<hr> <h3>There\'s an error response from the database.</h3><button class="close-button" aria-label="Dismiss alert" type="button" data-close><span aria-hidden="true">&times;</span></button></div>'
							);
							// }
						}
					});
				} else {
					txt = "Make sure you are uploading the correct file.";
					$('.status-messages').html(
						'<div class="alert callout cell large-12" data-closable > <h3>' + txt + '</h3></div>'
					);
				}
			} else {
				txt = "Make sure you are uploading the correct file.";

				$('.status-messages').html(
					'<div class="alert callout cell large-12" data-closable > <h3>' + txt + '</h3></div>'
				);
			}

		}); //upload loop records

	});

</script>
