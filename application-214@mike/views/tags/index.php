
<section id="main-content">
  <div id="guts">

	<div class="grid-container">
		<div class="grid-x grid-margin-y">
			<div class="cell medium-12">

			<?php  if ($events >= 1 || $lwc_events >= 1): ?>
				<div class="text-center">
					<h1 style="margin: 0;" class="text-center">
						<?php echo $title; ?>
					</h1>
				</div>

				<hr>
				<article class="grid-container">
					<div class="grid-x grid-margin-x small-up-3 medium-up-4 large-up-5">

						<?php  
							//race events
							if($events >= 1):
								/** Start Show all events based on tags */ 
								foreach ($events as $events_item): 
						?>

						<div class="cell">
							<?php 
							$eventStatus = $events_item['re_publish_status'];
							if($eventStatus == '0'){
								echo '<small class="floating-label">Draft</small>';
							}
							?>
							<a href="<?php echo base_url('events/'.$events_item['re_slug']); ?>">
								<?php
									//  echo $events_item['mf_id'];
									$mfId = $events_item['mf_id'];
									if($mfId != '0'): 
									$query = $this->db->query('SELECT * FROM media_files WHERE mf_id = "'.$events_item['mf_id'].'" ');
									$row = $query->row();
								?>
									<img src="<?=base_url('uploads/ar_').$row->mf_file_name;?>" alt="<?=$events_item['re_slug'];?>">
								<?php else: ?>
									<img src="https://placehold.it/300x300&amp;text=No Available Image!" alt="image for article">
								<?php endif; ?>
							</a>
							<br>
							<?php echo $events_item['re_name']; ?>
						</div>

						<?php 
							/** End Show all members */
								endforeach; 
							endif;
						?>
						
					  
						<?php  
							//lakwatcharity events
							if($lwc_events >= 1):
								/** Start Show all events based on tags */ 
								foreach ($lwc_events as $lwc_events_item): 
						?>

						<div class="cell">
							<?php 
							$eventStatus = $lwc_events_item['lwc_publish_status'];
							if($eventStatus == '0'){
								echo '<small class="floating-label">Draft</small>';
							}
							?>
							<a href="<?php echo base_url('lakwatcharity/'.$lwc_events_item['lwc_slug']); ?>">
								<?php
									//  echo $events_item['mf_id'];
									$mfId = $lwc_events_item['mf_id'];
									if($mfId != '0'): 
									$query = $this->db->query('SELECT * FROM media_files WHERE mf_id = "'.$lwc_events_item['mf_id'].'" ');
									$row = $query->row();
								?>
									<img src="<?=base_url('uploads/ar_').$row->mf_file_name;?>" alt="<?=$lwc_events_item['lwc_slug'];?>">
								<?php else: ?>
									<img src="https://placehold.it/300x300&amp;text=No Available Image!" alt="image for article">
								<?php endif; ?>
							</a>
							<br>
							<?php echo $lwc_events_item['lwc_name']; ?>
						</div>

						<?php 
							/** End Show all members */
								endforeach; 
							endif;
						?>
						
					</div>
				</article>
					
				 
					
					

			</div>
			<?php else: ?>

			<div class="cell medium-12">
				<div class="text-center">
					<h1 style="margin: 0;" class="text-center">
						No events using tag: "<?php echo str_replace('_', ' ', $tags); ?>"
					</h1>

				</div>
			</div>
			<?php endif; ?>

		</div>
	</div>

	</div>
</section>
