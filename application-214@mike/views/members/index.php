<?php

/*** Count all Admins */
$this->db->like('m_role','9');
$this->db->from('members');
$countAdmins = number_format($this->db->count_all_results());
     

/*** Count all Members */
$this->db->like('m_role','2');
$this->db->from('members');
$countMembers = number_format($this->db->count_all_results());

/*** Count all Family Members */
$this->db->like('is_main','0');
$this->db->from('family_friend_member');
$countFamilyMembers = number_format($this->db->count_all_results());

/*** Count all No roles */
$this->db->like('m_role','0');
$this->db->from('members');
$countNoRoles = number_format($this->db->count_all_results());
?>


<section id="main-content">
    <div id="guts" class="lw-members-wrap">

        <div class="grid-container">
            <div class="grid-x grid-margin-y">
                <div class="medium-12 cell">
                    <div class="text-center">
                        <h1 style="margin: 0;" class="text-center"><?php echo $title; ?></h1>
                    </div>

                    <hr>
                </div>
                <div class="medium-6 cell">
                    <table class="  ">
                        
                            <tr>
                                <td>Total: <?=$countAdmins+$countMembers+$countFamilyMembers;?></td>
                                <td>Admins: <?=$countAdmins;?></td>
                                <td>Members: <?=$countMembers+$countFamilyMembers;?></td>
                                <td>No Role: <?=$countNoRoles;?></td>
                            </tr>
                            

                        </tbody>
                    </table>
                </div>
                <div class="medium-9 cell">
                    <div class="small button-group">
                        <a class=" button hollow clear" >Filter View:</a> 
                        <a class="alert button toggle-vis" data-column="0">#</a>
                        <a class="alert button toggle-vis" data-column="1">Roles</a>
                        <a class="success button toggle-vis" data-column="2">Last Name</a>
                        <a class="success button toggle-vis" data-column="3">First Name</a>
                        <a class="success button toggle-vis" data-column="4">Middle Name</a>
                        <a class="success button toggle-vis" data-column="5">Email</a>
                        <a class="success button toggle-vis" data-column="6">Phone</a>
                        <a class="alert button toggle-vis" data-column="7">Address</a>
                        <a class="alert button toggle-vis" data-column="8">Clubs/Groups</a>
                        <a class="alert button toggle-vis" data-column="9">E.Contact</a>
                        <a class="success button toggle-vis" data-column="10">Actions</a>
                    </div>
                </div>
                <div class="medium-3 cell">
                    <a href="<?=base_url();?>members/add/new" class="button float-right">Add New Member</a>
                </div>
                <table id="memberList" class="hover" style="width: 100%;">
                    <thead>
                        <tr>
                            <th width="50">#</th>
                            <th width="200">Role</th>
                            <th width="300">Last Name</th>
                            <th width="300">First Name</th>
                            <th width="300">Middle Name</th>
                            <th width="300">Email</th>
                            <th width="200">Phone</th>
                            <th width="300">Address</th>
                            <th width="100">Club / Groups</th>
                            <th width="100">Emergency Contact</th>
                            <th width="100">Action</th>
                            
                        </tr>
                    </thead>
                    <tbody>


                    <?php
                    /** Start Show all members */
                    foreach ($members as $members_item): ?>
                        <tr>
                            <td><?php echo $members_item['m_id']; ?></td>
                            <td><?php echo get_role($members_item['m_role']); ?></td>
                            <td><a href="<?php echo base_url('members/'.$members_item['m_id']); ?>"><?php echo $members_item['m_last_name']; ?></a>,</td>
                            <td><?php echo $members_item['m_first_name']; ?> </td>
                            <td><?php echo $members_item['m_middle_name']; ?></td>
                            <td><?php echo $members_item['m_email']; ?></td>
                            <td><?php echo $members_item['m_mobile']; ?></td>
                            <td><?php echo $members_item['m_address']; ?></td>
                            <td><?php echo $members_item['m_clubs_groups']; ?></td>
                            <td>
                                <a href="#viewEmergencyContacts" class="" data-open="viewEmergencyContacts_<?=$members_item['m_id']; ?>">view</a>
                        
                            <!-- START viewEmergencyContacts -->
                            <div class="reveal" id="viewEmergencyContacts_<?=$members_item['m_id']; ?>" data-reveal>
                                <table class="hover">
                                    <tr>
                                        <td>Name:</td>
                                        <td><?php echo $members_item['m_last_name']; ?>, <?php echo $members_item['m_first_name']; ?> <?php echo $members_item['m_first_name']; ?></td>
                                    </tr>
                                    <tr>
                                        <td>User ID:</td>
                                        <td><?php echo $members_item['m_id']; ?></td>
                                    </tr>
                                    <tr>
                                        <td>BIB #:</td>
                                        <td>0001</td>
                                    </tr>
                                </table>
                    
                                <table class="hover">
                                        <h1 class="callout text-center">Emergency Contact Details</h1>
                                    <tr>
                                        <td>Name:</td>
                                        <td>Macalintal, Aileen</td>
                                    </tr>
                                    <tr>
                                        <td>Relation:</td>
                                        <td>friend</td>
                                    </tr>
                                    <tr>
                                        <td>Phone #:</td>
                                        <td>09257756169</td>
                                    </tr>
                                    <tr>
                                        <td>Email:</td>
                                        <td>test@email.com</td>
                                    </tr>
                                    
                                    <tr>
                                        <td>Address:</td>
                                        <td>ABC Streen</td>
                                    </tr>
                                    
                                    
                                </table>
                            </div>
                            <!-- END viewEmergencyContacts -->
                            </td>
                            <td><a class="button small" href="<?php echo base_url('members/edit/'.$members_item['m_id']); ?>">Edit</a></td>
                            
                        </tr>
                        <?php 
                    /** End Show all members */
                    endforeach; ?>

                    <?php
                    /** Start Show all family members */
                    foreach ($family_members as $family_members_item): ?>
                        <tr>
                            <td><?php echo $family_members_item['ffm_id']; ?></td>
                            <td><?php echo get_role(2); ?></td>
                            <td><a href="<?php echo base_url('members/'.$family_members_item['m_id']); ?>"><?php echo $family_members_item['ffm_last_name']; ?></a>,</td>
                            <td><?php echo $family_members_item['ffm_first_name']; ?> </td>
                            <td><?php echo $family_members_item['ffm_middle_name']; ?></td>
                            <td><?php echo $family_members_item['ffm_email']; ?></td>
                            <td><?php echo $family_members_item['ffm_mobile']; ?></td>
                            <td><?php echo $family_members_item['ffm_address']; ?></td>
                            <td><?php echo $family_members_item['ffm_clubs_groups']; ?></td>
                            <td>
                                <a href="#viewEmergencyContacts" class="" data-open="viewEmergencyContacts_<?=$family_members_item['ffm_id']; ?>">view</a>
                        
                            <!-- START viewEmergencyContacts -->
                            <div class="reveal" id="viewEmergencyContacts_<?=$family_members_item['ffm_id']; ?>" data-reveal>
                                <table class="hover">
                                    <tr>
                                        <td>Name:</td>
                                        <td><?php echo $family_members_item['ffm_last_name']; ?>, <?php echo $family_members_item['ffm_first_name']; ?> <?php echo $family_members_item['ffm_first_name']; ?></td>
                                    </tr>
                                    <tr>
                                        <td>User ID:</td>
                                        <td><?php echo $family_members_item['ffm_id']; ?></td>
                                    </tr>
                                    <tr>
                                        <td>BIB #:</td>
                                        <td>0001</td>
                                    </tr>
                                </table>
                    
                                <table class="hover">
                                        <h1 class="callout text-center">Emergency Contact Details</h1>
                                    <tr>
                                        <td>Name:</td>
                                        <td>Macalintal, Aileen</td>
                                    </tr>
                                    <tr>
                                        <td>Relation:</td>
                                        <td>friend</td>
                                    </tr>
                                    <tr>
                                        <td>Phone #:</td>
                                        <td>09257756169</td>
                                    </tr>
                                    <tr>
                                        <td>Email:</td>
                                        <td>test@email.com</td>
                                    </tr>
                                    
                                    <tr>
                                        <td>Address:</td>
                                        <td>ABC Streen</td>
                                    </tr>
                                    
                                    
                                </table>
                            </div>
                            <!-- END viewEmergencyContacts -->
                            </td>
                            <td><a class="button small" href="<?php echo base_url('members/edit/'.$members_item['m_id']); ?>">Edit</a></td>
                            
                        </tr>
                        <?php 
                    /** End Show all members */
                    endforeach; ?>
                    
                    </tbody>
                </table>
                <!-- <div class="large-12">
                    <button type="button" class="button float-right">Save</button>
                </div> -->
            </div>
        </div>
    </div>
</section>

 