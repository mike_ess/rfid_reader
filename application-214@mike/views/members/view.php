
<?php

//Personal Details
$userId = $members_item['m_id'];
$userName = $members_item['m_username'];
$firstName = ucfirst($members_item['m_first_name']);
$lastName = ucfirst($members_item['m_last_name']);
$middleName = ucfirst($members_item['m_middle_name']);
$username = ucfirst($members_item['m_username']);
$role = $members_item['m_role'];
$status = $members_item['m_status'];
$gender = ucfirst($members_item['m_gender']);
$address = ucfirst($members_item['m_address']);
$mobile_phone = $members_item['m_mobile'];
$email = $members_item['m_email'];
$age_group = $members_item['m_age_group'];
$clubs_group = ucfirst($members_item['m_clubs_groups']);
$photo = $members_item['m_photo'];
$created_time = $members_item['m_created_time'];
$updated_time = $members_item['m_updated_time'];
$last_login = $members_item['m_last_login_time'];

//Emergency Contact Details
$ecd_firstName = ucfirst($ecd_item['ecd_first_name']);
$ecd_lastName = ucfirst($ecd_item['ecd_last_name']);
$ecd_middleName = ucfirst($ecd_item['ecd_middle_name']);
$ecd_relationship = ucfirst($ecd_item['ecd_relationship']);
$ecd_address = ucfirst($ecd_item['ecd_address']);
$ecd_mobile = $ecd_item['ecd_mobile'];
$ecd_email = $ecd_item['ecd_email'];



?>

<section id="main-content">
  <div id="guts" class="lw-members-wrap">

	<div class="grid-container">
		<div class="grid-x grid-margin-x grid-margin-y">
			<div class="cell medium-12">
				
					<div class="grid-container">
						<div class="grid-x grid-margin-x">

							<div class="cell medium-9">
								<h2><?php if ($_SESSION['sess_user_email'] == $email){ echo 'Your Account';} else{ echo 'Account of: ';}?> <span style="font-style:italic;"><?=$lastName;?>, <?=$firstName;?> <?=$middleName;?></span></h2>
							</div>
							<?php if( isset($_SESSION['isAdmin'])) : ?> <div class="cell medium-3">
								<a href="<?=base_url('members');?>" class="button float-right">Back To Member List</a>
							</div><?php endif;?>


							<div class="cell medium-8 medium-offset-2">
								<div class="registration-error-messages"> </div>
							</div>
							<div class="cell medium-12">
								<ul class="tabs " data-deep-link="true" data-update-history="true" data-deep-link-smudge="true" data-deep-link-smudge-delay="500"
								data-tabs id="deeplinked-tabs">
									<li class="tabs-title is-active">
										<a href="#participated_events" aria-selected="true">Participated Events</a>
									</li>
									<li class="tabs-title">
										<a href="#member_info">Member Info.</a>
									</li>
									<li class="tabs-title">
										<a href="#reset_password">Change Password</a>
									</li>
									<!-- <li class="tabs-title">
										<a href="#claiming_details">Claiming Details</a>
									</li>
									<li class="tabs-title">
										<a href="#review_information">Review Info.</a>
									</li> -->
								</ul>

								<div class="tabs-content" data-tabs-content="deeplinked-tabs">
									<div class="tabs-panel is-active" id="participated_events">
										<div class="grid-container">
											<div class="grid-x grid-margin-x grid-margin-y" data-equalizer>
												
												<div class="medium-12 cell">
													<div class="" data-equalizer-watch>
														
														<table class="hover" id="participatedEvents">
															<thead>
																<tr>
																	<td>Event Name</td>
																	<td>Bib Number</td>
																	<td>Race Category</td>
																	<?php if(isset($_SESSION['isAdmin']) || isset($_SESSION['isAuthor'])): ?><td>Action</td><?php endif;?>
																</tr>
															</thead>
															<tbody>
																<?php foreach ($participated_event_single as $participated_events_single): ?>
																	<tr id="<?=$participated_events_single['p_id'];?>">
																		<td><?=$participated_events_single['re_name'];?></td>
																		<td><?=fix_bib_number($participated_events_single['bib_assigned_number']);?></td>
																		<td><?=$participated_events_single['rc_name'];?></td>
																		<?php if(isset($_SESSION['isAdmin']) || isset($_SESSION['isAuthor'])): ?><td></td><?php endif;?>
																	</tr>
																<?php endforeach; ?>
																<tr>
																	<td colspan="6"><hr></td>
																</tr>
																<tr>
																	<td colspan="6" class="text-center bold-font">Runner(s) added under <?php if ($_SESSION['sess_user_email'] == $email){ echo 'your';} else{ echo 'this';}?> account.</td>
																</tr>
																<tr>
																	<?php if ($participated_event) : ?><td class="bold-font">Runner's Name</td><?php endif;?>
																	<td class="bold-font">Event Name</td>
																	<td class="bold-font">Bib Number</td>
																	<td class="bold-font">Race Category</td>
																	<?php if(isset($_SESSION['isAdmin']) ): ?><td class="bold-font">Action</td><?php endif;?>
																</tr>
																<?php foreach ($participated_event_ffm as $participated_events_ffm): ?>
																	<tr id="<?=$participated_events_ffm['p_id'];?>">
																		<td><?php
																		//  echo $participated_events_ffm['ffm_id']; 
																		 echo $participated_events_ffm['ffm_last_name'].', '.$participated_events_ffm['ffm_first_name'];?></td>
																		<td><?=$participated_events_ffm['re_name'];?></td>
																		<td><?=fix_bib_number($participated_events_ffm['bib_assigned_number']);?></td>
																		<td><?=$participated_events_ffm['rc_name'];?></td>
																		<?php if(isset($_SESSION['isAdmin']) || isset($_SESSION['isAuthor'])): ?><td></td><?php endif;?>
																	</tr>
																<?php endforeach; ?>
 																
															</tbody>
														</table>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="tabs-panel" id="member_info">
                                        <form data-abide novalidate id="reg_form">
                                            <div class="grid-container">
												<div class="grid-x grid-margin-x grid-margin-y">

														<h2 class="cell large-12">Personal Contact Details</h2>
														<div class="medium-3 cell">
															<label>
																Username:
																<input type="text" disabled  value="<?=$userName;?>">
															</label>
														</div>
														<div class="medium-3 cell">
															<label>
																Password:
																<input type="text" disabled  value="*******">
															</label>
														</div>
														<div class="medium-3 cell">
                                                            <label> 
                                                                Role: 

                                                                <?php if(isset($_SESSION['isAdmin'])): ?>
                                                                    <select name="user_role">
                                                                        <option value="<?=$role;?>" selected><?=get_role($role);?></option>
                                                                        <option disabled>-----</option>
                                                                        <option value="0">No Role</option>
                                                                        <option value="2">Member</option>
                                                                        <option value="9">Admin</option>
                                                                    </select>
                                                                <?php else: ?>
                                                                    <input type="text" disabled value="<?=get_role($role);?>">
                                                                <?php endif; ?>
                                                                    
                                                            </label>
  														</div>
														<div class="medium-3 cell">
															<label>
                                                                Status:
                                                                <?php if(isset($_SESSION['isAdmin']) || isset($_SESSION['isAuthor'])): ?>
                                                                <select name="user_status">
																		<option value="<?=$status;?>" selected><?=get_status($status);?></option>
																		<option disabled>-----</option>
																		<option value="0">Needs Activation</option>
																		<option value="1">Activated</option>
																		<option value="2">Deactivated</option>
                                                                </select>
                                                                <?php else: ?>
                                                                <input type="text" disabled value="<?=get_status($status);?>">
                                                                <?php endif; ?>
															</label>
														</div>
														<div class="medium-3 cell">
															<label>
																Last Name:
																<input type="text" placeholder="Last Name" name="lastName" required value="<?=$lastName;?>">
															</label>
														</div>
														<div class="medium-3 cell">
															<label>
																First Name:
																<input type="text" placeholder="First Name" name="firstName" required value="<?=$firstName;?>">
															</label>
														</div>
														<div class="medium-3 cell">
															<label>
																Middle Name:
																<input type="text" placeholder="Middle Name" name="middleName" value="<?=$middleName;?>">
															</label>
														</div>
														<div class="medium-3 cell">
															<label>
																Gender:
																<select name="gender">
																	<option value="<?=$gender;?>" selected><?=$gender;?></option>
																	<option disabled>---</option>
																	<option value="male">Male</option>
																	<option value="female">Female</option>
																	<option value="notMentioned">I'd rather not say</option>
																</select>
															</label>
														</div>
														<div class="medium-4 cell">
															<label>Clubs / Groups:
																<input type="text" name="club_group" placeholder="Your membership to other clubs or groups" value="<?=$clubs_group;?>">
															</label>
														</div>
														<div class="medium-8 cell">
															<label>Address:
																<input name="address" type="text" placeholder="Home address" required value="<?=$address;?>">
															</label>
														</div>
														<div class="medium-4 cell">
															<label>Mobile / Phone:
																<input type="text" name="mobile_phone" placeholder="Mobile or Landline" required value="<?=$mobile_phone;?>">

															</label>
														</div>
														<div class="medium-4 cell">
															<label>Email:
																<input type="email" name="email" placeholder="your@email.com" pattern="email" required value="<?=$email;?>" disabled>
															</label>
														</div>
														<div class="medium-4 cell">
															<label>Age Group:
																<select name="age_group">
																	<option value="<?=$age_group;?>" selected><?=$age_group;?></option>
																	<option disabled>---</option>
																	<option value="5-9">5-9</option>
																	<option value="10-14">10-14</option>
																	<option value="15-19">15-19</option>
																	<option value="20-24">20-24</option>
																	<option value="25-29">25-29</option>
																	<option value="30-34" selected>30-34</option>
																	<option value="35-39">35-39</option>
																	<option value="40-44">40-44</option>
																	<option value="45-49">45-49</option>
																	<option value="50-54">50-54</option>
																	<option value="55-59">55-59</option>
																	<option value="60-64">60-64</option>
																	<option value="65-69">65-69</option>
																	<option value="70-80">70-80</option>
																	<option value="80-90">80-90</option>
																	<option value="100up">100up</option>
																</select>
															</label>
														</div>
														<h2 class="cell large-12">Emergency Contact Details</h2>
														<div class="medium-3 cell">
															<label>
																Last Name:
																<input type="text" placeholder="Last Name" name="ecd_last_name" required value="<?=$ecd_lastName;?>">
															</label>
														</div>
														<div class="medium-3 cell">
															<label>
																First Name:
																<input type="text" placeholder="First Name" name="ecd_first_name" required value="<?=$ecd_firstName;?>">
															</label>
														</div>
														<div class="medium-3 cell">
															<label>
																Middle Name:
																<input type="text" placeholder="Middle Name" name="ecd_middle_name" value="<?=$ecd_middleName;?>">
															</label>
														</div>
														<div class="medium-3 cell">
															<label>
																Relationship:
																<input type="text" name="ecd_relationship" value="<?=$ecd_relationship;?>">
															</label>
														</div>
														<div class="medium-12 cell">
															<label>Address:
																<input type="text" name="ecd_address" placeholder="Home address" value="<?=$ecd_address;?>">
															</label>
														</div>
														<div class="medium-4 cell">
															<label>Mobile / Phone:
																<input type="text" name="ecd_mobile" placeholder="Mobile or Landline" required value="<?=$ecd_mobile;?>">

															</label>
														</div>
														<div class="medium-4 cell">
															<label>Email:
																<input type="email" name="ecd_email" placeholder="your@email.com" pattern="email" value="<?=$ecd_email;?>">
																<span class="form-error">
																	Valid email is required.
																</span>
															</label>
														</div>
														<!-- <div class="large-12 cell">
															<hr>
															<div class="grid-container">
																<div class="grid-x grid-margin-x grid-margin-y">
																	<div class="medium-3 cell">
																		<a href="#participated_events" class="button float-left secondary">Previous</a>
																	</div>
																	<div class="medium-6 cell">
																	</div>
																	<div class="medium-3 cell">
																		<a href="#claiming_details" class="button success float-right next_claiming_details">Next</a>
																	</div>
																</div>
															</div>
														</div> -->
                                                        <hr class="medium-12  cell">
                                                        <div class="medium-12  cell">
                                                            <!-- <a href="#claiming_details" class="button secondary float-left" name="">Previous</a> -->
                                                            <button id="" type="submit" class="button float-right">Update</button>
															<input type="hidden" name="userId" readonly value="<?=$userId;?>">
														</div>
													
												</div>
											</div>
                                        </form>
									</div>
									<div class="tabs-panel" id="reset_password">
                                        <div class="grid-container">
                                            <div class="grid-x grid-margin-x">
                                                
                                                <div class="medium-6 cell">
                                                    <form data-abide novalidate id="update_password_form">
                                                        <div class="row">
                                                            <label>
                                                                Current Password:
                                                                <input type="password" name="oldPassword" required>
                                                            </label>
                                                        </div>
                                                        <hr>
                                                        <div class="row">
                                                            <label>
                                                                Password:
                                                                <input type="password" name="password" required>
                                                            </label>
                                                        </div>
                                                        <div class="row">
                                                            <label>
                                                                Re-type Password:
                                                                <input type="password" name="passconf" required>
                                                            </label>
                                                        </div>
                                                        <div class="row">
                                                        <button type="submit" class="button ">Update Password</button>
                                                        </div>
                                                        </form>
                                                    </div>
                                                
                                                    <div class="medium-6 cell text-center">
                                                        <h2>Can't remember? Request a reset password.</h2>
                                                            <hr>
                                                            <a class="button" data-open="requestPasswordResetModal" aria-controls="forgotPasswordModal" aria-haspopup="true" tabindex="0">Reset Password</a>
                                                                
                                                        <!-- REQUEST PASSWORD -->
                                                        <div class="reveal" id="requestPasswordResetModal" data-reveal>
                                                            <button class="close-button" data-close aria-label="Close reveal" type="button">
                                                            <span aria-hidden="true">&times;</span>
                                                            </button>
                                                            <h2>Request a password reset?</h2>
                                                            <hr>

                                                            <form data-abide novalidate id="requestPasswordForm">
                                                            <div class="stat-messages-forgot"></div>
                                                                <div class="cell small-12 text-center">
                                                                    <h2>We'll send details to your email (<?=$_SESSION['sess_user_email'];?>).</h2><br>
                                                                    <input class="hide" type="hidden" readonly name="email" required value="<?=$_SESSION['sess_user_email'];?>">
                                                                </div>
                                                                
                                                                <div class="cell small-3 text-center">
                                                                    <button type="submit" class="okayButton button">Okay!</button>
                                                                </div>
                                                            </form>
                                                        </div>
                                                        <!-- //REQUEST PASSWORD -->

                                                    </div>
                                                </div>
                                            </div>
											
										</div>
									</div>

								</div>
								
							</div>
							 
						</div>
				</div>
				 
		</div>
	</div>
</div>
</section>

		<script type="text/javascript">
			$(document).ready(function () {

                //forgotPassword
                $("#requestPasswordForm").on("submit", function (ev) {
                ev.preventDefault();
                    
                $.ajax({
                    url: '<?php echo base_url('members/ajax/do/reset_password')?>',
                    type: "POST",
                    dataType: "json",
                    data: $("#requestPasswordForm").serialize(),
                    success: function (data) {
                                
                        if (data.status == 'Success!') {
                            $('.stat-messages-forgot').html(
                            '<div class="success callout cell large-12" data-closable > <h3>'+data.msg+'</h3><button class="close-button" aria-label="Dismiss alert" type="button" data-close><span aria-hidden="true">&times;</span></button></div>');
                            $('.okayButton').remove();
                        console.log(data);
                        }
                        else if (data.status == 'Invalid!') {
                            $('.stat-messages-forgot').html(
                            '<div class="alert callout cell large-12" data-closable > <h3>'+data.msg+'</h3><button class="close-button" aria-label="Dismiss alert" type="button" data-close><span aria-hidden="true">&times;</span></button></div>');
                        console.log(data);
                        
                        }
                        else if (data.status == 'Not registered!') {
                            $('.stat-messages-forgot').html(
                            '<div class="alert callout cell large-12" data-closable > <h3>'+data.msg+'</h3><button class="close-button" aria-label="Dismiss alert" type="button" data-close><span aria-hidden="true">&times;</span></button></div>');
                        console.log(data);
                        
                        }
                        else{
                            $('.stat-messages-forgot').html(
                            '<div class="alert callout cell large-12" data-closable > <h3>This is not a valid email.</h3><button class="close-button" aria-label="Dismiss alert" type="button" data-close><span aria-hidden="true">&times;</span></button></div>');
                        console.log(data);
                        }
                        

                    },
                    error: function (data) {
                        console.log('Error:'+ data);
                        if (data.status == 'Error!'){
                        $('.stat-messages-forgot').html(
                            '<div class="alert callout cell large-12" data-closable >ERROR:<hr> <h3>There\'s an error response from the database.</h3><button class="close-button" aria-label="Dismiss alert" type="button" data-close><span aria-hidden="true">&times;</span></button></div>'
                            );
                        }
                    }	
                    });

                });

                //change race categories dropdown based on chosen race event
                $('select[name="race-event"]').on('change', function() {
                    var eventID = $(this).val();
                    console.log(eventID);
                    if(eventID) {
                        $.ajax({
                            url:  '<?php echo base_url('members/ajax/')?>'+eventID+'/re',
                            type: "GET",
                            dataType: "json",
                            success:function(data) {
                                console.log(data);
                                $('select[name="choose_race_category"]').empty();
                                $.each(data, function(key, value) {
                                    $('select[name="choose_race_category"]').append('<option data-name="'+value.rc_name +'" data-fee="'+value.rc_fee +'" value="'+ value.rc_id +'">'+ value.rc_name +' - ₱'+value.rc_fee +'</option>');
                                });
                                
                            }
                        });
                    }else{
                        $('select[name="choose_race_category"]').empty();
                    }
                });
				
				
				//update password
				$("#update_password_form").on("submit", function (ev) {
					ev.preventDefault();

					$.ajax({
						url: '<?php echo base_url('members/ajax/do/update_password')?>',
						type: "POST",
						dataType: "json",
						data: $("#update_password_form").serialize(),
						success: function (data) {

							if (data.status == 'Error!') {
								$('.registration-error-messages').html(
									'<div class="alert callout cell large-12" data-closable > <h3>'+data.msg+'.</h3><hr><ul class="error-wrapper"></ul><button class="close-button" aria-label="Dismiss alert" type="button" data-close><span aria-hidden="true">&times;</span></button></div>'
								);

									if(data.oldPassword != ''){ $('.error-wrapper').append('<li>'+ data.oldPassword +'</li>'  ); }
									if(data.password != ''){ $('.error-wrapper').append('<li>'+ data.password +'</li>'  ); }
									if(data.passconf != ''){ $('.error-wrapper').append('<li>'+ data.passconf +'</li>'  ); }
									$('html,body').animate({ scrollTop: 0}, 'slow');			
                                    console.log(data);						
							}
							 
							else if (data.status == 'Success!') {
								$('.registration-error-messages').html(
									'<div class="success callout cell large-12" data-closable > <h3>Password Updated Successfully!</h3><button class="close-button" aria-label="Dismiss alert" type="button" data-close><span aria-hidden="true">&times;</span></button></div>'
								);
								$('html,body').animate({ scrollTop: 0}, 'slow');
							}
                            else if (data.status == 'Wrong!') {
								$('.registration-error-messages').html(
									'<div class="alert callout cell large-12" data-closable > <h3>'+data.msg+'.</h3><button class="close-button" aria-label="Dismiss alert" type="button" data-close><span aria-hidden="true">&times;</span></button></div>'
								);
									$('html,body').animate({ scrollTop: 0}, 'slow');									
							}
                            else{
                                $('.registration-error-messages').html(
									'<div class="alert callout cell large-12" data-closable > <h3>We got confused!</h3><button class="close-button" aria-label="Dismiss alert" type="button" data-close><span aria-hidden="true">&times;</span></button></div>'
								);
								$('html,body').animate({ scrollTop: 0}, 'slow');
                            }

							
						},
						error: function (data) {
							// console.log('Error!' + data.raceKitDeliverPickup);
							console.log(data);
							$('.registration-error-messages').html(
									'<div class="alert callout cell large-12" data-closable >ERROR: <h3>There\'s an error response from the database.</h3><button class="close-button" aria-label="Dismiss alert" type="button" data-close><span aria-hidden="true">&times;</span></button></div>'
								);
							$('html,body').animate({ scrollTop: 0}, 'slow');
						}	
					});
				});
				
				
				//update member info
				$("#reg_form").on("submit", function (ev) {
					ev.preventDefault();
					// var eventId = $(this).attr('id');

					$.ajax({
						url: '<?php echo base_url('members/ajax/do/update')?>',
						type: "POST",
						dataType: "json",
						data: $("#reg_form").serialize(),
						success: function (data) {

							if (data.status == 'Error!') {
								$('.registration-error-messages').html(
									'<div class="alert callout cell large-12" data-closable > <h3>You have invalid details.</h3><ul class="error-wrapper"></ul><button class="close-button" aria-label="Dismiss alert" type="button" data-close><span aria-hidden="true">&times;</span></button></div>'
								);

									if(data.err_message != ''){ $('.error-wrapper').append('<li>'+ data.err_message +'</li>'  ); }
									if(data.firstName != ''){ $('.error-wrapper').append('<li>'+ data.firstName +'</li>'  ); }
									if(data.lastName != ''){ $('.error-wrapper').append('<li>'+ data.lastName +'</li>'  ); }
									if(data.address != ''){ $('.error-wrapper').append('<li>'+ data.address +'</li>'  ); }
									if(data.mobile_phone != ''){ $('.error-wrapper').append('<li>'+ data.mobile_phone +'</li>'  ); }
									if(data.age_group != ''){ $('.error-wrapper').append('<li>'+ data.age_group +'</li>'  ); }
									if(data.ecd_last_name != ''){ $('.error-wrapper').append('<li>'+ data.ecd_last_name +'</li>'  ); }
									if(data.ecd_first_name != ''){ $('.error-wrapper').append('<li>'+ data.ecd_first_name +'</li>'  ); }
									if(data.ecd_mobile != ''){ $('.error-wrapper').append('<li>'+ data.ecd_mobile +'</li>'  ); }
									if(data.ecd_email != ''){ $('.error-wrapper').append('<li>'+ data.ecd_email +'</li>'  ); }
									
								$('html,body').animate({ scrollTop: 0}, 'slow');
							}
							 
							else if (data.status == 'Success!') {
								$('.registration-error-messages').html(
									'<div class="success callout cell large-12" data-closable > <h3>Updated Successfully!</h3><button class="close-button" aria-label="Dismiss alert" type="button" data-close><span aria-hidden="true">&times;</span></button></div>'
								);
								// $('.email-error').hide();
								$('html,body').animate({ scrollTop: 0}, 'slow');

								setTimeout(() => {
									$('.close-button').click();
								}, 5000);
							}

							console.log(data);
						},
						error: function (data) {
							console.log(data.responseText);
							$('.registration-error-messages').html(
									'<div class="alert callout cell large-12" data-closable >ERROR: <h3>There\'s an error response from the database.</h3><button class="close-button" aria-label="Dismiss alert" type="button" data-close><span aria-hidden="true">&times;</span></button></div>'
								);
							$('html,body').animate({ scrollTop: 0}, 'slow');
						}	
					});
				});


				<?php if(isset($_SESSION['isAdmin']) || isset($_SESSION['isAuthor']) ): ?>

				//assign bib number
				$("#assignBibNumbers").on("submit", function (ev) {
					ev.preventDefault();
					
					$.ajax({
						url: '<?php echo base_url('members/ajax/do/assign_bib_number')?>',
						type: "POST",
						dataType: "json",
						data: $("#assignBibNumbers").serialize(),
						success: function (data) {

							if (data.stat_message == 'Success!') {
								$('.registration-error-messages').html(
									'<div class="success callout cell large-12" data-closable > <h3>'+data.err_message+'</h3><button class="close-button" aria-label="Dismiss alert" type="button" data-close><span aria-hidden="true">&times;</span></button></div>'
								);
								//$('html,body').animate({ scrollTop: 0}, 'slow');
								
								setTimeout(() => {
									window.location.href="<?php echo base_url('members/edit/'.$userId.'');?>";
								}, 2000);

							}
							else if (data.stat_message == 'Error!') {
								$('.registration-error-messages').html(
									'<div class="alert callout cell large-12" data-closable > <h3>'+data.err_message+'</h3><button class="close-button" aria-label="Dismiss alert" type="button" data-close><span aria-hidden="true">&times;</span></button></div>'
								);
							}
							else{
								$('.registration-error-messages').html(
									'<div class="alert callout cell large-12" data-closable > <h3>You have invalid details!</h3><hr><ul class="error-wrapper"></ul><button class="close-button" aria-label="Dismiss alert" type="button" data-close><span aria-hidden="true">&times;</span></button></div>'
								);
								if(data.race_event != ''){ $('.error-wrapper').append('<li>'+ data.race_event +'</li>'  ); }
								if(data.choose_race_category != ''){ $('.error-wrapper').append('<li>'+ data.choose_race_category +'</li>'  ); }
								if(data.assigned_bib_number != ''){ $('.error-wrapper').append('<li>'+ data.assigned_bib_number +'</li>'  ); }
								
							}
							
							$('html,body').animate({ scrollTop: 0}, 'slow');

							console.log(data);
						},
						error: function (data) {
							console.log(data);
							$('.registration-error-messages').html(
									'<div class="alert callout cell large-12" data-closable >ERROR: <h3>There\'s an error response from the database.</h3><button class="close-button" aria-label="Dismiss alert" type="button" data-close><span aria-hidden="true">&times;</span></button></div>'
								);
							$('html,body').animate({ scrollTop: 0}, 'slow');
						}	
					});
				});


				//participated events under members page
				$('#participatedEvents').DataTable({
					"columns": [
						null,
						null,
						null,
						{
						"data": null,
						"render": function ( data, type, row ) {
						
							return "<button id='" +row.DT_RowId + "' type='button' class='button small removeParticipant' >x</button>"
								
							}
						}
					]
				});
			

				
				//removeParticipant from event via ajax call
				$('.removeParticipant').on('click', function(){
					var p_id = $(this).attr('id');
					console.log(p_id);


					//confirm 
					var answer = confirm('Sure to remove this user from the event?\nAll participant info will be lost. (Payment Details, Singlet Sizes, and All race details. )');
					
					if(answer){
						//do ajax delete here
						$.ajax({
								url:  '<?php echo base_url('participants/ajax/')?>'+p_id,
								type: "GET",
								dataType: "json",
								success:function(data) {
									
									if(data.status == "Success!"){
										//then hide the row
										$('tr#'+p_id).fadeOut(); 
										
										setTimeout(() => {
											
											$('.registration-error-messages').html(
												'<div class="success callout cell large-12" data-closable > <h3>'+data.msg+'</h3><button class="close-button" aria-label="Dismiss alert" type="button" data-close><span aria-hidden="true">&times;</span></button></div>'
											);
											$('html,body').animate({ scrollTop: 0}, 'slow');
										}, 1500);
									}else{
										$('.registration-error-messages').html(
											'<div class="alert callout cell large-12" data-closable > <h3>'+data.msg+'</h3><button class="close-button" aria-label="Dismiss alert" type="button" data-close><span aria-hidden="true">&times;</span></button></div>'
										);
										$('html,body').animate({ scrollTop: 0}, 'slow');
									}
									
									console.log(data);
								},
								error:function(data){
									$('.registration-error-messages').html(
										'<div class="alert callout cell large-12" data-closable >ERROR! <h3>There\'s an error response from the database.</h3><button class="close-button" aria-label="Dismiss alert" type="button" data-close><span aria-hidden="true">&times;</span></button></div>'
									);
									$('html,body').animate({ scrollTop: 0}, 'slow');
									console.log(data.responseText);
								}
								
							});
							
					}



				});

					

				<?php endif; ?>
	
					
			}); //end ready document

		</script>
