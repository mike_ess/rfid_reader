<?php if( isset($_SESSION['isAdmin'])) : ?>
	

<section id="main-content">
  <div id="guts">

	<div class="grid-container">
		<div class="grid-x grid-margin-x grid-margin-y">
			<div class="cell medium-12">
				<form data-abide novalidate id="reg_form">
					<div class="grid-container">
						<div class="grid-x grid-margin-x grid-margin-y">


							
							<div class="cell medium-12">
							<h2>Add New Member</h2>
						</div>


							<div class="cell medium-8 medium-offset-2">
								<div class="registration-error-messages"> </div>
							</div>
							<div class="cell medium-12">
								<ul class="tabs " data-deep-link="true" data-update-history="true" data-deep-link-smudge="true" data-deep-link-smudge-delay="500"
								data-tabs id="deeplinked-tabs">
									<li class="tabs-title is-active">
										<a href="#race_details" aria-selected="true">Race Details & Kits</a>
									</li>
									<li class="tabs-title">
										<a href="#member_info">Member Info.</a>
									</li>
									<li class="tabs-title">
										<a href="#claiming_details">Claiming Details</a>
									</li>
									<li class="tabs-title">
										<a href="#review_information">Review Info.</a>
									</li>
								</ul>

								<div class="tabs-content" data-tabs-content="deeplinked-tabs">
									<div class="tabs-panel is-active" id="race_details">
										<div class="grid-container">
											<div class="grid-x grid-margin-x grid-margin-y">
												<!-- <div class="medium-7 cell">
													<div class="" >
														<img src="https://placehold.it/400x370&text=Look at me!" alt="image for article">
													</div>
												</div> -->
												<div class="medium-4 cell">
													<div class="" >

                                                        <label>
                                                        <h3>Select Race Event:</h3>
                                                        
                                                            <select name="race-event">
                                                                <option value=""></option>
                                                                <?php 
                                                                        /** Start Show all events */ 
                                                                        foreach ($events as $events_item): 
                                                                    ?>
                                                                    <option id="<?=$events_item['re_id'];?>" data-name="<?=$events_item['re_name'];?>"  value="<?=$events_item['re_id'];?>"  ><?=$events_item['re_name'];?></option>
                                                                    <?php endforeach; ?>
                                                                </select>
                                                             
                                                        </label>
                            

														<label>
															<h3>Choose Race Category:</h3>
															<select name="choose_race_category" >
																<option value="">-choose race event-</option>
																
																
															</select>
														</label>
														<label>
															<h3>Adult Size</h2>
																<select name="choose_singlet_size" id="" >
																	<option value="">-choose race event-</option>
																</select>
														<ul>
															<li>* Availability is subject to change without prior notice.</li>
															<li>* Unisex size</li>
														</ul>
														</label>

														<!-- <label class="bib_label hide">
                                                            <h3>Assign Bib Number:</h3>
                                                            <input type="number" placeholder="00001" name="assigned_bib_number">
															<span class="form-error bib-number-error">
																Please use another Bib Number.
															</span>
                                                        </label> -->


														<!-- <p class="callout warning text-center" style="margin-top: 20px;	">
															Total Amount: PHP <span>900</span>
														</p> -->
																			<hr>
														<a href="#member_info" class="button  small go_to_member_info_btn float-right" >Next</a>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="tabs-panel" id="member_info">
											<div class="grid-container">
												<div class="grid-x grid-margin-x grid-margin-y">

														<h2 class="cell large-12">Personal Contact Details</h2>
														<div class="medium-3 cell">
															<label>
																Last Name:
																<input type="text" placeholder="Last Name" name="lastName" required>
															</label>
														</div>
														<div class="medium-3 cell">
															<label>
																First Name:
																<input type="text" placeholder="First Name" name="firstName" required>
															</label>
														</div>
														<div class="medium-3 cell">
															<label>
																Middle Name:
																<input type="text" placeholder="Middle Name" name="middleName">
															</label>
														</div>
														<div class="medium-3 cell">
															<label>
																Gender:
																<select name="gender">
																	<option value="male">Male</option>
																	<option value="female">Female</option>
																	<option value="notMentioned">I'd rather not say</option>
																</select>
															</label>
														</div>
														<div class="medium-4 cell">
															<label>Clubs / Groups:
																<input type="text" name="club_group" placeholder="Your membership to other clubs or groups">
															</label>
														</div>
														<div class="medium-8 cell">
															<label>Address:
																<input name="address" type="text" placeholder="Home address" required>
															</label>
														</div>
														<div class="medium-4 cell">
															<label>Mobile / Phone:
																<input type="text" name="mobile_phone" placeholder="Mobile or Landline" required>

															</label>
														</div>
														<div class="medium-4 cell">
															<label>Email:
																<input type="email" name="email" placeholder="your@email.com" pattern="email" required>
																<span class="form-error email-error">
																	Valid email is required.
																</span>
															</label>
														</div>
														<div class="medium-4 cell">
															<label>Age Group:
																<select name="age_group">
																	<option value="5-9">5-9</option>
																	<option value="10-14">10-14</option>
																	<option value="15-19">15-19</option>
																	<option value="20-24">20-24</option>
																	<option value="25-29">25-29</option>
																	<option value="30-34" selected>30-34</option>
																	<option value="35-39">35-39</option>
																	<option value="40-44">40-44</option>
																	<option value="45-49">45-49</option>
																	<option value="50-54">50-54</option>
																	<option value="55-59">55-59</option>
																	<option value="60-64">60-64</option>
																	<option value="65-69">65-69</option>
																	<option value="70-80">70-80</option>
																	<option value="80-90">80-90</option>
																	<option value="100up">100up</option>
																</select>
															</label>
														</div>
														<h2 class="cell large-12">Emergency Contact Details</h2>
														<div class="medium-3 cell">
															<label>
																Last Name:
																<input type="text" placeholder="Last Name" name="ecd_last_name" required>
															</label>
														</div>
														<div class="medium-3 cell">
															<label>
																First Name:
																<input type="text" placeholder="First Name" name="ecd_first_name" required>
															</label>
														</div>
														<div class="medium-3 cell">
															<label>
																Middle Name:
																<input type="text" placeholder="Middle Name" name="ecd_middle_name">
															</label>
														</div>
														<div class="medium-3 cell">
															<label>
																Relationship:
																<input type="text" name="ecd_relationship">
															</label>
														</div>
														<div class="medium-12 cell">
															<label>Address:
																<input type="text" name="ecd_address" placeholder="Home address">
															</label>
														</div>
														<div class="medium-4 cell">
															<label>Mobile / Phone:
																<input type="text" name="ecd_mobile" placeholder="Mobile or Landline" required>

															</label>
														</div>
														<div class="medium-4 cell">
															<label>Email:
																<input type="email" name="ecd_email" placeholder="your@email.com" pattern="email">
																<span class="form-error">
																	Valid email is required.
																</span>
															</label>
														</div>
														<div class="large-12 cell">
															<hr>
															<div class="grid-container">
																<div class="grid-x grid-margin-x grid-margin-y">
																	<div class="medium-3 cell">
																		<a href="#race_details" class="button float-left secondary">Previous</a>
																	</div>
																	<div class="medium-6 cell">
																		<!-- <div class="registration-error-messages"> </div> -->
																	</div>
																	<div class="medium-3 cell">
																		<!-- <button id="<?=$events_item['re_id'];?>" type="submit" class="button float-right save-info-btn">Save Information</button> -->
																		<!-- <button id="<?=$events_item['re_id'];?>" type="submit" class="button float-right register-btn">Next</button> -->
																		<a href="#claiming_details" class="button  float-right next_claiming_details">Next</a>
																	</div>
																</div>
															</div>
														</div>
													
												</div>
											</div>
									</div>
									<div class="tabs-panel" id="claiming_details">
											<div class="grid-container">
												<div class="grid-x grid-margin-x">
												<div class="medium-3 cell">
													<label for="middle-label" class="text-right middle">I want my race kits to be</label>
												</div>
												<div class="medium-2 cell">
													<select name="raceKitDeliverPickup" id="middle-label" required>
														<option value="">-choose-</option>
														<option value="picked_up">picked up</option>
														<option value="delivered">delivered</option>
													</select>
												</div>
												<div class="medium-2 cell">
													<label for="middle-label" class="text-left middle race_kit_delivery_message"></label>
												</div>
												<div class="medium-5 cell race_kit_delivery_action"></div>
												<div class="medium-12 cell race_kit_pickup_locations hide">
													<div class="grid-container">
														<div class="grid-x grid-margin-x grid-margin-y">
															<div class="medium-12 cell">
																<p class="callout warning text-center">
																	Please call the location first for availability then you can reserve.
																</p>
															</div>

															<?php foreach ($pickup_location as $pls):  ?>
															<div class="medium-4 cell">
																<?php 
																						
																	$plId = $pls['mf_id'];
																	if($plId != '0'): 
																	$query = $this->db->query('SELECT * FROM media_files WHERE mf_id = "'.$plId.'" ');
																	$row = $query->row();
																	$check_num_rows = $query->num_rows();
																?>
																<?php if($check_num_rows >= 1): ?>
																<div class="featuredImageDiv">
																	<img src="<?=base_url('uploads/ar_');?><?=$row->mf_file_name; ?>">
																</div>
																<?php else: ?>
																<div class="featuredImageDiv">
																	<br><span class="label alert">Can't find the image. <br>Please check image location.</span>
																	<img src="https://placehold.it/200x200&amp;text=No Available Image!" alt="<?=$pls['pl_est_name'];?>">
																</div>
																<?php endif; ?>

																<?php else: ?>
																<img src="https://placehold.it/200x200&amp;text=No Available Image!" alt="No image.">
																<?php endif; ?>

																<h3>
																	<?=$pls['pl_est_name'];?>
																</h3>
																Address:
																<?=$pls['pl_est_address'];?><br>
																Phone:
																<?=$pls['pl_phone'];?>
															</div>
															<?php endforeach; ?>
														</div>
													</div>
												</div>

													<hr class="medium-12  cell">
													
													<div class="small-1 cell text-right">
														<input id="termsCondition" type="checkbox" name="terms_condition_checkbox" required>
														<span class="form-error terms_condition_checkbox-error">
																You have to agree with the terms.
															</span>
													</div>
													<div class="small-11 cell">
														<label for="middle-label"> <label for="termsCondition" style="float: left; margin-right: 5px;">I have read and agree with the</label> <a data-open="termsModal">Terms and Conditions</a> and will sign the <a data-open="waiverModal">Waiver</a> on the event day.</label>
														<div class="reveal" id="termsModal" data-reveal>
															<h1>Terms and Conditions</h1>
															<p class="lead">Your couch. It is mine.</p>
															<p>I'm a cool paragraph that lives inside of an even cooler modal. Wins!</p>
															<button class="close-button" data-close aria-label="Close modal" type="button">
																<span aria-hidden="true">&times;</span>
															</button>
														</div>
														<div class="reveal" id="waiverModal" data-reveal>
															<h1>Waiver</h1>
															<p class="lead">Your couch. It is mine.</p>
															<p>I'm a cool paragraph that lives inside of an even cooler modal. Wins!</p>
															<button class="close-button" data-close aria-label="Close modal" type="button">
																<span aria-hidden="true">&times;</span>
															</button>
														</div>
													</div>
													<hr class="medium-12  cell">

													<div class="medium-12  cell">
														
														<a href="#member_info" class="button secondary float-left" name="">Previous</a>
														<a href="#review_information" class="button float-right hide  next_review_info">Continue</a>
													</div>
												</div>
											</div>
									</div>
											
									<div class="tabs-panel" id="review_information">
											<div class="grid-container">
												<div class="grid-x grid-margin-x grid-margin-y">
													<div class="medium-6 cell">
														<div class="card">
															<div class="card-divider">
																Race Kits
															</div>
															<div class="card-section" style="padding: 0px;">
																
																<table class="hover unstriped" style="width: 100%;">
																	<thead>
																		<tr>
																			<td>Name</td>
																			<td>Singlet Size</td>
																			<td>Bib Number</td>
																		</tr>
																	</thead>
																	<tbody>
																		<tr>
																			<td id="raceKitName"></td>
																			<td id="raceKitSize"></td>
																			<td id="raceBibNumber"></td>
																		</tr>
																	</tbody>
																</table>
															</div>
														</div>
													</div>
													<div class="medium-6 cell">
														<div class="card">
															<div class="card-divider">
																Claiming Details <span id="pickup_or_delivery"></span>
															</div>
															<div class="card-section" style="padding: 0px;">
																
																<!-- if Delivery method -->
																<table id="deliverySelected" class="hover unstriped" style="width: 100%;">
																	<thead>
																		<tr>
																			<td>Receiver</td>
																			<td>Delivery Address</td>
																			<td>Fees</td>
																		</tr>

																		 
																	</thead>
																	<tbody>
																		<tr>
																			<td id="receiver"></td>
																			<td id="deliveryAddress"></td>
																			<td id="deliveryFees"></td>
																		</tr>
																	</tbody>
																</table>
																<!-- if pickup method -->
																<p id="pickupSelected" class="callout small hide">Pickup at the event area.</p>
															</div>
														</div>
													</div>
													<div class="medium-12 cell">
														<div class="card">
															<div class="card-divider">
																Registration Fees
															</div>
															<div class="card-section" style="padding: 0px;">
																
																<table class="hover unstriped" style="width: 100%;">
																	<thead>
																		<tr>
																			<td>Name</td>
																			<td>Race Event Name</td>
																			<td>Race Category</td>
																			<td>Registration Fee</td>
																			<td>Web Fees</td>
																		</tr>
																	</thead>
																	<tbody>
																		<tr>
																			<td id="fees_fullName"></td>
																			<td id="fees_eventName"></td>
																			<td id="fees_raceCategoryName"></td>
																			<td id="fees_regFee"></td>
																			<td id="fees_webFee"></td>
																		</tr>
																	</tbody>
																</table>
															</div>
														</div>
													</div>
													<div class="medium-9 cell">
														<div class="grid-container">
															<div class="grid-x grid-margin-x grid-margin-y">
																<div class="medium-12 cell">
																	<h2>Choose Payment Method:</h2>
																</div>
                                                                <?php foreach ($bank_payment_option as $bpo):  ?>
																<div class="medium-6 cell">
																	<label class="payment-method">
																		<input type="radio" name="payment_method" value="bank" required>
																		<?php 
																								
																			$bpo_id = $bpo['mf_id'];
																			if($bpo_id != '0'): 
																			$query = $this->db->query('SELECT * FROM media_files WHERE mf_id = "'.$bpo_id.'" ');
																			$row = $query->row();
																			$check_num_rows = $query->num_rows();
																		?>
																		<?php if($check_num_rows >= 1): ?>
																		<img src="<?=base_url('uploads/ar_');?><?=$row->mf_file_name; ?>">
																		<?php else: ?>
																		<br><span class="label alert">Can't find the image. <br>Please check image location.</span>
																		<img src="https://placehold.it/200x200&amp;text=No Available Image!" alt="<?=$bpo['bpo_name'];?>">
																		<?php endif; ?>

																		<?php else: ?>
																		<img src="https://placehold.it/200x200&amp;text=No Available Image!" alt="No image.">
																		<?php endif; ?>

																		<h3>
																			<?=$bpo['bpo_name'];?>
																		</h3>
																	</label>
																</div>
																<?php endforeach; ?>

																<?php foreach ($money_transfer_option as $mto):  ?>
																<div class="medium-6 cell">
																	<label class="payment-method">
																		<input type="radio" name="payment_method" value="bank" required>
																		
																		<?php 
																			$mto_id = $mto['mf_id'];
																			if($mto_id != '0'): 
																			$query = $this->db->query('SELECT * FROM media_files WHERE mf_id = "'.$mto_id.'" ');
																			$row = $query->row();
																			$check_num_rows = $query->num_rows();
																		?>

																		<?php if($check_num_rows >= 1): ?>
																		<img src="<?=base_url('uploads/ar_');?><?=$row->mf_file_name; ?>">
																		<?php else: ?>
																		<br><span class="label alert">Can't find the image. <br>Please check image location.</span>
																		<img src="https://placehold.it/200x200&amp;text=No Available Image!" alt="<?=$mto['mto_est_name'];?>">
																		<?php endif; ?>

																		<?php else: ?>
																		<img src="https://placehold.it/200x200&amp;text=No Available Image!" alt="No image.">
																		<?php endif; ?>

																		<h3>
																			<?=$mto['mto_est_name'];?>
																		</h3>
																	</label>
																</div>
																<?php endforeach; ?>
															</div>
														</div>
													</div>
													<div class="medium-3  cell ">
														<div class="card">
															<div class="card-divider">
																Total Amount To Pay
															</div>
															<div class="card-section" style="padding: 0px;">
																
																<table class="hover unstriped" style="width: 100%;">
																	<thead>
																		<tr>
																			<td id="totalAmount"></td>
																			
																		</tr>
																	</thead>
																	
																</table>
															</div>
														</div>
													</div>
													<hr class="medium-12  cell">
													<div class="medium-12  cell">
														<a href="#claiming_details" class="button secondary float-left" name="">Previous</a>
														<!-- <input name="race_event_id" value=" " readonly type="text"> -->
														<button id=" " type="submit" class="button float-right">Confirm</button>
													</div>
													
												</div>
											</div>
									</div>
								</div>
								
							</div>
							<!-- <div class="cell medium-6 medium-offset-3">
								<div class="registration-error-messages"> </div>
							</div> -->
						</div>
				</form>
				</div>
				 
		</div>
	</div>
	</div>
</section>
	
	
	<style>
		label.payment-method > input{
			visibility: hidden;
			position: absolute;
		}
		label.payment-method > input + img{
			cursor: pointer;
			padding: 10px;
		}
		label.payment-method > input + img:hover{
			background: #ccc;
		}
		label.payment-method > input:checked + img{
			background: yellow;
		}
	</style>

		<script type="text/javascript">
			$(document).ready(function () {

                //change race categories dropdown based on chosen race event
                $('select[name="race-event"]').on('change', function() {
                    var eventID = $(this).val();
                    console.log(eventID);
                    if(eventID) {
                        $.ajax({
                            url:  '<?php echo base_url('members/ajax/')?>'+eventID+'/re',
                            type: "GET",
                            dataType: "json",
                            success:function(data) {
                                console.log(data);
                                $('select[name="choose_race_category"]').empty();
                                $.each(data, function(key, value) {
                                    $('select[name="choose_race_category"]').append('<option data-name="'+value.rc_name +'" data-fee="'+value.rc_fee +'" value="'+ value.rc_id +'">'+ value.rc_name +' - ₱'+value.rc_fee +'</option>');
                                });
                                // $('.bib_label').removeClass('hide').show();
                            }
                        });
                    }else{
                        $('select[name="choose_race_category"]').empty();
						// $('.bib_label').addClass('hide').hide();
                    }
                });

                //change singlet sizes dropdown based on chosen race event
                $('select[name="race-event"]').on('change', function() {
                    var eventID = $(this).val();
                    console.log(eventID);
                    if(eventID) {
                        $.ajax({
                            url:  '<?php echo base_url('members/ajax/')?>'+eventID+'/rc ',
                            type: "GET",
                            dataType: "json",
                            success:function(data) {
                                console.log(data);
                                $('select[name="choose_singlet_size"]').empty();
                                $.each(data, function(key, value) {
                                    $('select[name="choose_singlet_size"]').append('<option data-name="'+ value.ss_name +'" data-value="'+ value.ss_id +'" value="'+ value.ss_id +'">'+ value.ss_name +'</option>');
                                });
                            }
                        });
                    }else{
                        $('select[name="choose_singlet_size"]').empty();
                    }
                });
                
				//review details
				$('#review_information-label, .next_review_info').on("click", function(){
					
					/* Currency */
					var currency = "₱";

					/* fields*/
					var raceBibNumber = $('#raceBibNumber');
					var raceKitName = $('#raceKitName');
					var raceKitSize = $('#raceKitSize');
					var receiver = $('#receiver');
					var deliveryAddress = $('#deliveryAddress');
					var deliveryFees = $('#deliveryFees');
					var fees_fullName = $('#fees_fullName');
					var fees_eventName = $('#fees_eventName');
					var fees_raceCategoryName = $('#fees_raceCategoryName');
					var fees_regFee = $('#fees_regFee');
					var fees_webFee = $('#fees_webFee');
					var totalAmount = $('#totalAmount');
					
					/* values*/
					var raceBibNumberVal = $('input[name="assigned_bib_number"]').val();
					var firstNameVal = $('input[name="firstName"]').val();
					var lastNameVal = $('input[name="lastName"]').val();
					var middleNameVal = $('input[name="middleName"]').val();
					var delivery_addressVal = $('input[name="delivery_address"]').val();
					var genderVal = $('select[name="gender"]').val();
                    
                    var choose_race_categoryValName = $('select[name="choose_race_category"]').find(':selected').data("name");
                    var choose_singlet_sizeValName = $('select[name="choose_singlet_size"]').find(':selected').data("name");
                    var choose_singlet_sizeVal = $('select[name="choose_singlet_size"]').find(':selected').data("value");
                    var race_eventVal = $('select[name="race-event"]').val();
                    var race_eventValName = $('select[name="race-event"]').find(':selected').data("name");
                    
                    if(race_eventVal == ''){
                        choose_race_categoryFeeVal = 0;
                    }else{
                        var choose_race_categoryFeeVal = $('select[name="choose_race_category"]').find(':selected').data("fee");
                    }


					var fees_webFeeVal = <?=$web_fee['f_cost'];?>;
					var deliveryFeesVal = <?=$delivery_fee_fixed['f_cost'];?>;

					if($('select[name="raceKitDeliverPickup"]').val() == 'delivered'){
						var totalAmountVal = fees_webFeeVal + deliveryFeesVal + choose_race_categoryFeeVal;
					}else{
						var totalAmountVal = fees_webFeeVal +  choose_race_categoryFeeVal;
					}



					/** VALIDATE FIRST */
					if(
						firstNameVal != "" &&
						lastNameVal != ""
					){
						//TOTAL
						totalAmount.html( currency+totalAmountVal.toLocaleString() );

						//registration fees
						fees_fullName.html(lastNameVal+', '+firstNameVal+' '+middleNameVal);
						fees_eventName.html(race_eventValName);
						fees_raceCategoryName.html(choose_race_categoryValName);
						fees_regFee.html(currency+choose_race_categoryFeeVal.toLocaleString());
						fees_webFee.html(currency+fees_webFeeVal.toLocaleString());


						//race kits section
						raceKitName.html(lastNameVal+', '+firstNameVal+' '+middleNameVal);
						raceKitSize.html(choose_singlet_sizeValName);

						if(raceBibNumberVal != ''){
							theBibNumber = raceBibNumberVal;
						}else{
							theBibNumber = 'To be assigned';
						}
						raceBibNumber.html(theBibNumber);
						
						//Claiming Details (Delivery) section
						receiver.html(lastNameVal+', '+firstNameVal+' '+middleNameVal);
						deliveryFees.html(currency+deliveryFeesVal.toLocaleString());
						deliveryAddress.html(delivery_addressVal);

					}else{
						console.log('blank: ' + choose_race_categoryValName);
						$('.registration-error-messages').html(
							'<div class="alert callout cell large-12" data-closable > <h3>Please fill in all needed details on each tabs.</h3><button class="close-button" aria-label="Dismiss alert" type="button" data-close><span aria-hidden="true">&times;</span></button></div>'
						);
						$('html,body').animate({ scrollTop: 0}, 'slow');
					}
				});

				//claiming details
				var $next_review_info = $('.next_review_info');
				var $terms_condition_checkbox = $('input[name="terms_condition_checkbox"]');
				var $raceKitDeliverPickup = $('select[name="raceKitDeliverPickup"]');


				//Race kit delivery pickup
				$raceKitDeliverPickup.on("change", function(){
						
					setTimeout(() => {
						$('input[name="delivery_address"]').focus();
						if($terms_condition_checkbox.prop('checked', true)){
							$terms_condition_checkbox.prop('checked', false)
						}
						$next_review_info.addClass('hide'); //hide
					}, 200);

				});

				//Terms and Condition Checkbox
				$terms_condition_checkbox.on("change", function(){
					
					if($raceKitDeliverPickup.val() == "delivered"){

						if($('input[name="delivery_address"]').val() == ''){
							setTimeout(() => {
								$('input[name="delivery_address"]').focus();
								if($terms_condition_checkbox.prop('checked', true)){
									$terms_condition_checkbox.prop('checked', false)
								}
								$next_review_info.addClass('hide'); //hide
								console.log('delivered BLANK');
							}, 200);
						}else{
							$next_review_info.removeClass('hide'); //show
							console.log('delivered NOT BLANK');
						}

					}
					else if($raceKitDeliverPickup.val() == "picked_up"){
						$next_review_info.removeClass('hide'); //show
							console.log('picked_up');
					}
					
					else{
						$next_review_info.addClass('hide'); //hide
					}
				});
 

				//raceKitDeliverPickup
				$("select[name='raceKitDeliverPickup']").on("change", function(){
					var thisValue = $(this).val();	
					var race_kit_delivery_message = $('.race_kit_delivery_message');
					var race_kit_delivery_action = $('.race_kit_delivery_action');
					var race_kit_pickup_locations = $('.race_kit_pickup_locations');
					var pickup_or_delivery = $('#pickup_or_delivery');
					var deliverySelected = $('#deliverySelected');
					var pickupSelected = $('#pickupSelected');

					if(thisValue == "picked_up"){
						race_kit_delivery_message.html('at pickup locations.');
						race_kit_delivery_action.html('<label for="middle-label" class="text-left middle" style="font-style:italic">see locations below</label>');
						race_kit_pickup_locations.removeClass('hide').show();
						pickup_or_delivery.text('');
						deliverySelected.addClass('hide').hide();
						pickupSelected.removeClass('hide').show();
					}else{
						race_kit_delivery_message.html('to this address');
						race_kit_delivery_action.html('<input type="text" value="" id="middle-label" name="delivery_address">');
						race_kit_pickup_locations.addClass('hide').hide();
						pickup_or_delivery.text('(Delivery)');
						deliverySelected.removeClass('hide').show();
						pickupSelected.addClass('hide').hide();
					}
				});

				
				
				//confirm registration
				$("#reg_form").on("submit", function (ev) {
					ev.preventDefault();
					// var eventId = $(this).attr('id');

					$.ajax({
						url: '<?php echo base_url('members/ajax/do/reg')?>',
						type: "POST",
						dataType: "json",
						data: $("#reg_form").serialize(),
						success: function (data) {

							if (data.status == "Error!") {
								$('.registration-error-messages').html(
									'<div class="alert callout cell large-12" data-closable > <h3>You have invalid details.</h3><hr><ul class="error-wrapper"></ul><button class="close-button" aria-label="Dismiss alert" type="button" data-close><span aria-hidden="true">&times;</span></button></div>'
								);

									if($('select[name="raceKitDeliverPickup"]').val() == 'delivered'){
										if(data.delivery_address != ''){ $('.error-wrapper').append('<li>'+ data.delivery_address +'</li>'  ); }
									}
									//if(data.choose_race_category != ''){ $('.error-wrapper').append('<li>'+ data.choose_race_category +'</li>'  ); }
									// if(data.assigned_bib_number != ''){ $('.error-wrapper').append('<li>'+ data.assigned_bib_number +'</li>'  ); }
									if(data.raceKitDeliverPickup != ''){ $('.error-wrapper').append('<li>'+ data.raceKitDeliverPickup +'</li>'  ); }
									if(data.terms_condition_checkbox != ''){ $('.error-wrapper').append('<li>'+ data.terms_condition_checkbox +'</li>'  ); }
									if(data.firstName != ''){ $('.error-wrapper').append('<li>'+ data.firstName +'</li>'  ); }
									if(data.lastName != ''){ $('.error-wrapper').append('<li>'+ data.lastName +'</li>'  ); }
									if(data.address != ''){ $('.error-wrapper').append('<li>'+ data.address +'</li>'  ); }
									if(data.mobile_phone != ''){ $('.error-wrapper').append('<li>'+ data.mobile_phone +'</li>'  ); }
									if(data.email != ''){ $('.error-wrapper').append('<li>'+ data.email +'</li>'  ); }
									if(data.age_group != ''){ $('.error-wrapper').append('<li>'+ data.age_group +'</li>'  ); }
									if(data.ecd_last_name != ''){ $('.error-wrapper').append('<li>'+ data.ecd_last_name +'</li>'  ); }
									if(data.ecd_first_name != ''){ $('.error-wrapper').append('<li>'+ data.ecd_first_name +'</li>'  ); }
									if(data.ecd_mobile != ''){ $('.error-wrapper').append('<li>'+ data.ecd_mobile +'</li>'  ); }
									if(data.ecd_email != ''){ $('.error-wrapper').append('<li>'+ data.ecd_email +'</li>'  ); }
									if(data.payment_method != ''){ $('.error-wrapper').append('<li>'+ data.payment_method +'</li>'  ); }
									// if(data.race_event_id != ''){ $('.error-wrapper').append('<li>'+ data.race_event_id +'</li>'  ); }

								$('.email-error').show().html(data.email);
								$('html,body').animate({ scrollTop: 0}, 'slow');
							}
							 
							if (data.status == 'Success!') {
								$('.registration-error-messages').html(
									'<div class="success callout cell large-12" data-closable > <h3>'+ data.msg +'</h3><button class="close-button" aria-label="Dismiss alert" type="button" data-close><span aria-hidden="true">&times;</span></button></div>'
								);
								$('.email-error').hide();
							}

							console.log(data);
						},
						error: function (data) {
							// console.log('Error!' + data.raceKitDeliverPickup);
							// console.log(data.responseText);
							$('.registration-error-messages').html(
									'<div class="alert callout cell large-12" data-closable >ERROR: <h3>There\'s an error response from the database.</h3><button class="close-button" aria-label="Dismiss alert" type="button" data-close><span aria-hidden="true">&times;</span></button></div>'
								);
							$('html,body').animate({ scrollTop: 0}, 'slow');
						}	
					});
				});



			});

		</script>
 
<?php endif;?>