
<section id="main-content">
  <div id="guts">

<div class="grid-container lw-events-page">
	<div class="grid-x grid-margin-x grid-margin-y">
		<div class="cell medium-8">
			<label>
				<h2>
					Register:
					<?=$events_item['re_name'];?>
				</h2>
			</label>
		</div>
		<div class="cell medium-8 medium-offset-2">
			<div class="registration-error-messages"> </div>
		</div>
		<div class="cell medium-12">

			<ul class="tabs " data-deep-link="true" data-update-history="true" data-deep-link-smudge="true"
			 data-deep-link-smudge-delay="500" data-tabs id="deeplinked-tabs">
				<li class="tabs-title is-active">
					<a href="#count_runners" aria-selected="true">
						<?php if(isset($_SESSION['isUserLoggedIn'])){echo 'Event Details'; } else{ echo 'How Many Runners?';} ?>
					</a>
				</li>
				<li class="tabs-title">
					<a href="#participant_info">Participant Info.</a>
				</li>
				<li class="tabs-title">
					<a href="#claiming_details">Claiming Details</a>
				</li>
				<li class="tabs-title">
					<a href="#review_information">Review Info.</a>
				</li>
			</ul>

			<div class="tabs-content" data-tabs-content="deeplinked-tabs">
				<div class="tabs-panel is-active" id="count_runners">
					<form data-abide novalidate id="count_runners_form">
						<div class="grid-container">
							<div class="grid-x grid-margin-x grid-margin-y" data-equalizer>
								<div class="medium-7 cell">
									<div class="callout" data-equalizer-watch>

										<?php
											$eventDescription = $events_item['re_description'];
											$mfId = $events_item['mf_id'];
											if($mfId != '0'): 
											$query = $this->db->query('SELECT * FROM media_files WHERE mf_id = "'.$events_item['mf_id'].'" ');
											$row = $query->row();
										?>
												
										<img src="<?=base_url('uploads/').$row->mf_file_name;?>" alt="<?=$events_item['re_slug'];?>">
										<?php else: ?>
										<img src="https://placehold.it/300x300&amp;text=No Available Image!" alt="image for article">
										<?php endif; ?>
										<hr>
										<?=$eventDescription;?>
									</div>
								</div>
								<div class="medium-5 cell">
									<div class="callout" data-equalizer-watch>
										<?php if(isset($_SESSION['isUserLoggedIn'])):?>
										<select readonly name="how_many_runners" required class="hide">
											<option value="2" selected>1</option>
										</select>
										<input type="hidden" value="1" readonly name="theRunnersCount">
										<!-- <span class="helper">You can add many runners later.</span> -->
										<button type="submit" class="button howManyRunnersBtn">Continue</button>
										<?php else: ?>
										<label>
											How many runners will join?
											<select name="how_many_runners" required>
												<option value="">-select-</option>
												<option value="2">1</option>
												<option value="3">2</option>
												<option value="4">3</option>
												<option value="5">4</option>
												<option value="6">5</option>
												<option value="7">6</option>
												<option value="8">7</option>
												<option value="9">8</option>
												<option value="10">9</option>
												<option value="11">10</option>
											</select>
										</label>
										<input type="hidden" readonly name="theRunnersCount">
										<button type="submit" class="button howManyRunnersBtn">Continue</button>
										<?php endif; ?>
										<!-- <a href="#participant_info" class="button success small go_to_participant_info_btn" style="position: absolute;bottom: 15px; right: 15px;">Next</a> -->
									</div>
								</div>
							</div>
						</div>
					</form>
				</div>
				<form data-abide novalidate id="reg_form">
					<div class="tabs-panel" id="participant_info">
						<div class="row">
							<ul class="runner_btn_div expanded button-group"></ul>
						</div>
						<div class="grid-container">
							<div class="grid-x grid-margin-x grid-margin-y">
								<hr class="cell large-12">
								<div class="cell large-12">
									<div class="runner_content_div"></div>
									
								</div>
								<hr class="cell large-12 eContactDiv hide1">
								<div class="hide eContactDiv cell large-12">
									<div class="grid-container">
										<div class="grid-x grid-margin-x grid-margin-y">
											<h2 class="cell large-12">Emergency Contact Details</h2>

											<div class="medium-3 cell">
												<label>Last Name:
													<input <?php if(isset($_SESSION['isUserLoggedIn'])){ echo 'readonly';} ?> class="" type="text" placeholder="Last Name" name="ecd_cr_first_name" required>
												</label>
											</div>
											<div class="medium-3 cell">
												<label>First Name:
													<input <?php if(isset($_SESSION['isUserLoggedIn'])){ echo 'readonly';} ?> class="" type="text" placeholder="First Name" name="ecd_cr_last_name" required>
												</label>
											</div>
											<div class="medium-3 cell">
												<label>Middle Name:
													<input <?php if(isset($_SESSION['isUserLoggedIn'])){ echo 'readonly';} ?> class="" type="text" placeholder="Middle Name" name="ecd_cr_middle_name">
												</label>
											</div>
											<div class="medium-3 cell">
												<label>Relationship:
													<input <?php if(isset($_SESSION['isUserLoggedIn'])){ echo 'readonly';} ?> class="" type="text" name="ecd_cr_relationship">
												</label>
											</div>
											<div class="medium-12 cell">
												<label>Address:
													<input <?php if(isset($_SESSION['isUserLoggedIn'])){ echo 'readonly';} ?> type="text" class="" name="ecd_cr_address" placeholder="Home address">
												</label>
											</div>
											<div class="medium-4 cell">
												<label>Mobile / Phone:
													<input <?php if(isset($_SESSION['isUserLoggedIn'])){ echo 'readonly';} ?> class="" type="text" name="ecd_cr_mobile" placeholder="Mobile or Landline" required>
												</label>
											</div>
											<div class="medium-4 cell">
												<label>Email:
													<input <?php if(isset($_SESSION['isUserLoggedIn'])){ echo 'readonly';} ?> class="" type="email" name="ecd_cr_email" placeholder="your@email.com" pattern="email">
													<span class="form-error">Valid email is required.</span>
												</label>
											</div>

										</div>
									</div>
								</div>
								<div class="cell large-12 text-center part_info">
								<?php if(isset($_SESSION['isUserLoggedIn'])):?>
									<h3 class="part_info_text">Please select "Continue" button on the first tab.</h3>
								<?php else: ?>
									<h3 class="part_info_text">Please select how many runners on the first tab.</h3>
								<?php endif; ?>
 
									<p class="hide chosen_div">Your main account is "<?php if(isset($_SESSION['isUserLoggedIn'])){echo 'Main'; }?> Runner <span class="chosen_runner_span"></span>" using the
										email: "<span class="chosen_runners_email"></span>".</p>
								</div>

								<div class="reveal" id="whatIsThis" data-reveal>
									<button class="close-button" data-close aria-label="Close modal" type="button">
										<span aria-hidden="true">&times;</span>
									</button>
									<h1>This mean that:</h1>
									<ul>
									<?php if(isset($_SESSION['isUserLoggedIn'])):?>
										<li>This will be your main account.</li>
										<li>If you don't have an account yet, we will create one based on this runner's details. </li>
									<?php else: ?>
										<li>This is your main account. We will copy all your details on the field.</li>
										<li>If you want to edit your details, please go to your account.</li>
									<?php endif; ?>
										<li>You will be held accountable to all runners you add.</li>
									</ul>
								</div>

								<hr class="cell large-12 part_info">

								<div class="cell large-12 part_info hide">
									<?php //hidden main runner's info copied from chosen runner's tab ?>

									<input type="hidden" name="choose_singlet_size" readonly>
									<input type="hidden" name="choose_race_category" readonly>

									<input type="text" name="chosen_runners_email" readonly>
									<input type="hidden" name="chosen_runners_first_name" readonly>
									<input type="hidden" name="chosen_runners_last_name" readonly>
									<input type="hidden" name="chosen_runners_middle_name" readonly>
									<input type="hidden" name="chosen_runners_gender" readonly>
									<input type="hidden" name="chosen_runners_address" readonly>
									<input type="hidden" name="chosen_runners_mobile" readonly>
									<input type="hidden" name="chosen_runners_ageGroup" readonly>
									<input type="hidden" name="chosen_runners_clubsGroups" readonly>

								 
								</div>
								<input type="text" readonly name="additionalRunners" value='0'>
								<?php if(isset($_SESSION['isUserLoggedIn'])):?>
									<div class="cell large-12 part_info hide">
										<div class="newRunnerItemGroup">
											<!-- <div id="newRunner_item_1"></div> -->
										</div>
										<div class="grid-container">
											<div class="grid-x grid-margin-x">
												<button type="button" class="button small cell small-6 addNewRunnerItem">Add More Runner</button>
												<button type="button" class="button small cell small-6 delNewRunnerItem">Delete Last</button>
											</div>
										</div>
									</div>
									
								<?php endif; ?>

								 
								
								<div class="cell large-12">
									<a href="#count_runners" class="button float-left secondary">Previous</a>
									<!-- <button type="submit" class="button float-right success">Next</button> -->
									<a href="#claiming_details" class="button success float-right next_claiming_details">Next</a>
									<!-- <button type="button" class="button success float-right next_claiming_details">Next</button> -->
								</div>
							</div>
						</div>

					</div>
					<div class="tabs-panel" id="claiming_details">
						<div class="grid-container">
							<div class="grid-x grid-margin-x">
								<div class="medium-3 cell">
									<label for="middle-label" class="text-right middle">I want my race kits to be</label>
								</div>
								<div class="medium-2 cell">
									<select name="raceKitDeliverPickup" id="middle-label" required>
										<option value="">-choose-</option>
										<option value="picked_up">picked up</option>
										<option value="delivered">delivered</option>
									</select>
								</div>
								<div class="medium-2 cell">
									<label for="middle-label" class="text-left middle race_kit_delivery_message"></label>
								</div>
								<div class="medium-5 cell race_kit_delivery_action"></div>
								<div class="medium-12 cell race_kit_pickup_locations hide">
									<div class="grid-container">
										<div class="grid-x grid-margin-x grid-margin-y">
											<div class="medium-12 cell">
												<p class="callout warning text-center">
													Please call the location first for availability then you can reserve.
												</p>
											</div>

											<?php foreach ($pickup_location as $pls):  ?>
											<div class="medium-4 cell">
												<?php 
																		
													$plId = $pls['mf_id'];
													if($plId != '0'): 
													$query = $this->db->query('SELECT * FROM media_files WHERE mf_id = "'.$plId.'" ');
													$row = $query->row();
													$check_num_rows = $query->num_rows();
												?>
												<?php if($check_num_rows >= 1): ?>
												<div class="featuredImageDiv">
													<img src="<?=base_url('uploads/ar_');?><?=$row->mf_file_name; ?>">
												</div>
												<?php else: ?>
												<div class="featuredImageDiv">
													<br><span class="label alert">Can't find the image. <br>Please check image location.</span>
													<img src="https://placehold.it/200x200&amp;text=No Available Image!" alt="<?=$pls['pl_est_name'];?>">
												</div>
												<?php endif; ?>

												<?php else: ?>
												<img src="https://placehold.it/200x200&amp;text=No Available Image!" alt="No image.">
												<?php endif; ?>

												<h3>
													<?=$pls['pl_est_name'];?>
												</h3>
												Address:
												<?=$pls['pl_est_address'];?><br>
												Phone:
												<?=$pls['pl_phone'];?>
											</div>
											<?php endforeach; ?>
										</div>
									</div>
								</div>

								<hr class="medium-12  cell">

								<div class="small-1 cell text-right">
									<input id="termsCondition" type="checkbox" name="terms_condition_checkbox" required>
									<span class="form-error terms_condition_checkbox-error">
										You have to agree with the terms.
									</span>
								</div>
								<div class="small-11 cell">
									<label for="middle-label"> <label for="termsCondition" style="float: left; margin-right: 5px;">I have read and
											agree with the</label> <a data-open="termsModal">Terms and Conditions</a> and will sign the <a data-open="waiverModal">Waiver</a>
										on the event day.</label>
									<div class="reveal" id="termsModal" data-reveal>
										<h1>Terms and Conditions</h1>
										<p class="lead">Your couch. It is mine.</p>
										<p>I'm a cool paragraph that lives inside of an even cooler modal. Wins!</p>
										<button class="close-button" data-close aria-label="Close modal" type="button">
											<span aria-hidden="true">&times;</span>
										</button>
									</div>
									<div class="reveal" id="waiverModal" data-reveal>
										<h1>Waiver</h1>
										<p class="lead">Your couch. It is mine.</p>
										<p>I'm a cool paragraph that lives inside of an even cooler modal. Wins!</p>
										<button class="close-button" data-close aria-label="Close modal" type="button">
											<span aria-hidden="true">&times;</span>
										</button>
									</div>
								</div>
								<hr class="medium-12  cell">

								<div class="medium-12  cell">

									<a href="#participant_info" class="button secondary float-left" name="">Previous</a>
									<a href="#review_information" class="button float-right hide success next_review_info">Continue</a>
								</div>
							</div>
						</div>
					</div>

					<div class="tabs-panel" id="review_information">
						<div class="grid-container">
							<div class="grid-x grid-margin-x grid-margin-y">
								<div class="medium-6 cell">
									<div class="card">
										<div class="card-divider">
											Race Kits
										</div>
										<div class="card-section" style="padding: 0px;">

											<table class="hover unstriped" style="width: 100%;">
												<thead>
													<tr>
														<td>Name</td>
														<td>Singlet Size</td>
														<td>Bib Number</td>
													</tr>
												</thead>
												<tbody id="raceKitName">
													<!-- <tr>
														<td id="raceKitName"></td>
														<td id="raceKitSize"></td>
														<td>To be assigned</td>
													</tr> -->
												</tbody>
											</table>
										</div>
									</div>
								</div>
								<div class="medium-6 cell">
									<div class="card">
										<div class="card-divider">
											Claiming Details <span id="pickup_or_delivery"></span>
										</div>
										<div class="card-section" style="padding: 0px;">

											<!-- if Delivery method -->
											<table id="deliverySelected" class="hover unstriped" style="width: 100%;">
												<thead>
													<tr>
														<td>Receiver</td>
														<td>Delivery Address</td>
														<td>Fees</td>
													</tr>
												</thead>
												<tbody>
													<tr>
														<td id="receiver"></td>
														<td id="deliveryAddress"></td>
														<td id="deliveryFees"></td>
													</tr>
												</tbody>
											</table>
											<!-- if pickup method -->
											<p id="pickupSelected" class="callout small hide">Pickup at the event area.</p>
										</div>
									</div>
								</div>
								<div class="medium-12 cell">
									<div class="card">
										<div class="card-divider">
											Registration Fees
										</div>
										<div class="card-section" style="padding: 0px;">

											<table class="hover unstriped" style="width: 100%;">
												<thead>
													<tr>
														<td>Name</td>
														<td>Race Event Name</td>
														<td>Race Category</td>
														<td>Registration Fee</td>
														<td>Web Fees</td>
													</tr>
												</thead>
												<tbody id="fees">

												</tbody>
											</table>
										</div>
									</div>
								</div>
								<div class="medium-9 cell">
									<div class="grid-container">
										<div class="grid-x grid-margin-x grid-margin-y">
											<div class="medium-12 cell">
												<h2>Choose Payment Method:</h2>
											</div>
											<!-- <div class="medium-6 cell">
												<label class="payment-method">
													<input type="radio" name="payment_method" value="bank" required>
													<img src="<?=base_url();?>images/payments.jpg">
												</label>
											</div> -->

											<?php foreach ($bank_payment_option as $bpo):  ?>
											<div class="medium-6 cell">
												<label class="payment-method">
													<input type="radio" name="payment_method" value="bank" required>
													<?php 
																			
														$bpo_id = $bpo['mf_id'];
														if($bpo_id != '0'): 
														$query = $this->db->query('SELECT * FROM media_files WHERE mf_id = "'.$bpo_id.'" ');
														$row = $query->row();
														$check_num_rows = $query->num_rows();
													?>
													<?php if($check_num_rows >= 1): ?>
													<img src="<?=base_url('uploads/ar_');?><?=$row->mf_file_name; ?>">
													<?php else: ?>
													<br><span class="label alert">Can't find the image. <br>Please check image location.</span>
													<img src="https://placehold.it/200x200&amp;text=No Available Image!" alt="<?=$bpo['bpo_name'];?>">
													<?php endif; ?>

													<?php else: ?>
													<img src="https://placehold.it/200x200&amp;text=No Available Image!" alt="No image.">
													<?php endif; ?>

													<h3>
														<?=$bpo['bpo_name'];?>
													</h3>
												</label>
											</div>
											<?php endforeach; ?>

											<?php foreach ($money_transfer_option as $mto):  ?>
											<div class="medium-6 cell">
												<label class="payment-method">
													<input type="radio" name="payment_method" value="bank" required>
													
													<?php 
														$mto_id = $mto['mf_id'];
														if($mto_id != '0'): 
														$query = $this->db->query('SELECT * FROM media_files WHERE mf_id = "'.$mto_id.'" ');
														$row = $query->row();
														$check_num_rows = $query->num_rows();
													?>

													<?php if($check_num_rows >= 1): ?>
													<img src="<?=base_url('uploads/ar_');?><?=$row->mf_file_name; ?>">
													<?php else: ?>
													<br><span class="label alert">Can't find the image. <br>Please check image location.</span>
													<img src="https://placehold.it/200x200&amp;text=No Available Image!" alt="<?=$mto['mto_est_name'];?>">
													<?php endif; ?>

													<?php else: ?>
													<img src="https://placehold.it/200x200&amp;text=No Available Image!" alt="No image.">
													<?php endif; ?>

													<h3>
														<?=$mto['mto_est_name'];?>
													</h3>
												</label>
											</div>
											<?php endforeach; ?>

										</div>
									</div>
								</div>
								<div class="medium-3  cell ">
									<div class="card">
										<div class="card-divider">
											Total Amount To Pay
										</div>
										<div class="card-section" style="padding: 0px;">

											<table class="hover unstriped" style="width: 100%;">
												<thead>
													<!-- <tr>
																			<td>+ <?=$web_fee['f_cost'];?> (Web Fees)<br><hr></td>
																		</tr> -->
													<tr>
														<td id="totalAmount"></td>
														<input name="raceCategoryTotal" type="hidden" readonly>
														<input name="webFeeTotal" type="hidden" readonly>
													</tr>
												</thead>

											</table>
										</div>
									</div>
								</div>
								<hr class="medium-12  cell final_confirm">
								<div class="medium-12  cell final_confirm">
									<a href="#claiming_details" class="button secondary float-left" name="">Previous</a>
									
									<?php if(isset($_SESSION['isUserLoggedIn'])):?>
										<input type="text" readonly name="additionalRunners">
									<?php endif; ?>

									<input type="text" readonly name="theRunnersCount">
									<input name="race_event_id" value="<?=$events_item['re_id'];?>" readonly type="hidden">
									<button id="<?=$events_item['re_id'];?>" type="submit" class="button float-right">Confirm</button>
								</div>

							</div>
						</div>
					</div>
				</form>
			</div>
		</div>	 
	</div>
</div>

</div>
</section>


	<script type="text/javascript">
		$(document).ready(function () {

			//tab & accordions
			$('body').on("click", '.toggleBtn', function () {
				var id = $(this).attr('id');

				$('.tg').addClass('hide').hide();

				setTimeout(() => {
					$('.toggleContent_' + id).removeClass('hide').show();
				}, 200);
				//console.log(id);
			});

			//how many runners
			$("#count_runners_form").on("submit", function (ev) {
				ev.preventDefault();
				var how_many_runners = $('select[name="how_many_runners"]').val();
				var theCount = how_many_runners - 1;

				if (how_many_runners == '') {
					return false;
				}
				<?php if(!isset($_SESSION['isUserLoggedIn'])): //removes alert for logged in users?>
				//confirm 
				var answer = confirm('Are you sure you want to register ' + theCount +
					' runner(s)?\nYou can\'t edit this anymore unless you refresh your page. ');
				if (answer) {
				<?php endif; //removes alert for logged in users?>

					var runnerbtn = "";
					var runnerContent = "";

					var i = 1;
					while (i < how_many_runners) {

						//buttons
						runnerbtn += '<li id="' + i + '" class="button li_button">';
					runnerbtn += '<a href="#runner' + i + '" class="toggleBtn button small runner_' + i + '" id="' + i + '"><?php if( !isset($_SESSION['isUserLoggedIn']) ): ?>Runner ' + i + '<?php else: ?> Participant Info. <?php endif; ?></a>';
						runnerbtn += '</li>';
						

						//personal info
						runnerContent += '<div class="tg toggleContent_' + i + ' grid-container callout hide">';
						runnerContent += '<div class="grid-x grid-margin-x grid-margin-y">';
						runnerContent += '<div class="cell medium-6"><h1><?php if(isset($_SESSION['isUserLoggedIn'])){echo 'Main'; }?> Runner <?php if(!isset($_SESSION['isUserLoggedIn'])){echo " ' + i + ' "; }?></h1> </div><div class="cell medium-6 text-right"><a class="float-right button alert small" data-open="whatIsThis">?</a>';
						runnerContent += '<label class="float-right">';
						runnerContent += '<input type="radio" name="make_main_account" id="' + i + '" value="" >';
						runnerContent += '<input type="hidden" name="mma_[]" id="mma_' + i + '" class="mma" value="no" readonly >';

						<?php if( isset($_SESSION['isUserLoggedIn']) ): ?>
						runnerContent += ' Copy all my details (Main Account).';
						<?php else: ?>
						runnerContent += ' Make this the main account.';
						<?php endif; ?>

						runnerContent += ' </label></div>';
						runnerContent += '<h2 class="cell large-12">Race Kits & Details</h2>';
						runnerContent += '<div class="cell medium-6">';
						runnerContent += '<label>';
						runnerContent += '<h3>Choose Race Category:</h3>';
						runnerContent += '<select <?php if(isset($_SESSION['isUserLoggedIn'])){ echo 'readonly';} ?> class="crc choose_race_category_' + i +'" name="choose_race_category_[]" required>';
						runnerContent += '<option value="">-choose-</option>';
						runnerContent +='<?php foreach ($categories as $race_category):?><option data-name="<?=$race_category["rc_name"];?>" data-fee="<?=$race_category["rc_fee"];?>" value="<?=$race_category["rc_id"];?>"><?=$race_category["rc_name"];?> - ₱<?=$race_category["rc_fee"];?></option><?php endforeach; ?>';
						runnerContent += '</select></label>';
						runnerContent += '</div>';
						runnerContent += '<div class="cell medium-6">';
						runnerContent += '<label>';
						runnerContent += '<h3>Singlets</h3>';
						runnerContent += '<select <?php if(isset($_SESSION['isUserLoggedIn'])){ echo 'readonly';} ?> class="choose_singlet_size_' + i + '" name="choose_singlet_size_[]" required>';
						runnerContent += '<option value="">-choose-</option>';
						runnerContent +='<?php foreach ($sizes as $singlet_sizes):?><option value="<?=$singlet_sizes["ss_id"];?>" data-value="<?=$singlet_sizes["ss_name"];?>"><?=$singlet_sizes["ss_name"];?></option><?php endforeach; ?>';
						runnerContent += '</select>';
						runnerContent += '</label>';
						runnerContent += '<ul>';
						runnerContent += '<li>* Availability is subject to change without prior notice.</li>';
						runnerContent += '</ul>';
						runnerContent += '</div>';
					runnerContent += '<hr class="cell large-12"><h2 class="cell large-12">Personal Contact Details <?php if(isset($_SESSION['isUserLoggedIn'])):?><button type="button" class="copyAllDetails button small float-right">Copy all details.</button><?php endif; ?></h2>';
						runnerContent += '<div class="medium-3 cell"><label>Last Name:<input <?php if(isset($_SESSION['isUserLoggedIn'])){ echo 'readonly';} ?> type="text" class="toggleAttr lastName_' + i + '" placeholder="Last Name" name="lastName_[]" required></label></div>';
						runnerContent +='<div class="medium-3 cell"><label>First Name:<input <?php if(isset($_SESSION['isUserLoggedIn'])){ echo 'readonly';} ?> type="text" class="toggleAttr firstName_' + i +'" placeholder="First Name" name="firstName_[]" required></label></div>';
						runnerContent +='<div class="medium-3 cell"><label>Middle Name:<input <?php if(isset($_SESSION['isUserLoggedIn'])){ echo 'readonly';} ?> type="text" class="toggleAttr middleName_' + i +'" placeholder="Middle Name" name="middleName_[]"></label></div>';
						runnerContent += '<div class="medium-3 cell">';
						runnerContent += '<label>Gender:';
						runnerContent += '<input type="text" readonly class="toggleInputSelectAttr hide gender1_' + i + ' "></input>';
						runnerContent += '<select <?php if(isset($_SESSION['isUserLoggedIn'])){ echo 'readonly';} ?> class="toggleSelectAttr show gender_' + i + '" name="gender_[]"><option value="male">Male</option><option value="female">Female</option><option value="notMentioned">I\'d rather not say</option></select></label>';
						runnerContent += '</div>';
						runnerContent += '<div class="medium-4 cell"><label>Clubs / Groups:<input <?php if(isset($_SESSION['isUserLoggedIn'])){ echo 'readonly';} ?> class="toggleAttr clubsGroups_' + i + '" type="text" name="club_group_[]" placeholder="Your membership to other clubs or groups"></label></div>';
						runnerContent += '<div class="medium-8 cell"><label>Address:<input <?php if(isset($_SESSION['isUserLoggedIn'])){ echo 'readonly';} ?> class="toggleAttr address_' + i + '" name="address_[]" type="text" placeholder="Home address" required></label></div>';
						runnerContent += '<div class="medium-4 cell"><label>Mobile / Phone:<input <?php if(isset($_SESSION['isUserLoggedIn'])){ echo 'readonly';} ?> class="toggleAttr mobile_' + i + '" type="text" name="mobile_phone_[]" placeholder="Mobile or Landline" required></label></div>';
						runnerContent += '<div class="medium-4 cell"><label>Email:<input <?php if(isset($_SESSION['isUserLoggedIn'])){ echo 'readonly';} ?> <?php if(isset($_SESSION['isUserLoggedIn'])){ echo 'value=" '.$_SESSION['sess_user_email'].' "';} ?> type="email" class="toggleAttr theEmail email_' + i + '" name="email_[]" placeholder="your@email.com" pattern="email" required><span class="form-error email-error">Valid email is required.</span></label></div>';
						runnerContent += '<div class="medium-4 cell">';
						runnerContent += '<label>Age Group:';
						runnerContent += '<input type="text" readonly class="toggleInputSelectAttr hide ageGroup1_' + i + ' "></input>';
						runnerContent += '<select <?php if(isset($_SESSION['isUserLoggedIn'])){ echo 'readonly';} ?> class="toggleSelectAttr show ageGroup_' + i + '" name="age_group_[]">';
						runnerContent += '<option value="5-9">5-9</option>';
						runnerContent += '<option value="10-14">10-14</option>';
						runnerContent += '<option value="15-19">15-19</option>';
						runnerContent += '<option value="20-24">20-24</option>';
						runnerContent += '<option value="25-29">25-29</option>';
						runnerContent += '<option value="30-34">30-34</option>';
						runnerContent += '<option value="35-39">35-39</option>';
						runnerContent += '<option value="40-44">40-44</option>';
						runnerContent += '<option value="45-49">45-49</option>';
						runnerContent += '<option value="50-54">50-54</option>';
						runnerContent += '<option value="55-59">55-59</option';
						runnerContent += '><option value="60-64">60-64</option>';
						runnerContent += '<option value="65-69">65-69</option>';
						runnerContent += '<option value="70-80">70-80</option>';
						runnerContent += '<option value="80-90">80-90</option>';
						runnerContent += '<option value="100up">100up</option>';
						runnerContent += '</select></label></div>';
						runnerContent += '</div>';
						runnerContent += '</div>';

						i++;
					}

					$('.runner_content_div').html(runnerContent);
					$('.runner_btn_div').html(runnerbtn);
					$('input[name="theRunnersCount"]').val(theCount);

					$('select[name="how_many_runners"]').attr('disabled', 'disabled');
					$('.howManyRunnersBtn').attr('disabled', 'disabled');
					// $('input[name="howManyRunnersValue"]').val(theCount);

					setTimeout(() => {
						$('#participant_info-label').click();
					}, 1000);

					if (theCount > 0) {
						$('.eContactDiv').removeClass('hide').show();
						$('.part_info').removeClass('hide').show();

						<?php if(!isset($_SESSION['isUserLoggedIn'])):?>
							$('.part_info_text').html('Click on runners tab to add details');
							<?php else: ?>
							$('.part_info_text').html('');
						<?php endif; ?>
					} else {
						//hide continue-cancel buttons
						//$('.part_info').addClass('hide').hide();
						$('.part_info_text').html('');
						$('.eContactDiv').addClass('hide').hide();
						$('.part_info').addClass('hide').hide()
					}
					// console.log(theCount);

				<?php if(!isset($_SESSION['isUserLoggedIn'])): //removes alert for logged in users?>
				} //answer
				<?php endif; ?>
			});

			<?php if(isset($_SESSION['isUserLoggedIn'])):?>
			
			//add more runners (logged in)
				var counter = 0;
				var newRunnerItemGroup   = $(".newRunnerItemGroup"); 
				var add_button      = $(".addNewRunnerItem");
				var remove_button   = $(".delNewRunnerItem");
				var additionalRunners = $('input[name="additionalRunners"]');

				add_button.click(function () {	

					if(additionalRunners.val() < 1){
						additionalRunners.val('1');
					}
					 

					else if (counter > 10) {
						alert("Only 10 runners allowed to add per transaction.");
						return false;
					}
					else{
						additionalRunners.val(counter + 1);
					}

					var newRunner_item_ = $(document.createElement('div')).attr("id", 'newRunner_item_' + counter);
					var runnerContent = '<div class="grid-container callout"><div class="grid-x grid-margin-x grid-margin-y"><div class="cell medium-6"><h1>Additional Runner '+ (counter + 1) +'</h1> <label class="float-right"><input class="hide" type="radio" name="make_main_account" id="' + counter + '" value="" ><input type="hidden" name="mma_[]" id="mma_' + counter + '" class="mma" value="no" readonly ></label></div><h2 class="cell large-12">Race Kits & Details</h2><div class="cell medium-6"><label><h3>Choose Race Category:</h3><select id="' + counter + '" class="crc choose_race_category_' + counter +'" name="choose_race_category_[]" required><option value="">-choose-</option><?php foreach ($categories as $race_category):?><option data-name="<?=$race_category["rc_name"];?>" data-fee="<?=$race_category["rc_fee"];?>" value="<?=$race_category["rc_id"];?>"><?=$race_category["rc_name"];?> - ₱<?=$race_category["rc_fee"];?></option><?php endforeach; ?></select></label></div><div class="cell medium-6"><label><h3>Singlets</h3><select class="choose_singlet_size_' + counter + '" name="choose_singlet_size_[]" required><option value="">-choose-</option><?php foreach ($sizes as $singlet_sizes):?><option value="<?=$singlet_sizes["ss_id"];?>" data-value="<?=$singlet_sizes["ss_name"];?>"><?=$singlet_sizes["ss_name"];?></option><?php endforeach; ?></select></label><ul><li>* Availability is subject to change without prior notice.</li></ul></div><hr class="cell large-12"><h2 class="cell large-12">Personal Contact Details</h2><div class="medium-3 cell"><label>Last Name:<input type="text" class="toggleAttr lastName_' + counter + '" placeholder="Last Name" name="lastName_[]" required></label></div><div class="medium-3 cell"><label>First Name:<input type="text" class="toggleAttr firstName_' + counter + '" placeholder="First Name" name="firstName_[]" required></label></div><div class="medium-3 cell"><label>Middle Name:<input type="text" class="toggleAttr middleName_' + counter + '" placeholder="Middle Name" name="middleName_[]"></label></div><div class="medium-3 cell"><label>Gender:<input type="text" readonly class="toggleInputSelectAttr hide gender1_' + counter + ' "></input><select class="toggleSelectAttr show gender_' + counter + '" name="gender_[]"><option value="male">Male</option><option value="female">Female</option><option value="notMentioned">I\'d rather not say</option></select></label></div><div class="medium-4 cell"><label>Clubs / Groups:<input class="toggleAttr clubsGroups_' + counter + '" type="text" name="club_group_[]" placeholder="Your membership to other clubs or groups"></label></div><div class="medium-8 cell"><label>Address:<input class="toggleAttr address_' + counter + '" name="address_[]" type="text" placeholder="Home address" required></label></div><div class="medium-4 cell"><label>Mobile / Phone:<input class="toggleAttr mobile_' + counter + '" type="text" name="mobile_phone_[]" placeholder="Mobile or Landline" required></label></div><div class="medium-4 cell"><label>Email:<input type="email" class="toggleAttr theEmail email_' + counter + '" name="email_[]" placeholder="your@email.com" pattern="email" required><span class="form-error email-error">Valid email is required.</span></label></div><div class="medium-4 cell"><label>Age Group:<input type="text" readonly class="toggleInputSelectAttr hide ageGroup1_' + counter + ' "></input><select class="toggleSelectAttr show ageGroup_' + counter + '" name="age_group_[]"><option value="5-9">5-9</option><option value="10-14">10-14</option><option value="15-19">15-19</option><option value="20-24">20-24</option><option value="25-29">25-29</option><option value="30-34">30-34</option><option value="35-39">35-39</option><option value="40-44">40-44</option><option value="45-49">45-49</option><option value="50-54">50-54</option><option value="55-59">55-59</option><option value="60-64">60-64</option><option value="65-69">65-69</option><option value="70-80">70-80</option><option value="80-90">80-90</option><option value="100up">100up</option></select></label></div></div></div>';

					newRunner_item_.after().html(runnerContent);
					newRunner_item_.appendTo(newRunnerItemGroup);
					//additionalRunners.val(counter);
					console.log(counter);
					counter++;
				});

				remove_button.click(function () {
					if(counter == 1 || additionalRunners.val() == '-1'){
						additionalRunners.val('0');
					}
					
					else if (counter == 0) {
						 additionalRunners.val('0');
						alert("No more additional runners to remove");
						return false;
					}
					else{
						additionalRunners.val(counter - 1);
					}

					counter--;
					$("#newRunner_item_"	 + counter).remove();
					console.log(counter);

					

				});
			// });
			<?php endif; ?>

			$('#participant_info-label').on('click', function () {
				setTimeout(() => {
					$('.runner_1').click();
				}, 1000);
				// console.log('click');
			});

			//if crc is changed, update race cat
			$('body').on("change", '.crc', function () {
				var id = $(this).attr("id");
				var crcVal = $('.choose_race_category_' + id).val();
				var mma = $('#mma_' + id).val();

				if (mma == '1') {

					$('input[name="choose_race_category"]').val(crcVal);
					// console.log(crcVal);
				}
			});

			//if email is changed, remove the checked in "Make this main acocunt"
			$('body').on("change", '.theEmail', function () {

				var emailVal = $(this).val();
				var chosen_runners_email_input = $('input[name="chosen_runners_email"]');
				var chosen_runners_email_div = $('.chosen_runners_email');
				var chosen_div = $('.chosen_div');

				chosen_runners_email_div.text('');
				chosen_div.addClass('hide').hide();
				
				<?php if(!isset($_SESSION['isUserLoggedIn'])):?>
				chosen_runners_email_input.val('');
				$('input[name="make_main_account"]').prop('checked', false);
				<?php endif;?>
				//console.log( emailVal );
			});
			
			//button copyAllDetails clicked
			$('body').on("click", '.copyAllDetails', function () {
				setTimeout(() => {
					$('input[name="make_main_account').prop('checked', true).click();
					$('.copyAllDetails').hide();
				}, 800); 
			});

			//get the ID of make_main_account
			$('body').on("click", 'input[name="make_main_account"]', function () {

				if ($(this).is(':checked')) {
					$('.copyAllDetails').hide();
					var id = $(this).attr("id");
					var toggleContentDiv = $('.toggleContent_' + id);

					var $mma = $('#mma_' + id);
					var $mma_class = $('.mma');


					//general input class for toggling attributes
					var toggleAttr = $('.toggleAttr');
					var toggleInputSelectAttr = $('.toggleInputSelectAttr');
					var toggleSelectAttr = $('.toggleSelectAttr');

					//values
					var existing_raceCatVal = toggleContentDiv.find('.choose_race_category_' + id).val();
					var existing_singletSizeVal = toggleContentDiv.find('.choose_singlet_size_' + id).val();

					var existing_emailVal = toggleContentDiv.find('.email_' + id).val();
					var existing_firstName = toggleContentDiv.find('.firstName_' + id).val();
					var existing_lastName = toggleContentDiv.find('.lastName_' + id).val();
					var existing_middleName = toggleContentDiv.find('.middleName_' + id).val();
					var existing_gender = toggleContentDiv.find('.gender_' + id).val();
					var existing_address = toggleContentDiv.find('.address_' + id).val();
					var existing_mobile = toggleContentDiv.find('.mobile_' + id).val();
					var existing_ageGroup = toggleContentDiv.find('.ageGroup_' + id).val();
					var existing_clubsGroups = toggleContentDiv.find('.clubsGroups_' + id).val();

					//classes
					var email = toggleContentDiv.find('.email_' + id);
					var firstName = toggleContentDiv.find('.firstName_' + id);
					var lastName = toggleContentDiv.find('.lastName_' + id);
					var middleName = toggleContentDiv.find('.middleName_' + id);
					var address = toggleContentDiv.find('.address_' + id);
					var mobile = toggleContentDiv.find('.mobile_' + id);
					var clubsGroups = toggleContentDiv.find('.clubsGroups_' + id);

					var ageGroup = toggleContentDiv.find('.ageGroup_' + id);
					var ageGroup1 = toggleContentDiv.find('.ageGroup1_' + id);

					var gender = toggleContentDiv.find('.gender_' + id);
					var gender1 = toggleContentDiv.find('.gender1_' + id);


					//inputs
					var chosen_div = $('.chosen_div');
					var chosen_runners_email_div = $('.chosen_runners_email');

					var chosen_runners_singlet_size_input = $('input[name="choose_singlet_size"]');
					var chosen_runners_race_cat_input = $('input[name="choose_race_category"]');

					var chosen_runners_email_input = $('input[name="chosen_runners_email"]');
					var chosen_runners_first_name_input = $('input[name="chosen_runners_first_name"]');
					var chosen_runners_last_name_input = $('input[name="chosen_runners_last_name"]');
					var chosen_runners_middle_name_input = $('input[name="chosen_runners_middle_name"]');
					var chosen_runners_gender_input = $('input[name="chosen_runners_gender"]');
					var chosen_runners_address_input = $('input[name="chosen_runners_address"]');
					var chosen_runners_mobile_input = $('input[name="chosen_runners_mobile"]');
					var chosen_runners_ageGroup_input = $('input[name="chosen_runners_ageGroup"]');
					var chosen_runners_clubsGroups_input = $('input[name="chosen_runners_clubsGroups"]');

					//ecd values
					// var existing_ecd_cr_relationship = toggleContentDiv.find('.ecd_relationship_'+id).val();
					// var existing_ecd_cr_first_name = toggleContentDiv.find('.ecd_firstName_'+id).val();
					// var existing_ecd_cr_last_name = toggleContentDiv.find('.ecd_lastName_'+id).val();
					// var existing_ecd_cr_middle_name = toggleContentDiv.find('.ecd_middleName_'+id).val();
					// var existing_ecd_cr_address = toggleContentDiv.find('.ecd_address_'+id).val();
					// var existing_ecd_cr_mobile = toggleContentDiv.find('.ecd_mobile_'+id).val();
					// var existing_ecd_cr_email = toggleContentDiv.find('.ecd_email_'+id).val();

					//ecd classes
					// var get_ecd_cr_relationship = toggleContentDiv.find('.ecd_relationship_'+id);
					// var get_ecd_cr_first_name = toggleContentDiv.find('.ecd_firstName_'+id);
					// var get_ecd_cr_last_name = toggleContentDiv.find('.ecd_lastName_'+id);
					// var get_ecd_cr_middle_name = toggleContentDiv.find('.ecd_middleName_'+id);
					// var get_ecd_cr_address = toggleContentDiv.find('.ecd_address_'+id);
					// var get_ecd_cr_mobile = toggleContentDiv.find('.ecd_mobile_'+id);
					// var get_ecd_cr_email = toggleContentDiv.find('.ecd_email_'+id);

					//ecd inputs
					var ecd_cr_relationship_input = $('input[name="ecd_cr_relationship"]');
					var ecd_cr_first_name_input = $('input[name="ecd_cr_first_name"]');
					var ecd_cr_last_name_input = $('input[name="ecd_cr_last_name"]');
					var ecd_cr_middle_name_input = $('input[name="ecd_cr_middle_name"]');
					var ecd_cr_address_input = $('input[name="ecd_cr_address"]');
					var ecd_cr_mobile_input = $('input[name="ecd_cr_mobile"]');
					var ecd_cr_email_input = $('input[name="ecd_cr_email"]');



					$.ajax({
						url: '<?php echo base_url('events/ajax/check_email')?>',
						type: "POST",
						dataType: "json",
						data: "existing_mail=" + existing_emailVal + "&existing_raceCat=" + existing_raceCatVal +
							"&existing_singletSize=" + existing_singletSizeVal,
						success: function (data) {

							//check if email is already registered
							if (data.status == 'RegNewAccount') {
								$('.registration-error-messages').html(
									'<div class="success callout cell large-12" data-closable > <h3>' + data.msg +
									'</h3><button class="close-button" aria-label="Dismiss alert" type="button" data-close><span aria-hidden="true">&times;</span></button></div>'
								);

								$('html,body').animate({
									scrollTop: 0
								}, 'slow');


								$('.chosen_div').removeClass('hide').show();
								$('.chosen_runner_span').html(id);

								chosen_runners_email_div.html(existing_emailVal);

								chosen_runners_race_cat_input.val(existing_raceCatVal);
								chosen_runners_singlet_size_input.val(existing_singletSizeVal);

								chosen_runners_email_input.val(existing_emailVal);
								chosen_runners_first_name_input.val(existing_firstName);
								chosen_runners_last_name_input.val(existing_lastName);
								chosen_runners_middle_name_input.val(existing_middleName);
								chosen_runners_gender_input.val(existing_gender);
								chosen_runners_address_input.val(existing_address);
								chosen_runners_mobile_input.val(existing_mobile);
								chosen_runners_ageGroup_input.val(existing_ageGroup);
								chosen_runners_clubsGroups_input.val(existing_clubsGroups);

								// ecd_cr_relationship_input.val(existing_ecd_cr_relationship); 
								// ecd_cr_first_name_input.val(existing_ecd_cr_first_name); 
								// ecd_cr_last_name_input.val(existing_ecd_cr_last_name); 
								// ecd_cr_middle_name_input.val(existing_ecd_cr_middle_name); 
								// ecd_cr_address_input.val(existing_ecd_cr_address); 
								// ecd_cr_mobile_input.val(existing_ecd_cr_mobile);
								// ecd_cr_email_input.val(existing_ecd_cr_email);

								$mma_class.val('0')

								setTimeout(() => {
									$mma.val('1');
								}, 800);

								console.log('RegNewAccount!');
							} else if (data.status == 'EventDone') {

								$('.registration-error-messages').html(
									'<div class="alert callout cell large-12" data-closable > <h3>' + data.msg +
									'</h3><button class="close-button" aria-label="Dismiss alert" type="button" data-close><span aria-hidden="true">&times;</span></button></div>'
								);

								$('html,body').animate({
									scrollTop: 0
								}, 'slow');
								$('.copyAllDetails').show();
								console.log('EventDone!');
							} else if (data.status == 'AlreadyLoggedIn') {
								toggleAttr.prop('readonly', false);
								toggleInputSelectAttr.addClass('hide').hide();
								toggleSelectAttr.removeClass('hide').show();

								gender1.val('');
								ageGroup1.val('');
								$('.registration-error-messages').html(
									'<div class="alert callout cell large-12" data-closable > <h3>' + data.msg +
									'</h3><button class="close-button" aria-label="Dismiss alert" type="button" data-close><span aria-hidden="true">&times;</span></button></div>'
								);

								chosen_div.addClass('hide').hide();
								$('input[name="make_main_account"]').prop('checked', false);
								chosen_runners_email_input.val('');

								$mma_class.val('0')

								setTimeout(() => {
									$mma.val('1');
								}, 800);

								$('html,body').animate({
									scrollTop: 0
								}, 'slow');

								console.log('AlreadyLoggedIn!');
							} else if (data.status == 'IsLoggedIn') {


								// else{

								//auto fill all fields for this runner based on details of logged in user

								var get_email = data.email;
								var get_firstName = data.firstName;
								var get_lastName = data.lastName;
								var get_middleName = data.middleName;
								var get_gender = data.gender;
								var get_address = data.address;
								var get_mobile = data.mobile;
								var get_ageGroup = data.ageGroup;
								var get_clubsGroups = data.clubsGroups;

								var get_ecd_relationship = data.ecd_relationship;
								var get_ecd_firstName = data.ecd_firstName;
								var get_ecd_lastName = data.ecd_lastName;
								var get_ecd_middleName = data.ecd_middleName;
								var get_ecd_address = data.ecd_address;
								var get_ecd_mobile = data.ecd_mobile;
								var get_ecd_email = data.ecd_email;


								toggleAttr.prop('readonly', false);

								toggleInputSelectAttr.addClass('hide').hide();
								toggleSelectAttr.removeClass('hide').show();

								gender1.val('');
								ageGroup1.val('');
								setTimeout(() => {
									//toggleInputSelectAttr.removeClass('hide').show();
									//toggleSelectAttr.hide();

									gender1.val(get_gender).removeClass('hide').show();
									ageGroup1.val(get_ageGroup).removeClass('hide').show();

									gender.addClass('hide').hide();
									ageGroup.addClass('hide').hide();

									// gender.val(get_gender).prop('disabled', true); 
									// ageGroup.val(get_ageGroup).prop('disabled', true);

									email.val(get_email).prop('readonly', true);
									firstName.val(get_firstName).prop('readonly', true);
									lastName.val(get_lastName).prop('readonly', true);
									middleName.val(get_middleName).prop('readonly', true);
									address.val(get_address).prop('readonly', true);
									mobile.val(get_mobile).prop('readonly', true);
									clubsGroups.val(get_clubsGroups).prop('readonly', true);

									chosen_runners_race_cat_input.val(existing_raceCatVal);
									chosen_runners_singlet_size_input.val(existing_singletSizeVal);

									chosen_runners_email_div.html(get_email);
									chosen_runners_email_input.val(get_email);
									chosen_runners_first_name_input.val(get_firstName);
									chosen_runners_last_name_input.val(get_lastName);
									chosen_runners_middle_name_input.val(get_middleName);
									chosen_runners_gender_input.val(get_gender);
									chosen_runners_address_input.val(get_address);
									chosen_runners_mobile_input.val(get_mobile);
									chosen_runners_ageGroup_input.val(get_ageGroup);
									chosen_runners_clubsGroups_input.val(get_clubsGroups);

									// get_ecd_cr_relationship.val(get_ecd_relationship);
									// get_ecd_cr_first_name.val(get_ecd_firstName);
									// get_ecd_cr_last_name.val(get_ecd_lastName);
									// get_ecd_cr_middle_name.val(get_ecd_middleName);
									// get_ecd_cr_address.val(get_ecd_address);
									// get_ecd_cr_mobile.val(get_ecd_mobile);
									// get_ecd_cr_email.val(get_ecd_email);


									ecd_cr_relationship_input.val(get_ecd_relationship);
									ecd_cr_first_name_input.val(get_ecd_firstName);
									ecd_cr_last_name_input.val(get_ecd_lastName);
									ecd_cr_middle_name_input.val(get_ecd_middleName);
									ecd_cr_address_input.val(get_ecd_address);
									ecd_cr_mobile_input.val(get_ecd_mobile);
									ecd_cr_email_input.val(get_ecd_email);


									$('.registration-error-messages').html(
										'<div class="success callout cell large-12" data-closable > <h3>' + data.msg +
										'</h3><button class="close-button" aria-label="Dismiss alert" type="button" data-close><span aria-hidden="true">&times;</span></button></div>'
									);

									chosen_div.removeClass('hide').show();
									//$('input[name="make_main_account"]').prop('checked', true);

									$mma_class.val('0');

									$('.chosen_runner_span').html('1');

									setTimeout(() => {
										$mma.val('1');
									}, 800);

									$('html,body').animate({
										scrollTop: 0
									}, 'slow');

									console.log('IsLoggedIn!');
								}, 800);
								// }


							}
							//if logged-in, check if race cat and singlet is not blank
							else if (data.status == 'RaceCatSingletError!') {
								toggleAttr.prop('readonly', false);

								toggleInputSelectAttr.addClass('hide').hide();
								toggleSelectAttr.removeClass('hide').show();

								gender1.val('');
								ageGroup1.val('');

								// ageGroup1.val('').addClass('hide').hide();
								// gender1.val('').addClass('hide').hide();

								// ageGroup.removeClass('hide').show();
								// gender.removeClass('hide').show();


								// email.removeProp('readonly'); 
								// firstName.removeProp('readonly'); 
								// lastName.removeProp('readonly'); 
								// middleName.removeProp('readonly'); 
								// gender.removeProp('readonly'); 
								// address.removeProp('readonly'); 
								// mobile.removeProp('readonly');
								//  ageGroup.removeProp('disabled'); 
								// clubsGroups.removeProp('readonly'); 

								$('input[name="make_main_account"]').prop('checked', false);
								$mma_class.val('0');
								$('.registration-error-messages').html(
									'<div class="alert callout cell large-12" data-closable> <h3>Please choose Race Category and Singlet before we proceed.</h3><hr><ul class="error-wrapper"></ul><button class="close-button" aria-label="Dismiss alert" type="button" data-close><span aria-hidden="true">&times;</span></button></div>'
								);

								if (data.existing_raceCat_error != '') {
									$('.error-wrapper').append('<li>' + data.existing_raceCat_error + '</li>');
								}
								if (data.existing_singletSize_error != '') {
									$('.error-wrapper').append('<li>' + data.existing_singletSize_error + '</li>');
								}


								chosen_div.addClass('hide').hide();
								$('html,body').animate({
									scrollTop: 0
								}, 'slow');
								$('.copyAllDetails').show();
								console.log('RaceCatSingletError!');
							} else if (data.status == 'AlreadyRegistered') {
								//--- if already registered, show login modal if not logged in
								$('.registration-error-messages').html(
									'<div class="alert callout cell large-12" data-closable > <h3>' + data.msg +
									'</h3><button class="close-button" aria-label="Dismiss alert" type="button" data-close><span aria-hidden="true">&times;</span></button></div>'
								);

								chosen_runners_email_div.text('');

								chosen_runners_singlet_size_input.val('');
								chosen_runners_race_cat_input.val('');

								chosen_runners_email_input.val('');
								chosen_runners_first_name_input.val('');
								chosen_runners_last_name_input.val('');
								chosen_runners_middle_name_input.val('');
								chosen_runners_gender_input.val('');
								chosen_runners_address_input.val('');
								chosen_runners_mobile_input.val('');
								chosen_runners_ageGroup_input.val('');
								chosen_runners_clubsGroups_input.val('');

								// ecd_cr_relationship_input.val(''); 
								// ecd_cr_first_name_input.val(''); 
								// ecd_cr_last_name_input.val(''); 
								// ecd_cr_middle_name_input.val(''); 
								// ecd_cr_address_input.val(''); 
								// ecd_cr_mobile_input.val('');
								// ecd_cr_email_input.val(''); 

								chosen_div.addClass('hide').hide();
								$('input[name="make_main_account"]').prop('checked', false);
								$mma_class.val('0');
								$('html,body').animate({
									scrollTop: 0
								}, 'slow');
								$('.copyAllDetails').show();
								console.log('AlreadyRegistered!');
							} else {
								$('.registration-error-messages').html(
									'<div class="alert callout cell large-12" data-closable> <h3>We need some details!</h3><hr><ul class="error-wrapper"></ul><button class="close-button" aria-label="Dismiss alert" type="button" data-close><span aria-hidden="true">&times;</span></button></div>'
								);

								if (data.email_error != '') {
									$('.error-wrapper').append('<li>' + data.email_error + '</li>');
								}
								if (data.existing_raceCat_error != '') {
									$('.error-wrapper').append('<li>' + data.existing_raceCat_error + '</li>');
								}
								if (data.existing_singletSize_error != '') {
									$('.error-wrapper').append('<li>' + data.existing_singletSize_error + '</li>');
								}

								//console.log('Error!'+data.existing_singletSize_error);

								chosen_runners_race_cat_input.val('');
								chosen_runners_singlet_size_input.val('');

								chosen_runners_email_input.val('');
								chosen_runners_email_div.text('');
								chosen_runners_first_name_input.val('');
								chosen_runners_last_name_input.val('');
								chosen_runners_middle_name_input.val('');
								chosen_runners_gender_input.val('');
								chosen_runners_address_input.val('');
								chosen_runners_mobile_input.val('');
								chosen_runners_ageGroup_input.val('');
								chosen_runners_clubsGroups_input.val('');

								// ecd_cr_relationship_input.val(''); 
								// ecd_cr_first_name_input.val(''); 
								// ecd_cr_last_name_input.val(''); 
								// ecd_cr_middle_name_input.val(''); 
								// ecd_cr_address_input.val(''); 
								// ecd_cr_mobile_input.val('');
								// ecd_cr_email_input.val(''); 

								chosen_div.addClass('hide').hide();
								$('input[name="make_main_account"]').prop('checked', false);
								$mma_class.val('0');
								//email.focus();
								//alert("Please add email address!");

								$('html,body').animate({
									scrollTop: 0
								}, 'slow');
								
								$('.copyAllDetails').show();
							}

							 
						},
						error: function (data) {
							chosen_div.addClass('hide').hide();
							$('input[name="make_main_account"]').prop('checked', false);
							$mma_class.val('0');
							$('.registration-error-messages').html(
								'<div class="alert callout cell large-12" data-closable > <h3>There is something wrong!</h3><button class="close-button" aria-label="Dismiss alert" type="button" data-close><span aria-hidden="true">&times;</span></button></div>'
							);
							$('.copyAllDetails').show();
							console.log(data);
						}

					});



				}

			});



			//review details
			$('#review_information-label, .next_review_info').on("click", function () {


				/* Currency */
				var currency = "₱";

				/* fields*/
				var raceKitName = $('#raceKitName');
				var raceKitSize = $('#raceKitSize');
				var receiver = $('#receiver');
				var deliveryAddress = $('#deliveryAddress');
				var deliveryFees = $('#deliveryFees');
				// var fees_fullName = $('#fees_fullName');
				// var fees_eventName = $('#fees_eventName');
				// var fees_raceCategoryName = $('#fees_raceCategoryName');
				var fees = $('#fees');
				var fees_webFee = $('#fees_webFee');
				var totalAmount = $('#totalAmount');

				/* values*/
				// var additionalRunnersINPUT =  $('input[name="additionalRunners"]').val();
				var additionalRunners = parseInt($('input[name="additionalRunners"]').val());
				var theRunnersCount = parseInt($('input[name="theRunnersCount"]').val());

				// if(additionalRunnersINPUT < 1 || additionalRunnersINPUT == ''){
				// 	var theRunnersCount_and_additionalRunners = theRunnersCount;
				// }else{
					var theRunnersCount_and_additionalRunners = theRunnersCount + additionalRunners;
				// }

				console.log(theRunnersCount_and_additionalRunners);

				var chosen_runners_first_name = $('input[name="chosen_runners_first_name"]').val();
				var chosen_runners_last_name = $('input[name="chosen_runners_last_name"]').val();
				var chosen_runners_middle_name = $('input[name="chosen_runners_middle_name"]').val();


				//---firstNames
				var firstNameVal = $('input[name="firstName_"]').val();
				var firstNames = [];
				$('input[name^="firstName_"]').each(function () {
					firstNames.push($(this).val());
				});

				//---lastNames
				var lastNameVal = $('input[name="lastName_"]').val();
				var lastNames = [];
				$('input[name^="lastName_"]').each(function () {
					lastNames.push($(this).val());
				});

				//---middleNames
				var middleNameVal = $('input[name="middleName_ "]').val();
				var middleNames = [];
				$('input[name^="middleName_"]').each(function () {
					middleNames.push($(this).val());
				});

				//---choose_singlet_sizes
				var choose_singlet_sizeVal = $('select[name="choose_singlet_size_"] option:selected').data("value"); //.find(':selected').data("value");
				var choose_singlet_sizes = [];
				$('select[name^="choose_singlet_size_"] option:selected').each(function () {
					choose_singlet_sizes.push($(this).data("value"));
				});
				console.log(choose_singlet_sizes + ',');

				//---choose_race_categories
				var choose_race_categoryValName = $('select[name="choose_race_category_"] option:selected').data("name");
				var choose_race_category_names = [];
				$('select[name^="choose_race_category_"] option:selected').each(function () {
					choose_race_category_names.push($(this).data("name"));
				});
				console.log(choose_race_category_names + ',');

				//---choose_race_categoryFees
				var choose_race_categoryFeeVal = $('select[name="choose_race_category_"] option:selected').data("fee");
				var choose_race_category_fees = [];
				$('select[name^="choose_race_category_"] option:selected').each(function () {
					choose_race_category_fees.push($(this).data("fee"));
				});
				//console.log(choose_race_category_fees+',');

				var raceCategoryTotal = choose_race_category_fees.reduce(add, 0);

				function add(a, b) {
					return a + b;
				}

				console.log("raceCategoryTotal: " + raceCategoryTotal);


				var delivery_addressVal = $('input[name="delivery_address"]').val();
				//var genderVal = $('select[name="gender_[]"]').val();
				var fees_webFeeVal = <?=$web_fee['f_cost'];?>;
				var fees_webFeeTotal = <?=$web_fee['f_cost'];?> * theRunnersCount_and_additionalRunners;
				var deliveryFeesVal = <?=$delivery_fee_fixed['f_cost'];?>;

				if ($('select[name="raceKitDeliverPickup"]').val() == 'delivered') {
					var totalAmountVal = fees_webFeeTotal + deliveryFeesVal + raceCategoryTotal;
				} else {
					var totalAmountVal = fees_webFeeTotal + raceCategoryTotal;
				}

				//---Total price for race categories by each runner
				$('input[name=raceCategoryTotal]').val(raceCategoryTotal);
				$('input[name=webFeeTotal]').val(fees_webFeeTotal);

				/** VALIDATE FIRST */
				if (
					$('select[name="raceKitDeliverPickup"]').val() != "" &&
					$('select[name="choose_race_category_[]"]').val() != "" &&
					$('select[name="choose_singlet_size_[]"]').val() != "" &&
					firstNameVal != "" &&
					lastNameVal != ""
				) {

					//race kits section						 
					var namesList = "";
					var i = 0;
					while (i < theRunnersCount_and_additionalRunners) {
						namesList += '<tr>';
						namesList += '<td>' + lastNames[i] + ', ' + firstNames[i] + ' ' + middleNames[i] + '</td>';
						namesList += '<td>' + choose_singlet_sizes[i] + '</td>';
						namesList += '<td>To be assigned</td>';
						namesList += '</tr>';

						i++
					}

					raceKitName.html(namesList);



					//Claiming Details (Delivery) section
					receiver.html(chosen_runners_last_name + ', ' + chosen_runners_first_name + ' ' + chosen_runners_middle_name);
					deliveryFees.html(currency + deliveryFeesVal.toLocaleString());
					deliveryAddress.html(delivery_addressVal);



					//TOTAL
					totalAmount.html(currency + totalAmountVal.toLocaleString());

					//registration fees
					// fees_fullName.html(lastNameVal+', '+firstNameVal+' '+middleNameVal);
					// fees_eventName.html('<?= $events_item['re_name'];?>');

					var feesList = "";
					var i = 0;
					while (i < theRunnersCount_and_additionalRunners) {


						feesList += '<tr id="' + [i] + '">';
						feesList += '<td>' + lastNames[i] + ', ' + firstNames[i] + ' ' + middleNames[i] + '</td>';
						feesList += '<td><?= $events_item['re_name'];?></td>';
						feesList += '<td>' + choose_race_category_names[i] + '</td>';
						feesList += '<td>' + currency + choose_race_category_fees[i];
						feesList += '</td>';
						feesList += '<td>' + currency + fees_webFeeVal.toLocaleString() + '</td>';
						feesList += '</tr>';


						i++
					}

					fees.html(feesList);

				} else {
					console.log('blank: ' + choose_race_categoryValName);
					$('.registration-error-messages').html(
						'<div class="alert callout cell large-12" data-closable > <h3>Please fill in all needed details on each tabs.</h3><button class="close-button" aria-label="Dismiss alert" type="button" data-close><span aria-hidden="true">&times;</span></button></div>'
					);
					$('html,body').animate({
						scrollTop: 0
					}, 'slow');
				}
			});

			//claiming details
			var $next_review_info = $('.next_review_info');
			var $terms_condition_checkbox = $('input[name="terms_condition_checkbox"]');
			var $raceKitDeliverPickup = $('select[name="raceKitDeliverPickup"]');


			//Race kit delivery pickup
			$raceKitDeliverPickup.on("change", function () {

				setTimeout(() => {
					$('input[name="delivery_address"]').focus();
					if ($terms_condition_checkbox.prop('checked', true)) {
						$terms_condition_checkbox.prop('checked', false)
					}
					$next_review_info.addClass('hide'); //hide
				}, 200);

			});

			//Terms and Condition Checkbox
			$terms_condition_checkbox.on("change", function () {

				if ($raceKitDeliverPickup.val() == "delivered") {

					if ($('input[name="delivery_address"]').val() == '') {
						setTimeout(() => {
							$('input[name="delivery_address"]').focus();
							if ($terms_condition_checkbox.prop('checked', true)) {
								$terms_condition_checkbox.prop('checked', false)
							}
							$next_review_info.addClass('hide'); //hide

							$('.registration-error-messages').html(
								'<div class="alert callout cell large-12" data-closable > <h3>Please add delivery address.</h3><button class="close-button" aria-label="Dismiss alert" type="button" data-close><span aria-hidden="true">&times;</span></button></div>'
							);
							console.log('delivered BLANK');
						}, 200);
					} else {
						$next_review_info.removeClass('hide'); //show
						console.log('delivered NOT BLANK');
					}

				} else if ($raceKitDeliverPickup.val() == "picked_up") {
					$next_review_info.removeClass('hide'); //show
					console.log('picked_up');
				} else {
					$next_review_info.addClass('hide'); //hide
				}
			});

			//choose_race_category & choose_singlet_size
			var $choose_singlet_size = $("select[name='choose_singlet_size']");
			var $choose_race_category = $("select[name='choose_race_category']");
			var $go_to_participant_info_btn = $('.go_to_participant_info_btn');

			$choose_singlet_size.on("change", function () {
				// console.log($choose_singlet_size.val() );
				checRaceDetailsValue();
			});
			$choose_race_category.on("change", function () {
				// console.log('choose_race_category');
				checRaceDetailsValue();
			});

			function checRaceDetailsValue() {
				if ($choose_singlet_size.val() && $choose_race_category.val() != "") {
					$go_to_participant_info_btn.removeClass('hide').show();
				} else {
					$go_to_participant_info_btn.addClass('hide').hide();
				}
			}

			//raceKitDeliverPickup
			$("select[name='raceKitDeliverPickup']").on("change", function () {
				var thisValue = $(this).val();
				var race_kit_delivery_message = $('.race_kit_delivery_message');
				var race_kit_delivery_action = $('.race_kit_delivery_action');
				var race_kit_pickup_locations = $('.race_kit_pickup_locations');
				var pickup_or_delivery = $('#pickup_or_delivery');
				var deliverySelected = $('#deliverySelected');
				var pickupSelected = $('#pickupSelected');

				if (thisValue == "picked_up") {
					race_kit_delivery_message.html('at pickup locations.');
					race_kit_delivery_action.html(
						'<label for="middle-label" class="text-left middle" style="font-style:italic">see locations below</label>');
					race_kit_pickup_locations.removeClass('hide').show();
					pickup_or_delivery.text('');
					deliverySelected.addClass('hide').hide();
					pickupSelected.removeClass('hide').show();
				} else {
					race_kit_delivery_message.html('to this address');
					race_kit_delivery_action.html(
						'<input type="text" value="" id="middle-label" name="delivery_address" placeholder="Main account\'s address.">'
					);
					race_kit_pickup_locations.addClass('hide').hide();
					pickup_or_delivery.text('(Delivery)');
					deliverySelected.removeClass('hide').show();
					pickupSelected.addClass('hide').hide();
				}
			});



			//confirm runner's registration
			$("#reg_form").on("submit", function (ev) {
				ev.preventDefault();
				// var eventId = $(this).attr('id');

				var additionalRunners = parseInt($('input[name="additionalRunners"]').val());
				// var theRunnersCount = parseInt($('input[name="theRunnersCount"]').val());
				
				var how_many_runners = parseInt($('select[name="how_many_runners"]').val());
				var theCount = how_many_runners + additionalRunners - 1;

				$.ajax({
					url: '<?php echo base_url('events/ajax/reg')?>',
					type: "POST",
					dataType: "json",
					data: $("#reg_form").serialize(),
					success: function (data) {
						var test = $("#reg_form").serialize();


						if (data.status == 'Success!') {

							$('.registration-error-messages').html(
								'<div class="success callout cell large-12" data-closable > <h3>' + theCount +
								' Runner(s) Added! <a data-open="loginModal" aria-controls="loginModal" aria-haspopup="true" tabindex="0">Login</a> or refresh this page to add more runners.<br>We sent an email for your login details.</h3><button class="close-button" aria-label="Dismiss alert" type="button" data-close><span aria-hidden="true">&times;</span></button></div>'
							);
							//$('.final_confirm').remove();
							$('html,body').animate({
								scrollTop: 0
							}, 'slow');
							console.log(data);
						}
						if (data.status == 'Updated!') {

							$('.registration-error-messages').html(
								'<div class="success callout cell large-12" data-closable > <h3>' + theCount +
								' Runner(s) Added as participants.</h3><button class="close-button" aria-label="Dismiss alert" type="button" data-close><span aria-hidden="true">&times;</span></button></div>'
							);

							$('html,body').animate({
								scrollTop: 0
							}, 'slow');
							console.log(data);
						}
						if (data.status == 'Error!') {
							$('.registration-error-messages').html(
								'<div class="alert callout cell large-12" data-closable > <h3>There is something wrong! Please check that all fields has content.</h3><hr><ul class="error-wrapper"></ul><button class="close-button" aria-label="Dismiss alert" type="button" data-close><span aria-hidden="true">&times;</span></button></div>'
							);
							if (data.chosen_runners_email != '') {
								$('input[name="make_main_account"]').prop('checked', false);
							}

							if (data.ecd_cr_first_name != '') {
								$('.error-wrapper').append('<li>' + data.ecd_cr_first_name + '</li>');
							}
							if (data.ecd_cr_mobile != '') {
								$('.error-wrapper').append('<li>' + data.ecd_cr_mobile + '</li>');
							}
							if (data.chosen_runners_email != '') {
								$('.error-wrapper').append('<li>' + data.chosen_runners_email + '</li>');
							}
							if (data.emails != '') {
								$('.error-wrapper').append('<li>' + data.emails + '</li>');
							}
							if (data.lastNames != '') {
								$('.error-wrapper').append('<li>' + data.lastNames + '</li>');
							}
							if (data.firstNames != '') {
								$('.error-wrapper').append('<li>' + data.firstNames + '</li>');
							}
							if (data.genders != '') {
								$('.error-wrapper').append('<li>' + data.genders + '</li>');
							}
							if (data.addresses != '') {
								$('.error-wrapper').append('<li>' + data.addresses + '</li>');
							}
							if (data.ageGroups != '') {
								$('.error-wrapper').append('<li>' + data.ageGroups + '</li>');
							}
							if (data.raceKitDeliverPickup != '') {
								$('.error-wrapper').append('<li>' + data.raceKitDeliverPickup + '</li>');
							}
							if (data.delivery_address != '') {
								$('.error-wrapper').append('<li>' + data.delivery_address + '</li>');
							}
							if (data.payment_method != '') {
								$('.error-wrapper').append('<li>' + data.payment_method + '</li>');
							}
							if (data.terms_condition_checkbox != '') {
								$('.error-wrapper').append('<li>' + data.terms_condition_checkbox + '</li>');
							}

							if (data.choose_singlet_size_ != '') {
								$('.error-wrapper').append('<li>' + data.choose_singlet_size_ + '</li>');
							}
							if (data.choose_race_category_ != '') {
								$('.error-wrapper').append('<li>' + data.choose_race_category_ + '</li>');
							}

							$('html,body').animate({
								scrollTop: 0
							}, 'slow');
							console.log('testmike');
						}
						if (data.status == 'Confused!') {
							$('.registration-error-messages').html(
								'<div class="alert callout cell large-12" data-closable > <h3>' + data.msg +
								'</h3><button class="close-button" aria-label="Dismiss alert" type="button" data-close><span aria-hidden="true">&times;</span></button></div>'
							);
							$('html,body').animate({
								scrollTop: 0
							}, 'slow');
							console.log('Confused!');
						}
						if (data.status == 'AlreadyParticipant!') {
							$('.registration-error-messages').html(
								'<div class="alert callout cell large-12" data-closable > <h3>' + data.msg +
								'</h3><button class="close-button" aria-label="Dismiss alert" type="button" data-close><span aria-hidden="true">&times;</span></button></div>'
							);
							$('html,body').animate({
								scrollTop: 0
							}, 'slow');
							console.log('AlreadyParticipant!');
						}
						//console.log('Success:'+ test);
					},
					error: function (data) {
						var test = $("#reg_form").serialize();

						if (data.status == 'Confused!') {
							$('.registration-error-messages').html(
								'<div class="alert callout cell large-12" data-closable > <h3>' + data.msg +
								'</h3><button class="close-button" aria-label="Dismiss alert" type="button" data-close><span aria-hidden="true">&times;</span></button></div>'
							);
							$('html,body').animate({
								scrollTop: 0
							}, 'slow');
							console.log('Error. Confused!');
						} else {
							$('.registration-error-messages').html(
								'<div class="alert callout cell large-12" data-closable > <h3>You have invalid details!</h3><hr><ul class="error-wrapper"></ul><button class="close-button" aria-label="Dismiss alert" type="button" data-close><span aria-hidden="true">&times;</span></button></div>'
							);
							if (data.lastNames != '') {
								$('.error-wrapper').append('<li>' + data.lastNames + '</li>');
							}
							if (data.emails != '') {
								$('.error-wrapper').append('<li>' + data.emails + '</li>');
							}
							console.log('Invalid Details!');

							$('html,body').animate({
								scrollTop: 0
							}, 'slow');
						}
					}
				});
			});



		});

	</script>
