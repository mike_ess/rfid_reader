    

<section id="main-content">
  <div id="guts">

<div class="grid-container">
    <div class="grid-x grid-margin-x grid-margin-y">
        <div class="cell medium-9">

                <h2 class="text-center"><?=$events_item['re_name'];?></h2>
                <hr>
                <?php if(isset($_SESSION['isAdmin'])  ||  isset($_SESSION['isAuthor']) ||  isset($_SESSION['isContributor']) ) : ?>
				   <p > <a href="<?php echo base_url('events/edit/').$events_item['re_id']; ?>" class="button small">Edit This Event</a></p>
				<?php endif;?>
                <a href="<?=base_url('events/register/').$events_item['re_slug'];?>">
                    <?php
                        //  echo $events_item['mf_id'];
                        $mfId = $events_item['mf_id'];
                        if($mfId != '0'): 
                        $query = $this->db->query('SELECT * FROM media_files WHERE mf_id = "'.$events_item['mf_id'].'" ');
                        $row = $query->row();
                    ?>
                        <img src="<?=base_url('uploads/').$row->mf_file_name;?>" alt="<?=$events_item['re_slug'];?>">
                    <?php else: ?>
                        <img src="https://placehold.it/500x500&amp;text=No Available Image!" alt="image for article">
                    <?php endif; ?>
                </a>
                <?php echo $events_item['re_description']; ?>
                <hr>
                
                <?php if($events_item['re_tags'] != ''): ?>
                <div class="tags-wrap"><h4 class="float-left">Tags:</h4> <?php 
                    // $tags = explode(',', str_replace(' ', '', $events_item['re_tags'])); 
                    $tags = explode(',', $events_item['re_tags']); 
                    foreach($tags as $tag) {
                        $tag = ltrim($tag);
                        $taglink = str_replace(' ', '_', $tag);
                        echo '<p class="float-left"><a href="'.base_url('tags/').$taglink.'"> '.$tag.'</a>, </p>';
                    } ?> 
                </div>
                <?php endif; ?>

                

                <!-- <div class="grid-container">
                    <div class="grid-x grid-margin-x grid-margin-y callout">
                        <div class="cell medium-12 text-center">Other Events<hr></div>
                
                        <div class="cell medium-6">left</div>
                        <div class="cell medium-6 text-right">right</div>
                    </div>
                </div> -->
        </div>
        
        <div class="cell medium-3">
           
            <?php if($events_item['re_status'] == 'not_done'): ?>
                <a class="button large full" href="<?=base_url('events/register/').$events_item['re_slug'];?>"> Register to this event</a>
            <?php else: ?>
            <h3>This event is done.</h3>
            <a class="button small full" href="<?=base_url('results/events/').$events_item['re_slug'].'/'.$event_categories['rc_id'];?>"> Register to this event</a>
            <?php endif; ?>

       </div>
    </div>
</div>

</div>
</section>