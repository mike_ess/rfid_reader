<section id="main-content">
  <div id="guts">
<div class="grid-container">
	<div class="grid-x grid-margin-y">
		<div class="cell medium-12">



			<div class="text-center">
				<h1 style="margin: 0;" class="text-center">
					<?php echo $title; ?>
				</h1>
				<?php if(isset($_SESSION['isAdmin'])  ||  isset($_SESSION['isAuthor']) ||  isset($_SESSION['isContributor']) ) : ?>
				<a href="<?php echo base_url('events/add/new'); ?>" class="button float-right">Add New Event</a>
				<?php endif;?>
			</div>

			<hr>
			<article class="grid-container">
				<div class="grid-x grid-margin-x  grid-margin-y small-up-2 medium-up-4 large-up-5">

					<?php 
                        /** Start Show all events */ 
                        foreach ($events as $events_item): 
                    ?>

					<div class="cell">
                        <?php 
							$eventStatus = $events_item['re_publish_status'];
							
							if($eventStatus == '0'){
								echo '<small class="floating-label">Draft</small>';
                        }
                        ?>
                        <a href="<?php echo base_url('events/'.$events_item['re_slug']); ?>">
							
							<?php
							//  echo $events_item['mf_id'];
							$mfId = $events_item['mf_id'];
							if($mfId != '0'): 
							$query = $this->db->query('SELECT * FROM media_files WHERE mf_id = "'.$events_item['mf_id'].'" ');
							$row = $query->row();
							?>
								<img src="<?=base_url('uploads/ar_').$row->mf_file_name;?>" alt="<?=$events_item['re_slug'];?>">
							<?php else: ?>
								<img src="https://placehold.it/300x300&amp;text=No Available Image!" alt="image for article">
							<?php endif; ?>
						</a>
						<br>
						<?php echo $events_item['re_name']; ?>
					</div>

					<?php 
                        /** End Show all members */
                        endforeach; 
                    ?>

				</div>
			</article>
			<hr>

		</div>

	</div>
</div>
</div>
</section>
