 

<section id="main-content">
  <div id="guts">


	<form data-abide novalidate id="edit_event">
		<div class="grid-container">
			<div class="grid-x grid-margin-x grid-margin-y">
				<div class="cell medium-9">


					<div class="grid-container">
						<div class="grid-y grid-margin-y">
                            <?php if( !empty($events['re_name'] )): ?>
							
                                <div class="cell medium-12">
										 
									<a href="<?=base_url('events');?>" class="button float-left">Back To Event List</a>
									<hr>
								</div>
                            
                                <div class="cell medium-12">
                                    <div class="status-messages"> </div>
                                </div>
                                <div class="cell small-12">
                                    <label>Event Name
                                        <input type="text" value="<?=$events['re_name'];?>" placeholder="The Event's Name" name="event_title" aria-describedby="eventHint" aria-errormessage="eventNameError" required>
                                        <span class="form-error" id="eventNameError">
                                            This is important!
                                        </span>
                                    </label>
                                    <p class="help-text" id="eventHint">The name visible on users.</p>
                                </div>
                                <div class="cell small-12">
                                    <label>Event Slug
                                        <input type="text" value="<?=$events['re_slug'];?>" name="event_slug" placeholder="the-events-slug" aria-describedby="slugHint" aria-errormessage="eventSlugError" required>
                                        <span class="form-error" id="eventSlugError">
                                            This must not contain spaces!
                                        </span>
                                    </label>
                                    <p class="help-text" id="slugHint">URL friendly / The permanent link.</p>
                                </div>
                                <div class="cell small-12">
                                    <label>Description
                                        <textarea id="tinymce" name="event_description" placeholder="Event Description" rows="20" required><?=$events['re_description'];?></textarea>
                                    </label>
                                </div>
                                <div class="cell small-12">
                                    <div class="grid-container">
                                        <div class="grid-x grid-margin-x grid-margin-y">
                                            <div class="cell medium-4">
                                                <label>
                                                    <h3>Event Status:</h3>
                                                    <select name="event_status" aria-errormessage="eventStatus">
                                                        <option value="<?=$events['re_status'];?>" selected><?=$events['re_status'];?></option>
                                                        <option value="0" disabled>-----</option>
                                                        <option value="not_done">Not Done</option>
                                                        <option value="done">Done</option>
                                                    </select>
                                                    <span class="form-error" id="eventStatus">
                                                        This is important!
                                                    </span>
                                                </label>
                                            </div>
                                            <div class="cell medium-4">
                                                <label>
                                                    <h3>Start Date:</h3>
                                                    <input class="" value="<?=$events['re_start_date'];?>" name="event_start_date" type="date" aria-errormessage="eventStartDate" required>
                                                    <span class="form-error" id="eventStartDate">
                                                        This is important!
                                                    </span>
                                                </label>
                                            </div>
                                            <div class="cell medium-4">
                                                <label>
                                                    <h3>End Date:</h3>
                                                    <input class="" value="<?=$events['re_end_date'];?>" name="event_end_date" type="date" aria-errormessage="eventEndDate" required>
                                                    <span class="form-error" id="eventEndDate">
                                                        This is important!
                                                    </span>
                                                </label>
                                            </div>
                                            <div class="cell small-12">
                                                <label>
                                                    <h3>Event Tags:</h3>
                                                    <input class="" value="<?=$events['re_tags'];?>" name="event_tags" placeholder="Run, Bike, Trail" type="text" aria-describedby="eventTags">
                                                    <p class="help-text" id="eventTags">Separated by commas.</p>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?php else: ?>
                                <div class="cell medium-12">No Event for this ID.</div>
                            <?php endif; ?>
						</div>
					</div>

				</div>
				<div class="cell medium-3">
                    <?php if( !empty($events['re_name'] )): ?>
                        
                        <hr>
                        <section class="callout">
                            <label>
                                <?php  if( isset($_SESSION['isAdmin']) || isset($_SESSION['isAuthor'])  ): ?>
                                
								<h3>Publish Status:</h3>
                                <select name="event_publish_status"  aria-errormessage="event_publish_status">
                                    <option value="<?=$events['re_publish_status'];?>" selected> <?=get_publish_status($events['re_publish_status']);?> </option>
                                    <option value="na" disabled>---</option>
                                    <option value="0">Draft</option>
                                    <option value="1">Publish</option>
                                </select>

                                <?php elseif (  isset($_SESSION['isContributor']) ): ?>
                                
								<h3>Publish:</h3>
                                <input type="hidden" name="event_publish_status" value="Draft" readonly>

                                <?php endif;?>

                                <span class="form-error" id="event_publish_status">
                                    This is important!
                                </span>
                                <?php  if( isset($_SESSION['isAdmin']) || isset($_SESSION['isAuthor'])  ): ?>
                                    <button class="button expanded success" type="submit" name="save-event">Save</button>
                                <?php else: ?>
                                    <button class="button expanded success" type="submit" name="save-event">Save for approval</button>
                                <?php endif; ?>
								<input type="hidden" readonly value="<?=$events['re_id'];?>" name="re_id"  >
                            </label>
                        </section>
                        <section class="callout">
                            <label>Event Featured Image
                                <br>
                                <!-- <label for="featuredImage" class="button btn_image">Add Featured Image</label> -->
                                <input required type="input" name="event_featured_image" value="<?=$events['mf_id'];?>" readonly>						


                                <button type="button" class="button small addFeaturedImageModal" data-open="openMediaFiles">
								<?php if($events['mf_id'] == '') { echo 'Add Featured Image'; } else{ echo 'Change';} ?></button>
								<?php 
									$query = $this->db->query('SELECT * FROM media_files WHERE mf_id = "'.$events['mf_id'].'" ');
									$check_num_rows = $query->num_rows();
									if($check_num_rows >= 1):
								?>
                                <div class="featuredImageDiv">
                                	<img src="<?=base_url('uploads/ar_');?><?=$src_image->mf_file_name; ?>">
								</div>
								<?php else: ?>
								<div class="featuredImageDiv">
									<br><span class="label alert">Can't find the image. <br>Please check image location.</span>
								</div>
								<?php endif; ?>
                            </label>
                        </section>
                        <section class="callout">
                                <h3>Race Categories:</h3>

                                <div class="newCatItemGroup">
                                    <div class="existingCategories">
                                    <?php foreach ($category_items as $categories) : ?>
                                        
                                        <span class="label warning" id="rc_id-<?=$categories['rc_id'];?>"><?=$categories['rc_name'];?> ₱<?=$categories['rc_fee'];?><a class="delItem" data-column="rc_id" data-table="race_categories" id="<?=$categories['rc_id'];?>">x</a></span>
                                    <?php endforeach; ?>
                                    </div>
                                    <!-- <div id="newCat_item_1">
                                        <label>Cat. Item: 1 
                                            <input type='text' id='catName_1' placeholder="Ex: 3K, Triathlon" name="event_race_categories_[]" required >
                                        </label>
                                        <div class="input-group">
                                            <span class="input-group-label">₱</span>
                                            <input class="input-group-field" type='number' name="catItemPrice_[]" required id='price_catName_1' placeholder="Price for Cat. Item: 1">
                                        </div>
                                    </div> -->
                                </div>
                                <div class="grid-container">
                                    <div class="grid-x grid-margin-x">
                                        <button type="button" class="button small cell small-6 addNewCatItem">Add</button>
                                        <button type="button" class="button small cell small-6 delNewCatItem">Delete Last</button>
                                    </div>
                                </div>
                                

                        </section>
                        <section class="callout">
                            <h3>Singlet Sizes:</h3>
                            <div class="newSingletItemGroup">
                                <div class="existingSinglet">
                                    <?php foreach ($singlet_items as $singlets) : ?>
                                        <span class="label warning" id="ss_id-<?=$singlets['ss_id'];?>"><?=$singlets['ss_name'];?><a class="delItem" data-column="ss_id" data-table="singlet_sizes" id="<?=$singlets['ss_id'];?>">x</a></span>
                                    <?php endforeach; ?>
                                </div>
                                <!-- <div id="newSinglet_item_1">
                                    <label>Size: 1 
                                        <input type='text' id='singletName_1' placeholder="Size: 1 ex: Small, X-Large" name="event_singlets_[]" required >
                                    </label>
                                    
                                </div> -->
                            </div>
                            <div class="grid-container">
                                <div class="grid-x grid-margin-x">
                                    <button type="button" class="button small cell small-6 addNewSingletItem">Add</button>
                                    <button type="button" class="button small cell small-6 delNewSingletItem">Delete Last</button>
                                </div>
                            </div>
                        </section>
					
                    <?php endif; ?>
					
				</div>
			</div>
		</div>
	</form>

<?php $this->load->view('templates/media-items') ?>


</div>
</section>

<script type="text/javascript">
	$(document).ready(function () {

		//insert media to a post
		$('body').on('click', '.btn-insert-to-post', function(){
			var id = $(this).attr("id");
			var imageSrc = $(this).attr("data-src");
			var featuredImageDiv = $('.featuredImageDiv');
			var addFeaturedImageModal = $('.addFeaturedImageModal');
			var event_featured_image_input = $('input[name="event_featured_image"]');
			

			addFeaturedImageModal.hide();
			featuredImageDiv.html('<button type="button" class="floating-label button small addFeaturedImageModal" data-open="openMediaFiles" id="'+id+'">change</button><img src="'+imageSrc+'" >');
			// featuredImageDiv.html('<button type="button" class="floating-label button small removeFeaturedImage" id="'+id+'">x</button><img src="'+imageSrc+'" >');
			$('#openMediaFiles').foundation('close');
			event_featured_image_input.val(id);
		});

		//remove existing RaceCat or SingletSize
		$('body').on('click', '.delItem', function(){
			var dataTable = $(this).attr('data-table');
			var dataColumn = $(this).attr('data-column');
			var id = $(this).attr('id');
			var delItem = $('#'+dataColumn+'-'+id);
			// console.log(id);

			$.ajax({
				url: '<?php echo base_url('members/ajax/do/remove_item')?>',
				type: "POST",
				dataType: "json",
				data: 'id='+id+'&dataColumn='+dataColumn+'&dataTable='+dataTable,
				success: function (data) {
				 
					if (data.status == 'Success!') {
						$('.status-messages').html(
						'<div class="success callout cell large-12" data-closable > <h3>'+data.msg+'</h3><button class="close-button" aria-label="Dismiss alert" type="button" data-close><span aria-hidden="true">&times;</span></button></div>'
						);
						
						delItem.fadeOut();
					console.log(data);
					}
					
					
					if (data.status == 'Error!') {
						$('.status-messages').html(
						'<div class="alert callout cell large-12" data-closable > <h3>'+data.msg+'</h3><button class="close-button" aria-label="Dismiss alert" type="button" data-close><span aria-hidden="true">&times;</span></button></div>'
						);
					console.log(data);
					}
					$('html,body').animate({ scrollTop: 0}, 'slow');

				},
				error: function (data) {
					console.log('Error:'+ data);
					if (data.status == 'Error!'){
					$('.stat-messages-modal').html(
						'<div class="alert callout cell large-12" data-closable >ERROR:<hr> <h3>There\'s an error response from the database.</h3><button class="close-button" aria-label="Dismiss alert" type="button" data-close><span aria-hidden="true">&times;</span></button></div>'
						);
					}
				}	
			});	
		});

		//remove removeFeaturedImage
		$('body').on('click', '.removeFeaturedImage', function(){
			var id = $(this).attr("id");


			$.ajax({
				url: '<?php echo base_url('members/ajax/do/remove_featured_image')?>',
				type: "POST",
				dataType: "json",
				data: 'imgId='+id,
				success: function (data) {
					
					var featuredImageDiv = $('.featuredImageDiv');	
					var addFeaturedImageModal = $('.addFeaturedImageModal');
					var statusMessagesModal = $('.status-messages-modal');
					var event_featured_image_input = $('input[name="event_featured_image"]');
					
					if (data.status == 'Success!') {
						$('.status-messages-modal').html(
						'<div class="success callout cell large-12" data-closable > <h3>'+data.msg+'</h3><button class="close-button" aria-label="Dismiss alert" type="button" data-close><span aria-hidden="true">&times;</span></button></div>'
						);
						
						$('#media_'+id).remove();
						statusMessagesModal.html('');
						featuredImageDiv.html('');
						addFeaturedImageModal.show();
						event_featured_image_input.val('');
					
					console.log(data);
					}
					
					
					if (data.status == 'Error!') {
						$('.status-messages-modal').html(
						'<div class="alert callout cell large-12" data-closable > <h3>'+data.msg+'</h3><button class="close-button" aria-label="Dismiss alert" type="button" data-close><span aria-hidden="true">&times;</span></button></div>'
						);
					console.log(data);
					}
					$('html,body').animate({ scrollTop: 0}, 'slow');

				},
				error: function (data) {
					console.log('Error:'+ data);
					if (data.status == 'Error!'){
					$('.stat-messages-modal').html(
						'<div class="alert callout cell large-12" data-closable >ERROR:<hr> <h3>There\'s an error response from the database.</h3><button class="close-button" aria-label="Dismiss alert" type="button" data-close><span aria-hidden="true">&times;</span></button></div>'
						);
					}
				}	
			});	
		});

		//automate slug
		$('body').on('focusout', 'input[name="event_title"]', function(){
			   var titleVal = $(this).val().toLowerCase().replace(/\s+/g, '_');
			   var event_slug = $('input[name="event_slug"]');

			   event_slug.val( titleVal );
		});

 
		

		//edit event
		$("#edit_event").on("submit", function (ev) {
			ev.preventDefault();
			tinyMCE.triggerSave();
			$.ajax({
				url: '<?php echo base_url('members/ajax/do/edit_event')?>',
				type: "POST",
				dataType: "json",
				data: $("#edit_event").serialize(),
				success: function (data) {
							
					if (data.status == 'Success!') {
						$('.status-messages').html(
						'<div class="success callout cell large-12" data-closable > <h3>'+data.msg+'</h3><button class="close-button" aria-label="Dismiss alert" type="button" data-close><span aria-hidden="true">&times;</span></button></div>'
						);
					// console.log(data);
						setTimeout(() => {
							window.location.href="<?php echo base_url('events/edit/'.$events["re_id"]);?>";
						}, 1000);
					}
					else if (data.status == 'Invalid!') {
						$('.status-messages').html(
						'<div class="alert callout cell large-12" data-closable > <h3>'+data.msg+'</h3><hr><ul class="error-wrapper"></ul><button class="close-button" aria-label="Dismiss alert" type="button" data-close><span aria-hidden="true">&times;</span></button></div>'
						);
						
						if(data.event_title != ''){ $('.error-wrapper').append('<li>'+ data.event_title +'</li>'  ); }
						if(data.event_slug != ''){ $('.error-wrapper').append('<li>'+ data.event_slug +'</li>'  ); }
						// if(data.event_race_categories != ''){ $('.error-wrapper').append('<li>'+ data.event_race_categories +'</li>'  ); }
						// if(data.catItemPrice != ''){ $('.error-wrapper').append('<li>'+ data.catItemPrice +'</li>'  ); }
						// if(data.event_singlets != ''){ $('.error-wrapper').append('<li>'+ data.event_singlets +'</li>'  ); }
						if(data.event_status != ''){ $('.error-wrapper').append('<li>'+ data.event_status +'</li>'  ); }
						if(data.event_start_date != ''){ $('.error-wrapper').append('<li>'+ data.event_start_date +'</li>'  ); }
						if(data.event_end_date != ''){ $('.error-wrapper').append('<li>'+ data.event_end_date +'</li>'  ); }
						if(data.event_tags != ''){ $('.error-wrapper').append('<li>'+ data.event_tags +'</li>'  ); }
						if(data.event_publish_status != ''){ $('.error-wrapper').append('<li>'+ data.event_publish_status +'</li>'  ); }
						if(data.event_featured_image != ''){ $('.error-wrapper').append('<li>'+ data.event_featured_image +'</li>'  ); }

					console.log(data);
					}
					
					else{
						$('.status-messages').html(
						'<div class="alert callout cell large-12" data-closable > <h3>'+data.msg+'</h3><button class="close-button" aria-label="Dismiss alert" type="button" data-close><span aria-hidden="true">&times;</span></button></div>'
						);
					console.log(data);
					}
					
					$('html,body').animate({ scrollTop: 0}, 'slow');

				},
				error: function (data) {
					console.log('Error:'+ data);
					if (data.status == 'Error!'){
					$('.stat-messages').html(
						'<div class="alert callout cell large-12" data-closable >ERROR:<hr> <h3>There\'s an error response from the database.</h3><button class="close-button" aria-label="Dismiss alert" type="button" data-close><span aria-hidden="true">&times;</span></button></div>'
						);
					}
				}	
			});

		});
	
		//add new category item
		var counter = 2;
		var newCatItemGroup   = $(".newCatItemGroup"); 
		var add_button      = $(".addNewCatItem");
		var remove_button   = $(".delNewCatItem");
		
		add_button.click(function () {

			if (counter > 10) {
				alert("Only 10 categories allowed");
				return false;
			}

			var newCat_item_ = $(document.createElement('div'))
				.attr("id", 'newCat_item_' + counter);

			newCat_item_.after().html('<label>Cat. Item:' + counter +  
				'<input type="text" placeholder=" Ex: '+ counter +'K, Triathlon " id="catName_' + counter + '" value="" name="event_race_categories_[]" required > </label><div class="input-group"><span class="input-group-label">₱</span> <input required name="catItemPrice_[]" class="input-group-field" type="number" placeholder="Price for Cat. Item: '+ counter +'" id="price_catName_'+ counter +'" ></div>');

			newCat_item_.appendTo(newCatItemGroup);


			counter++;
		});

		remove_button.click(function () {
			if (counter == 1) {
				alert("No more categories to remove");
				return false;
			}
			counter--;
			$("#newCat_item_"	 + counter).remove();
		});

		//add new singlet item
		var counter_singlet = 2;
		var newSingletItemGroup   = $(".newSingletItemGroup"); 
		var add_button_singlet      = $(".addNewSingletItem");
		var remove_button_singlet   = $(".delNewSingletItem");
		
		add_button_singlet.click(function () {

			if (counter_singlet > 20) {
				alert("Only 20 singles allowed");
				return false;
			}

			var newSinglet_item_ = $(document.createElement('div'))
				.attr("id", 'newSinglet_item_' + counter_singlet);

			newSinglet_item_.after().html('<label>Size:' + counter_singlet +  
				'<input required type="text"  name="event_singlets_[]" placeholder=" Size: '+ counter_singlet +' ex: Small, X-Large" name="event_singlets_[]" id="event_singlets_' + counter_singlet + '" value="" > </label>');

			newSinglet_item_.appendTo(newSingletItemGroup);


			counter_singlet++;
		});

		remove_button_singlet.click(function () {
			if (counter_singlet == 1) {
				alert("No more items to remove");
				return false;
			}
			counter_singlet--;
			$("#newSinglet_item_"	 + counter_singlet).remove();
		});
	});

</script>