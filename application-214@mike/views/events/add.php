 


<section id="main-content">
  <div id="guts">

	<form data-abide novalidate id="add_new_event">
		<div class="grid-container">
			<div class="grid-x grid-margin-x grid-margin-y">
				<div class="cell medium-9">

					<div class="text-center">
						<h4 style="margin: 0;" class="text-center">
							<?=$title;?>
						</h4>
					</div>

					<hr>

					<div class="grid-container">
						<div class="grid-y grid-margin-y">
							<div class="cell medium-12">
								<div class="status-messages"> </div>
							</div>
							<div class="cell small-12">
								<label>Event Name
									<input type="text" placeholder="The Event's Name" name="event_title" aria-describedby="eventHint" aria-errormessage="eventNameError" required>
									<span class="form-error" id="eventNameError">
										This is important!
									</span>
								</label>
								<p class="help-text" id="eventHint">The name visible on users.</p>
							</div>
							<div class="cell small-12">
								<label>Event Slug
									<input type="text" name="event_slug" placeholder="the-events-slug" aria-describedby="slugHint" aria-errormessage="eventSlugError" required>
									<span class="form-error" id="eventSlugError">
										This must not contain spaces!
									</span>
								</label>
								<p class="help-text" id="slugHint">URL friendly / The permanent link.</p>
							</div>
							<div class="cell small-12">
								<label>Description
									<textarea id="tinymce" name="event_description" placeholder="Event Description" rows="20" required></textarea>
								</label>
							</div>
							<div class="cell small-12">
								<div class="grid-container">
									<div class="grid-x grid-margin-x grid-margin-y">
										<div class="cell medium-4">
											<label>
												<h3>Event Status:</h3>
												<select name="event_status" aria-errormessage="eventStatus">
													<option value="not_done">Not Done</option>
													<option value="done">Done</option>
												</select>
												<span class="form-error" id="eventStatus">
													This is important!
												</span>
											</label>
										</div>
										<div class="cell medium-4">
											<label>
												<h3>Start Date:</h3>
												<input class="" name="event_start_date" type="date" aria-errormessage="eventStartDate" required>
												<span class="form-error" id="eventStartDate">
													This is important!
												</span>
											</label>
										</div>
										<div class="cell medium-4">
											<label>
												<h3>End Date:</h3>
												<input class="" name="event_end_date" type="date" aria-errormessage="eventEndDate" required>
												<span class="form-error" id="eventEndDate">
													This is important!
												</span>
											</label>
										</div>
										<div class="cell small-12">
											<label>
												<h3>Event Tags:</h3>
												<input class="" name="event_tags" placeholder="Run, Bike, Trail" type="text" aria-describedby="eventTags">
												<p class="help-text" id="eventTags">Separated by commas.</p>
											</label>
										</div>
									</div>
								</div>
							</div>
							
						</div>
					</div>


					
				</div>
				<div class="cell medium-3">
					
					<section class="callout">
						<label>
							<?php  if( isset($_SESSION['isAdmin']) || isset($_SESSION['isAuthor'])  ): ?>
							<h3>Publish Status:</h3>
							<select name="event_publish_status"  aria-errormessage="event_publish_status">
								<option value="0">Draft</option>
								<option value="1">Publish</option>
							</select>
							<?php elseif (  isset($_SESSION['isContributor']) ): ?>
							<h3>Publish:</h3>
							<input type="hidden" name="event_publish_status" value="Draft" readonly>
							<?php endif;?>
							<span class="form-error" id="event_publish_status">
								This is important!
							</span>
							<?php  if( isset($_SESSION['isAdmin']) || isset($_SESSION['isAuthor'])  ): ?>
								<button class="button expanded success" type="submit" name="save-event">Save</button>
							<?php else: ?>
								<button class="button expanded success" type="submit" name="save-event">Save for approval</button>
							<?php endif; ?>
						</label>
					</section>
					<section class="callout">
					<label>Event Featured Image
						<br>
						<!-- <label for="featuredImage" class="button btn_image">Add Featured Image</label> -->
						 <input required type="input" name="event_featured_image" readonly>						 
						<button type="button" class="button small addFeaturedImageModal" data-open="openMediaFiles">Add Featured Image</button>
						<div class="featuredImageDiv"></div>
					</label>

					


					</section>
					<section class="callout">
							<h3>Race Categories:</h3>

							<div class="newCatItemGroup">
								<div id="newCat_item_1">
									<label>Cat. Item: 1 
										<input type='text' id='catName_1' placeholder="Ex: 3K, Triathlon" name="event_race_categories_[]" required >
									</label>
									<div class="input-group">
									 	<span class="input-group-label">₱</span>
										<input class="input-group-field" type='number' name="catItemPrice_[]" required id='price_catName_1' placeholder="Price for Cat. Item: 1">
									</div>
								</div>
							</div>
							<div class="grid-container">
								<div class="grid-x grid-margin-x">
									<button type="button" class="button small cell small-6 addNewCatItem">Add</button>
									<button type="button" class="button small cell small-6 delNewCatItem">Delete Last</button>
								</div>
							</div>
							 

					</section>
					<section class="callout">
							<h3>Singlet Sizes:</h3>
							<div class="newSingletItemGroup">
									<div id="newSinglet_item_1">
										<label>Size: 1 
											<input type='text' id='singletName_1' placeholder="Size: 1 ex: Small, X-Large" name="event_singlets_[]" required >
										</label>
										
									</div>
								</div>
								<div class="grid-container">
									<div class="grid-x grid-margin-x">
										<button type="button" class="button small cell small-6 addNewSingletItem">Add</button>
										<button type="button" class="button small cell small-6 delNewSingletItem">Delete Last</button>
									</div>
								</div>
					</section>
					
					
				</div>
			</div>
		</div>
	</form>

<!-- media files modal -->
<div class="reveal large" id="openMediaFiles" data-reveal data-close-on-click="false" data-close-on-esc="false">
	<button class="close-button" data-close aria-label="Close modal" type="button">
		<span aria-hidden="true">&times;</span>
	</button>
	
	<h1>Upload Featured Image</h1>
	<hr>
	<div class="grid-container">
		<div class="grid-x grid-margin-x grid-margin-y">
			<div class="cell medium-12">
				<div class="status-messages-modal"> </div>
			</div>
			<div class="cell medium-12">
				<form data-abide novalidate id="add_featured_image">
					<input required type="file" name="event_featured_image" id="userfile" >
					<button type="submit" class="button btn_image" name="">Upload</button>
				</form>
			</div>
			<div class="cell medium-12">
				<hr>
				<h2>All Uploaded Media</h2>
				<hr>
				<table id="mediaList" class="hover" style="width: 100%;">
					<thead>
						<tr>
							<th width="">ID</th>
							<th width="">File Name</th>
							<th width="">Preview</th>
							<th width="">Action</th>
						</tr>
					</thead>
					<tbody>

					<?php foreach ($media_items as $media_item) : ?>
						<tr id="<?=$media_item['mf_id'];?>">
							<td><?=$media_item['mf_id'];?></td>
							<td><?=$media_item['mf_file_name'];?></td>
							<td><img class="thumb" src="<?=base_url('uploads/ar_');?><?=$media_item['mf_file_name'];?> "></td>
							<td>
							<button type="button" data-src="<?=base_url().'uploads/ar_'.$media_item["mf_file_name"];?>" id="<?=$media_item['mf_id'];?>" class="button success small btn-insert-to-post">+</button> 
							<button type="button" class="floating-label button small alert removeFeaturedImage" id="<?=$media_item['mf_id'];?>">x</button> </td>
						</tr>

					<?php endforeach; ?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div> 
<!-- //media files modal -->

</div>
</section>

<script type="text/javascript">
	$(document).ready(function () {

		//insert media to a post
		$('body').on('click', '.btn-insert-to-post', function(){
			var id = $(this).attr("id");
			var imageSrc = $(this).attr("data-src");
			var featuredImageDiv = $('.featuredImageDiv');
			var addFeaturedImageModal = $('.addFeaturedImageModal');
			var event_featured_image_input = $('input[name="event_featured_image"]');
			

				addFeaturedImageModal.hide();
				featuredImageDiv.html('<button type="button" class="floating-label button small addFeaturedImageModal" data-open="openMediaFiles" id="'+id+'">change</button><img src="'+imageSrc+'" >');
				// featuredImageDiv.html('<button type="button" class="floating-label button small removeFeaturedImage" id="'+id+'">x</button><img src="'+imageSrc+'" >');
				$('#openMediaFiles').foundation('close');
				event_featured_image_input.val(id);
		});

		//remove removeFeaturedImage
		$('body').on('click', '.removeFeaturedImage', function(){
			var id = $(this).attr("id");


			$.ajax({
				url: '<?php echo base_url('members/ajax/do/remove_featured_image')?>',
				type: "POST",
				dataType: "json",
				data: 'imgId='+id,
				success: function (data) {
					
					var featuredImageDiv = $('.featuredImageDiv');	
					var addFeaturedImageModal = $('.addFeaturedImageModal');
					var statusMessagesModal = $('.status-messages-modal');
					var event_featured_image_input = $('input[name="event_featured_image"]');
					
					if (data.status == 'Success!') {
						$('.status-messages').html(
						'<div class="success callout cell large-12" data-closable > <h3>'+data.msg+'</h3><button class="close-button" aria-label="Dismiss alert" type="button" data-close><span aria-hidden="true">&times;</span></button></div>'
						);
						
						statusMessagesModal.html('');
						featuredImageDiv.html('');
						addFeaturedImageModal.show();
						event_featured_image_input.val('');
					
					console.log(data);
					}
					
					
					if (data.status == 'Error!') {
						$('.status-messages').html(
						'<div class="alert callout cell large-12" data-closable > <h3>'+data.msg+'</h3><button class="close-button" aria-label="Dismiss alert" type="button" data-close><span aria-hidden="true">&times;</span></button></div>'
						);
					console.log(data);
					}
					$('html,body').animate({ scrollTop: 0}, 'slow');

				},
				error: function (data) {
					console.log('Error:'+ data);
					if (data.status == 'Error!'){
					$('.stat-messages-modal').html(
						'<div class="alert callout cell large-12" data-closable >ERROR:<hr> <h3>There\'s an error response from the database.</h3><button class="close-button" aria-label="Dismiss alert" type="button" data-close><span aria-hidden="true">&times;</span></button></div>'
						);
					}
				}	
			});	
		});

		//automate slug
		$('body').on('focusout', 'input[name="event_title"]', function(){
			   var titleVal = $(this).val().toLowerCase().replace(/\s+/g, '_');
			   var event_slug = $('input[name="event_slug"]');

			   event_slug.val( titleVal );
		});


		//add featured image
		 $("#add_featured_image").on("submit", function (ev) {
			ev.preventDefault();  

			// var id = $(this).attr('id');
			
			// console.log(id);

			//return false;
			// if(efi_val != ''){
				$.ajax({
					url: '<?php echo base_url('members/ajax/do/add_featured_image')?>',
					contentType: false,  
					cache: false,  
					processData:false,
					type: "POST",
					dataType: "json",
					fileElementId	:'userfile',
					data: new FormData(this),
					success: function (data) {
						
								
						if (data.status == 'Success!') {
							$('.status-messages-modal').html(
							'<div class="success callout cell large-12" data-closable > <h3>'+data.msg+'</h3><hr><ul class="error-wrapper-modal"></ul></div>'
							);
							if(data.img != ''){ $('.error-wrapper-modal').append('<li>'+ data.img +'</li>'  ); }

						console.log(data);
						}
						
						
						if (data.status == 'Error!') {
							$('.status-messages-modal').html(
							'<div class="alert callout cell large-12" data-closable > <h3>'+data.msg+'</h3><button class="close-button" aria-label="Dismiss alert" type="button" data-close><span aria-hidden="true">&times;</span></button></div>'
							);
						console.log(data);
						}
						if (data.status == 'Invalid!') {
							$('.status-messages-modal').html(
								'<div class="alert callout cell large-12" data-closable > <h3>'+data.msg+'</h3><hr><ul class="error-wrapper-modal"></ul><button class="close-button" aria-label="Dismiss alert" type="button" data-close><span aria-hidden="true">&times;</span></button></div>'
								);
								
								if(data.event_featured_image != ''){ $('.error-wrapper-modal').append('<li>'+ data.event_featured_image +'</li>'  ); }
						}
						
						$('html,body').animate({ scrollTop: 0}, 'slow');

					},
					error: function (data) {
						console.log('Error:'+ data);
						if (data.status == 'Error!'){
						$('.stat-messages-modal').html(
							'<div class="alert callout cell large-12" data-closable >ERROR:<hr> <h3>There\'s an error response from the database.</h3><button class="close-button" aria-label="Dismiss alert" type="button" data-close><span aria-hidden="true">&times;</span></button></div>'
							);
						}
					}	
				});
			// }
		});

		//add new event
		$("#add_new_event").on("submit", function (ev) {
			ev.preventDefault();
			
			$.ajax({
				url: '<?php echo base_url('members/ajax/do/add_new_event')?>',
				type: "POST",
				dataType: "json",
				data: $("#add_new_event").serialize(),
				success: function (data) {
							
					if (data.status == 'Success!') {
						$('.status-messages').html(
						'<div class="success callout cell large-12" data-closable > <h3>'+data.msg+'</h3><button class="close-button" aria-label="Dismiss alert" type="button" data-close><span aria-hidden="true">&times;</span></button></div>'
						);
					console.log(data);
					}
					else if (data.status == 'Invalid!') {
						$('.status-messages').html(
						'<div class="alert callout cell large-12" data-closable > <h3>'+data.msg+'</h3><hr><ul class="error-wrapper"></ul><button class="close-button" aria-label="Dismiss alert" type="button" data-close><span aria-hidden="true">&times;</span></button></div>'
						);
						
						if(data.event_title != ''){ $('.error-wrapper').append('<li>'+ data.event_title +'</li>'  ); }
						if(data.event_slug != ''){ $('.error-wrapper').append('<li>'+ data.event_slug +'</li>'  ); }
						if(data.event_race_categories != ''){ $('.error-wrapper').append('<li>'+ data.event_race_categories +'</li>'  ); }
						if(data.catItemPrice != ''){ $('.error-wrapper').append('<li>'+ data.catItemPrice +'</li>'  ); }
						if(data.event_singlets != ''){ $('.error-wrapper').append('<li>'+ data.event_singlets +'</li>'  ); }
						if(data.event_status != ''){ $('.error-wrapper').append('<li>'+ data.event_status +'</li>'  ); }
						if(data.event_start_date != ''){ $('.error-wrapper').append('<li>'+ data.event_start_date +'</li>'  ); }
						if(data.event_end_date != ''){ $('.error-wrapper').append('<li>'+ data.event_end_date +'</li>'  ); }
						if(data.event_tags != ''){ $('.error-wrapper').append('<li>'+ data.event_tags +'</li>'  ); }
						if(data.event_publish_status != ''){ $('.error-wrapper').append('<li>'+ data.event_publish_status +'</li>'  ); }
						if(data.event_featured_image != ''){ $('.error-wrapper').append('<li>'+ data.event_featured_image +'</li>'  ); }

					console.log(data);
					}
					
					else{
						$('.status-messages').html(
						'<div class="alert callout cell large-12" data-closable > <h3>This is not a valid email.</h3><button class="close-button" aria-label="Dismiss alert" type="button" data-close><span aria-hidden="true">&times;</span></button></div>'
						);
					console.log(data);
					}
					
					$('html,body').animate({ scrollTop: 0}, 'slow');

				},
				error: function (data) {
					console.log('Error:'+ data);
					if (data.status == 'Error!'){
					$('.stat-messages').html(
						'<div class="alert callout cell large-12" data-closable >ERROR:<hr> <h3>There\'s an error response from the database.</h3><button class="close-button" aria-label="Dismiss alert" type="button" data-close><span aria-hidden="true">&times;</span></button></div>'
						);
					}
				}	
			});

		});
	
		//add new category item
		var counter = 2;
		var newCatItemGroup   = $(".newCatItemGroup"); 
		var add_button      = $(".addNewCatItem");
		var remove_button   = $(".delNewCatItem");
		
		add_button.click(function () {

			if (counter > 10) {
				alert("Only 10 categories allowed");
				return false;
			}

			var newCat_item_ = $(document.createElement('div'))
				.attr("id", 'newCat_item_' + counter);

			newCat_item_.after().html('<label>Cat. Item:' + counter +  
				'<input type="text" placeholder=" Ex: '+ counter +'K, Triathlon " id="catName_' + counter + '" value="" name="event_race_categories_[]" required > </label><div class="input-group"><span class="input-group-label">₱</span> <input required name="catItemPrice_[]" class="input-group-field" type="number" placeholder="Price for Cat. Item: '+ counter +'" id="price_catName_'+ counter +'" ></div>');

			newCat_item_.appendTo(newCatItemGroup);


			counter++;
		});

		remove_button.click(function () {
			if (counter == 1) {
				alert("No more items to remove");
				return false;
			}
			counter--;
			$("#newCat_item_"	 + counter).remove();
		});

		//add new singlet item
		var counter_singlet = 2;
		var newSingletItemGroup   = $(".newSingletItemGroup"); 
		var add_button_singlet      = $(".addNewSingletItem");
		var remove_button_singlet   = $(".delNewSingletItem");
		
		add_button_singlet.click(function () {

			if (counter_singlet > 20) {
				alert("Only 20 singlets allowed");
				return false;
			}

			var newSinglet_item_ = $(document.createElement('div'))
				.attr("id", 'newSinglet_item_' + counter_singlet);

			newSinglet_item_.after().html('<label>Size:' + counter_singlet +  
				'<input required type="text"  name="event_singlets_[]" placeholder=" Size: '+ counter_singlet +' ex: Small, X-Large" name="event_singlets_[]" id="event_singlets_' + counter_singlet + '" value="" > </label>');

			newSinglet_item_.appendTo(newSingletItemGroup);


			counter_singlet++;
		});

		remove_button_singlet.click(function () {
			if (counter_singlet == 1) {
				alert("No more items to remove");
				return false;
			}
			counter_singlet--;
			$("#newSinglet_item_"	 + counter_singlet).remove();
		});
	});

</script>