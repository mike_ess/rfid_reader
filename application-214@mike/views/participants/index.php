
<section id="main-content">
  <div id="guts">

<div class="grid-container">
	<div class="grid-x grid-margin-y">
		<div class="cell medium-12">
			<div class="text-center">
				<h1 style="margin: 0;" class="text-center"><?php echo $title; ?></h1>
			</div>

			<hr>
			<article class="grid-container">
				<div class="grid-x grid-margin-x small-up-3 medium-up-4 large-up-5">

					<?php 
                        /** Start Show all events */ 
                        foreach ($events as $events_item): 

                        $reSlug =  $events_item['re_slug'];
                        $mfId = $events_item['mf_id'];
                        $reName = $events_item['re_name'];
                        $reId = $events_item['re_id'];

                        $query_rcId = $this->db->query('SELECT * FROM race_categories WHERE re_id = "'.$reId.'" ');
                        $row_rcId = $query_rcId->row();
                        $rc_rcId = $row_rcId->rc_id;
                    ?>

					<div class="cell">
						<?php 
                            $eventStatus = $events_item['re_publish_status'];
                            
                            if($eventStatus == '0'){
                                echo '<small class="floating-label">Draft</small>';
                            }
                        ?>
						<a href="<?php echo base_url('participants/events/'.$reSlug.'/'.$rc_rcId); ?>">
							<?php
            
                                if($mfId != '0'): 
                                $query = $this->db->query('SELECT * FROM media_files WHERE mf_id = "'.$mfId.'" ');
                                $row = $query->row();
                            ?>
								<img src="<?=base_url('uploads/').$row->mf_file_name;?>" alt="<?=$reSlug;?>">
								<?php else: ?>
								<img src="https://placehold.it/500x500&amp;text=No Available Image!" alt="image for article">
								<?php endif; ?>
						</a>
						<br>
						<?php echo $reName; ?>
					</div>

					<?php 
                        /** End Show all members */
                        endforeach; 
                    ?>

				</div>
			</article>
			<hr>

		</div>

	</div>
</div>

</div>
</section>
