
<?php
// echo 'eventName = '.$current_events_item['re_name'].'<br>';
echo 'rE_id = '.$current_events_item['re_id'] . '<br>';
echo 'rC_id = '.$rc_id . '<br>';

$theRaceCatId = $rc_id; //$the_rc_name['rc_id'];
$theRaceEventId = $current_events_item['re_id'];

$get_query_rcId = $this->db->query('SELECT * FROM race_categories WHERE rc_id = "'.$theRaceCatId.'" ');
$get_row_rcId = $get_query_rcId->row();

$the_rcName = $get_row_rcId->rc_name;

// echo $the_rcName;



?>

<section id="main-content">
  <div id="guts">

<div class="grid-container">
    <div class="grid-x grid-margin-y grid-margin-x">
        

     
        <div class="cell medium-12">
            <div class="grid-container">
                <div class="grid-x grid-margin-y grid-margin-x">
                
                    <div class="cell medium-7">
                        <form>
                            <label>Choose event name to view result:
                                <div class="input-group">
                                    <!-- <span class="input-group-label">$</span> -->
                                    <!-- <input class="input-group-field" type="number"> -->
                                    <select name="choose-race-event-select" class="input-group-field">
                                        
                                        <?php 
                                            /** Start Show all events */ 
                                            foreach ($events as $events_item): 
                                                $reId = $events_item['re_id'];
                                                $query_rcId = $this->db->query('SELECT * FROM race_categories WHERE re_id = "'.$reId.'" ');
                                                $row_rcId = $query_rcId->row();
                                                $rc_rcId = $row_rcId->rc_id;
                                        ?>
                                        <option value="<?=base_url() . 'participants/events/' . $events_item['re_slug'].'/'.$rc_rcId;?>" <?php if ($events_item['re_name'] == $current_events_item['re_name']) echo 'selected';?> ><?=$events_item['re_name'];?></option>
                                        <?php endforeach; ?>

                                    </select>
                                    <div class="input-group-button">
                                        <button type="button" class="button small choose-race-event-button"  disabled>Go</button>
                                    </div>
                                </div>
                            </label>
                        </form>
                    </div>
                    <div class="medium-12 cell">
                         
                         <ul class="">
                            <?php 
                            /** Start Show all race categories */
                            foreach ($categories as $race_categories) :
                            ?>
                            <?php //if($race_categories['rc_name'] == $current_category_name['rc_name']) { echo 'is-active'; } ?>
                                <li class="">
                                <a id="<?=$race_categories['rc_id'];?>" href="<?=base_url('participants/events/'). $current_events_item['re_slug'] .'/'.$race_categories['rc_id']; ?>" aria-selected="true"><?=$race_categories['rc_name']; ?></a>
                                </li>
                                
                            <?php
                        

                        endforeach; ?>
                     </ul>
                     </div>
                    <div class="medium-12 cell">
                       
                           
                        <h2 class="text-center">Race Participants for: <?=$current_events_item['re_name']; ?> - <?=$the_rcName; ?></h2>
                                
                            
                        <hr>
                            <div class="small button-group">
                                <a class=" button hollow clear" >Filter View:</a> 
                                <a class="success button toggle-vis" data-column="0">Bib #</a>
                                <a class="success button toggle-vis" data-column="1">Last Name</a>
                                <a class="success button toggle-vis" data-column="2">First Name</a>
                                <a class="alert button toggle-vis" data-column="3">Middle Name</a>
                                <a class="alert button toggle-vis" data-column="4">Club/Grp.</a>
                                <a class="alert button toggle-vis" data-column="5">FFM ID</a>
                                <a class="alert button toggle-vis" data-column="6">Race Event</a>
                                <a class="alert button toggle-vis" data-column="7">Race Cat.</a>
                                <a class="success button toggle-vis" data-column="8">Singlet</a>
                                <a class="success button toggle-vis" data-column="9">P. Status</a>
                                <a class="success button toggle-vis" data-column="10">P. Method</a>
                                <a class="success button toggle-vis" data-column="11">P. Amount</a>
                                <a class="success button toggle-vis" data-column="12">P. Total</a>
                                <a class="alert button toggle-vis" data-column="13">Del. Add.</a>
                                <a class="alert button toggle-vis" data-column="14">Part. Date</a>
                                <a class="success button toggle-vis" data-column="15">Action</a>
                            </div>
                        <hr>
                        <div class="" >
                        
                            <div class="" id="<?=$the_rcName; ?>">
                                <table id="resultList" class="hover" style="width: 100%;">
                                    <thead>
                                        <tr>
                                            <th width="">BIB No.</th>
                                            <th width="">Last Name</th>
                                            <th width="">First Name</th>
                                            <th width="">Middle Name</th>

                                            <th width="">Club/Grp.</th>
                                            <th width="">FFM ID</th>
                                            <th width="">Race Event</th>
                                            <th width="">Race Cat.</th>
                                            <th width="">Singlet</th>
                                            <th width="">P. Status</th>
                                            <th width="">P. Method</th>
                                            <th width="">P. Amount</th>
                                            <th width="">P. Total</th>
                                            <th width="">Delivery Add.</th>
                                            <th width="">Participated Date</th>

                                            <th width="">Action</th>
                                           
                                        </tr>
                                    </thead>
                                    
                                    <tbody>
                                        <?php
                                        
                                            //loop start
                                      
                                            foreach ($participants_only as $participants) {

                                                
                                                echo " <tr id='".$participants['m_id']."'>";
                                                echo "<td>".fix_bib_number($participants['bib_assigned_number']) . "</td>";
                                                // echo "<td>";

                                                //check if has ffm details
                                                $query = $this->db->query('SELECT * FROM family_friend_member WHERE bib_id = '.$participants['bib_id'].' ' );
                                                $ifHasFfm = $query->num_rows();  //check if has FFM 
                                                $row = $query->row();
                                                
                                                //race event
                                                $query_re = $this->db->query('SELECT * FROM race_events WHERE re_id = '.$participants['re_id'].' ' );
                                                $row_re = $query_re->row();
                                                $show_re_name = $row_re->re_name; 
                                                
                                                //race categories
                                                $query_rc = $this->db->query('SELECT * FROM race_categories WHERE rc_id = '.$rc_id.' ' );
                                                $row_rc = $query_rc->row();
                                                $show_rc_name = $row_rc->rc_name; 
                                               
                                                //singlet size
                                                $query_ss = $this->db->query('SELECT * FROM singlet_sizes WHERE ss_id = '.$participants['ss_id'].' ' );
                                                $row_ss = $query_ss->row();
                                                $show_ss_name = $row_ss->ss_name; 

                                                //date
                                                $partDate = strtotime($participants['p_added_date']);

                                                if($ifHasFfm >= 1){
                                                    echo "<td>".$row->ffm_last_name."</td>";
                                                    echo "<td>".$row->ffm_first_name."</td>";
                                                    echo "<td>".$row->ffm_middle_name."</td>";
                                                }else{
                                                    echo "<td>".$participants['m_last_name']."</td>";
                                                    echo "<td>".$participants['m_first_name']."</td>";
                                                    echo "<td>".$participants['m_middle_name']."</td>";
                                                    
                                                    
                                                    
                                                }
                                                echo "<td>".$participants['m_clubs_groups']."</td>";
                                                echo "<td>".$participants['ffm_id']."</td>";

                                                echo "<td>".$show_re_name."</td>";
                                                echo "<td>".$show_rc_name."</td>";
                                                echo "<td>".$show_ss_name."</td>";
                                                echo "<td>".get_payment_status($participants['p_payment_status'])."</td>";

                                                echo "<td>".$participants['p_payment_method']."</td>";
                                                echo "<td>₱ ".$participants['p_payment_amount']."</td>";
                                                echo "<td>₱ ".$participants['p_payment_total']."</td>";
                                                echo "<td>".$participants['p_delivery_address']."</td>";
                                                echo "<td>".date('M d, Y | H:i:s', $partDate)."</td>";

                                                echo "</td>";
                                                echo "<td></td>";
                                                // echo "<td><button id='dp_".$participants['m_id']."' type='button' class='deleteParticipant button small'>Delete</button></td>";
                                                
                                                echo "</tr>";
                                            }
                                            

                                        ?>
         
                                    </tbody>
                                </table>
                            </div>
                       
                       
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php //$this->load->view('templates/sidebar') ?>
        
    </div>
</div>

</div>
            </section>
  
  <script type="text/javascript">
$(document).ready(function(){
    var table =  $('#resultList').DataTable( {
        "columns": [
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            {
              "data": null,
              "render": function ( data, type, row ) {
                // return "<button id='" +row.DT_RowId + "' type='button' class='button small deleteParticipant' onClick1='deleteThis(" +row.id + ")' >Delete1</button>"
                return "<a id='" +row.DT_RowId + "'  class='button small editParticipant' onClick='editThis(" +row.id + ")' >Edit</a>"
               
                }
            }
        ],
        "columnDefs": [
            {
                "targets": [ 0 ], //bib #
                "visible": true,
            },
            {
                "targets": [ 1 ], //last name
                "visible": true,
            },
            {
                "targets": [ 2 ], //first name
                "visible": true,
            },
            {
                "targets": [ 3 ], //middle name
                "visible": false,
            },
            {
                "targets": [ 4 ], //clubs/groups
                "visible": false,
            },
            {
                "targets": [ 5 ], //ffm id
                "visible": false,
            },
            {
                "targets": [ 6 ], //race event
                "visible": false,
            },
            {
                "targets": [ 7 ], //race cat.
                "visible": false,
            },
            {
                "targets": [ 8 ], //singlet
                "visible": true,
            },
            {
                "targets": [ 9 ], // p. status
                "visible": true,
            },
            {
                "targets": [ 10 ], // p. method
                "visible": true,
            },
            {
                "targets": [ 11 ], // p. total
                "visible": true,
            },
            {
                "targets": [ 12 ], // p. amount
                "visible": true,
            },
            {
                "targets": [ 13 ], // delivery address
                "visible": false,
            },
            {
                "targets": [ 14 ], // participated date
                "visible": false,
            },
            {
                "targets": [ 15 ], // actions
                "visible": true,
            }
        ]
    } );
   

    //edit member / participants
    $('.editParticipant').on('click', function(){
        var id = $(this).attr('id');
        window.location.href = "<?php echo base_url('members/edit/')?>"+id;

    });

     // delete participants via ajax call
    $('.deleteParticipant').on('click', function(){
        var p_id = $(this).attr('id');
        console.log(p_id);

        //do ajax delete here
        $.ajax({
                url:  '<?php echo base_url('participants/ajax/')?>'+p_id,
                type: "GET",
                dataType: "json",
                success:function(data) {
                    console.log(data);
                    
                    //then hide the row
                    $('tr#'+p_id).fadeOut(); 
                },
                error:function(data){
                    console.log(data);
                }

            });


    });

    //toggle views  
    $('a.toggle-vis').on( 'click', function (e) {
        e.preventDefault();
 
        // Get the column API object
        var column = table.column( $(this).attr('data-column') );
 
        // Toggle the visibility
        column.visible( ! column.visible() );

        // $(this).toggleClass("success alert");
    } );
    
});
</script>