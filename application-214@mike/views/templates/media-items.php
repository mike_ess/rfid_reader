<!-- media files modal -->
<div class="reveal large" id="openMediaFiles" data-reveal data-close-on-click="false" data-close-on-esc="false">
	<button class="close-button" data-close aria-label="Close modal" type="button">
		<span aria-hidden="true">&times;</span>
	</button>
	
	<h1>Upload Featured Image</h1>
	<hr>
	<div class="grid-container">
		<div class="grid-x grid-margin-x grid-margin-y">
			<div class="cell medium-12">
				<div class="status-messages-modal"> </div>
			</div>
			<div class="cell medium-12">
				<form data-abide novalidate id="add_featured_image">
					<input required type="file" name="event_featured_image" id="userfile" >
					<button type="submit" class="button btn_image" name="">Upload</button>
				</form>
			</div>
			<div class="cell medium-12">
				<hr>
				<h2>All Uploaded Media</h2>
				<hr>
				<table id="mediaList" class="hover" style="width: 100%;">
					<thead>
						<tr>
							<th width="">ID</th>
							<th width="">File Name</th>
							<th width="">Preview</th>
							<th width="">Action</th>
						</tr>
					</thead>
					<tbody>

					<?php foreach ($media_items as $media_item) : ?>
						<tr id="media_<?=$media_item['mf_id'];?>">
							<td><?=$media_item['mf_id'];?></td>
							<td><?=$media_item['mf_file_name'];?></td>
							<td><img class="thumb" src="<?=base_url('uploads/ar_');?><?=$media_item['mf_file_name'];?> "></td>
							<td>
							<button type="button" data-src="<?=base_url().'uploads/ar_'.$media_item["mf_file_name"];?>" id="<?=$media_item['mf_id'];?>" class="button success small btn-insert-to-post">+</button> 
							<button type="button" class="floating-label button small alert removeFeaturedImage" id="<?=$media_item['mf_id'];?>">x</button> </td>
						</tr>

					<?php endforeach; ?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div> 
<!-- //media files modal -->

<script type="text/javascript">
	$(document).ready(function () {
	//add featured image
		 $("#add_featured_image").on("submit", function (ev) {
			ev.preventDefault();  
			$.ajax({
				url: '<?php echo base_url('members/ajax/do/add_featured_image')?>',
				contentType: false,  
				cache: false,  
				processData:false,
				type: "POST",
				dataType: "json",
				fileElementId	:'userfile',
				data: new FormData(this),
				success: function (data) {
					
							
					if (data.status == 'Success!') {
						$('.status-messages-modal').html(
						'<div class="success callout cell large-12" data-closable > <h3>'+data.msg+'</h3><hr><ul class="error-wrapper-modal"></ul></div>'
						);
						if(data.img != ''){ $('.error-wrapper-modal').append('<li>'+ data.img +'</li>'  ); }

					console.log(data);
					}
					
					
					if (data.status == 'Error!') {
						$('.status-messages-modal').html(
						'<div class="alert callout cell large-12" data-closable > <h3>'+data.msg+'</h3><button class="close-button" aria-label="Dismiss alert" type="button" data-close><span aria-hidden="true">&times;</span></button></div>'
						);
					console.log(data);
					}
					if (data.status == 'Invalid!') {
						$('.status-messages-modal').html(
							'<div class="alert callout cell large-12" data-closable > <h3>'+data.msg+'</h3><hr><ul class="error-wrapper-modal"></ul><button class="close-button" aria-label="Dismiss alert" type="button" data-close><span aria-hidden="true">&times;</span></button></div>'
							);
							
							if(data.event_featured_image != ''){ $('.error-wrapper-modal').append('<li>'+ data.event_featured_image +'</li>'  ); }
					}
					
					$('html,body').animate({ scrollTop: 0}, 'slow');

				},
				error: function (data) {
					console.log('Error:'+ data);
					if (data.status == 'Error!'){
					$('.stat-messages-modal').html(
						'<div class="alert callout cell large-12" data-closable >ERROR:<hr> <h3>There\'s an error response from the database.</h3><button class="close-button" aria-label="Dismiss alert" type="button" data-close><span aria-hidden="true">&times;</span></button></div>'
						);
					}
				}	
			});
 		});
    });

</script>

