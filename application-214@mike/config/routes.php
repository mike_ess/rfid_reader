<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
// $route['default_controller'] = 'welcome';
// $route['404_override'] = '';
// $route['translate_uri_dashes'] = FALSE;



/**
 * Login Pages and validations
 */
// $route['login'] = 'login';
// $route['validate/(:any)'] = 'validate/index/$1';



 /**
  * LakwatCharity
  */
  $route['lakwatcharity'] = 'lakwatcharity';
  $route['lakwatcharity/(:any)'] = 'lakwatcharity/view/$1';
  $route['lakwatcharity/add/new'] = 'lakwatcharity/add/new';
  $route['lakwatcharity/edit/(:any)'] = 'lakwatcharity/edit/$1';
  

  
  
/**
 * Tag Archives Pages
 */
$route['tags'] = 'tags';
$route['tags/(:any)'] = 'tags/view/$1';


/**
 * Bib Number Pages
 */
$route['bib_numbers'] = 'bib_numbers';
$route['bib_numbers/(:any)'] = 'bib_numbers/view/$1';

/**
 * Admin Settings
 */
$route['admin_settings'] = 'admin_settings';
// $route['admin_settings/(:any)'] = 'admin_settings/view/$1';
$route['admin_settings/csv_import'] = 'admin_settings/csv_import';


/**
 * Members Pages
 */
$route['members'] = 'members';
$route['members/(:any)'] = 'members/view/$1';

//ADMIN VIEW ONLY
$route['members/edit/(:any)'] = 'members/edit/$1'; 
$route['members/add/new'] = 'members/add'; 
$route['members/ajax/(:any)/(:any)'] = 'members/myformAjax/$1/$2'; //the ajax call. controllers/function/id/do_action

/**
 * Participants (ADMIN VIEW ONLY)
 */
 $route['participants'] = 'participants';
 $route['participants/events/(:any)'] = 'participants/view/$1';//re slug
 $route['participants/events/(:any)/(:any)'] = 'participants/view/$1/$2';//re_slug/rc_id
 $route['participants/deleteParticipant/(:any)'] = 'participants/deleteParticipant/$1'; //the ajax call to delete participant. controllers/function/id
 $route['participants/changePaymentStatus/(:any)/(:any)'] = 'participants/changePaymentStatus/$1/$2'; //the ajax call to change participant payment status. controllers/function/id/action

/**
 * Race Events Pages
 */
$route['events'] = 'events';
$route['events/(:any)'] = 'events/view/$1';
$route['events/register/(:any)'] = 'events/register/$1';
$route['events/add/new'] = 'events/add/new';
$route['events/edit/(:any)'] = 'events/edit/$1';
$route['events/ajax/(:any)'] = 'events/registerFromEventsPage/$1'; //ajax registration. controllers/function/id

/**
 * Race Results Pages
 */
$route['results'] = 'results';
$route['results/events/(:any)'] = 'results/view/$1';//re slug
$route['results/events/(:any)/(:any)'] = 'results/view/$1/$2';//re_slug/rc_id
// $route['results/ajax/(:any)'] = 'results/deleteParticipant/$1'; //the ajax call. controllers/function/id


 /**
 * Who We Ware pages
 */
 $route['whoweare'] = 'whoweare';

 
/**
 * News and Updates pages
 */
 $route['news'] = 'news';
 $route['news/(:any)'] = 'news/view/$1';
 $route['news/create'] = 'news/create';
 

/**
 * Generic and Default Pages
 */
 $route['default_controller'] = 'pages/view';
//  $route['404_override'] = 'pages/view/page_not_found';
 $route['(:any)'] = 'pages/view/$1';
 $route['(:any)/(:any)'] = 'pages/view/$1/$2';
 
 