-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Aug 30, 2018 at 09:08 AM
-- Server version: 10.1.8-MariaDB
-- PHP Version: 5.6.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ci_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `bank_payment_option`
--

CREATE TABLE `bank_payment_option` (
  `bpo_id` int(11) NOT NULL,
  `bpo_name` varchar(200) NOT NULL,
  `bpo_acct_name` varchar(200) NOT NULL,
  `bpo_acct_number` varchar(200) NOT NULL,
  `bpo_type` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bank_payment_option`
--

INSERT INTO `bank_payment_option` (`bpo_id`, `bpo_name`, `bpo_acct_name`, `bpo_acct_number`, `bpo_type`) VALUES
(1, 'Banco De Oro (BDO)', 'Test Acct. Name', '123234', 'Savings (Peso)');

-- --------------------------------------------------------

--
-- Table structure for table `bib_number`
--

CREATE TABLE `bib_number` (
  `bib_id` int(11) NOT NULL,
  `bib_assigned_number` int(11) NOT NULL COMMENT 'add trailing zeros when rendering',
  `m_id` int(11) NOT NULL,
  `re_id` int(11) NOT NULL,
  `rc_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `email_templates`
--

CREATE TABLE `email_templates` (
  `ges_pass_reset` mediumtext NOT NULL,
  `ae_pass_reset` mediumtext NOT NULL,
  `ae_reg_received` mediumtext NOT NULL,
  `ae_payment_pending` mediumtext NOT NULL,
  `ae_payment_complete` mediumtext NOT NULL,
  `ae_payment_refunded` mediumtext NOT NULL,
  `ae_payment_cancelled` mediumtext NOT NULL,
  `ae_delivery_completed` mediumtext NOT NULL,
  `cust_pass_reset` mediumtext NOT NULL,
  `cust_reg_received` mediumtext NOT NULL,
  `cust_payment_pending` mediumtext NOT NULL,
  `cust_payment_complete` mediumtext NOT NULL,
  `cust_payment_refunded` mediumtext NOT NULL,
  `cust_payment_cancelled` mediumtext NOT NULL,
  `cust_delivery_completed` mediumtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `emergency_contact_details`
--

CREATE TABLE `emergency_contact_details` (
  `ecd_id` int(11) NOT NULL,
  `ecd_relationship` varchar(50) NOT NULL,
  `ecd_first_name` varchar(100) NOT NULL,
  `ecd_middle_name` varchar(100) NOT NULL,
  `ecd_last_name` varchar(100) NOT NULL,
  `ecd_address` varchar(200) NOT NULL,
  `ecd_mobile` varchar(100) NOT NULL,
  `ecd_email` varchar(100) NOT NULL,
  `m_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `family_friend_member`
--

CREATE TABLE `family_friend_member` (
  `ffm_id` int(11) NOT NULL,
  `m_id` int(11) NOT NULL,
  `is_main` int(1) NOT NULL,
  `ecd_id` int(11) NOT NULL,
  `bib_id` int(11) NOT NULL,
  `re_id` int(11) NOT NULL,
  `ffm_email` varchar(200) NOT NULL,
  `ffm_first_name` varchar(100) NOT NULL,
  `ffm_middle_name` varchar(100) NOT NULL,
  `ffm_last_name` varchar(100) NOT NULL,
  `ffm_gender` varchar(10) NOT NULL,
  `ffm_address` varchar(200) NOT NULL,
  `ffm_mobile` varchar(100) NOT NULL,
  `ffm_age_group` varchar(20) NOT NULL,
  `ffm_clubs_groups` varchar(200) NOT NULL,
  `ffm_created_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `ffm_updated_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `fees`
--

CREATE TABLE `fees` (
  `f_id` int(11) NOT NULL,
  `f_name` varchar(200) NOT NULL,
  `f_slug` varchar(200) NOT NULL,
  `f_cost` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `fees`
--

INSERT INTO `fees` (`f_id`, `f_name`, `f_slug`, `f_cost`) VALUES
(1, 'Web Fee', 'web_fee', 60),
(2, 'Delivery Fee (Manila)', 'delivery_fee_manila', 250),
(3, 'Delivery Fee (Outside Manila)', 'delivery_fee_outside_manila', 350),
(4, 'Delivery Fee (Fixed)', 'delivery_fee_fixed', 500);

-- --------------------------------------------------------

--
-- Table structure for table `loops_records`
--

CREATE TABLE `loops_records` (
  `lr_id` int(11) NOT NULL,
  `lr_loop_number` varchar(20) NOT NULL,
  `lr_loop_elapsed_time` varchar(20) NOT NULL,
  `bib_assigned_number` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `media_files`
--

CREATE TABLE `media_files` (
  `mf_id` int(11) NOT NULL,
  `mf_file_name` mediumtext NOT NULL,
  `mf_added_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `mf_added_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `media_files`
--

INSERT INTO `media_files` (`mf_id`, `mf_file_name`, `mf_added_date`, `mf_added_by`) VALUES
(13, 'golf4.jpg', '2018-08-13 05:00:48', 1),
(14, 'big-one.jpg', '2018-08-13 04:59:31', 1),
(56, 'pexels-photo-371633.jpeg', '2018-08-13 05:01:27', 1),
(57, 'pexels-photo-3716331.jpeg', '2018-08-16 05:37:53', 31);

-- --------------------------------------------------------

--
-- Table structure for table `members`
--

CREATE TABLE `members` (
  `m_id` int(11) NOT NULL,
  `m_username` varchar(250) NOT NULL,
  `m_password` varchar(250) NOT NULL,
  `m_role` int(2) NOT NULL COMMENT '9=admin, 2=member, 3=author, 4=contributor',
  `m_status` int(1) NOT NULL COMMENT '0=needs activation, 1=activated, 2=deactivated',
  `m_first_name` varchar(100) CHARACTER SET utf8 NOT NULL,
  `m_last_name` varchar(100) CHARACTER SET utf8 NOT NULL,
  `m_middle_name` varchar(100) CHARACTER SET utf8 NOT NULL,
  `m_gender` varchar(50) NOT NULL,
  `m_address` varchar(200) CHARACTER SET utf8 NOT NULL,
  `m_mobile` varchar(25) NOT NULL,
  `m_email` varchar(100) NOT NULL,
  `m_age_group` varchar(10) NOT NULL,
  `m_clubs_groups` varchar(150) CHARACTER SET utf8 NOT NULL,
  `m_photo` varchar(150) NOT NULL,
  `m_created_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `m_updated_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `m_last_login_time` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `members`
--

INSERT INTO `members` (`m_id`, `m_username`, `m_password`, `m_role`, `m_status`, `m_first_name`, `m_last_name`, `m_middle_name`, `m_gender`, `m_address`, `m_mobile`, `m_email`, `m_age_group`, `m_clubs_groups`, `m_photo`, `m_created_time`, `m_updated_time`, `m_last_login_time`) VALUES
(1, 'mike.asuncion.EqW', '1bbd886460827015e5d605ed44252251', 9, 1, 'Mike', 'Asuncion', 'Ferrer', 'Male', 'New 123 asdfsadf ', '123123213', 'admin@email.com', '30-34', 'Eversun 2', '', '2018-07-25 07:41:57', '2018-08-17 00:47:56', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `money_transfer_option`
--

CREATE TABLE `money_transfer_option` (
  `mto_id` int(11) NOT NULL,
  `mto_est_name` varchar(200) NOT NULL,
  `mto_receivers_name` varchar(200) NOT NULL,
  `mto_mobile` varchar(200) NOT NULL,
  `mto_address` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `participants`
--

CREATE TABLE `participants` (
  `p_id` int(11) NOT NULL,
  `m_id` int(11) NOT NULL,
  `ffm_id` int(11) NOT NULL,
  `bib_id` int(11) NOT NULL,
  `rc_id` int(11) NOT NULL,
  `re_id` int(11) NOT NULL,
  `ss_id` int(11) NOT NULL,
  `p_payment_status` int(11) NOT NULL COMMENT '0=Pending, 1=Paid, 2=Refunded',
  `p_payment_method` varchar(20) NOT NULL,
  `p_payment_amount` int(11) NOT NULL,
  `p_payment_total` int(11) NOT NULL,
  `p_delivery_address` varchar(200) NOT NULL,
  `p_added_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `p_updated_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pickup_location`
--

CREATE TABLE `pickup_location` (
  `pl_id` int(11) NOT NULL,
  `pl_est_name` varchar(200) NOT NULL,
  `pl_est_address` varchar(200) NOT NULL,
  `pl_phone` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `race_categories`
--

CREATE TABLE `race_categories` (
  `rc_id` int(11) NOT NULL,
  `rc_name` varchar(200) CHARACTER SET utf8 NOT NULL,
  `re_id` int(11) NOT NULL,
  `rc_fee` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `race_events`
--

CREATE TABLE `race_events` (
  `re_id` int(11) NOT NULL,
  `re_status` varchar(10) NOT NULL COMMENT '"done" or "not_done"',
  `re_name` varchar(200) CHARACTER SET utf8 NOT NULL,
  `re_slug` varchar(200) NOT NULL,
  `re_description` longtext NOT NULL,
  `re_start_date` varchar(100) NOT NULL,
  `re_end_date` varchar(100) NOT NULL,
  `re_created_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `re_updated_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `re_added_by` int(11) NOT NULL,
  `re_update_by` int(11) NOT NULL,
  `re_tags` varchar(250) NOT NULL,
  `re_publish_status` int(2) NOT NULL COMMENT '1=published, 0=draft',
  `mf_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `settings_general`
--

CREATE TABLE `settings_general` (
  `sg_paypal_email` varchar(100) NOT NULL,
  `sg_admin_email` varchar(100) NOT NULL,
  `sg_upload_path` varchar(250) NOT NULL,
  `sg_thumb_max_height` int(3) NOT NULL,
  `sg_thumb_max_width` int(3) NOT NULL,
  `sg_ari_max_height` int(3) NOT NULL,
  `sg_ari_max_width` int(3) NOT NULL,
  `sg_ari_prefix` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `settings_general`
--

INSERT INTO `settings_general` (`sg_paypal_email`, `sg_admin_email`, `sg_upload_path`, `sg_thumb_max_height`, `sg_thumb_max_width`, `sg_ari_max_height`, `sg_ari_max_width`, `sg_ari_prefix`) VALUES
('m.asuncion@eversun.ph', 'm.asuncion@eversun.ph', 'uploads', 300, 300, 7, 7, 'ar_');

-- --------------------------------------------------------

--
-- Table structure for table `singlet_sizes`
--

CREATE TABLE `singlet_sizes` (
  `ss_id` int(11) NOT NULL,
  `ss_name` varchar(20) NOT NULL,
  `re_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `time_records`
--

CREATE TABLE `time_records` (
  `tr_id` int(11) NOT NULL,
  `tr_gun_start` varchar(20) NOT NULL,
  `bib_assigned_number` int(11) NOT NULL,
  `tr_start_time` varchar(20) NOT NULL,
  `tr_finish_time` varchar(20) NOT NULL,
  `tr_loops_laps` varchar(20) NOT NULL,
  `lr_id` int(11) NOT NULL,
  `re_id` int(11) NOT NULL,
  `rc_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `validation_link`
--

CREATE TABLE `validation_link` (
  `v_id` int(11) NOT NULL,
  `m_id` int(11) NOT NULL,
  `v_link` tinytext NOT NULL,
  `v_created_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `bank_payment_option`
--
ALTER TABLE `bank_payment_option`
  ADD PRIMARY KEY (`bpo_id`);

--
-- Indexes for table `emergency_contact_details`
--
ALTER TABLE `emergency_contact_details`
  ADD PRIMARY KEY (`ecd_id`);

--
-- Indexes for table `family_friend_member`
--
ALTER TABLE `family_friend_member`
  ADD PRIMARY KEY (`ffm_id`);

--
-- Indexes for table `fees`
--
ALTER TABLE `fees`
  ADD PRIMARY KEY (`f_id`);

--
-- Indexes for table `loops_records`
--
ALTER TABLE `loops_records`
  ADD PRIMARY KEY (`lr_id`);

--
-- Indexes for table `media_files`
--
ALTER TABLE `media_files`
  ADD PRIMARY KEY (`mf_id`);

--
-- Indexes for table `members`
--
ALTER TABLE `members`
  ADD PRIMARY KEY (`m_id`);

--
-- Indexes for table `money_transfer_option`
--
ALTER TABLE `money_transfer_option`
  ADD PRIMARY KEY (`mto_id`);

--
-- Indexes for table `participants`
--
ALTER TABLE `participants`
  ADD PRIMARY KEY (`p_id`);

--
-- Indexes for table `pickup_location`
--
ALTER TABLE `pickup_location`
  ADD PRIMARY KEY (`pl_id`);

--
-- Indexes for table `race_categories`
--
ALTER TABLE `race_categories`
  ADD PRIMARY KEY (`rc_id`);

--
-- Indexes for table `race_events`
--
ALTER TABLE `race_events`
  ADD PRIMARY KEY (`re_id`);

--
-- Indexes for table `singlet_sizes`
--
ALTER TABLE `singlet_sizes`
  ADD PRIMARY KEY (`ss_id`);

--
-- Indexes for table `time_records`
--
ALTER TABLE `time_records`
  ADD PRIMARY KEY (`tr_id`);

--
-- Indexes for table `validation_link`
--
ALTER TABLE `validation_link`
  ADD PRIMARY KEY (`v_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `bank_payment_option`
--
ALTER TABLE `bank_payment_option`
  MODIFY `bpo_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `emergency_contact_details`
--
ALTER TABLE `emergency_contact_details`
  MODIFY `ecd_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `family_friend_member`
--
ALTER TABLE `family_friend_member`
  MODIFY `ffm_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `fees`
--
ALTER TABLE `fees`
  MODIFY `f_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `loops_records`
--
ALTER TABLE `loops_records`
  MODIFY `lr_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `media_files`
--
ALTER TABLE `media_files`
  MODIFY `mf_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=58;
--
-- AUTO_INCREMENT for table `members`
--
ALTER TABLE `members`
  MODIFY `m_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=46;
--
-- AUTO_INCREMENT for table `money_transfer_option`
--
ALTER TABLE `money_transfer_option`
  MODIFY `mto_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `participants`
--
ALTER TABLE `participants`
  MODIFY `p_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `pickup_location`
--
ALTER TABLE `pickup_location`
  MODIFY `pl_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `race_categories`
--
ALTER TABLE `race_categories`
  MODIFY `rc_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `race_events`
--
ALTER TABLE `race_events`
  MODIFY `re_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `singlet_sizes`
--
ALTER TABLE `singlet_sizes`
  MODIFY `ss_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `time_records`
--
ALTER TABLE `time_records`
  MODIFY `tr_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `validation_link`
--
ALTER TABLE `validation_link`
  MODIFY `v_id` int(11) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
